var allvalid=0;
$(document).ready(function() {
	var cartItemsTot = parseInt(readCookie("cartItemsTot"));
	if(!cartItemsTot) cartItemsTot = 0;
	if(cartItemsTot!=0){

		var totPrice = 0;
		for(var i=1, j=0; j<cartItemsTot; i++) {
			if(readCookie("cartItem"+i))
			{
				cartItem = readCookie("cartItem"+i);
				cartFormat = readCookie("cartFormat"+i);
				cartPrice = readCookie("cartPrice"+i);
				cartItemAmount = readCookie("cartItemAmount"+i);
				$(".film").after(
					"<tr>"+
							"<td class='filmtitle'>"+getTitle(cartItem)+" &#45; "+cartFormat+"</td>"+
							"<td>&euro;"+cartPrice+"</td>"+
							"<td>"+cartItemAmount+"</td>"+
					"</tr>"
				);
				totPrice += parseInt(cartPrice)*parseInt(cartItemAmount);
				
		/*		//debug: erase cookies
				deleteCookie("cartItem"+i);
				deleteCookie("cartFormat"+i);
				deleteCookie("cartPrice"+i);
				deleteCookie("cartItemAmount"+i);
				deleteCookie("cartItemsTot");*/
				
				j++;
			}
		}
		$("#totprice").html(totPrice);
	} else emptyPage();	
});

function emptyPage() {
	$("#checkout").remove();
	$("#btnbar").html("<td id='buttons' colspan='3'>Your cart is empty.</td>");
	$("#cart img").attr("src", "img/cart.png");
}