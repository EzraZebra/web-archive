var allvalid=0;
$(document).ready(function() {
	var cartItemsTot = parseInt(readCookie("cartItemsTot"));
	if(!cartItemsTot) cartItemsTot = 0;
	if(cartItemsTot!=0){

		var totPrice = 0;
		for(var i=1, j=0; j<cartItemsTot; i++) {
			if(readCookie("cartItem"+i))
			{
				cartItem = readCookie("cartItem"+i);
				cartFormat = readCookie("cartFormat"+i);
				cartPrice = readCookie("cartPrice"+i);
				cartItemAmount = readCookie("cartItemAmount"+i);
				$(".film").after(
					"<tr>"+
							"<td class='filmtitle'>"+getTitle(cartItem)+" &#45; "+cartFormat+"</td>"+
							"<td>&euro;"+cartPrice+"</td>"+
							"<td>"+cartItemAmount+"</td>"+
					"</tr>"
				);
				totPrice += parseInt(cartPrice)*parseInt(cartItemAmount);
				
		/*		//debug: erase cookies
				deleteCookie("cartItem"+i);
				deleteCookie("cartFormat"+i);
				deleteCookie("cartPrice"+i);
				deleteCookie("cartItemAmount"+i);
				deleteCookie("cartItemsTot");*/
				
				j++;
			}
		}
		$("#totprice").html(totPrice);
	} else emptyPage();	
	
	$("input").change(function() {
		if($(this).attr("type") == "text") {
			validate($(this));
			if(allvalid==9) {
				$("#submit").attr("src", "img/checkout/complete.png");
				$("#submit").removeAttr("disabled");
			} else {
				$("#submit").attr("src", "img/checkout/completedisabled.png");
				$("#submit").attr("disabled", "disabled");
			}	
		}
	});
	
	setDays();
	$("#month").change(setDays);
	$("#year").change(setDays);
	for(var i=1901; i<1995; i++) $("#year").append("<option>"+i+"</option>");
	for(var i=2012; i<2031; i++) $("#cardyear").append("<option>"+i+"</option>");
	for(var i=2; i<13; i++) {
		if(i<10) var month = "0"+i;
			else var month = i;
		$("#cardmonth").append("<option>"+month+"</option>");
	}	
});

function validate(element) {
	var elementId = element.attr("id");
	var input = element.val().trim();
	var val = $("#"+elementId+"val");
	if(elementId == "name" || elementId == "cardname") var regex = /[A-Za-z\s]{5,}/;
		else if(elementId == "email") var regex = /\b[A-Za-z0-9._]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}\b/; //http://www.regular-expressions.info/email.html
			else if(elementId == "address") var regex = /[A-Za-z0-9\s]+/;
				else if(elementId == "zipcode") var regex = /[A-Za-z]{0,2}[0-9]{4,}/;
					else if(elementId == "city" || elementId == "stateprovince" ||elementId == "country") var regex = /[A-Za-z\s]+/;
						else if(elementId == "cardnumber"){ //http://www.regular-expressions.info/creditcard.html
							var cardtype = $("#cardtype").val();
							if(cardtype == "Visa/Delta/Electron") var regex = /^4[0-9]{12}(?:[0-9]{3})?$/;
								else if(cardtype == "MasterCard/EuroCard") var regex = /^5[1-5][0-9]{14}$/;
									else if(cardtype == "American Express") var regex = /^3[47][0-9]{13}$/;
										else if(cardtype == "Solo/Maestro") var regex = /^(5[06-8]|6\d)\d{14}(\d{2,3})?$/; //http://groups.google.com/group/activemerchant/browse_thread/thread/01469e5a1620a06f
						}
	if(regex.test(input) && input.search(regex) == 0 && input.match(regex)[0] == input && ((elementId!="name" && elementId!="cardname") || input.indexOf(" ")>1)) {
		if($(val).attr("class") != "val") allvalid++;
		$(val).html("<img src='img/checkout/valid.png' alt='valid'/> Valid!");
		$(val).attr('class', 'val');
	}	else {
			if($(val).attr('class') == 'val') allvalid--;
			$(val).html("<img src='img/checkout/invalid.png' alt='invalid'/> Invalid.");
			$(val).attr('class', 'inval');
		}
}

function findDays() {
	var month = $("#month").val();
	if(month == "February") {
		var year = parseInt($("#year").val());
		if(year%4 == 0 && (year%100 != 0 || year%400 == 0)) return 29;
			else return 28;
	} else if(month == "April" || month == "June" || month == "September" || month == "November") return 30;
		else return 31;
}

function setDays() {
	noDays = findDays();
	$("#day").html("");
	for(var i=1; i<=noDays; i++) {
		if(i<10) var day = "0"+i;
			else var day = i;
		$("#day").append("<option>"+day+"</option>");
	}
}

function emptyPage() {
	$("#checkout").remove();
	$("#btnbar").html("<td id='buttons' colspan='3'>Your cart is empty.</td>");
	$("#cart img").attr("src", "img/cart.png");
}