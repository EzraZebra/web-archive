var orimgw, orimgh;
$(document).ready(function() {
	pageid = $(document.body).attr("id");
	if(pageid == "azpage" || pageid == "genrepage" || pageid == "datepage" || pageid == "directorpage") {
		cover = ".cover";
		var maximgw = 100;
	} else {
		cover = "#cover";
		var maximgw = 150;
		orimgw = $(cover).width();
		orimgh = $(cover).height();
	}
	$(cover).each(function() {
		imgw = $(this).attr("width"), imgh = $(this).attr("height");
	
		if(imgw > maximgw){
			imgh = maximgw/imgw*imgh;
			imgw = maximgw;
			$(this).attr("width", imgw).attr("height", imgh);
		}
		
		$(this).parent().parent().height(imgh);
	});
	
	if(parseInt(readCookie("cartItemsTot")) > 0) $("#cart img").attr("src", "img/cartfull.png");
});

$("#cover").click(function () {
	var locx = (screen.width - orimgw)/2,
		locy = (screen.height - orimgh)/2;
		
	var src = $(this).attr("src");
	var popup = window.open("cover.html","cartdialog","width="+orimgw+",height="+orimgh+",scrollbars=0");
	if($.browser.msie)
		$(popup.document).ready(function () {
			$(popup.document).contents().find('img').attr("src", src);
		});
	else	
		popup.onload = function () {
			$(popup.document).contents().find('img').attr("src", src);
		};

	popup.moveTo(locx, locy);
	popup.focus();
});

$(".price").click(function () {		
	var id = $(this).parent().parent().parent().attr("id");
	
	var title = getTitle(id);
					
	if($(this).parent().index() == 2) var format = "DVD";
		else var format = "Blu-ray";
		
	var price = $(this).text();
	price = parseInt(price.substring(1,price.length));
	
	text = title+" ("+format+") has been added to your cart!";
	
	var cartItemsTot = parseInt(readCookie("cartItemsTot"));
	if(!cartItemsTot) cartItemsTot = 0;
	
	var exists = false, i=1;
	for(j=0; j<cartItemsTot; i++) {
		if(readCookie("cartItem"+i))
		{
			if(readCookie("cartItem"+i) == id && readCookie("cartFormat"+i) == format) {
				exists = true;
				var cartItemAmount = parseInt(readCookie("cartItemAmount"+i))+1;
				addCookie("cartItemAmount"+i, cartItemAmount.toString(), false);
				break;
			}
			j++;
		} 
	}
	
	if(!exists){
		addCookie("cartItem"+i, id, false);
		addCookie("cartFormat"+i, format, false);
		addCookie("cartPrice"+i, price, false);
		addCookie("cartItemAmount"+i, "1", false);
		addCookie("cartItemsTot", (++cartItemsTot).toString(), false);
	}
	
	if($("#cart img").attr("src") == "img/cart.png") $("#cart img").attr("src", "img/cartfull.png");
	
	var locx = (screen.width - 350)/2,
		locy = (screen.height - 100)/2;
		
	var popup = window.open("cartdialog.html","cartdialog","width=350,height=100,scrollbars=0");
	if($.browser.msie)
		$(popup.document).ready(function () {
			$(popup.document).contents().find('p').html(text);
		});
	else	
		popup.onload = function () {
			$(popup.document).contents().find('p').html(text);
		};

	popup.moveTo(locx, locy);
	popup.focus();
});

function getTitle(id) {
	if(id == "_2001ASpaceOdyssey") return "2001: A Space Odyssey";
		else if(id == "DonnieDarko") return "Donnie Darko";
			else if(id == "FearAndLoathingInLasVegas") return "Fear and Loathing in Las Vegas";
				else if(id == "SevenSamurai") return "Seven Samurai";
					else if(id == "WildAtHeart") return "Wild at Heart";
						else return "Not Found";
}

function addCookie(name, value, deleteCookie) {
	var date = new Date();
	if(deleteCookie) var time = -1;
		else var time = 250000000000;
	date.setTime(date.getTime()+time);
	date = date.toGMTString();
	document.cookie = name+"="+value+"; expires="+date+"; path=/";
}
	
function readCookie(name) {
	var name = name+"=";
	var cookieArray = document.cookie.split(';');
	for(var i=0; i<cookieArray.length; i++) {
		var cookie = cookieArray[i];
		while (cookie.charAt(0)==' ') cookie = cookie.substring(1, cookie.length);
		if(cookie.indexOf(name)==0) return cookie.substring(name.length, cookie.length);
	}
	return null;
}

function deleteCookie(name) {
	addCookie(name, "", true);
}