$(document).ready(function() {
	var cartItemsTot = parseInt(readCookie("cartItemsTot"));
	if(!cartItemsTot) cartItemsTot = 0;
	if(cartItemsTot!=0){

		var totPrice = 0;
		for(var i=1, j=0; j<cartItemsTot; i++) {
			if(readCookie("cartItem"+i))
			{
				cartItem = readCookie("cartItem"+i);
				cartFormat = readCookie("cartFormat"+i);
				cartPrice = readCookie("cartPrice"+i);
				cartItemAmount = readCookie("cartItemAmount"+i);
				$("#btnbar").before(
					"<tr>"+
							"<td class='filmtitle'><a href='"+cartItem+".html'>"+getTitle(cartItem)+" &#45; "+cartFormat+"</a></td>"+
							"<td>&euro;"+cartPrice+"</td>"+
							"<td><input type='text' value='"+cartItemAmount+"' size='3' maxlength='3' class='amount' id='_"+i+"'/></td>"+
					"</tr>"
				);
				totPrice += parseInt(cartPrice)*parseInt(cartItemAmount);
				
		/*		//debug: erase cookies
				deleteCookie("cartItem"+i);
				deleteCookie("cartFormat"+i);
				deleteCookie("cartPrice"+i);
				deleteCookie("cartItemAmount"+i);
				deleteCookie("cartItemsTot");*/
				
				j++;
			}
		}
		$("#totprice").html(totPrice);

		$("#update").click(updateCart);
		//$(".amount").change(updateCart);
		$(".amount").keyup(function(e){ if(e.keyCode == 13) updateCart(); });
		$("#confirm").click(emptyCart);
		$("#cancel").click(function() { $("#confirmation").css("visibility", "hidden"); $("#empty").show(); })
		$("#empty").click(function(){			
			$("#empty").hide();
			$("#confirmation").css("visibility", "visible");
		});
	} else emptyPage();
});

function updateCart() {
	var cartItemsTot = parseInt(readCookie("cartItemsTot"));
	if(!cartItemsTot) cartItemsTot = 0;
	var newCartItemsTot = cartItemsTot;
	
	var totPrice = 0;
	for(var i=1, j=0; j<cartItemsTot; i++) {
		if(readCookie("cartItem"+i))
		{
			cartItemAmount = $("#_"+i).attr("value");
			if(cartItemAmount > 0) {
				addCookie("cartItemAmount"+i, cartItemAmount, false);
				cartPrice = readCookie("cartPrice"+i);
				totPrice += parseInt(cartPrice)*parseInt(cartItemAmount);
			}	else {
					deleteCookie("cartItem"+i);
					deleteCookie("cartFormat"+i);
					deleteCookie("cartPrice"+i);
					deleteCookie("cartItemAmount"+i);
					addCookie("cartItemsTot", (--newCartItemsTot).toString(), false);
					$("#_"+i).parent().parent().remove();
					if(newCartItemsTot==0) emptyPage();
				}
			//alert("i: "+i+"; j: "+j+"; totPrice: "+totPrice); //debug: check indexes/price count
			j++;
		}
	}
	
	$("#totprice").html(totPrice);
}

function emptyCart() {
	var cartItemsTot = parseInt(readCookie("cartItemsTot"));
	if(!cartItemsTot) cartItemsTot = 0;
	var newCartItemsTot = cartItemsTot;
	
	for(var i=1, j=0; j<cartItemsTot; i++) {
		if(readCookie("cartItem"+i))
		{
			deleteCookie("cartItem"+i);
			deleteCookie("cartFormat"+i);
			deleteCookie("cartPrice"+i);
			deleteCookie("cartItemAmount"+i);
			addCookie("cartItemsTot", (--newCartItemsTot).toString(), false);
			$("#_"+i).parent().parent().remove();
				
			//alert("i: "+i+"; j: "+j+"; newCartItemsTot: "+newCartItemsTot); //debug: check indexes/item count
			j++;
		}
	}
	emptyPage();
}

function emptyPage() {
	$("#checkout").remove();
	$("#btnbar").html("<td id='buttons' colspan='3'>Your cart is empty.</td>");
	$("#cart img").attr("src", "img/cart.png");
}