-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 19, 2019 at 12:02 AM
-- Server version: 10.0.38-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `a2e_online`
-- Required privileges: SELECT
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cid` int(11) NOT NULL,
  `en` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nl` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cid`, `en`, `nl`) VALUES
(1, 'JEWELRY', 'JUWELEN'),
(2, 'ACCESSORIES', 'ACCESSOIRES'),
(3, 'DECORATIONS', 'DECORATIE'),
(4, 'OUR BRAND', 'EIGEN MERKEN');

-- --------------------------------------------------------

--
-- Table structure for table `main`
--

CREATE TABLE `main` (
  `en` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nl` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `mid` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `main`
--

INSERT INTO `main` (`en`, `nl`, `mid`) VALUES
('<span class=\"title\">WELCOME TO THE ONLINE CATALOGUE OF A2E EXPORT TRADING</span><br />\r\n<span class=\"title2\">OUR PRODUCTS</span><br />\r\nA2E export trading specializes in handmade fashion accessories and decorations, most from the Philippines, but we also sell some textiles and other factory made products. In this online catalogue you will find a range of accessories and we aim to also present a range of decorations within the next months. Our catalogue is updated about weekly; for reference check the note top left on this page, right under the logo.<br />\r\nIf you can\'t find what you are looking for or if you have special requests don\'t hesitate to contact us. We can supply custome made accessories and decorations from our own workshop.<br />\r\nDon\'t forget to check out our own productions, hand made in our workshop in Cebu, Philippines. You can recognize them by the \'A2E\' mark on the photo. Our exclusive designs are sold under brand name \'Sampagita\', which you\'ll find at the bottom of the menu. We aim to launch this brand early May, so please visit us again soon.<br />\r\n<span class=\"title2\">PRICES</span><br />\r\nPRICES ARE SHIPPING INCLUDED! (airfreight; airport to airport))<br />\r\nALL PRICES mentioned in this catalogue ARE FOR REFERENCE ONLY. Prices can vary with currency rates, season\'s supply of components and of course the quantity ordered and the total size of the order. <br />\r\nWe calculate the best price for each order seperately, also taking into account the destination of shipping (airfreight). For bulk orders by seafreight prices will be considerately lower than reference prices. <br />\r\nQuantities mentioned (example: $29 per 60pcs) are minimum quantities. Minimum amount for the total order is US$550 or &euro;400.<br />\r\nPayements are accepted in US dollars or Euros only.<br />\r\n<span class=\"title2\">ORDERING</span><br />\r\nFor your convenience we have prepared an order form you can download by clicking the link in the top menu. To order simply fill out the form and send it to us by email. Please also mention country of destination. We will reply to your inquiry with the same excel file in which the order is calculated automatically with up-to-date prices and including discounts and such. You can freely adjust this file if you want to change the order or make a new inquiry. When your order is final send back the excel file and we will then confirm your order.<br />\r\nProcessing of orders takes 5 to 12 working days, depending on quantities and products. Shipping takes 2 to 5 days, airplanes leaving 2 to 3 times per week, both depending on destination. Detailed information will be included with the reply to your inquiries. For large bulk orders through sea freight special arrangements will be made.<br />\r\n<span class=\"title2\">SAMPLES</span><br />\r\nSamples can be requested to be added for free to a regular shipping. For samples-only a fixed fee of $7 or &euro;5 will be charged and shipping will be made on customer\'s expense. Shipping of samples-only will be made through airmail or courier express.<br />\r\n<span class=\"title2\">GENERAL CONDITIONS</span><br />\r\nOnly orders of $550+ or &euro;400+ will be accepted for regular shipping. Smaller orders can only be shipped by airmail which may take up to 5weeks delivery time.<br />\r\nAn advance payement of 30% on the total amount of order is requested. For orders over $2,500 or &euro;2,000 advance payements can be agreed upon. Only when the advance payement is confirmed the order will be processed.<br />\r\nBalance payement will be made within 10 calender days of delivery.<br />\r\nPayements can be made through bank-to-bank transfer on the Belgian FortisBank account as mentioned on the business card. For payements made directly to our Philippine bank account a discount of 0.80% will be granted.<br />\r\nFor large bulk orders other payement procedings can be agreed upon.', '<span class=\"title\">WELKOM OP DE ONLINE CATALOGUS VAN A2E EXPORT TRADING</span><br />\r\n<span class=\"title2\">ONZE PRODUCTEN</span><br />\r\nA2E export trading is gespecialiseerd in handgemaakte modeaccessoires voornamelijk uit de Filipijnen maar wij leveren ook industrieel gemaakte producten. In deze online catalogus vindt u een groot gamma aan accessoires en wij beogen binnen dit en enkele maanden ook een gamma decoraties te kunnen presenteren. Onze catalogus wordt zowat wekelijks geupdate, als referentie kan u het vakje bovenaan links op deze pagina checken.<br />\r\nAls u niet zou vinden waar u naar op zoek bent of u heeft een ander speciaal verzoek aarzel dan niet ons te contacteren. We kunnen ook naar uw ontwerp gemaakte accessoires en decoraties leveren, geproduceerd in ons eigen atelier.<br />\r\nVergeet dus niet onze eigen producten, handgemaakt in ons atelier in Cebu, Filipijnen, te bekijken. U kan ze herkennen door de \'A2E\' op de foto. Eigen ontwerpen worden verkocht onder de merknaam \'Sampagita\', u vindt ze onderaan in het menu. Dit merk zal begin mei gelanceerd worden.<br />\r\n<span class=\"title2\">PRIJZEN</span><br />\r\nALLE PRIJZEN ZIJN INCLUSIEF TRANSPORTKOSTEN! (airfreight/airport to airport)<br />\r\nALLE PRIJZEN in deze catalogus zijn UITSLUITEND VOOR REFERENTIE! Zij kunnen variëren naargelang wisselkoersen, seizoensgebonden beschikbaarheid van componenten en uiteraard naargelang de bestelde hoeveelheid en de grootte van de totale bestelling.<br />\r\nWij berekenen de beste prijs voor elke bestelling apart en houden hierbij rekening met het land van bestemming (airfreight). Voor grote bulkbestellingen per zeetransport zullen de prijzen aanzienlijk lager uitvallen dan de referentieprijzen hier vermeld.<br />\r\nAangegeven hoeveelheden (bijv. &euro;29 per 60pcs) zijn minimum hoeveelheden voor elk product. Minimum totaalbestelling bedraagt &euro;400.<br />\r\nBetalingen worden uitsluitend in USdollars of Euros geaccepteerd.<br />\r\n<span class=\"title2\">BESTELLEN</span><br />\r\nOm te bestellen kan u het bestelformulier downloaden (link in menu bovenaan), vervolgens emailt u dit formulier naar ons door. Gelieve ook het land van bestemming expliciet te vermelden. In ons antwoord zullen we dit excel bestand, waarin de bestelling automatisch wordt berekend, aanpassen naar up-to-date-prijzen, inclusief kortingen en dergelijke. Indien u uw bestelling wenst te wijzigen kan u vrijblijvend wijzigingen aanbrengen en wanneer uw bestelling definief is kan u het aangepaste bestand naar ons terugzenden, waarna we uw bestelling bevestigen.<br />\r\nDe verwerking van een bestellilng duurt ongeveer 5 à 12 werkdagen, naargelang hoeveelheden en grootte van de bestelling. Transport duurt 2 à 5 kalenderdagen met vliegtuigen die vertrekken 2 of 3 keer per week; beide afhankelijk van het land van bestemming. Meer gedetailleerde informatie wordt steeds bijgevoegd in ons antwoord op het oorspronkelijke bestellingsverzoek.<br />\r\n<span class=\"title2\">STALEN</span><br />\r\nStalen kunnen gratis worden bekomen als toevoeging bij een levering. Wenst u uitsluitend stalen dan wordt er een vaste vergoeding van &euro;5 aangerekend en de verzendkosten zijn ten laste van de klant. Verzending van aparte stalen wordt uitsluitend per luchtpost of per express koerier gedaan.<br />\r\n<span class=\"title2\">ALGEMENE VOORWAARDEN</span><br />\r\nAlleen bestelling vanaf &euro;400 worden geaccepteerd in reguliere verscheping. Kleinere bestellingen worden uitsluitend per luchtpost verstuurd, wat tot 5 weken leveringstermijn kan betekenen.<br />\r\nEr wordt een voorschot van 30% op het totaalbedrag van bestelling gevraagd. Voor bestellingen boven de &euro;2000 kan een andere afspraak worden gemaakt. Pas na bevestiging van betaling van voorschot wordt de bestelling effectief verwerkt.<br />\r\nVereffening van de betaling moet ten laatste 10 kalenderdagen na ontvangst van goederen worden gedaan.<br />\r\nBetalingen gebeuren met gewone bankoverschrijving op onze rekening bij FortisBank België (zie business card). Overschrijvingen rechtstreeks op onze rekening in de Filipijnen genieten een korting van 0.80%.<br />\r\nVoor grote bulkbestellingen kunnen andere regelingen worden getroffen.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `pid` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  `img` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `code` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `desc_en` text COLLATE utf8mb4_unicode_520_ci,
  `desc_nl` text COLLATE utf8mb4_unicode_520_ci,
  `dollar` int(11) NOT NULL,
  `euro` int(11) NOT NULL,
  `unit_en` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `unit_nl` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`pid`, `tid`, `img`, `code`, `desc_en`, `desc_nl`, `dollar`, `euro`, `unit_en`, `unit_nl`) VALUES
(1, 1, 'NK002close.jpg', 'WH-NK002', '', '', 30, 27, 'dozen', 'dozijn'),
(2, 1, 'NK003.jpg', 'WH-NK003', '', '', 25, 20, '20pcs', '20stuks'),
(23, 1, 'NK019.jpg', 'WH-NK019', NULL, NULL, 20, 17, '5pcs', '5stuks'),
(3, 1, 'NK003br.jpg', 'WH-NK003/br', '', '', 15, 12, '30pcs', '30stuks'),
(4, 1, 'NK003wbk.jpg', 'WH-NK003/wbk', '', '', 23, 20, 'dozen', 'dozijn'),
(22, 1, 'NK018.jpg', 'WH-NK018', NULL, NULL, 53, 49, '35pcs', '35stuks'),
(5, 1, 'NK003wbl.jpg', 'WH-NK003/wbl', '', '', 30, 27, 'dozen', 'dozijn'),
(6, 1, 'NK003wh.jpg', 'WH-NK003/wh', '', '', 22, 18, '13pcs', '13stuks'),
(21, 1, 'NK017.jpg', 'WH-NK017', NULL, NULL, 25, 21, '11pcs', '11stuks'),
(7, 1, 'NK034d.jpg', 'WH-NK034/d', 'Cleopatra with pendants in blackpen on raisin, 30-45x14mm, 40cm long', '', 35, 27, 'dozen', 'dozijn'),
(20, 1, 'NK016.jpg', 'WH-NK016', NULL, NULL, 25, 21, 'dozen', 'dozijn'),
(8, 1, 'NK035.jpg', 'WH-NK035', '', '', 12, 8, '30pcs', '30stuks'),
(9, 1, 'NK034c.jpg', 'WH-NK034/c', '', '', 20, 17, '5pcs', '5stuks'),
(19, 1, 'NK014.jpg', 'WH-NK014', NULL, NULL, 60, 56, '30pcs', '30stuks'),
(10, 1, 'NK034b.jpg', 'WH-NK034/b', '', '', 30, 25, '20pcs', '20stuks'),
(11, 1, 'NK034a.jpg', 'WH-NK034/a', '', '', 35, 27, '10pcs', '10stuks'),
(18, 1, 'NK010.jpg', 'WH-NK010', NULL, NULL, 20, 16, '5pcs', '5stuks'),
(12, 1, 'NK029.jpg', 'WH-NK029', '', '', 35, 27, 'dozen', 'dozijn'),
(13, 1, 'NK028.jpg', 'WH-NK028', '', '', 10, 7, '3pcs', '3stuks'),
(17, 1, 'NK006.jpg', 'WH-NK006', NULL, NULL, 30, 25, 'dozen', 'dozijn'),
(14, 1, 'NK027.jpg', 'WH-NK027', '', '', 35, 27, 'dozen', 'dozijn'),
(15, 1, 'NK026.jpg', 'WH-NK026', '', '', 15, 10, '10pcs', '10stuks'),
(16, 1, 'NK005.jpg', 'WH-NK005', NULL, NULL, 50, 45, '10pcs', '10stuks'),
(24, 1, 'NK022.jpg', 'WH-NK022', NULL, NULL, 35, 31, '15pcs', '15stuks'),
(25, 1, 'NK024.jpg', 'WH-NK024', NULL, NULL, 60, 56, '25pcs', '25stuks'),
(26, 1, 'NK025.jpg', 'WH-NK025', NULL, NULL, 25, 20, '5pcs', '5stuks'),
(29, 2, 'BC001.jpg', 'WH-BC001', 'Bracelet in white rose shell chips, 20cm, assorted colors.', 'Armbanden in white rose sschelp chips, 20cm, geassorteerde kleuren', 50, 38, '100pcs', '100stuks'),
(30, 2, 'BC002.jpg', 'WH-BC002', 'Elastic bangle in hardwood, 20 elements, 60mm; assorted black and natural. ', 'Elastische armband in hardhout, 20 elementen, 60mm; geassorteerd zwart en naturel. ', 110, 85, '50pcs', '50stuks'),
(31, 2, 'BC004.jpg', 'WH-BC004', 'Bracelets in glass beads with 1 hammershell cracking, 20cm, assorted colors.', 'Armbanden in glaskraal met 1 hammershell cracking, 20cm, geassorteerde kleuren.', 52, 40, '100pcs', '100stuks'),
(32, 2, 'BC005.jpg', 'WH-BC005', 'Bracelets in glass beads with 2 hammershell crackings, 20cm, assorted colors.', 'Armbanden in glaskraal met 2 hammershell crackings, 20cm, geassorteerde kleuren.', 60, 46, '100pcs', '100stuks'),
(33, 2, 'BC009.jpg', 'WH-BC009', 'Bangle with shell cracking/blocking, 20mm,ass. colors.', ' Armband met schelp \'cracking/blocking\', 20mm,geass. kl.', 157, 120, '50pcs', '50stuks'),
(34, 2, 'BC015.jpg', 'WH-BC015', 'Elastic bangle with laminated hammershell on raisin, 16 elements, 40x13mm. ', 'Elastische armband met gelamineerde hammershell op kunsthars, 16 elementen, 40x13mm.', 97, 74, '50pcs', '50stuks'),
(35, 2, 'BC017.jpg', 'WH-BC017', 'Elastic bangle in bayong wood, 20 elements, 46mm.', 'Elastische armband in bayonghout, 20 elementen, 46mm.', 123, 94, '50pcs', '50stuks'),
(36, 2, 'BC021.jpg', 'WH-BC021', 'Open sculpted bangle in hardwood, 58mm.', 'Open gesculpteerde armband in hardhout, 58mm.', 94, 72, '50pcs', '50stuks'),
(37, 2, 'BC022.jpg', 'WH-BC022', 'Open sculpted bangle in hardwood, 34mm.', 'Open gesculpteerde armband in hardhout, 34mm.', 77, 59, '50pcs', '50stuks'),
(38, 2, 'BC009.jpg', 'WH-BC008', 'Bangle with shell cracking/blocking, 27mm,ass. colors.', 'Armband met schelp \'cracking/blocking\', 27mm,geass. kl.', 192, 147, '50pcs', '50stuks'),
(39, 3, 'ER001.jpg', 'WH-ER001', 'Assorted earrings in white and light brown capiz. Length 32 to 47mm. ', NULL, 52, 40, '100pcs', '100stuks'),
(40, 3, 'ER008.jpg', 'WH-ER008', 'Earrings oval with hole; violet lilly, tiger cowrie and MOP, 40x25mm, assorted.', NULL, 52, 40, '60pcs', '60stuks'),
(41, 3, 'ER009.jpg', 'WH-ER009', 'Earrings diamond shape blacklip and brownlip, 45x31mm, assorted. ', NULL, 55, 42, '60pcs', '60stuks'),
(42, 3, 'ER010.jpg', 'WH-ER010', 'Earrings rectangular shell pendant; 42x32mm, blacklip, brownlip and MOP assorted.', NULL, 55, 42, '60pcs', '60stuks'),
(43, 3, 'ER011.jpg', 'WH-ER011', 'Earrings 4sided blacklip and brownlip shel, 32x23mm, assorted. .', NULL, 23, 18, '2dozen', '2dozijn'),
(44, 3, 'ER012.jpg', 'WH-ER012', 'Earrings carved leaf; melo shell, ??and tiger cowrie, 30x19mm, assorted.', NULL, 30, 23, '60pcs', '60stuks'),
(45, 3, 'ER013.jpg', 'WH-ER013', 'Earrings oval shell pendant; blackpen, blacklip and capiz, 48x33mm, assorted.', NULL, 57, 44, '60pcs', '60stuks'),
(46, 3, 'ER014.jpg', 'WH-ER014', 'Earrings carved flower 40x40mm pendant; blacklip, brownlip and MOP assorted.', 'Oorbellen met gegraveerde bloemhangertjes; blacklip, brownlip en parelmoer geassorteerd.', 55, 42, '60pcs', '60stuks'),
(47, 3, 'ER015.jpg', 'WH-ER015', 'Earrings 2sided oval; cowrie and , 35x27mm, assorted. ', 'Oorbellen met tweezijdige schelphangertjes, ?schelp 35x27mm, geassorteerd. ', 20, 15, '2dozen', '2dozijn'),
(48, 3, 'ER016.jpg', 'WH-ER016', 'Earrings spade shape; violet lilly, MOP and tiger cowrie, 43x34mm, assorted.', 'Oorbellen in spadevorm; violet lilly, parelmoer en tiger cowrie, 43x34mm, geassorteerd.', 52, 40, '60pcs', '60stuks'),
(49, 3, 'ER017.jpg', 'WH-ER017', 'Earring with 3sided pendant 30x15mm; blackpen and ? assorted .', 'Oorbellen met driezijdige hangertjes 30x15mm; blackpen and ? geassorteerd .', 20, 15, '2dozen', '2dozijn'),
(50, 3, 'ER018.jpg', 'WH-ER018', 'Earrings with rectangular pendant 30x16mm; blackpen, avalone shell assorted.', 'Oorbellen met rechthoekige hangertjes 30x16mm; blackpen en avalone schelp geassorteerd.', 31, 24, '60pcs', '60stuks'),
(51, 3, 'ER023.jpg', 'WH-ER023', 'Earrings 2sided round 22mm; green turbo and tiger cowrie assorted.', 'Oorbellen met tweezijdige schelprondjes 22mm, green turbo en tiger cowrie, geassorteerd.', 21, 16, '2dozen', '2dozijn'),
(52, 3, 'ER028.jpg', 'WH-ER028', 'Earrings paua blocking on raisin round 30mm. ', 'Oorbellen met 30mm rondjes in paua blocking op kunsthars. ', 29, 22, '2dozen', '2dozijn'),
(53, 3, 'ER030.jpg', 'WH-ER030', 'Oorbellen met paua op kunsthars en jadekraal; lengte 40mm.', 'Earrings with paua on raisin and jade bead; 40mm long.', 29, 22, '2dozen', '2dozijn'),
(54, 3, 'ER032.jpg', 'WH-ER032', 'Earrings with teardrop in paua shell; 55x33mm. .', 'Oorbellen met hanger in pauaschelp, traanvorm 55x33mm.', 39, 30, '2dozen', '2dozijn'),
(55, 3, 'ER035.jpg', 'WH-ER035', 'Earrings with capiz shell pendant; round 25mm and teardrop 30mm; assorted.', 'Oorbellen met capz hanger, rond 25mm en traanvorm 30mm; geassorteerd.', 47, 36, '100pcs', '100stuk'),
(56, 4, 'ASSJST001.jpg', 'ASS-JST001', '6 sets in glass beads, shell crackings and shell pendant, 20 and 40cm long.', '6 sets in glaskraal, schelp crackings and hanger in schelp, 20 en 40cm lang.', 85, 65, '36sets', '36sets'),
(57, 4, 'ASSJST002.jpg', 'ASS-JST002', 'Assorted sets w/ 2 and 3 sided shell pieces and glass beads; bracelets 20cm long..', 'Geassorteerde sets met twee- en drieziijdige schelphangers en glaskraal; armband 20cm lang..', 44, 34, '36sets', '36sets'),
(58, 4, 'JST011.jpg', 'WH-JST011', 'Four piece set in glass beads with paua pendats and 16mm jade beads; 40, 20 and 4cm long.', 'Set 4 stuks in glaskraal met paua hangers en 16mm jadekralen, 40, 20 and 4cm lang.', 48, 37, 'dozen sets', 'dozijn sets'),
(59, 4, 'JST012.jpg', 'WH-JST012', 'Three piece set in glass beads with paua teardrop 55x33mm and cracking, necklace 40cm long.', 'Set 3 stuks in glaskraal met traanvormige paua hangers 55x33mm en cracking, halsketting 40cm lang.', 46, 35, 'dozen sets', 'dozijn sets'),
(60, 4, 'JST013.jpg', 'WH-JST013', 'Set in glasbeads w/hammershell crackings and mother of pearl pendants (D=65mm and L=44mm); necklace 40cm long.', 'Set4 stuks in glaskraal met hammershell crackings en parelmoer hangers (D=65mm en L=44mm); halsketting 40cm lang.', 46, 35, 'dozen sets', 'dozijn sets'),
(61, 4, 'JST019.jpg', 'WH-JST019', 'Three piece set in polished troca, necklace 40cm and earrings 40mm long.', 'Set 3 stuks in gepolijste trocschelp, halsketting 40cm en oorbellen 40mm lang.', 52, 40, 'dozen sets', 'dozijn sets'),
(62, 10, 'CP001.jpg', 'WH-CP001', 'Coin purse in whole tambayang shell, copper frame, D=7to8cm. ', 'Portemonnaie in hele tambayangschelp, kader in koper, D=7à8cm. ', 104, 80, '100pcs', '100stuks'),
(63, 10, 'CP002.jpg', 'WH-CP002', 'Coin purse in bontal, 8x9cm. ', 'Portemonnaie in bontavezel, 8x9cm. ', 108, 83, '100pcs', '100stuks'),
(64, 10, 'CP003.jpg', 'WH-CP003', 'Coin purse in bontal, 7x6cm.', 'Portemonnaie in bontalvezel, 7x6cm.', 78, 60, '100pcs', '100stuks');

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `tid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `en` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nl` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`tid`, `cid`, `en`, `nl`) VALUES
(1, 1, 'necklaces', 'halskettingen'),
(2, 1, 'bracelets', 'armbanden'),
(3, 1, 'earrings', 'oorbellen'),
(4, 1, 'sets', 'sets'),
(5, 1, 'others', 'andere'),
(6, 1, 'components', 'componenten'),
(7, 2, 'hair fashion', 'haarmode'),
(8, 2, 'belts', 'riemen'),
(9, 2, 'hats&caps', 'hoofddeksels'),
(10, 2, 'bags&purses', 'tassen'),
(11, 2, 'footwear', 'schoeisel'),
(12, 3, 'gifts', 'cadeau'),
(13, 3, 'interior', 'interieur'),
(14, 3, 'garden', 'tuin'),
(15, 4, 'Sampagita', 'Sampagita');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `main`
--
ALTER TABLE `main`
  ADD PRIMARY KEY (`mid`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`tid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `main`
--
ALTER TABLE `main`
  MODIFY `mid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
