<?php
include_once str_replace('/public_html', '', $_SERVER["DOCUMENT_ROOT"]).'/auth/db_connect';

if(mysqli_connect_errno()) echo "Failed to connect to MySQL: " . mysqli_connect_error();

$lang = 'en';
$langerr = null;
if($_GET['lang'] == 'nl') $lang = 'nl'; elseif($_GET['lang'] == 'en') $lang = 'en'; else $langerr = 'Error: Language not available.';
?>

<html>
<head>
<title>a2e-online.com - <? if(isset($_GET['prod'])) echo 'Productpage'; else echo 'Homepage';?></title>
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="headerfill"><img src="images/header.png"></div>
<table cellpadding="0" id="navborder"><tr><td>
<table id="nav"><tr>
	<td id="update">last updated: April 21, 2007</td>
	<td><span class="navitem">
		<?php
		$order['en'] = 'order form';
		$order['nl'] = 'bestelformulier';
		echo '
				&gt;<a href="home.php?lang='.$lang.'">start</a>&nbsp;
				&gt;<a>'.$order[$lang].'</a>&nbsp;
				&gt;<a href onclick="javascript:window.open(\'bcard.html\', \'_blank\', \'width=620,height=580\')">business card</a>&nbsp;
				&gt;<a href="mailto:info@a2e-online.com">contact</a>
			 ';
		?>
	</span></td>
</tr></table>
</td></tr></table>
<table id="container"><tr>
<td id="navleft">
	<div class="contentborder">
	<div class="content">
		<div id="navleftalign">
		<?php
		$query = "SELECT * FROM `categories`";	
		$sql = mysqli_query($mysqli_site, $query) or die ( mysqli_error($mysqli_site) );
		while($record = mysqli_fetch_object($sql))
		{
			echo '
					<span class="title2">'.$record->$lang.'</span><br />
					<span class="navitem">
				 ';

			$query2 = "SELECT * FROM `types` WHERE cid='".$record->cid."'";
			$sql2 = mysqli_query($mysqli_site, $query2) or die ( mysql_error($mysqli_site) );
			while($record2 = mysqli_fetch_object($sql2))
			{
				$query3 = "SELECT * FROM `products` WHERE tid='".$record2->tid."'";
				$sql3 = mysqli_query($mysqli_site, $query3) or die ( mysql_error($mysqli_site) );
				if(mysqli_num_rows($sql3) > 0) $href = 'href="home.php?lang='.$lang.'&prod='.$record2->tid.'"';
				else $href = null;
				mysqli_free_result($sql3);
				echo '<a '.$href.'>'.$record2->$lang.'</a><br />';
			}
			mysqli_free_result($sql2);
			echo '</span>';
		}
		mysqli_free_result($sql);
		?>
		</div>
	</div>
	</div>
</td>
<td id="content">
	<div class="contentborder">
	<div class="content">
		<?php
			if($langerr) echo '<div align="center">'.$langerr.'</div>'; elseif(isset($_GET['prod'])) include 'product.php'; else include 'main.php';
			mysqli_close($mysqli_site);
		?>
	</div>
	</div>
</td>
</tr></table>
</body>