<?php

//BEGIN data fetch and error handling
function is_natural($val)
{
	$return = ((string)$val === (string)(int)$val);
	if ($return && intval($val) < 1)
		$return = false;
	return $return;
}

$tid = null;
if(is_natural($_GET['prod']))
{
	$tid = $_GET['prod'];
	$query = "SELECT tid FROM `types` WHERE tid=$tid";	
	$sql = mysqli_query($mysqli_site, $query) or die ( mysqli_error($mysqli_site) );
	$typeexists = mysqli_num_rows($sql);
	mysqli_free_result($sql);
} else $typeexists = false;

if($typeexists)
{
	$query = "SELECT img,code,desc_en,desc_nl,dollar,euro,unit_en,unit_nl FROM `products` WHERE tid = $tid";
	$sql = mysqli_query($mysqli_site, $query) or die ( mysqli_error($mysqli_site) );
	$numrows = mysqli_num_rows($sql);
	$numpages = ceil($numrows/12);

	if(isset($_GET['p'])) $p = $_GET['p'];
	else $p = 1;
	if((!is_natural($p) || $p > $numpages) && $numpages)
	{
		if($lang == 'en') $err = 'Error: Invalid pagenumber.<br />';
			elseif($lang == 'nl') $err = 'Fout: Ongeldig paginanummer.<br />';
	}
		elseif(!$numpages)
		{
			if($lang = 'en') $err = 'No products available.<br />';
			elseif($lang = 'nl') $err = 'Geen producten beschikbaar.<br />';
		}
	
	if($numpages > 1) 
	{
		$pag = '<div class="page">page: ';
		for($i=1; $i<=$numpages; $i++)
		{
			if($i != $p) $pag .= '<a href="home.php?lang='.$lang.'&prod='.$tid.'&p='.$i.'">';
			$pag .= $i;
			if($i != $p) $pag .= '</a>';
			if($i != $numpages) $pag .= ' ';
		}
		$pag .= '</div>';
	}	else $pag = null;
	echo $pag;
}
	elseif($lang == 'en') $err = 'Error: Invalid product type ID.<br />';
		elseif($lang == 'nl') $err = 'Fout: Ongeldig producttype-ID.<br />';

//END URL check and error handling

//page output
if(isset($err)) echo '<div align="center">'.$err.'</div>';
	else
	{
		$desc = 'desc_'.$lang;
		$unit = 'unit_'.$lang;
		$s = ((($p-1)*12)+1);
		
		echo '<table id="thumbcontainer">';

		$i=1;			
		while($record = mysqli_fetch_object($sql))
		{			
			if($i >= $s && $i <= $s+11)
			{
				if(($i-1)%3 == 0) echo '<tr align="center">';
				echo '
						<td>
							<div class="thumbborder">
							<div class="thumb">
								<div class="thumbline"><img width="250" height="188" src="images/'.$record->img.'"></div>
								<div class="thumbline2"><b>'.$record->code.'.</b> '.$record->$desc.'<br>
								<b>US$'.$record->dollar.'/&euro;'.$record->euro.' per '.$record->$unit.'.</b></div>
							</div>
							</div>
						</td>
					 ';
				if($i%3 == 0 || $i == $numrows) echo '</tr>';
			}
			$i++;
		}
		mysqli_free_result($sql);

		echo '</table>'.$pag;
	}
?>