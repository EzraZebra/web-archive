#### ezrazebra.net (2018)
Personal website with CMS.  
[Live demo](https://ezrazebra.net/archive/2018_ezrazebra.net/) ([login](https://ezrazebra.net/archive/2018_ezrazebra.net/admin/): `demo1@ezrazebra.net` / `demo1`)

`jQuery 3.3.1`  
`PHP 7`  
`MySQL`

_[design](https://ezrazebra.net/archive/designs_by_dirk_hertmans/2014%20ZebraCMS/Templates/ZCMS~BCC2~VEO~brown.png) &copy;2014 Dirk Hertmans_
