let fullSlide = {
	check: function() {
		return (	document.fullscreenElement ||    // alternative standard method
					document.mozFullScreenElement || document.webkitFullscreenElement || document.msFullscreenElement	)  // current working methods
	},

	enter: function() {
		if(!this.check()) {
			if (document.documentElement.requestFullscreen) document.documentElement.requestFullscreen();
			else if (document.documentElement.msRequestFullscreen) document.documentElement.msRequestFullscreen();
			else if (document.documentElement.mozRequestFullScreen) document.documentElement.mozRequestFullScreen();
			else if (document.documentElement.webkitRequestFullscreen) document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);

			return true;
		}
		else return false;
	},

	exit: function() {		
		if (document.exitFullscreen) document.exitFullscreen();
		else if (document.msExitFullscreen) document.msExitFullscreen();
		else if (document.mozCancelFullScreen) document.mozCancelFullScreen();
		else if (document.webkitExitFullscreen) document.webkitExitFullscreen();
	},

	toggle: function() {
		if(!this.enter()) this.exit();
	},
	
	toggleBtn: function() {
		if(!fullSlide.check()) $('#slideFullscrBtn').removeClass('exit-fullscreen').addClass('enter-fullscreen');
		else $('#slideFullscrBtn').removeClass('enter-fullscreen').addClass('exit-fullscreen');
	}
};

function updateState(stateData) {
	let $langA = $('#langSelect a[href]');

	history.pushState({id: stateData.page_data}, '', stateData.url);
	$langA.attr("href", '/archive/2018_ezrazebra.net/'+$langA.parent().attr("data-lang")+'/'+stateData.page_url);
}

function browsePage(page, isAlbum, title, stateData) {
	updateState(stateData);

	$('html').attr('data-page', page);
	$('title').html(title);

	$('html, body').css('overflow', 'hidden');
	$('.current-page').addClass('hidden-page').removeClass('current-page');
	if(isAlbum) $('.back[data-page="'+page+'"]').addClass('show');
	else $('.back.show').addClass('hide').removeClass('show');
	$('#content > section[data-page-data="'+stateData.page_data+'"]')
		.addClass('current-page')
		.one('transitionend webkitTransitionEnd oTransitionEnd msTransitionEnd', function() {
			$(this).off('transitionend webkitTransitionEnd oTransitionEnd msTransitionEnd');
			$(window).scrollTop(0);
			$('.hidden-page').removeClass('hidden-page');
			$('.back.hide').removeClass('hide');
			$('html, body').css('overflow', '');
		});
}

function browse(browseData) {
	let iCount = browseData.$container.find('.slide-wrapper').length,
		iNew = parseInt(browseData.$btn.attr('data-index')),
		iNext = iNew+1, iPrevious = iNew-1,
		$new = browseData.$container.find('.slide-wrapper[data-index="'+iNew+'"]'),
		$old = browseData.$container.find('.slide-current'),
		$btns = browseData.$container.find('.slide-prev-btn, .slide-next-btn');
		
	$btns.off('click.'+browseData.eventName).css({ transition: 'opacity 0.2s', 'opacity': 0.35 });

	if(iNext > iCount) iNext = 1;
	if(iPrevious < 1) iPrevious = iCount;

	updateState(browseData.stateData);
	
	browseData.$container.find('.slide-index').text(iNew);
	browseData.$container.find('.slide-prev-btn').attr('href', iPrevious).attr('data-index', iPrevious);
	browseData.$container.find('.slide-next-btn').attr('href', iNext).attr('data-index', iNext);

	if(!browseData.doSlide) {
		$old.removeClass('slide-current');
		$new.addClass('slide-current');
		if(typeof browseData.callback === 'function') browseData.callback();
		$btns.css({ transition: '', 'opacity': '' }).on('click.'+browseData.eventName, browseData.clickEvent);
	}
	else {
		if(browseData.$btn.hasClass('slide-prev-btn')) {
			$old.addClass('hide-right');
			$new.addClass('show-left');
		}
		else {
			$old.addClass('hide-left');
			$new.addClass('show-right');
		}

		$new.on('transitionend webkitTransitionEnd oTransitionEnd msTransitionEnd', function() {
			$(this).off('transitionend webkitTransitionEnd oTransitionEnd msTransitionEnd');
			$new.addClass('slide-current');
			$old.removeClass('slide-current');
			$('.show-right').removeClass('show-right');
			$('.show-left').removeClass('show-left');
			$('.hide-left').removeClass('hide-left');
			$('.hide-right').removeClass('hide-right');
			if(typeof browseData.callback === 'function') browseData.callback();
			setTimeout(function(){ //fucking IE fix
				$btns.css({ transition: '', 'opacity': '' }).on('click.'+browseData.eventName, browseData.clickEvent);
			}, 100);
		});
	}
}

function browseSlideshow($btn, opening, inPage) {
	let	page = $('html').attr('data-page'),
		album = $btn.attr('data-album'),
		iNew = parseInt($btn.attr('data-index')),
		url = iNew;

	if(opening) {
		url = album+'/'+url;
		if(inPage) url = page+'/'+url;
	}
	
	browse({
		$btn: $btn, $container: $('#slideshow'),
		stateData: { page_url: page+'/'+album+'/'+iNew, page_data: page+'_'+album+'_'+iNew, url: url },
		doSlide: !opening,
		eventName: 'slideshow', clickEvent: function() { browseSlideshow($(this), false, false); }
	})
}

function browsePoetry() {
	poetryBrowsed = true;
	
	$('html, body').css('overflow', 'hidden');
	browse({
		$btn: $(this), $container: $('section[data-page-data="poetry"]'),
		stateData: { page_url: 'poetry/'+parseInt($(this).attr("data-index")), page_data: 'poetry', url: $(this).attr('href') },
		doSlide: true,
		eventName: 'poetry', clickEvent: browsePoetry,
		callback: function() { $('html, body').css('overflow', ''); }
	});
}

function enableSlideKB() {
	$(document).on('keyup.fullSlide', function(e) {
		if(e.keyCode === 70) $('#slideFullscrBtn').click(); // F
		if(e.keyCode === 27 && !fullSlide.check()) $('#slideCloseBtn').click(); //	esc
		if(e.keyCode === 37) $('#slideshow .slide-prev-btn').click();   // left
		if(e.keyCode === 39) $('#slideshow .slide-next-btn').click();   // right 
	});
}

function enablePoetryKB() {
	$(document).on('keyup.poetry', function(e) {
		if(e.keyCode === 37) $('section[data-page-data="poetry"] .slide-prev-btn').click();   // left
		if(e.keyCode === 39) $('section[data-page-data="poetry"] .slide-next-btn').click();   // right 
	});
}

function eventsMain($wrapper, page) {
	//LOAD PAGE
	$wrapper.find('.nav-button').click(function(event) {
		if(!event.ctrlKey && !event.shiftKey && !event.metaKey) {
			let page = $(this).attr("data-page"),
				stateData = {
					page_data: page,
					page_url: page,
					url: $(this).attr("href")
				},
				album = null,
				isAlbum = false;

			if(typeof $(this).attr("data-album") !== "undefined") {
				album = $(this).attr("data-album");
				isAlbum = true;
				stateData.page_data += '_'+album;
			}
		
			if($('#content > section.current-page').attr('data-page-data') != stateData.page_data) {
				let title = $(this).attr("data-title"),
					isAdmin = typeof $('html').attr('data-admin') !== 'undefined' && $('html').attr('data-admin') == 'true';
				if(isAdmin) stateData.page_url = 'admin/'+stateData.page_url;
				if(isAlbum) stateData.page_url += '/'+album;
				
				if($('#content > section[data-page-data="'+stateData.page_data+'"]').length === 0) {
					$('#pageLoading').show();
					let pageLocation;
					if(isAlbum) pageLocation = 'album.php';
					else pageLocation = page+'.php';
					if(isAdmin) pageLocation = '/archive/2018_ezrazebra.net/admin/'+pageLocation;
					else pageLocation = '/archive/2018_ezrazebra.net/content/'+pageLocation;

					$.ajax({
						url: pageLocation,
						type:'HEAD',
						error: function() {
							page = 'notfound';
							pageLocation = '/archive/2018_ezrazebra.net/content/notfound.php';
							stateData.page_data = 'notfound';
						},
						complete: function() {
							pageLocation += '?p='+page;
							if(isAlbum) pageLocation += '&album='+album;
							if(isAdmin) pageLocation += '&admin=true';

							$('<section />').attr('data-page-data', stateData.page_data).appendTo('#content').load(pageLocation, function() {
								$(this)[0].offsetHeight;
								eventsMain($(this), page);
								if(page == 'poetry') enablePoetryKB();
								else $(document).off('keyup.poetry');
								$('#pageLoading').hide();
								browsePage(page, isAlbum, title, stateData);
							});
						}
					});
				}
				else {
					if(page == 'poetry' && poetryBrowsed) {
						let poem = $('.poem-wrapper.slide-current').attr('data-index');
						stateData.page_url += '/'+poem;
						stateData.url += '/'+poem;
					}
					
					browsePage(page, isAlbum, title, stateData);
				}

			}

			return false;
		}
	});
	//OPEN SLIDESHOW
	$wrapper.find('.photoThumb').click(function(event) {
		if(!event.ctrlKey && !event.shiftKey && !event.metaKey) {
			let page = $('html').attr('data-page'),
				$btn = $(this),
				album = $btn.attr('data-album'),
				inPage = false;
			if(typeof $btn.attr('data-inpage') !== 'undefined' && $btn.attr('data-inpage') == 'true') inPage = true;
				
			if($('#slideshow').attr('data-page') != page || $('#slideshow').attr('data-album') != album)
				$("#slideshow").empty().load('/archive/2018_ezrazebra.net/content/slideshow.php?p='+page+'&album='+album+'&inpage='+inPage, function() {
					eventsSlideshow();
					browseSlideshow($btn, true, inPage);
				}).attr('data-page', page).attr('data-album', album);
			else browseSlideshow($btn, true, inPage);

			$('#slideshow').addClass('slideshow-show');
			if(inPage && typeof $btn.attr('data-title') !== 'undefined') $('title').text($btn.attr('data-title'));
			enableSlideKB();

			return false;
		}
	});
	//BROWSE POETRY
	if(page == 'poetry' || page == 'all') {
		$('section[data-page-data="poetry"]').find('.slide-prev-btn, .slide-next-btn').click(false).on('click.poetry', browsePoetry);
		if($('#poemToolbar').length !== 0) {
			if(interval === null)
				interval = setInterval(function() {
					$('#poemToolbar').css(	'transform',
											'translateY('+$(window).scrollTop()+'px) translateX('+$(window).scrollLeft()+'px)');
				}, 70);
		}
		else clearInterval(interval);
	}
	else clearInterval(interval);

	setTimeout(function() {
		$('body').trigger('pageLoad', [ $wrapper ]);
	}, 1);
}

function eventsSlideshow() {
	//CLOSE SLIDESHOW
	$('#slideCloseBtn').click(function() {
		let page = $('html').attr('data-page'),
			page_url = page,
			page_data = page;
		if(typeof $(this).attr('data-album') !== 'undefined') {
			let album = $(this).attr('data-album');
			page_url += '/'+album;
			page_data += '_'+album;
		}

		fullSlide.exit();
		$(document).off('keyup.fullSlide');
		updateState({
			page_url: page_url,
			page_data: page_data,
			url: $(this).attr("href")
		});
		$('#slideshow').removeClass('slideshow-show');
		if(typeof $(this).attr('data-title') !== 'undefined') $('title').text($(this).attr('data-title'));
		
		return false;
	});
	$('.slide-wrapper').click(function(e) {
		if($(e.target).is('.slide-wrapper')) $('#slideCloseBtn').click();
	});
	//BROWSE SLIDESHOW
	$('#slideshow').find('.slide-prev-btn, .slide-next-btn').click(false).on('click.slideshow', function() {
		browseSlideshow($(this), false, false);
	});
	//SWIPE SLIDESHOW
	$('.slideshowImg > img').on('touchstart', function(e) {
		let $img = $(this),
			start = e.touches[0].pageX,
			offset = 0;
		
		$img.on('touchmove.swipe', function(e) {
			offset = e.touches[0].pageX-start;
			
			$img.css('left', offset);
			
			return false;
		});
		
		$img.on('touchend.swipe touchcancel.swipe', function() {
			$img.off('touchmove.swipe').off('touchend.swipe').off('touchcancel.swipe');
			if(Math.abs(offset) > $(window).width()*0.25) {
				if(offset > 0) $('#slideshow .slide-prev-btn').click();
				else $('#slideshow .slide-next-btn').click();
			}
			
			$img.animate( {
				left: 0,
				right: 0
			}, 1000);
			
			return false;
		});
		
		return false;
	});
	//TOGGLE FULLSCREEN
	$('#slideFullscrBtn').click(function() { fullSlide.toggle() });
}

var poetryBrowsed = false;
var interval = null;
$(document).ready(function() {
	eventsMain($('body'), 'all');
	eventsSlideshow();

	if($('#slideshow').hasClass('slideshow-show')) enableSlideKB();
	if($('html').attr('data-page') == 'poetry') {
		if($('#poemToolbar').attr('data-poem-browsed') == 'true') poetryBrowsed = true;
		enablePoetryKB();
	}
	
	$(document).on('webkitfullscreenchange mozfullscreenchange msfullscreenchange fullscreenchange', fullSlide.toggleBtn);

	$(window).on('popstate', function() {
		location.reload();
	});
});