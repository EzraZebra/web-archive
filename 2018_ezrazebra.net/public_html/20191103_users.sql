-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 03, 2019 at 04:29 PM
-- Server version: 10.3.18-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `users`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_logins_24h`
--

CREATE TABLE `failed_logins_24h` (
  `time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_logins_ip`
--

CREATE TABLE `failed_logins_ip` (
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempt_count` int(11) NOT NULL,
  `last_attempt` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_logins_total`
--

CREATE TABLE `failed_logins_total` (
  `attempt_count` int(11) NOT NULL,
  `time_start` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `failed_logins_total`
--

INSERT INTO `failed_logins_total` (`attempt_count`, `time_start`) VALUES
(0, '2018-03-13 23:00:01');

-- --------------------------------------------------------

--
-- Table structure for table `failed_logins_uid`
--

CREATE TABLE `failed_logins_uid` (
  `uid` int(11) NOT NULL,
  `attempt_count` int(11) NOT NULL,
  `last_attempt` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `uid` int(11) NOT NULL,
  `ezrazebra` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`uid`, `ezrazebra`) VALUES
(1, 'admin'),
(2, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `uid` int(11) NOT NULL,
  `username` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `join_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `username`, `password`, `email`, `join_date`) VALUES
(2, 'demo1', '$2y$10$qrp/oZpzMGzZZZvmZXLhOesVyBB3e2S2Vk6QRe.7Lx1czZftWwPIC', 'demo1@ezrazebra.net', '2019-02-18 19:57:00'),
(1, 'admin', '$2y$10$.qsjXG7tjMIcfFFhKvk8g.9KHG5O21VmMVoe1IdG4bm2.Opie/JtW', 'admin@ezrazebra.net', '2019-02-19 00:54:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_logins_ip`
--
ALTER TABLE `failed_logins_ip`
  ADD UNIQUE KEY `ip` (`ip`);

--
-- Indexes for table `failed_logins_uid`
--
ALTER TABLE `failed_logins_uid`
  ADD UNIQUE KEY `uid` (`uid`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD UNIQUE KEY `uid` (`uid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
