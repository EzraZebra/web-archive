<?php
	session_start();
	
	//PAGE
		if(isset($_GET['p'])) $page = $_GET['p'];
		else $page = 'home';

		$page_data = $page;
		$isAlbum = false;
		$isImage = false;

		if(isset($_GET['album'])) {
			$isAlbum = true;
			$get_album = $_GET['album'];
			$page_data .= '_'.$get_album;
		}

		if(isset($_GET['image'])) {
			$isImage = true;
			$get_image = $_GET['image'];
		}

		$root_folder = '/archive/2018_ezrazebra.net/';
		$content_path = $_SERVER["DOCUMENT_ROOT"].$root_folder.'content/';
		if($isAlbum) $include_path = $content_path.'album.php';
		else {
			$include_path = $content_path.$page.'.php';
			if(!file_exists($include_path)) $include_path = $_SERVER["DOCUMENT_ROOT"].$root_folder.'content/notfound.php';
		}
	
	//LANGUAGE
		include_once($_SERVER["DOCUMENT_ROOT"].$root_folder.'includes/lang_dic.php');
		$dic = new Dictionary(true);
?>

<!DOCTYPE html>
<html lang="<?= $dic->lang; ?>" data-page="<?= $page; ?>">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet">
	<link href="<?= $root_folder; ?>css/style.css" rel="stylesheet">
    <noscript><style> .jsonly { display: none !important; } </style></noscript>
	
	<!-- For IE 9 and below. ICO should be 32x32 pixels in size -->
	<!--[if lte IE 9]><link rel="shortcut icon" href="<?= $root_folder; ?>favicon.ico"><![endif]-->

	<!-- Firefox, Chrome, Safari, IE 11+ and Opera. 196x196 pixels in size. -->
	<link rel="icon" href="<?= $root_folder; ?>favicon.png">

	<title><?php if(isset($dic->pages[$page_data])) echo $dic->pages[$page_data]; else echo $dic->pages['notfound']; ?></title>
</head>
<body>
<!-- - - - - - - - - - - -->
<!--		HEADER		 -->
<!-- - - - - - - - - - - -->
<header id="pageHeader">
	<div id="headerTop"></div>
	<!--		NAV		-->
	<div id="headerMain">
		<a	id="headerLogo" href="<?= $root_folder.$dic->lang.'/home'; ?>"
			class="nav-button" data-page="home" data-title="<?= $dic->pages['home']; ?>">
			<img src="<?= $root_folder; ?>/favicon.png" alt="E" title="<?= $dic->pages['home']; ?>" />
		</a>
		<nav><ul>
			<li><a href="<?= $root_folder.$dic->lang.'/music'; ?>" class="nav-button" data-page="music" data-title="<?= $dic->pages['music']; ?>">
				<?= $dic->pages['music']; ?>
			</a></li>
			<li class="nav-divider">&ndash;</li>
			<li><a href="<?= $root_folder.$dic->lang.'/poetry'; ?>" class="nav-button" data-page="poetry" data-title="<?= $dic->pages['poetry']; ?>">
				<?= $dic->pages['poetry']; ?>
			</a></li>
		</ul></nav>
		<div id="langSelect"><ul>
			<li data-lang="en"><a 
				<?php if($dic->lang === 'nl') echo 'href="'.str_replace('nl', 'en', $_SERVER['REQUEST_URI']).'"'; ?>>
			</a></li>
			<li data-lang="nl"><a 
				<?php if($dic->lang === 'en') echo 'href="'.str_replace('en', 'nl', $_SERVER['REQUEST_URI']).'"'; ?>>
			</a></li>
		</ul></div>
	</div>
	<div id="headerBottom"></div>
	<div id="headerNav">
		<nav	class="back<?php if($isAlbum && $page == 'photo') echo ' show' ?>"
				title="<?= $dic->gen['back_to'].$dic->pages['photo']; ?>"
				data-page="photo"><ul>
			<li><a class="nav-button" data-page="photo" data-title="<?= $dic->pages['photo']; ?>" href="<?= $root_folder.$dic->lang.'/photo'; ?>">
				<?= $dic->gen['back']; ?>
			</a></li>
		</ul></nav>
		<nav	class="back<?php if($isAlbum && $page == 'webdev') echo ' show'; ?>"
				title="<?= $dic->gen['back_to'].$dic->pages['webdev']; ?>"
				data-page="webdev"><ul>
			<li><a class="nav-button" data-page="webdev" data-title="<?= $dic->pages['webdev']; ?>" href="<?= $root_folder.$dic->lang.'/webdev'; ?>">
				<?= $dic->gen['back']; ?>
			</a></li>
		</ul></nav>
		<nav	class="back<?php if($isAlbum && $page == 'minesweeper') echo ' show'; ?>"
				title="<?= $dic->gen['back_to'].$dic->pages['minesweeper']; ?>"
				data-page="minesweeper"><ul>
			<li><a class="nav-button" data-page="minesweeper" data-title="<?= $dic->pages['minesweeper']; ?>" href="<?= $root_folder.$dic->lang.'/minesweeper'; ?>">
				<?= $dic->gen['back']; ?>
			</a></li>
		</ul></nav>
		<nav><ul>
			<li><a href="<?= $root_folder.$dic->lang.'/photo'; ?>" class="nav-button" data-page="photo" data-title="<?= $dic->pages['photo']; ?>">
				<?= $dic->pages['photo']; ?>
			</a></li>
			<li class="nav-divider">&ndash;</li>
			<li><a href="<?= $root_folder.$dic->lang.'/more'; ?>" class="nav-button" data-page="more" data-title="<?= $dic->pages['more']; ?>">
				<?= $dic->pages['more']; ?>
			</a></li>
		</ul></nav>
	</div>
</header>
<!-- - - - - - - - - - - -->
<!--		CONTENT		 -->
<!-- - - - - - - - - - - -->
<div id="content">
	<section class="current-page" data-page-data="<?= $page_data; ?>">
		<?php 
			if(isset($_GET['error'])) echo $dic->error['error'].': '.$_GET['error'];
			include($include_path);
		?>
	</section>
</div>
<div	id="slideshow" <?php if($isAlbum && $isImage) echo
		'class="slideshow-show" data-album="'.$get_album.'" data-page="'.$page.'"'; ?>>
	<?php if($isAlbum && $isImage) include($content_path.'/slideshow.php'); ?>
</div>

<img id="pageLoading" src="<?= $root_folder; ?>img/loading-white.gif" alt="Loading..." />

<script src="<?= $root_folder; ?>jquery/jquery-3.3.1.min.js"></script>
<script src="<?= $root_folder; ?>jquery/script.js"></script>

<!-- - - - - - - - - - - - - - - -->
<!--		BROWSER UPDATE		 -->
<!-- - - - - - - - - - - - - - - -->
<script> 
	var $buoop = {notify:{i:-5,f:-4,o:-4,s:-2,c:-4},insecure:true,unsupported:true,api:5}; 
	function $buo_f(){ 
		var e = document.createElement("script"); 
		e.src = "//browser-update.org/update.min.js"; 
		document.body.appendChild(e);
	};
	try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
	catch(e){window.attachEvent("onload", $buo_f)}
</script>
</body>
</html>