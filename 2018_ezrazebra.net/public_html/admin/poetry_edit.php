<?php
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/auth/db_connect';
if(!isset($auth)) {
	include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/login_func.php';
	$auth = new Auth();
	$auth->sec_session_start();
}
if(!isset($dic)) {
	include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/lang_dic.php';
	$dic = new Dictionary();
}

if(!$auth->isLoggedIn()) include($_SERVER['DOCUMENT_ROOT'].'/archive/2018_ezrazebra.net/includes/login_form.php');
elseif(!$auth->isAdmin('ezrazebra')) echo '<div class="error">'.$dic->error['no_permission'].'</div>';
else {
	$path = $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/content/poetry/';

	if(isset($_POST['action']) && $_POST['action'] == 'save') {
		echo '<div class="error">';
		if(isset($_POST['filename'], $_POST['poem']) && $file = fopen($path.basename($_POST['filename']).'.txt', 'w')) {
			if(fwrite($file, $_POST['poem']) !== false) echo $dic->result['changes_saved'];
			else echo $dic->error['unknown'];
			
			fclose($file);
		}
		else echo $dic->error['unknown'];
		echo '</div>';
	}

	$poems = array_reverse(glob($path.'*.txt'));
	$poemCount = count($poems);
	if(!isset($get_index)) $get_index = $_GET['edit'];

	foreach($poems as $i => $poem) {
		$filename = basename($poem, '.txt');
		$index = $i+1;
		if($index == $get_index) $class = ' slide-current';
		else $class = null;
		echo '
			<div class="slideshowEdit slide-wrapper'.$class.'" data-index="'.$index.'">
				<div id="slideshowTitle">
					'.$filename.'
					<span> &ndash; <span class="slide-index">'.$get_index.'</span> &sol; '.$poemCount.'</span>
				</div>
				<form class="form-confirm" action="'.$index.'" method="POST" name="poetry_edit" autocomplete="off">
					<input type="hidden" name="action" value="save" />
					<input type="hidden" name="filename" value="'.$filename.'" />
					<div>
						<textarea name="poem" spellcheck="true" rows="15" required>'.htmlentities(file_get_contents($poem)).'</textarea>
					</div>
					<div><input type="submit" value="'.$dic->settings['save'].'" /></div>
				</form>
			</div>';
	}

	if($get_index == $poemCount) $next = 1;
	else $next = $get_index+1;
	if($get_index == 1) $previous = $poemCount;
	else $previous = $get_index-1;

	echo '	<a	class="slide-prev-btn slideshow-button" data-index="'.$previous.'"
				href="'.$previous.'" title="'.$dic->slideshow['previous'].'">
			</a>
			<a	class="slide-next-btn slideshow-button" data-index="'.$next.'"
				href="'.$next.'" title="'.$dic->slideshow['next'].'">
			</a>
			<div id="slideshowToolbar">
				<a	id="slideCloseBtn" class="slideshow-button"
					href="/archive/2018_ezrazebra.net/'.$dic->lang.'/admin/poetry" title="'.$dic->slideshow['close'].'">
				</a>
			</div>';
}
?>