<?php
if(!isset($auth)) {
	include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/login_func.php';
	$auth = new Auth();
	$auth->sec_session_start();
}
if(!isset($dic)) {
	include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/lang_dic.php';
	$dic = new Dictionary();
}
include_once($_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/func_misc.php');

echo '<h1 class="hidden">'.$dic->pages['music'].'</h1>';

print_error_session();

if(!$auth->isLoggedIn()) include($_SERVER['DOCUMENT_ROOT'].'/archive/2018_ezrazebra.net/includes/login_form.php');
elseif(!$auth->isAdmin('ezrazebra')) echo '<div class="error">'.$dic->error['no_permission'].'</div>';
else {
	include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/auth/db_connect';
	if(isset($_POST['action']) && $_POST['action'] == 'save') {
		if(isset($_POST['info_en'], $_POST['info_nl'], $_POST['width'], $_POST['height'], $_POST['playlist'])) {
			if ($stmt = $mysqli_site->prepare("	UPDATE music 
												SET info_en = ?, info_nl = ?,
													width = ?, height = ?,
													playlist = ?")) {
				$stmt->bind_param('ssiis',	$_POST['info_en'], $_POST['info_nl'],
											$_POST['width'], $_POST['height'],
											$_POST['playlist']);
				$stmt->execute();
				$stmt->close();
				$error = $dic->result['changes_saved'];
			}
			else $error = $mysqli_site->error.'<br />';
		}
		else $error = $dic->error['unknown'].'<br />';
	}
	if(isset($error)) echo '<div class="error">'.$error.'</div>';

	if($result = $mysqli_site->query("SELECT * FROM music")) {
		$settings = $result->fetch_assoc();
		echo '
			<form class="table form-confirm" action="music" method="POST" name="music_settings" autocomplete="off">
				<input type="hidden" name="action" value="save" />
				<label class="row" for="info_en">
					<span class="cell">'.$dic->misc['info'].' &lpar;EN&rpar;&colon;</span>
					<span class="cell">
						<textarea name="info_en" id="info_en" spellcheck="true" rows="3" required>'.$settings['info_en'].'</textarea>
					</span>
				</label>
				<label class="row" for="info_nl">
					<span class="cell">'.$dic->misc['info'].' &lpar;NL&rpar;&colon;</span>
					<span class="cell">
						<textarea name="info_nl" id="info_nl" spellcheck="true" rows="3" required>'.$settings['info_nl'].'</textarea>
					</span>
				</label>
				<div class="row">
					<span class="cell">'.$dic->settings['size'].'&colon;</span>
					<span class="cell">
						<label class="row" for="width">
							<span class="cell">'.$dic->settings['width'].'&colon;</span>
							<span class="cell">
								<input type="number" name="width" id="width" max="999" required value="'.$settings['width'].'" />px
							</span>
						</label>
						<label class="row" for="height">
							<span class="cell">'.$dic->settings['height'].'&colon;</span>
							<span class="cell">
								<input type="number" name="height" id="height" max="999" required value="'.$settings['height'].'" />px
							</span>
						</label>
					</span>
				</div>
				<label class="row" for="playlist">
					<span class="cell">'.$dic->misc['playlist'].'&colon;</span>
					<span class="cell">
						<input type="url" name="playlist" id="playlist" maxlength="255" required value="'.$settings['playlist'].'" />
					</span>
				</label>
				<div class="row"><span class="cell"></span>
					<span class="cell"><input type="submit" value="'.$dic->settings['save'].'" /></span>
				</div>
			</form>';
	}
	else echo $mysqli_site->error.'<br />';
}
?>