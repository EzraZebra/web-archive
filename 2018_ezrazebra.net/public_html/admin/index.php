<?php	
	//PAGE
		if(isset($_GET['p'])) $page = $_GET['p'];
		else $page = 'music';

		if(isset($_GET['edit'])) {
			$isEdit = true;
			$get_index = $_GET['edit'];
		}
		else $isEdit = false;

		$root_folder = '/archive/2018_ezrazebra.net/';
		$content_path = $_SERVER["DOCUMENT_ROOT"].$root_folder.'admin';
		$include_path = $content_path.'/'.$page.'.php';
		if(!file_exists($include_path)) $include_path = $_SERVER["DOCUMENT_ROOT"].$root_folder.'content/notfound.php';
	
	//LANGUAGE
		include_once($_SERVER["DOCUMENT_ROOT"].$root_folder.'includes/lang_dic.php');
		$dic = new Dictionary(true);
		
	//AUTH
		include_once $_SERVER["DOCUMENT_ROOT"].$root_folder.'includes/login_func.php';
		$auth = new Auth();
		$auth->sec_session_start();
?>

<!DOCTYPE html>
<html lang="<?= $dic->lang; ?>" data-admin="true" data-page="<?= $page; ?>">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet">
	<link href="<?= $root_folder; ?>css/style.css" rel="stylesheet">
	<link href="<?= $root_folder; ?>admin/css/style.css" rel="stylesheet">
    <noscript><style> .jsonly { display: none !important; } </style></noscript>

	<title><?php if(isset($dic->admin['panel'])) echo $dic->admin['panel']; else echo $dic->pages['notfound']; ?></title>
</head>
<body>
<!-- - - - - - - - - - - -->
<!--		HEADER		 -->
<!-- - - - - - - - - - - -->
<header id="pageHeader">
	<div id="headerTop"></div>
	<!--		NAV		-->
	<div id="headerMain">
		<a	id="headerLogo" href="<?= $root_folder.$dic->lang.'/home'; ?>">
			<img src="<?= $root_folder; ?>favicon.png" alt="E" title="<?= $dic->pages['home']; ?>" />
		</a>
		<nav><ul>
			<li><?= $dic->admin['panel']; ?></li>
		</ul></nav>
		<div id="langSelect"><ul>
			<li data-lang="en"><a 
				<?php if($dic->lang === 'nl') echo 'href="'.str_replace('nl', 'en', $_SERVER['REQUEST_URI']).'"'; ?>>
			</a></li>
			<li data-lang="nl"><a 
				<?php if($dic->lang === 'en') echo 'href="'.str_replace('en', 'nl', $_SERVER['REQUEST_URI']).'"'; ?>>
			</a></li>
		</ul></div>
	</div>
	<div id="headerBottom"></div>
	<div id="headerNav">
		<?php if($auth->isLoggedIn()) { ?>
		<nav><ul>
			<li><?= $auth->username; ?></li>
			<li class="nav-divider">&ndash;</li>
			<li><a href="<?= $root_folder.'logout?redirect='.urlencode($_SERVER['REQUEST_URI']); ?>">Log out</a></li>
		</ul></nav>
		<?php } ?>
	</div>
	<nav id="pagesDropdown"><ul>
		<li><a	href="<?= $root_folder.$dic->lang.'/admin/music'; ?>" class="nav-button" data-page="music">
			<?= $dic->pages['music']; ?>
		</a></li>
		<li><a	href="<?= $root_folder.$dic->lang.'/admin/poetry'; ?>" class="nav-button" data-page="poetry">
			<?= $dic->pages['poetry']; ?>
		</a></li>
		<li><a	href="<?= $root_folder.$dic->lang.'/admin/photo'; ?>" class="nav-button" data-page="photo">
			<?= $dic->pages['photo']; ?>
		</a></li>
		<li><a	href="<?= $root_folder.$dic->lang.'/admin/webdev'; ?>" class="nav-button" data-page="webdev">
			<?= $dic->pages['webdev']; ?>
		</a></li>
		<li><a	href="<?= $root_folder.$dic->lang.'/admin/paper_revalidatie'; ?>" class="nav-button" data-page="paper_revalidatie">
			<?= $dic->pages['paper_revalidatie']; ?>
		</a></li>
		<li><a	href="<?= $root_folder.$dic->lang.'/admin/minesweeper'; ?>" class="nav-button" data-page="minesweeper">
			<?= $dic->pages['minesweeper']; ?>
		</a></li>
		<li><a	href="<?= $root_folder.$dic->lang.'/admin/contact'; ?>" class="nav-button" data-page="contact">
			<?= $dic->pages['contact']; ?>
		</a></li>
	</ul></nav>
</header>
<!-- - - - - - - - - - - -->
<!--		CONTENT		 -->
<!-- - - - - - - - - - - -->
<div id="content">		
	<section class="current-page" data-page-data="<?= $page; ?>">
		<?php include($include_path); ?>
	</section>
</div>
<div	id="slideshow" <?php if($isEdit) echo
		'class="slideshow-show" data-index="'.$get_index.'" data-page="'.$page.'"'; ?>>
	<?php if($isEdit) include($content_path.'/'.$page.'_edit.php'); ?>
</div>

<img id="pageLoading" src="<?= $root_folder; ?>img/loading-white.gif" alt="Loading..." />
<div class="hidden" id="confMsg"><?= $dic->settings['confirm_msg']; ?></div>

<script src="<?= $root_folder; ?>jquery/jquery-3.3.1.min.js"></script>
<script src="<?= $root_folder; ?>jquery/script.js"></script>
<script src="<?= $root_folder; ?>admin/jquery/script.js"></script>

<!-- - - - - - - - - - - - - - - -->
<!--		BROWSER UPDATE		 -->
<!-- - - - - - - - - - - - - - - -->
<script> 
	var $buoop = {notify:{i:-5,f:-4,o:-4,s:-2,c:-4},insecure:true,unsupported:true,api:5}; 
	function $buo_f(){ 
		var e = document.createElement("script"); 
		e.src = "//browser-update.org/update.min.js"; 
		document.body.appendChild(e);
	};
	try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
	catch(e){window.attachEvent("onload", $buo_f)}
</script>
</body>
</html>