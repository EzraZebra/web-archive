function browseEdit($btn, opening) {
	let	page = $('html').attr('data-page'),
		iNew = parseInt($btn.attr('data-index')),
		url = iNew;
		
	if(opening) url = page+'/'+url;
	
	browse({
		$btn: $btn, $container: $('#slideshow'),
		stateData: { page_url: page+'/'+iNew, page_data: page+'_'+iNew, url: url },
		doSlide: !opening,
		eventName: 'slideshow', clickEvent: function() { browseEdit($(this), false); }
	})
}

$(document).ready(function() {	
	$('body').on('pageLoad', function(e, $wrapper) {
		//OPEN EDIT
		$wrapper.find('.edit-button').click(function(event) {
			if(!event.ctrlKey && !event.shiftKey && !event.metaKey) {
				let page = $('html').attr('data-page'),
					$btn = $(this),
					index = $btn.attr('data-index');
					
				if($('#slideshow').attr('data-page') != page || $('#slideshow').attr('data-index') != index)
					$("#slideshow").empty().load('/archive/2018_ezrazebra.net/admin/'+page+'_edit.php?edit='+index, function() {
						eventsSlideshow();
						browseEdit($btn, true);
					}).attr('data-page', page).attr('data-index', index);
				else browseEdit($btn, true);

				$('#slideshow').addClass('slideshow-show');
				enableSlideKB();

				return false;
			}
		});
	
		$('.form-confirm').submit(function() {
			if(!confirm($('#confMsg').text())) return false;
		});
	});
});