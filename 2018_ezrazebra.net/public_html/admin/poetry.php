<?php
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/auth/db_connect';
if(!isset($auth)) {
	include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/login_func.php';
	$auth = new Auth();
	$auth->sec_session_start();
}
if(!isset($dic)) {
	include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/lang_dic.php';
	$dic = new Dictionary();
}
include_once($_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/func_misc.php');

echo '<h1 class="hidden">'.$dic->pages['poetry'].'</h1>';

print_error_session();

if(!$auth->isLoggedIn()) include($_SERVER['DOCUMENT_ROOT'].'/archive/2018_ezrazebra.net/includes/login_form.php');
elseif(!$auth->isAdmin('ezrazebra')) echo '<div class="error">'.$dic->error['no_permission'].'</div>';
else {
	$path = $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/content/poetry/';
	if(isset($_POST['action'], $_FILES["poems"]) && $_POST['action'] === 'upload') {
		$uploaded = 0;
		$failed = 0;
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		echo '<div class="error">';
		foreach($_FILES["poems"]["error"] as $i => $error) {
			if($error === UPLOAD_ERR_OK) {
				// basename() may prevent filesystem traversal attacks;
				// further validation/sanitation of the filename may be appropriate
				$tmp_poem = $_FILES["poems"]["tmp_name"][$i];
				if(finfo_file($finfo, $tmp_poem) === 'text/plain') {
					$filename = basename($_FILES['poems']['name'][$i]);
					$poem = $path.$filename;
					if(file_exists($poem)) echo str_replace('%r', $filename, $dic->error['filename_taken']).'<br />';
					else {
						move_uploaded_file($tmp_poem, $poem);
						$uploaded++;
					}
				}
				else $failed++;
			}
			else $failed++;
		}
		finfo_close($finfo);
		
		if($uploaded > 0) echo str_replace('%r', $uploaded, $dic->result['upload_success']);
		if($failed > 0) echo '<br />'.str_replace('%r', $failed, $dic->error['upload_fail']);
		echo '</div>';
	}
	
	echo '
		<form class="table" enctype="multipart/form-data" action="poetry" method="POST" name="poetry_upload" autocomplete="off">
			<input type="hidden" name="action" value="upload" />
			<label class="row" for="poems">
				<span class="cell">'.$dic->settings['add'].'&colon;</span>
				<span class="cell"><input type="file" accept="text/plain" name="poems[]" id="poems" multiple required /></span>
			</label>
			<div class="row"><span class="cell"></span>
				<span class="cell"><input type="submit" value="'.$dic->settings['upload'].'" /></span>
			</div>
		</form><br />';

	if(isset($_POST['action'])) {
		if($_POST['action'] === 'save' && isset($_POST['filename'], $_POST['orig_filename'])) {
			$renamed = 0;
			echo '<div class="error">';

			foreach($_POST['orig_filename'] as $i => $orig_file) {
				$file = $_POST['filename'][$i];

				if($orig_file !== $file) {
					$orig_file = $path.basename($orig_file).'.txt';
					$filename = basename($file).'.txt';
					$file = $path.$filename;
					if(file_exists($file)) echo str_replace('%r', $filename, $dic->error['filename_taken']).'<br />';
					else {
						rename($orig_file, $file);
						$renamed++;
					}
				}
			}
			
			echo str_replace('%r', $renamed, $dic->result['files_renamed']);
			echo '</div>';
		}
		elseif($_POST['action'] === 'delete' && isset($_POST['delete'], $_POST['delete_filename'])) {
			$deleted = 0;
			echo '<div class="error">';

			foreach($_POST['delete'] as $i => $delete) {
				$file = $path.basename($_POST['delete_filename'][$i]).'.txt';
				if(file_exists($file)) {
					unlink($file);
					$deleted++;
				}
			}
			
			echo str_replace('%r', $deleted, $dic->result['files_deleted']);
			echo '</div>';
		}
	}

	echo '
			<form class="form-confirm" action="poetry" method="POST" name="poetry_save" id="poetry_save" autocomplete="off">
				<input type="hidden" name="action" value="save" />
			</form>
			<form class="form-confirm" action="poetry" method="POST" name="poetry_delete" id="poetry_delete" autocomplete="off">
				<input type="hidden" name="action" value="delete" />
			</form>
			<div>
				<input type="submit" form="poetry_save" value="'.$dic->settings['save'].'" />
				<input type="submit" form="poetry_delete" value="'.$dic->settings['delete'].'" />
			</div>';

	foreach(array_reverse(glob($path.'*.txt')) as $i => $poem) {
		$filename = basename($poem, '.txt');
		echo '
			<div class="settings-list">
				<input type="hidden" form="poetry_save" name="orig_filename[]" value="'.$filename.'" />
				<input type="hidden" form="poetry_delete" name="delete_filename[]" value="'.$filename.'" />
				<input type="checkbox" form="poetry_delete" name="delete[]" />
				<input	type="text" form="poetry_save" name="filename[]"
						pattern="[^/]+" maxlength="25" required value="'.$filename.'" />
				<a	class="linkbutton edit-button" data-index="'.($i+1).'"
					href="/archive/2018_ezrazebra.net/'.$dic->lang.'/admin/poetry/'.($i+1).'">
					'.$dic->settings['edit'].'
				</a>
			</div>';
	}

	echo '	<div>
				<input type="submit" form="poetry_save" value="'.$dic->settings['save'].'" />
				<input type="submit" form="poetry_delete" value="'.$dic->settings['delete'].'" />
			</div>';
}
?>