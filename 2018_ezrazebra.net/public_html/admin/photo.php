<?php
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/auth/db_connect';
if(!isset($auth)) {
	include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/login_func.php';
	$auth = new Auth();
	$auth->sec_session_start();
}
if(!isset($dic)) {
	include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/lang_dic.php';
	$dic = new Dictionary();
}
include_once($_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/func_misc.php');

echo '<h1 class="hidden">'.$dic->pages['photo'].'</h1>';

print_error_session();

if(!$auth->isLoggedIn()) include($_SERVER['DOCUMENT_ROOT'].'/archive/2018_ezrazebra.net/includes/login_form.php');
elseif(!$auth->isAdmin('ezrazebra')) echo '<div class="error">'.$dic->error['no_permission'].'</div>';
else {
	include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/auth/db_connect';
	$path = $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/content/photo/';
	if(	isset($_POST['action'], $_POST['foldername'], $_POST['title_en'], $_POST['title_nl'], $_FILES["photos"])
		&& $_POST['action'] === 'upload') {
		
		$foldername = basename($_POST['foldername']);
		$upload_path = $path.$foldername.'/';
		if (!is_dir($upload_path)) {
			mkdir($upload_path);
		
			if ($stmt = $mysqli_site->prepare("	INSERT INTO photo (folder, title_en, title_nl)
												VALUES (?, ?, ?)")) {
				$stmt->bind_param('sss', $foldername, $_POST['title_en'], $_POST['title_nl']);
				$stmt->execute();
				$stmt->close();
			}
			else echo $mysqli_site->error.'<br />';
			
			$uploaded = 0;
			$failed = 0;
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			echo '<div class="error">';
			foreach($_FILES["photos"]["error"] as $i => $error) {
				if($error === UPLOAD_ERR_OK) {
					// basename() may prevent filesystem traversal attacks;
					// further validation/sanitation of the filename may be appropriate
					$tmp_photos = $_FILES["photos"]["tmp_name"][$i];
					if(finfo_file($finfo, $tmp_photos) === 'image/jpeg') {
						$filename = basename($_FILES['photos']['name'][$i]);
						$photo = $upload_path.$filename;
						if(file_exists($photo)) echo str_replace('%r', $filename, $dic->error['filename_taken']).'<br />';
						else {
							move_uploaded_file($tmp_photos, $photo);
							create_thumb($photo);
							
							$uploaded++;
						}
					}
					else $failed++;
				}
				else $failed++;
			}
			finfo_close($finfo);
			
			if($uploaded > 0) echo str_replace('%r', $uploaded, $dic->result['upload_success']);
			if($failed > 0) echo '<br />'.str_replace('%r', $failed, $dic->error['upload_fail']);
		}
		else echo str_replace('%r', $foldername, $dic->error['foldername_taken']);
		echo '</div>';
	}
	
	
	echo '
		<form class="table" enctype="multipart/form-data" action="photo" method="POST" name="photo_upload" autocomplete="off">
			<input type="hidden" name="action" value="upload" />
			<label class="row" for="photos">
				<span class="cell">'.$dic->settings['add'].'&colon;</span>
				<span class="cell"><input type="file" accept="image/jpeg" name="photos[]" id="photos" multiple required /></span>
			</label>
			<label class="row" for="foldername">
				<span class="cell">'.$dic->settings['foldername'].'&colon;</span>
				<span class="cell"><input	type="text" name="foldername" id="foldername" required
											pattern="[a-zA-Z0-9_-]+" maxlength="25" placeholder="a-z, A-Z, 0-9, -, _" /></span>
			</label>
			<label class="row" for="title_en">
				<span class="cell">'.$dic->settings['albumname'].' (EN)&colon;</span>
				<span class="cell"><input type="text" name="title_en" id="title_en" maxlength="25" required /></span>
			</label>
			<label class="row" for="title_nl">
				<span class="cell">'.$dic->settings['albumname'].' (NL)&colon;</span>
				<span class="cell"><input type="text" name="title_nl" id="title_nl" maxlength="25" required /></span>
			</label>
			<div class="row"><span class="cell"></span>
				<span class="row"><input type="submit" value="'.$dic->settings['upload'].'" /></span>
			</div>
		</form><br />';

	if(isset($_POST['action'])) {
		if($_POST['action'] === 'save' && isset($_POST['foldername'], $_POST['orig_foldername'])) {
			$renamed = 0;
			echo '<div class="error">';

			foreach($_POST['orig_foldername'] as $i => $orig_folder) {
				$folder = $_POST['foldername'][$i];

				if($orig_folder !== $folder) {
					$orig_foldername = basename($orig_folder);
					$orig_folder = $path.$orig_foldername;
					$foldername = basename($folder);
					$folder = $path.$foldername;
					if(is_dir($folder)) echo str_replace('%r', $foldername, $dic->error['foldername_taken']).'<br />';
					else {
						rename($orig_folder, $folder);
						if ($stmt = $mysqli_site->prepare("	UPDATE photo
															SET folder = ?
															WHERE folder = ?")) {
							$stmt->bind_param('ss', $foldername, $orig_foldername);
							$stmt->execute();
							$stmt->close();
						}
						else echo $mysqli_site->error.'<br />';
						$renamed++;
					}
				}
			}
			
			echo str_replace('%r', $renamed, $dic->result['folders_renamed']);
			echo '</div>';
		}
		elseif($_POST['action'] === 'delete' && isset($_POST['delete'], $_POST['delete_foldername'])) {
			$deleted = 0;
			echo '<div class="error">';

			foreach($_POST['delete'] as $i => $delete) {
				$foldername = basename($_POST['delete_foldername'][$i]);
				$folder = $path.$foldername;
				if(is_dir($folder)) {
					$thumbfolder = $path.$foldername.'/thumbs';
					if(is_dir($thumbfolder)) {
						$files = array_diff(scandir($thumbfolder), array('.','..'));
						foreach ($files as $file) unlink($thumbfolder.'/'.$file);
						rmdir($thumbfolder);
					}
					
					$files = array_diff(scandir($folder), array('.','..'));
					foreach ($files as $file) unlink($folder.'/'.$file);
					rmdir($folder);
				}

				if ($stmt = $mysqli_site->prepare("	DELETE FROM photo
													WHERE folder = ?")) {
					$stmt->bind_param('s', $foldername);
					$stmt->execute();
					$stmt->close();
				}

				$deleted++;
			}
			
			echo str_replace('%r', $deleted, $dic->result['files_deleted']);
			echo '</div>';
		}
	}

	echo '		<form class="form-confirm" action="photo" method="POST" name="photo_save" id="photo_save" autocomplete="off">
					<input type="hidden" name="action" value="save" />
				</form>
				<form class="form-confirm" action="photo" method="POST" name="photo_delete" id="photo_delete" autocomplete="off">
					<input type="hidden" name="action" value="delete" />
				</form>
				<div>
					<input type="submit" form="photo_save" value="'.$dic->settings['save'].'" />
					<input type="submit" form="photo_delete" value="'.$dic->settings['delete'].'" />
				</div>';

	$albums = scandir($path,1);
	foreach($albums as $album) {
		if($album === '.' or $album === '..') continue;

		if (is_dir($path.'/'.$album)) {
			echo '
				<div class="settings-list">
					<input type="hidden" form="photo_save" name="orig_foldername[]" value="'.$album.'" />
					<input type="hidden" form="photo_delete" name="delete_foldername[]" value="'.$album.'" />
					<input type="checkbox" form="photo_delete" name="delete[]" />
					<input	type="text" form="photo_save" name="foldername[]" required value="'.$album.'"
							pattern="[a-zA-Z0-9_-]+" maxlength="25" placeholder="a-z, A-Z, 0-9, -, _" />
					<a	class="linkbutton edit-button" data-album="'.$album.'"
						href="/archive/2018_ezrazebra.net/'.$dic->lang.'/admin/photo/'.$album.'">
						'.$dic->settings['edit'].'
					</a>
				</div>';
		}
	}
	unset($album);
echo '			<div>
					<input type="submit" form="photo_save" value="'.$dic->settings['save'].'" />
					<input type="submit" form="photo_delete" value="'.$dic->settings['delete'].'" />
				</div>';
}
?>