<?php
if(!isset($dic)) {
	include_once($_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/lang_dic.php');
	$dic = new Dictionary();
}

function print_error_session() {
	global $dic;

	if(isset($_SESSION['error']) && $_SESSION['error'] != null) {
		$error = $_SESSION['error'];
		unset($_SESSION['error']);
		if(is_array($error)) {
			$replace = $error[1];
			$error = $error[0];
		}

		if(isset($dic->error[$error])) {
			$error = $dic->error[$error];
			if(isset($replace)) $error = str_replace('%r', $replace, $error);
		}
		else $error = $dic->error['unknown'];
		
		echo '<div class="error">'.$error.'</div>';
	}
}

function create_thumb($photo) {
	$thumbSize = 200;
	
	list($width, $height) = getimagesize($photo);
	if($width < $height) {
		$newWidth = $thumbSize;
		$newHeight = $thumbSize*$height/$width;
	}
	else {
		$newHeight = $thumbSize;
		$newWidth = $thumbSize*$width/$height;
	}
	
	$img = imagecreatefromjpeg($photo);
	$tmp = imagecreatetruecolor($thumbSize, $thumbSize);
	imagecopyresampled($tmp, $img, ($thumbSize-$newWidth)/2, ($thumbSize-$newHeight)/2, 0, 0, $newWidth, $newHeight, $width, $height);

	$path_parts = pathinfo($photo);
	$thumb_path = $path_parts['dirname'].'/thumbs/';
	if(!is_dir($thumb_path)) mkdir($thumb_path);
	
	imagejpeg($tmp, $thumb_path.$path_parts['basename']);

	imagedestroy($img);
	imagedestroy($tmp);
}