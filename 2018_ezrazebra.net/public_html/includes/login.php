<?php
if(!isset($auth)) {
	include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/login_func.php';
	$auth = new Auth();
}

$redirect = $_GET['redirect'];
$auth->sec_session_start($redirect);
 
if(isset($_POST['email'], $_POST['password'])) {
	$result = $auth->login($_POST['email'], $_POST['password']);

	if($result == 'success') header('Location: '.$redirect);
	else {
		// Login failed 
		$_SESSION['error'] = $result;
		header('Location: '.$redirect);
	}
} else {
    // The correct POST variables were not sent to this page.
	$_SESSION['error'] = 'login_failed';
	header('Location: '.$redirect);
}
?>