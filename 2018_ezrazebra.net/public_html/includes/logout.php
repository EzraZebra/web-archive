<?php
if(!isset($auth)) {
	include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/login_func.php';
	$auth = new Auth();
}

$redirect = $_GET['redirect'];
$auth->sec_session_start($redirect);
 
// Unset all session values 
$_SESSION = array();
 
// get session parameters 
$params = session_get_cookie_params();
 
// Delete the actual cookie. 
setcookie(session_name(),
	'', time() - 42000, 
	$params["path"], 
	$params["domain"], 
	$params["secure"], 
	$params["httponly"]);
 
// Destroy session 
session_destroy();
header('Location: '.$redirect);
?>