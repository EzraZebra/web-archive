<?php
	class GlobalDictionary {
		function __construct($setLang=false) {
			$this->init($setLang);
		}

		public function init($setLang) {
			$this->lang = $this->getLang($setLang);
			$this->misc = $this->dicMisc();
			$this->settings = $this->dicSettings();
			$this->result = $this->dicResult();
			$this->error = $this->dicError();
			$this->admin = $this->dicAdmin();
			$this->users = $this->dicUsers();
		}
		
		private function getLang($setLang) {
			if(isset($_GET['lang'])) $lang = $_GET['lang'];
			elseif(isset($_SESSION['lang'])) $lang = $_SESSION['lang'];
			elseif(isset($_COOKIE['lang'])) $lang = $_COOKIE['lang'];
			elseif(isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
			else $lang = 'en';
			
			if($lang !== 'nl') $lang = 'en';
			
			if($setLang) $this->setLang($lang);
			
			return $lang;
		}

		private function setLang($lang) {
			if((!isset($_GET['error']) || $_GET['error'] !== '404') && (!isset($_GET['lang']) || $_GET['lang'] !== $lang))		
				header('Location: /archive/2018_ezrazebra.net/'
						.$lang.str_replace('/archive/2018_ezrazebra.net', '', $_SERVER['REQUEST_URI']));

			if(!isset($_SESSION['lang']) || $_SESSION['lang'] !== $lang)
				$_SESSION['lang'] = $lang;
			
			if(!isset($_COOKIE['lang']) || $_COOKIE['lang'] !== $lang)
				setcookie('lang', $lang, time()+60*60*24*30, '/');
		}
		
		private function dicMisc() {
			$dic['en']['info'] = 'Info';
			$dic['nl']['info'] = 'Info';
			$dic['en']['playlist'] = 'Playlist';
			$dic['nl']['playlist'] = 'Afspeellijst';
			
			return $dic[$this->lang];
		}
		
		private function dicSettings() {
			$dic['en']['size'] = 'Size';
			$dic['nl']['size'] = 'Grootte';
			$dic['en']['width'] = 'Width';
			$dic['nl']['width'] = 'Breedte';
			$dic['en']['height'] = 'Height';
			$dic['nl']['height'] = 'Hoogte';
			$dic['en']['save'] = 'Save Changes';
			$dic['nl']['save'] = 'Veranderingen Opslaan';
			$dic['en']['upload'] = 'Upload';
			$dic['nl']['upload'] = 'Uploaden';
			$dic['en']['add'] = 'Add';
			$dic['nl']['add'] = 'Toevoegen';
			$dic['en']['delete'] = 'Delete Selection';
			$dic['nl']['delete'] = 'Verwijder Selectie';
			$dic['en']['edit'] = 'Edit';
			$dic['nl']['edit'] = 'Bewerken';
			$dic['en']['foldername'] = 'Folder Name';
			$dic['nl']['foldername'] = 'Map Naam';
			$dic['en']['albumname'] = 'Album Name';
			$dic['nl']['albumname'] = 'Album Naam';
			$dic['en']['confirm_msg'] = 'Are you sure?';
			$dic['nl']['confirm_msg'] = 'Bent u zeker?';
			
			return $dic[$this->lang];
		}
		
		private function dicResult() {
			$dic['en']['changes_saved'] = 'Changes saved.';
			$dic['nl']['changes_saved'] = 'Veranderingen opgeslagen.';
			$dic['en']['upload_success'] = '%r files uploaded.';
			$dic['nl']['upload_success'] = '%r bestanden geüpload.';
			$dic['en']['files_deleted'] = '%r files deleted.';
			$dic['nl']['files_deleted'] = '%r bestanden verwijderd.';
			$dic['en']['files_renamed'] = '%r files renamed.';
			$dic['nl']['files_renamed'] = '%r bestanden van naam veranderd.';
			$dic['en']['folders_deleted'] = '%r folders deleted.';
			$dic['nl']['folders_deleted'] = '%r mappen van naam verwijderd.';
			$dic['en']['folders_renamed'] = '%r folders renamed.';
			$dic['nl']['folders_renamed'] = '%r mappen van naam veranderd.';
			
			return $dic[$this->lang];
		}
		
		private function dicError() {
			$dic['en']['error'] = 'Error';
			$dic['nl']['error'] = 'Fout';
			$dic['en']['unknown'] = 'Something went wrong.';
			$dic['nl']['unknown'] = 'Er is iets fout gelopen.';
			$dic['en']['login_failed'] = 'Login failed.';
			$dic['nl']['login_failed'] = 'Inloggen mislukt.';
			$dic['en']['ini_set'] = 'Could not initiate a safe session.';
			$dic['nl']['ini_set'] = 'Kon geen veilige sessie opstarten.';
			$dic['en']['login_delay'] = 'You have to wait %r seconds before logging in.';
			$dic['nl']['login_delay'] = 'U moet %r seconden wachten voor u kan inloggen.';
			$dic['en']['no_permission'] = 'You do not have permission to view this page.';
			$dic['nl']['no_permission'] = 'U hebt geen toestemming om deze pagina te bekijken.';
			$dic['en']['upload_fail'] = '%r uploads failed.';
			$dic['nl']['upload_fail'] = '%r uploads mislukt.';
			$dic['en']['filename_taken'] = 'There is already a file with the name <i>%r</i>.';
			$dic['nl']['filename_taken'] = 'Er is al een bestand met de naam <i>%r</i>.';
			$dic['en']['foldername_taken'] = 'There is already a folder with the name <i>%r</i>.';
			$dic['nl']['foldername_taken'] = 'Er is al een map met de naam <i>%r</i>.';
			
			return $dic[$this->lang];
		}
			
		private function dicAdmin() {
			$dic['en']['panel'] = 'Admin Panel';
			$dic['nl']['panel'] = 'Admin Paneel';
			
			return $dic[$this->lang];
		}
		
		private function dicUsers() {
			$dic['en']['login'] = 'Log In';
			$dic['nl']['login'] = 'Inloggen';
			$dic['en']['logout'] = 'Log Out';
			$dic['nl']['logout'] = 'Uitloggen';
			$dic['en']['email'] = 'Email';
			$dic['nl']['email'] = 'E-mail';
			$dic['en']['password'] = 'Password';
			$dic['nl']['password'] = 'Wachtwoord';
			$dic['en']['guest'] = 'Guest';
			$dic['nl']['guest'] = 'Gast';
			
			return $dic[$this->lang];
		}
	}
?>