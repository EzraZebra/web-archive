<?php
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/auth/db_connect';
include 'lang_dic_global.php';

class Dictionary extends GlobalDictionary {
	function __construct($setLang=false) {
		$this->init($setLang);
		$this->gen = $this->dicGen();
		$this->pages = $this->dicPages();
		$this->music = $this->dicMusic();
		$this->webdev = $this->dicWebdev();
		$this->paper_revalidatie = $this->dicPaperRevalidatie();
		$this->slideshow = $this->dicSlideshow();
	}
		
	private function dicGen() {
		$dic['en']['back'] = htmlentities('<< Back');
		$dic['nl']['back'] = htmlentities('<< Terug');
		$dic['en']['back_to'] = 'Back to ';
		$dic['nl']['back_to'] = 'Terug naar ';
		$dic['en']['download'] = 'Download';
		$dic['nl']['download'] = 'Downloaden';
		$dic['en']['jsrequired'] = 'JavaScript is required to display this page.';
		$dic['nl']['jsrequired'] = 'JavaScript is vereist om deze pagina weer te geven.';
		
		return $dic[$this->lang];
	}
	
	private function dicPages() {
		$dic['en']['activate'] = 'Account Activation';
		$dic['nl']['activate'] = 'Account Activatie';
		$dic['en']['home'] = 'EzraZebra';
		$dic['nl']['home'] = 'EzraZebra';
		$dic['en']['notfound'] = 'Page Not Found';
		$dic['nl']['notfound'] = 'Pagina Niet Gevonden';
		$dic['en']['music'] = 'Music';
		$dic['nl']['music'] = 'Muziek';
		$dic['en']['poetry'] = 'Poetry';
		$dic['nl']['poetry'] = 'Poëzie';
		$dic['en']['photo'] = 'Photography';
		$dic['nl']['photo'] = 'Fotografie';
		$dic['en']['more'] = 'More';
		$dic['nl']['more'] = 'Meer';
		$dic['en']['webdev'] = 'Web Development';
		$dic['nl']['webdev'] = 'Webontwikkeling';
		$dic['en']['paper_revalidatie'] = 'Paper: Revalidatie van Verslaafden (2015)';
		$dic['nl']['paper_revalidatie'] = 'Paper: Revalidatie van Verslaafden (2015)';
		$dic['en']['minesweeper'] = 'Minesweeper (2009)';
		$dic['nl']['minesweeper'] = 'Mijnenveger (2009)';
		$dic['en']['contact'] = 'Contact';
		$dic['nl']['contact'] = 'Contact';
		
		$dic['en']['photo_2005_greece'] = 'Greece (2005)';
		$dic['nl']['photo_2005_greece'] = 'Griekenland (2005)';
		$dic['en']['photo_2007_vienna'] = 'Vienna (2007)';
		$dic['nl']['photo_2007_vienna'] = 'Wenen (2007)';
		$dic['en']['photo_2008_salisbury'] = 'Salisbury (2008)';
		$dic['nl']['photo_2008_salisbury'] = 'Salisbury (2008)';
		$dic['en']['photo_2012-2011'] = '2011–2012';
		$dic['nl']['photo_2012-2011'] = '2011–2012';
		$dic['en']['photo_2015-2012_enter-exit'] = 'ENTER/EXIT (2012–2015)';
		$dic['nl']['photo_2015-2012_enter-exit'] = 'ENTER/EXIT (2012–2015)';
		$dic['en']['photo_2017_senegal'] = 'Senegal (2017)';
		$dic['nl']['photo_2017_senegal'] = 'Senegal (2017)';
		$dic['en']['webdev_ezrazebra'] = 'EzraZebra (2018)';
		$dic['nl']['webdev_ezrazebra'] = 'EzraZebra (2018)';
		$dic['en']['webdev_schizos'] = 'Schizo\'s Playground (2014)';
		$dic['nl']['webdev_schizos'] = 'Schizo\'s Playground (2014)';
		$dic['en']['webdev_homodiaspora'] = 'Homo Diaspora (2014)';
		$dic['nl']['webdev_homodiaspora'] = 'Homo Diaspora (2014)';
		$dic['en']['webdev_cineFiles'] = 'cineFiles (2011)';
		$dic['nl']['webdev_cineFiles'] = 'cineFiles (2011)';
		$dic['en']['webdev_a2e'] = 'a2e-online (2007)';
		$dic['nl']['webdev_a2e'] = 'a2e-online (2007)';
		$dic['en']['minesweeper_screenshots'] = 'Screenshots';
		$dic['nl']['minesweeper_screenshots'] = 'Screenshots';
		
		foreach($dic['en'] as &$entry) {
			$entry = htmlentities($entry);
		}
		unset($entry);
		foreach($dic['nl'] as &$entry) {
			$entry = htmlentities($entry);
		}
		unset($entry);

		return $dic[$this->lang];
	}
	
	private function dicMusic() {
		global $mysqli_site;
		if($result = $mysqli_site->query("SELECT info_en, info_nl FROM music")) $row = $result->fetch_assoc();
		$dic['en']['info'] = $row['info_en'];
		$dic['nl']['info'] = $row['info_nl'];
		
		return $dic[$this->lang];
	}
	
	private function dicWebdev() {
		$dic['en']['visit'] = 'Visit Website';
		$dic['nl']['visit'] = 'Bezoek Website';
		$dic['en']['ezrazebra_credits'] = 'Design by Dirk Hertmans';
		$dic['nl']['ezrazebra_credits'] = 'Ontwerp door Dirk Hertmans';
		$dic['en']['ezrazebra_written_in'] = 'Written In';
		$dic['nl']['ezrazebra_written_in'] = 'Geschreven In';
		$dic['en']['ezrazebra_software'] = 'Software Used';
		$dic['nl']['ezrazebra_software'] = 'Gebruikte Software';
		$dic['en']['ezrazebra_help'] = 'Help';
		$dic['nl']['ezrazebra_help'] = 'Hulp';
		$dic['en']['schizos'] = 'Unfinished project with a login and registration system and an admin panel with which news messages, users and advertisements can be managed.';
		$dic['nl']['schizos'] = 'Onafgewerkt project met een login- en registratiesysteem en een admin panel waarmee nieuwsberichten, gebruikers en advertenties beheerd kunnen worden.';
		$dic['en']['schizos_instruct'] = 'You can log in with username and password "demo1" to test the admin panel.';
		$dic['nl']['schizos_instruct'] = 'Je kan inloggen met gebruikersnaam en paswoord "demo1" om het admin panel te testen.';
		$dic['en']['schizos_credits'] = 'Concept and design in collaboration with Dirk Hertmans.';
		$dic['nl']['schizos_credits'] = 'Concept en ontwerp in samenwerking met Dirk Hertmans.';
		$dic['en']['homodiaspora'] = 'Unfinished project in a very early stage, mapping the evolution of the Homo genus.';
		$dic['nl']['homodiaspora'] = 'Onafgewerkt project in een zeer vroeg stadium dat de evolutie van het Homo genus in kaart brengt.';
		$dic['en']['cineFiles'] = 'School assignment: webstore with shopping cart.';
		$dic['nl']['cineFiles'] = 'Schoolopdracht: webwinkel met winkelkarretje.';
		$dic['en']['cineFiles_credits'] = 'Design by Dirk Hertmans.';
		$dic['nl']['cineFiles_credits'] = 'Ontwerp door Dirk Hertmans.';
		$dic['en']['a2e'] = 'Unfinished website that features a catalogue of products fetched from a database.';
		$dic['nl']['a2e'] = 'Onafgewerkte website met een catalogus van producten opgehaald uit een database.';
		$dic['en']['a2e_credits'] = 'Contents, concept and design by Dirk Hertmans.';
		$dic['nl']['a2e_credits'] = 'Inhoud, concept en ontwerp door Dirk Hertmans.';
		
		return $dic[$this->lang];
	}

	private function dicPaperRevalidatie() {
		$dic['en']['subtitle'] = 'Het aanbod en de toegankelijkheid in Vlaanderen';
		$dic['nl']['subtitle'] = 'Het aanbod en de toegankelijkheid in Vlaanderen';
		
		return $dic[$this->lang];
	}

	private function dicSlideshow() {
		$dic['en']['fullscreen'] = 'Toggle fullscreen';
		$dic['nl']['fullscreen'] = 'Volledig scherm aan- of uitschakelen';
		$dic['en']['close'] = 'Close (Esc)';
		$dic['nl']['close'] = 'Sluiten (Esc)';
		$dic['en']['next'] = 'Next (arrow right)';
		$dic['nl']['next'] = 'Volgende (pijl naar rechts)';
		$dic['en']['previous'] = 'Previous (arrow left)';
		$dic['nl']['previous'] = 'Vorige (pijl naar links)';
		
		return $dic[$this->lang];
	}
}
?>