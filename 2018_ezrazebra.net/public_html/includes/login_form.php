<?php
	if(!isset($dic)) {
		include_once($_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/lang_dic.php');
		$dic = new Dictionary();
	}
	
	$redirect = '/archive/2018_ezrazebra.net/'.$dic->lang;
	
	if(isset($_GET['admin']) && $_GET['admin'] == 'true') $redirect .= '/admin';
	
	if(isset($page)) $redirect .= '/'.$page;
	else $redirect .= '/'.$_GET['p'];
	
	if(isset($_GET['album'])) $redirect .= '/'.$_GET['album'];
?>

<form	action="/archive/2018_ezrazebra.net/login?redirect=<?= urlencode($redirect); ?>"
		method="POST" name="login_form">
	<input	type="email" name="email"
			autocomplete="email" required
			size="25"
			placeholder="<?= $dic->users['email']; ?>" /><br />
	<input	type="password" name="password"
			size="25" required
			placeholder="<?= $dic->users['password'] ?>" /><br />
	<input type="submit" value="<?= $dic->users['login'] ?>" />
</form>