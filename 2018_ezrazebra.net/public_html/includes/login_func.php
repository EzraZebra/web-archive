<?php
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/auth/db_users_connect';
if(!isset($dic)) {
	include_once($_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/lang_dic.php');
	$dic = new Dictionary();
}

class Auth {
	function __construct() {
		global $dic;

		$this->username = $dic->users['guest'];
		$this->user_id = null;
	}

	public function sec_session_start($redirect=null) {
		// Forces sessions to only use cookies.
		if (ini_set('session.use_only_cookies', 1) === false) {
			$_SESSION['error'] = 'ini_set';
			if($redirect === null) $redirect = $_SERVER['REQUEST_URI'];
			header("Location: ".$redirect);
			exit();
		}
		
		$cookieParams = session_get_cookie_params();
		session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], true, true);
		session_name('sec_session_id');
		session_start();
		session_regenerate_id(); // regenerated the session, delete the old one. 
	}

	public function login($email, $password) {
		global $mysqli_user;

		// Using prepared statements means that SQL injection is not possible. 
		if ($stmt = $mysqli_user->prepare("	SELECT uid, username, password 
										FROM users
										WHERE email = ?
										LIMIT 1")) {
			$stmt->bind_param('s', $email);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($user_id, $username, $db_password);
			$stmt->fetch();

			if($stmt->num_rows == 1) {
				$ip = $_SERVER['REMOTE_ADDR'];
				$delay = $this->checkbrute($user_id, $ip);
				if($delay > 0) return array('login_delay', $delay);
				else {
					// We are using the password_verify function to avoid timing attacks.
					if(password_verify($password, $db_password)) {
						// XSS protection as we might print this values
						$_SESSION['user_id'] = preg_replace("/[^0-9]+/", "", $user_id);
						$this->user_id = $user_id;
						// XSS protection as we might print this value
						$_SESSION['username'] = preg_replace(	"/[^a-zA-Z0-9_\-]+/", "", $username);
						$this->username = htmlentities($username);
						$_SESSION['login_string'] = hash('sha512', $db_password.$_SERVER['HTTP_USER_AGENT']);
							
						$mysqli_user->query("	UPDATE failed_logins_uid uid, failed_logins_ip ip
											SET	uid.attempt_count = '0', ip.attempt_count = '0'
											WHERE	uid = '$user_id'
											AND		ip = '$ip'");
						return 'success';
					} else {
						$now = date("Y-m-d H:i:s", time());
						if($result = $mysqli_user->query("	SELECT * FROM failed_logins_uid
														WHERE uid = '$user_id'")) {
							if($result->num_rows != 0) {
								$result->close();
								$mysqli_user->query("	UPDATE failed_logins_uid
													SET	attempt_count = attempt_count+1, last_attempt = '$now'
													WHERE	uid = '$user_id'");
							}
							else $mysqli_user->query("	INSERT INTO failed_logins_uid(uid, attempt_count, last_attempt)
													VALUES('$user_id', '1', '$now')");
						}
						if($result = $mysqli_user->query("	SELECT * FROM failed_logins_ip
														WHERE	ip = '$ip'")) {
							if($result->num_rows != 0) {
								$result->close();
								$mysqli_user->query("	UPDATE failed_logins_ip
													SET	attempt_count = attempt_count+1, last_attempt = '$now'
													WHERE	ip = '$ip'");
							}
							else $mysqli_user->query("	INSERT INTO failed_logins_ip(ip, attempt_count, last_attempt)
													VALUES('$ip', '1', '$now')");
						}
						$mysqli_user->query("	INSERT INTO failed_logins_24h(time)
											VALUES('$now')");
					}
				}
			}
		}
		return 'login_failed';
	}

	private function checkbrute($user_id, $ip) {
		global $mysqli_user;
		$now = time();
		
		if($stmt = $mysqli_user->prepare("	SELECT uid.attempt_count, uid.last_attempt, ip.attempt_count, ip.last_attempt
										FROM failed_logins_uid uid, failed_logins_ip ip
										WHERE	uid = ?
										AND		ip = ?")) {
			$stmt->bind_param('is', $user_id, $ip);
			$stmt->execute();
			$stmt->store_result();

			$attempt_count_uid = 0;
			$attempt_count_ip = 0;
			if($stmt->num_rows != 0) {
				$stmt->bind_result($attempt_count_uid, $last_attempt_uid, $attempt_count_ip, $last_attempt_ip);
				$stmt->fetch();
				$stmt->close();
			}
			if($attempt_count_uid != 0 || $attempt_count_ip != 0) {
				$attempt_count = max($attempt_count_uid, $attempt_count_ip);
				$last_attempt = max(strtotime($last_attempt_uid), strtotime($last_attempt_ip));
				if($attempt_count == 1) return $last_attempt+5-$now;
				elseif($attempt_count == 2) return $last_attempt+15-$now;
				else return $last_attempt+45-$now;
			}
		}

		$dayAgo = date("Y-m-d H:i:s", time()-24*60*60);
		if($result = $mysqli_user->query("	SELECT *
										FROM failed_logins_24h
										WHERE time < '$dayAgo'")) {
			if($result->num_rows != 0) {
				$sum = $result->num_rows;
				$result->close();
				$mysqli_user->query("	DELETE FROM failed_logins_24h
									WHERE time < '$dayAgo'");
				$mysqli_user->query("	UPDATE failed_logins_total
									SET attempt_count=attempt_count+$sum");
			}
		}
		if($result = $mysqli_user->query("	SELECT COUNT(*), MAX(time)
										FROM failed_logins_24h")) {
			if($result->num_rows != 0) {
				$row = $result->fetch_row();
				$result->close();
				$attempt_count = $row[0];
				$last_attempt = strtotime($row[1]);
				
				if($result = $mysqli_user->query("	SELECT attempt_count, time_start
												FROM failed_logins_total")) {
					$row = $result->fetch_assoc();
					$result->close();
					if($attempt_count > $row['attempt_count']/max(round(($now-strtotime($row['time_start']))/(24*60*60)), 1)*3)
						return $last_attempt+5-$now;
				}
			}
		}
		return 0;
	}

	public function isLoggedIn() {
		global $mysqli_user, $dic;

		$this->username = $dic->users['guest'];

		if(isset($_SESSION['user_id'], $_SESSION['username'], $_SESSION['login_string'])) {
			if($stmt = $mysqli_user->prepare("	SELECT password 
											FROM users 
											WHERE uid = ?
											LIMIT 1")) {
				$stmt->bind_param('i', $_SESSION['user_id']);
				$stmt->execute();
				$stmt->store_result();

				if($stmt->num_rows == 1) {
					$stmt->bind_result($password);
					$stmt->fetch();

					if(hash_equals(hash('sha512', $password.$_SERVER['HTTP_USER_AGENT']), $_SESSION['login_string']) ){
						$this->username = htmlentities($_SESSION['username']);
						$this->user_id = $_SESSION['user_id'];
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public function isAdmin($site) {
		global $mysqli_user;
		
		if($this->isLoggedIn()) {
			if($result = $mysqli_user->query("	SELECT * FROM permissions
											WHERE uid = '$this->user_id'")) {
				$row = $result->fetch_assoc();
				$result->close();
				return $row[$site] == 'admin';
			}
		}
		return false;
	}
			
}
?>