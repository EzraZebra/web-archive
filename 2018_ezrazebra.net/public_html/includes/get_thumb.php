<?php	
	header('Content-Type: image/jpeg');
	
	$photos = glob($_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/content/'.$_GET['page'].'/'.$_GET['album'].'/*.*');
	
	if(isset($_GET['image'])) $photo = $photos[$_GET['image']-1];
	else $photo = $photos[array_rand($photos)];
	
	$thumbSize = 200;
	
	list($width, $height) = getimagesize($photo);
	if($width < $height) {
		$newWidth = $thumbSize;
		$newHeight = $thumbSize*$height/$width;
	}
	else {
		$newHeight = $thumbSize;
		$newWidth = $thumbSize*$width/$height;
	}
	
	$img = imagecreatefromjpeg($photo);
	$tmp = imagecreatetruecolor($thumbSize, $thumbSize);
	imagecopyresampled($tmp, $img, ($thumbSize-$newWidth)/2, ($thumbSize-$newHeight)/2, 0, 0, $newWidth, $newHeight, $width, $height);

	imagejpeg($tmp);

	imagedestroy($img);
	imagedestroy($tmp);
?>