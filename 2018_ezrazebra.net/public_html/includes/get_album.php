<?php
	function print_album($page, $album, $inPage='false') {
		global $dic;
		$photos = glob($_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/content/'.$page.'/'.$album.'/*.*');
		
		if($photos) {
			$photoCount = count($photos);
			if($inPage == 'true') $data_inpage = 'data-inpage="true" data-title="'.$dic->pages[$page.'_'.$album].'"';
			else $data_inpage = null;
			
			for($i=1; $i<=$photoCount; $i++) {
				echo '
					<a	class="photoThumb" href="/archive/2018_ezrazebra.net/'.$dic->lang.'/'.$page.'/'.$album.'/'.$i.'"
						'.$data_inpage.' data-album="'.$album.'" data-index="'.$i.'">
						<img src="/archive/2018_ezrazebra.net/includes/get_thumb.php?page='.$page.'&album='.$album.'&image='.$i.'" alt="'.$i.'" />
					</a>';
			}
		}
	}
?>