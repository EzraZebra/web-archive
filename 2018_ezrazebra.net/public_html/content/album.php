<?php
if(!isset($dic)) {
	include_once($_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/lang_dic.php');
	$dic = new Dictionary();
}
include_once($_SERVER['DOCUMENT_ROOT'].'/archive/2018_ezrazebra.net/includes/get_album.php');

if(!isset($page)) $page = $_GET['p'];
if(!isset($get_album)) $get_album = $_GET['album'];

echo '<h1>'.$dic->pages[$page.'_'.$get_album].'</h1>';

if($page == 'webdev') {	
	if(isset($dic->webdev[$get_album.'_credits'])) echo '
			<h2>'.$dic->webdev[$get_album.'_credits'].'</h2>';
		
	if($get_album != 'ezrazebra') {
		$projects = array(	"schizos" => "2013-2012_schizosplayground.com",
							"homodiaspora" => "2013_homo-diaspora",
							"cineFiles" => "2010_cineFiles.com",
							"a2e" => "2007_a2e-online.com"	);
					
		echo	
			$dic->webdev[$get_album].'<br /><br />
			<a class="ui-element" href="/archive/'.$projects[$get_album].'" target="_blank">'.$dic->webdev['visit'].'</a>';

		if(isset($dic->webdev[$get_album.'_instruct'])) echo '
			<div id="webdevInstruct">'.$dic->webdev[$get_album.'_instruct'].'</div>';
	}
	else {
		echo '
			<style>
				.aboutIcons a {
					margin: 0.5em; margin-top: 0;
					display: inline-block;
					font-size: 0.9em;
				}
				.aboutIcons img {
					width: 48px; height: 48px;
				}
			</style>';

		print_album('webdev', 'ezrazebra', 'false');

		echo '
			<h2>'.$dic->webdev['ezrazebra_written_in'].'</h2>
			<div class="aboutIcons">
				<a class="small-ext" href="//www.w3.org/html/" target="_blank">
					<img src="/archive/2018_ezrazebra.net/content/webdev/ezrazebra/icons/html5.png" alt="HTML5" /><br />
					HTML5
				</a>
				<a class="small-ext" href="///www.w3.org/Style/CSS/" target="_blank">
					<img src="/archive/2018_ezrazebra.net/content/webdev/ezrazebra/icons/css3.png" alt="CSS3" /><br />
					CSS3
				</a>
				<a class="small-ext" href="//jquery.com" target="_blank">
					<img src="/archive/2018_ezrazebra.net/content/webdev/ezrazebra/icons/jquery.png" alt="jQuery" /><br />
					jQuery
				</a>
				<a class="small-ext" href="//php.net" target="_blank">
					<img src="/archive/2018_ezrazebra.net/content/webdev/ezrazebra/icons/php.png" alt="PHP" /><br />
					PHP
				</a>
			</div>

			<h2>'.$dic->webdev['ezrazebra_software'].'</h2>
			<div class="aboutIcons">
				<a class="small-ext" href="//notepad-plus-plus.org" target="_blank">
					<img src="/archive/2018_ezrazebra.net/content/webdev/ezrazebra/icons/npp.png" alt="Notepad++" /><br />
					Notepad&plus;&plus;
				</a>
				<a class="small-ext" href="//www.gimp.org" target="_blank">
					<img src="/archive/2018_ezrazebra.net/content/webdev/ezrazebra/icons/wilber.png" alt="GIMP" /><br />
					GIMP
				</a>
				<a class="small-ext" href="//filezilla-project.org" target="_blank">
					<img src="/archive/2018_ezrazebra.net/content/webdev/ezrazebra/icons/filezilla.png" alt="FileZilla" /><br />
					FileZilla
				</a>
				<a class="small-ext" href="//loading.io" target="_blank">
					<img src="/archive/2018_ezrazebra.net/content/webdev/ezrazebra/icons/loadingio.png" alt="loading.io" /><br />
					loading.io
				</a>
			</div>

			<h2>'.$dic->webdev['ezrazebra_help'].'</h2>
			<div class="aboutIcons">
				<a class="small-ext" href="//duckduckgo.com" target="_blank">
					<img src="/archive/2018_ezrazebra.net/content/webdev/ezrazebra/icons/duckduckgo.png" alt="DuckDuckGo" /><br />
					DuckDuckGo
				</a>
				<a class="small-ext" href="//stackoverflow.com" target="_blank">
					<img src="/archive/2018_ezrazebra.net/content/webdev/ezrazebra/icons/stackoverflow.png" alt="Stack Overflow" /><br />
					Stack Overflow
				</a>
				<a class="small-ext" href="//www.w3.org/developers/tools/" target="_blank">
					<img src="/archive/2018_ezrazebra.net/content/webdev/ezrazebra/icons/w3.png" alt="W3C" /><br />
					W3C
				</a>
			</div>';
	}
}
else print_album($page, $get_album);
?>