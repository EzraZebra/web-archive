<?php
if(!isset($dic)) {
	include_once($_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/lang_dic.php');
	$dic = new Dictionary();
}
?>
<style>
	#webdevInstruct {
		font-size: 0.9em; font-style: italic;
	}
</style>

<?php
$projects = array(	"ezrazebra",
					"schizos",
					"homodiaspora",
					"cineFiles",
					"a2e"	);

echo '
	<h1>'.$dic->pages['webdev'].'</h1>
	<ul class="link-list no-icon">';

foreach($projects as $project) echo '
		<li><a	class="nav-button" href="/archive/2018_ezrazebra.net/'.$dic->lang.'/webdev/'.$project.'"
				data-page="webdev" data-album="'.$project.'" data-title="'.$dic->pages['webdev_'.$project].'">
			'.$dic->pages['webdev_'.$project].'
		</a></li>';
	
echo '	
	</ul>';
?>