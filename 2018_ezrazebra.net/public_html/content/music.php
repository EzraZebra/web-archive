<?php
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/auth/db_connect';
if(!isset($dic)) {
	include_once($_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/lang_dic.php');
	$dic = new Dictionary();
}
if($result = $mysqli_site->query("SELECT width, height, playlist FROM music")) $row = $result->fetch_assoc();
?>

<style>
	iframe {
		width: <?= $row['width']; ?>px; max-width: 85vw;
	}
</style>

<h1 class="hidden"><?= $dic->pages['music']; ?></h1>

<div>
	<span class="infoSpan jsonly"><?= $dic->music['info']; ?></span><br />
	<noscript><span class="infoSpan"><?= $dic->gen['jsrequired']; ?></span><br /></noscript>
	<iframe height="<?= $row['height']; ?>" frameborder="no"
			src="https://w.soundcloud.com/player/?url=<?= $row['playlist']; ?>&color=ff5500&auto_play=false&hide_related=true&show_comments=true&show_user=false&show_reposts=true&show_playcount=true"></iframe>
</div>