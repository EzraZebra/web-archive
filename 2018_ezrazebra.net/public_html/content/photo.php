<?php
if(!isset($dic)) {
	include_once($_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/lang_dic.php');
	$dic = new Dictionary();
}

echo '<h1 class="hidden">'.$dic->pages['photo'].'</h1>';

$path = $_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/content/photo';
$albums = scandir($path,1);
foreach($albums as $album) {
	if($album === '.' or $album === '..') continue;

	if (is_dir($path.'/'.$album)) {
		echo '
			<a	class="titleThumb nav-button" href="/archive/2018_ezrazebra.net/'.$dic->lang.'/photo/'.$album.'"
				data-page="photo" data-album="'.$album.'" data-title="'.$dic->pages['photo_'.$album].'">
				<img src="/archive/2018_ezrazebra.net/includes/get_thumb.php?page=photo&album='.$album.'" alt="'.$dic->pages['photo_'.$album].'" />
				<span class="thumbTitle">'.$dic->pages['photo_'.$album].'</span>
			</a>';
	}
}
unset($album);
?>