<?php
	if(!isset($dic)) {
		include_once($_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/lang_dic.php');
		$dic = new Dictionary();
	}
?>

<h1 class="hidden"><?= $dic->pages['more']; ?></h1>

<ul class="link-list no-icon">
	<li><a	href="webdev"
			class="nav-button" data-page="webdev" data-title="<?= $dic->pages['webdev']; ?>">
			<?= $dic->pages['webdev']; ?>
		</a>
	</li>
	<li><a	href="paper_revalidatie"
			class="nav-button" data-page="paper_revalidatie" data-title="<?= $dic->pages['paper_revalidatie']; ?>">
			<?= $dic->pages['paper_revalidatie']; ?>
		</a>
	</li>
	<li><a	href="minesweeper"
			class="nav-button" data-page="minesweeper" data-title="<?= $dic->pages['minesweeper']; ?>">
			<?= $dic->pages['minesweeper']; ?>
		</a>
	</li>
	<li><a	href="contact"
			class="nav-button" data-page="contact" data-title="<?= $dic->pages['contact']; ?>">
			<?= $dic->pages['contact']; ?>
		</a>
	</li>
</ul>