12/05/2013

Where to go
Who to be
What to say
How to feel

Same answer
Anything goes
Nothing comes
Fading and taunting

Mocking and pushing
me to give up
Pushing me to carry on

Shows me the answers
To life and being
Takes them away
And burns them up

Ashes in my hands
Hold the seeds of hope
Slip through bony fingers
Fingers bleeding and
keeping

Seeping and sleeping

Words and thoughts
Fiction and friction
Fucking around and
shutting down

Hiding, abiding
Sliding and fighting
Fighting for existence
Cursing persistence