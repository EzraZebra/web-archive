<?php
	if(!isset($dic)) {
		include_once($_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/lang_dic.php');
		$dic = new Dictionary();
	}
?>

<h1 class="hidden"><?= $dic->pages['poetry']; ?></h1>

<?php
	$href = null;
	$data_browsed = 'true';
	if(!isset($_GET['poem'])) {
		$get_poem = 1;
		$href = 'poetry/';
		$data_browsed = 'false';
	}
	else $get_poem = $_GET['poem'];
	
	$poems = array_reverse(glob($_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/content/poetry/*.txt'));
	$poemCount = count($poems);
	
	if($get_poem == 1) $iPrevious = $poemCount;
	else $iPrevious = $get_poem-1;
	if($get_poem == $poemCount) $iNext = 1;
	else $iNext = $get_poem+1;

	for($i=1; $i<=$poemCount; $i++) {
		$poem = preg_replace('/\t+/', '&emsp;&emsp;', nl2br(htmlentities(file_get_contents($poems[$i-1]))));

		if($i == $get_poem) $class = ' slide-current';
		else $class = null;

		echo '
			<div class="poem-wrapper slide-wrapper'.$class.'" data-index="'.$i.'">
				<div>'.$poem.'</div>
			</div>';
	}
?>
			<div id="poemToolbar" data-poem-browsed="<?= $data_browsed; ?>">
				<a	class="slide-prev-btn" href="<?= $href.$iPrevious; ?>" title="<?= $dic->slideshow['previous']; ?>"
					data-index="<?= $iPrevious; ?>"></a>
				<?= '<span class="slide-index">'.$get_poem.'</span> &sol; '.$poemCount; ?>
				<a	class="slide-next-btn" href="<?= $href.$iNext; ?>" title="<?= $dic->slideshow['next']; ?>"
					data-index="<?= $iNext; ?>"></a>
			</div>