<?php
	if(!isset($dic)) {
		include_once($_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/lang_dic.php');
		$dic = new Dictionary();
	}
?>

<h1><?= $dic->pages['paper_revalidatie']; ?></h1>
<h2><?= $dic->paper_revalidatie['subtitle']; ?></h2>
<a class="pdfThumb" href="/archive/2018_ezrazebra.net/content/paper_revalidatie/Paper_Revalidatie.pdf" target="_blank">
	<img src="/archive/2018_ezrazebra.net/content/paper_revalidatie/paper_thumb.jpg" alt="Paper Revalidatie" />
</a>