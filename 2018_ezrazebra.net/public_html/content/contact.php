<?php
	if(!isset($dic)) {
		include_once($_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/lang_dic.php');
		$dic = new Dictionary();
	}
?>

<style>
	#linkMail { background-image: url('/archive/2018_ezrazebra.net/content/contact/protonmail.png'); }
	#linkFacebook { background-image: url('/archive/2018_ezrazebra.net/content/contact/facebook.ico'); }
	#linkSoundcloud { background-image: url('/archive/2018_ezrazebra.net/content/contact/soundcloud.ico'); }
	#linkDiscogs { background-image: url('/archive/2018_ezrazebra.net/content/contact/discogs.png'); }
</style>

<h1><?= $dic->pages['contact']; ?></h1>

<ul class="link-list">
	<li><a target="_blank" id="linkMail" href="mailto:***">***</a></li>
	<li><a target="_blank" id="linkFacebook" href="#">***</a></li>
	<li><a target="_blank" id="linkSoundcloud" href="https://soundcloud.com/ezrazebra">EzraZebra</a></li>
	<li><a target="_blank" id="linkDiscogs" href="#">***</a></li>
</ul>