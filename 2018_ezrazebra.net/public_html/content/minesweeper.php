<?php
	if(!isset($dic)) {
		include_once($_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/lang_dic.php');
		$dic = new Dictionary();
	}

	include_once($_SERVER['DOCUMENT_ROOT'].'/archive/2018_ezrazebra.net/includes/get_album.php');
?>


<h1><?= $dic->pages['minesweeper']; ?></h1>

<a class="download" href="/archive/2018_ezrazebra.net/content/minesweeper/Minesweeper-win-x64.zip"><?= $dic->gen['download']; ?> &lpar;Windows&rpar;</a>
<br /><br />

<?php print_album('minesweeper', 'screenshots', 'true'); ?>