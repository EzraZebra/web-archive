<?php
	if(!isset($dic)) {
		include_once($_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/includes/lang_dic.php');
		$dic = new Dictionary();
	}

	if(!isset($page)) $page = $_GET['p'];
	if(!isset($get_album)) $get_album = $_GET['album'];
	if(!isset($get_image)) {
		if(isset($_GET['image'])) $get_image = $_GET['image'];
		else $get_image = null;
	}

	$photos = glob($_SERVER["DOCUMENT_ROOT"].'/archive/2018_ezrazebra.net/content/'.$page.'/'.$get_album.'/*.*');
	$photoCount = count($photos);

	for($i=1; $i<=$photoCount; $i++) {
		if($get_image != null && $i == $get_image) $class = ' slide-current';
		else $class = null;
	
		echo '
			<div class="slideshowImg slide-wrapper'.$class.'" data-index="'.$i.'">
				<img src="'.str_replace($_SERVER["DOCUMENT_ROOT"], '', $photos[$i-1]).'" alt="'.$i.'">
			</div>';
	}

	if($get_image != null) {
		if($get_image == $photoCount) $next = 1;
		else $next = $get_image+1;
		if($get_image == 1) $previous = $photoCount;
		else $previous = $get_image-1;
	}
	else {
		$next = null;
		$previous = null;
	}
	
	if(isset($_GET['inpage'])) $inPage = $_GET['inpage'];
	$closeHref = '/archive/2018_ezrazebra.net/'.$dic->lang.'/'.$page;
	$data_title = null;
	if(!isset($inPage) || $inPage != 'true') $closeHref .= '/'.$get_album;
	elseif($inPage == 'true') $data_title = 'data-title="'.$dic->pages[$page].'"';
	
	if($photoCount > 1) echo '
			<a	class="slide-prev-btn slideshow-button" data-album="'.$get_album.'" data-index="'.$previous.'"
				href="'.$previous.'" title="'.$dic->slideshow['previous'].'">
			</a>
			<a	class="slide-next-btn slideshow-button" data-album="'.$get_album.'" data-index="'.$next.'"
				href="'.$next.'" title="'.$dic->slideshow['next'].'">
			</a>';
?>
			<div id="slideshowTitle">
				<?= $dic->pages[$page.'_'.$get_album]; ?>
				<span> &ndash; <span class="slide-index"><?= $get_image; ?></span> &sol; <?= $photoCount; ?></span>
			</div>
			<div id="slideshowToolbar">
				<a	id="slideCloseBtn" class="slideshow-button" data-album="<?= $get_album; ?>" <?= $data_title; ?>
					href="<?= $closeHref; ?>" title="<?= $dic->slideshow['close']; ?>">
				</a>
				<button	id="slideFullscrBtn" class="slideshow-button enter-fullscreen jsonly"
						title="<?= $dic->slideshow['fullscreen']; ?>">
				</button>
			</div>