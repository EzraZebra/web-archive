-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 03, 2019 at 04:30 PM
-- Server version: 10.3.18-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ezrazebra`
--

-- --------------------------------------------------------

--
-- Table structure for table `music`
--

CREATE TABLE `music` (
  `info_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `info_nl` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `width` int(3) NOT NULL,
  `height` int(3) NOT NULL,
  `playlist` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `music`
--

INSERT INTO `music` (`info_en`, `info_nl`, `width`, `height`, `playlist`) VALUES
('Playback will continue while you browse the site.', 'Het afspelen zal verdergaan terwijl je door de site bladert.', 600, 410, 'https://soundcloud.com/ezrazebra/sets/ezrazebra');

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE `photo` (
  `folder` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_en` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_nl` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photo`
--

INSERT INTO `photo` (`folder`, `title_en`, `title_nl`) VALUES
('2015-2012_enter-exit', 'ENTER/EXIT (2012-2015)', 'ENTER/EXIT (2012-2015)'),
('2012-2011', '2011 - 2012', '2011 - 2012'),
('2008_salisbury', 'Salisbury (2008)', 'Salisbury (2008)'),
('2005_greece', 'Greece (2005)', 'Griekenland (2005)'),
('2007_vienna', 'Vienna (2007)', 'Wenen (2007)'),
('2017_senegal', 'Senegal (2017)', 'Senegal (2017)');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`folder`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
