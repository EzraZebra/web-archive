<?php
/*************************************************************/
/******		HOMO DIASPORA - MAP OF HUMAN EVOLUTION		******/
/******				BY EZRA HERTMANS					******/
/*************************************************************/
define('INCLUDE_PATH', $_SERVER["DOCUMENT_ROOT"].'/archive/2013_homo-diaspora/');

//INCLUDES
include_once str_replace('/public_html', '', $_SERVER["DOCUMENT_ROOT"]).'/auth/db_connect';
include INCLUDE_PATH.'includes/Populations.php';
include INCLUDE_PATH.'includes/Map.php';
include INCLUDE_PATH.'includes/PopulationDetails.php';

//INITIALIZE
	//get age
	if(isset($_GET['age'])) $getAge = $_GET['age'];
		else $getAge = null;

	//initialize populations
	$populations = new Populations($getAge);
	$map = new Map($populations);
?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Homo Diaspora</title>
	<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="container">

	<!--WRAPPER (WIDTH/HEIGHT)-->
	<div	id="wrapper"
			style="<?php echo '		width: '.$map->width.'px;
									height: '.$map->height.'px;'; ?>">

		<!--MAP (WIDTH/HEIGHT/SOURCE)-->
		<img	id="map"
				alt="map"
				style="<?php echo '	width: '.$map->width.'px;
									height: '.$map->height.'px;'; ?>"
				src="<?php echo '	map.php&#63;x='.$map->x.'&amp;y='.$map->y.'&amp;width='.$map->width.'&amp;height='.$map->height; ?>" />

		<!--LEGEND (MARGIN/WIDTH)-->
		<div	id="legend"
				style="<?php echo '	margin-top: '.$map->height.'px;
									width: '.($map->width-10).'px;'; ?>">

			<!--LEGEND CONTENT-->
				<!--LEGEND TITLE-->
				<div id="legendtitle">
					<a href="?age=all">Show All</a>
					<br />

					<a href="&#63;age=origins" title="Early human evolution in Africa (2.5 Ma &mdash; 1.9 Ma)">Origins</a>
					<a href="&#63;age=origooaf" title="Intermediate" class="minorinterm">&#8596;</a>
					<a href="&#63;age=outofafrica" title="Early human migration out of Africa (1.9 Ma &mdash; 400k)">Out of Africa</a>
					<a href="&#63;age=ooafmodr" title="Intermediate" class="minorinterm">&#8596;</a>
					<a href="&#63;age=modernity" title="Evolution of sapiens, neanderthalensis, etc. (400k &mdash; 70k)">Modernity</a>
					<br />

					<a href="&#63;age=modrsooa" title="Intermediate">&#8597;&#8597;</a>
					<br />

					<a href="&#63;age=sapiensoutofafrica" title="Sapiens' migration out of Africa (70k &mdash; 15k)">Sapiens Out of Africa</a>
					<a href="&#63;age=sooanwld" title="Intermediate" class="minorinterm">&#8596;</a>
					<a href="&#63;age=newworld" title="Sapiens populates the New World (15k &mdash; present)">The New World</a>
					<br />

					<?php echo $populations->getAgeString(); ?>
				</div>

				<?php
					//// GET POPULATION DATA
					for($i=1; $populations->getNext(); $i++) {
						//initialize population details
						$populationDetails = new PopulationDetails($populations);
				?>

					<!-- LEGEND MARKER -->
					<div	class="marker"
							style="<?php echo '	top: '.$populations->current['position']['top'].'px;
												left: '.$populations->current['position']['left'].'px;'; ?>">

						<!-- LEGEND MARKER DOT/TITLE -->
						<a href="<?php echo '#'.$i; ?>">
							<!-- DOT -->
							<span	class="markerDot"
									style="<?php //echo 'background: url(\'images/'.$populations->current['marker'].'\') no-repeat;'; 
									echo 'background-color: #'.$populations->current['color'].';'.$populations->getBorder(); ?>">
								<?php echo $i; ?>
							</span>
							<!-- TITLE -->
							<span class="markerTitle markercontent">
								<?php echo '<b>'.$populations->current['species_view'].'</b> ('.$populations->current['age'].')';?>
							</span>
						</a>

						<!-- LEGEND MARKER CONTENT -->
						<div class="markercontent"><?php
							$marker = $populationDetails->formatMarker();

							echo	 $marker['cc']
									.$marker['tools']
									.$marker['culture']
									.$marker['life']
									.$marker['ancestry']
									.$marker['kinship']
									.$marker['progeny'];
						
							//unset marker
							unset($marker);
						?></div>
						<br />

						<!-- LEGEND MARKER IMAGES -->
						<?php
							//FOR ALL IMAGES
							if(isset($populationDetails->details['images']))
							for($j=0; $j<sizeof($populationDetails->details['images']); $j++) for($k=0; $k<sizeof($populationDetails->details['images'][$j]); $k++) {
						?>

						<a class="markercontent" href="<?php echo 'images/fossils/'.$populationDetails->details['images'][$j][$k]; ?>" target="_blank">
							<img src="<?php echo 'images/fossils/'.$populationDetails->details['images'][$j][$k]; ?>" alt="image" />
						</a>

						<?php
							//END FOR ALL IMAGES
							}
						?>
					</div>
					<!-- END LEGEND MARKER -->

					<!-- OUTPUT GENE FLOW GRAPHICS -->
					<?php $populationDetails->getGeneFlow(); ?>

					<!-- LEGEND ITEM -->
					<div class="legenditem">
						<!-- LEGEND ITEM INDEX -->
						<span class="legendindex">
							<?php echo $i; ?>
						</span>

						<!-- LEGEND ITEM TITLE -->
						<?php
							echo '<b>'.$populations->current['species_view'].'</b>, '.$populations->current['location'].', '.$populations->current['age'].'<br />';
						?>

						<!-- LEGEND ITEM DETAILS -->
						<div class="detail"><ol>
							<?php
								// GET DETAIL INFO
								while($populationDetails->getNext()) {
							?>

							<!-- LEGEND ITEM DETAILS ITEM -->
							<li>
								<?php
									$detail = $populationDetails->formatLegend();

									echo	$detail['age']
											.$detail['location']
											.$detail['name']
											.$detail['cc']
											.$detail['description']
											.$detail['tools']
											.$detail['culture']
											.$detail['life']
											.$detail['ancestry']
											.$detail['kinship']
											.$detail['progeny'];
								?>

								<!-- LEGEND ITEM DETAILS ITEM CITATIONS -->
								<div class="bibl">
									<?php echo $populationDetails->current['citations']; ?>
								</div>
							</li>
							<!-- END LEGEND ITEM DETAILS ITEM -->

							<?php
								// END GET DETAIL INFO
								}

								//unset details
								unset($detail);
							?>
						</ol></div>
						<!-- END LEGEND ITEM DETAILS -->

						<?php
							//// OUTPUT IMAGES
							if(isset($populationDetails->details['images'])) {
						?>

						<!-- LEGEND ITEM IMAGES -->
						<div class="imgwrapper">
							<!-- LEGEND ITEM IMAGES TOOLS -->
							<span class="tools" id="<?php echo $i; ?>"><a href="#map">Back To Map</a> &#124; <span class="showimg">Show Images</span></span>
							<!-- LEGEND ITEM IMAGES IMAGE -->
							<span class="img">
								<?php
									// FOR EACH IMAGEGROUP
									for($j=0; $j<sizeof($populationDetails->details['images']); $j++) {
								?>
								<span>
									<?php 
										echo $populationDetails->details['imageIndexes'][$j];
										
										// FOR EACH IMAGE
										for($k=0; $k<sizeof($populationDetails->details['images'][$j]); $k++) {
									?>
									<a target="_blank" href="<?php echo 'images/fossils/'.$populationDetails->details['images'][$j][$k]; ?>"><img alt="image" src="<?php echo 'images/fossils/'.$populationDetails->details['images'][$j][$k]; ?>" /></a>
									<?php
										// END FOR EACH IMAGE
										}
									?>
								</span>
								<?php
									// END FOR EACH IMAGEGROUP
									}

									unset($images);
								?>
							</span>
							<!-- END LEGEND ITEM IMAGES IMAGE -->
						</div>
						<!-- END LEGEND ITEM IMAGES -->

						<?php
							//// END OUTPUT IMAGES
							}
							//// NO IMAGE
							else {
						?>

						<!-- LEGEND ITEM NO IMAGE -->
						<span class="imgwrapper noImg" id="<?php echo $i; ?>"><a href="#map">Back To Map</a></span>
						
						<?php
							//// END NO IMAGE
							}
						?>

					</div>
					<!-- END LEGEND ITEM -->
				
				<?php
					//// END GET POPULATION DATA
					}
				?>
			<!--END LEGEND CONTENT-->
		</div>
		<!-- END LEGEND -->
	</div>
	<!-- END WRAPPER -->
</div>
<div id="spacer"></div>
</body>
</html>