<?php
/*************************************************************/
/******		HOMO DIASPORA - MAP OF HUMAN EVOLUTION		******/
/******				BY EZRA HERTMANS					******/
/*************************************************************/

class Populations
{
	//// CONSTRUCTOR
	function __construct($age) {
		//PREPARE VARS
		$this->populations = null;
		$this->size = 0;
		$this->index = 0;
		$this->setAgeRange($age);

		//INITIALIZE
		$this->populate();
	}
	
	//// SET AGE RANGE
	private function setAgeRange($age) {
		switch($age) {
			// Origins
			case 'origins': $this->age_max = 2500; $this->age_min = 1900;
				break;
			// Intermediate
			case 'origooaf': $this->age_max = 2200; $this->age_min = 1600;
				break;
			// Out of Africa
			case 'outofafrica': $this->age_max = 1900; $this->age_min = 400;
				break;
			// Intermediate
			case 'ooafmodr': $this->age_max = 565; $this->age_min = 235;
				break;
			// Modernity
			case 'modernity': $this->age_max = 400; $this->age_min = 70;
				break;
			// Intermediate
			case 'modrsooa': $this->age_max = 97.5; $this->age_min = 42.5;
				break;
			// Sapiens Out of Africa
			case 'sapiensoutofafrica': $this->age_max = 70; $this->age_min = 15;
				break;
			// Intermediate
			case 'sooanwld': $this->age_max = 22.5; $this->age_min = 7.5;
				break;
			// The New World
			case 'newworld': $this->age_max = 15; $this->age_min = 0;
				break;
			// ALL
			case 'all': default: $this->age_max = 2500; $this->age_min = 0;
				break;
		}
	}

	//// SET POPULATION DATA
	private function populate() {
		global $mysqli_site;

		//// GET POPULATION DATA
		$populationResult = mysqli_query($mysqli_site,
										"	SELECT	f.spec_sub_trans_id species_id, f.admixture_id,
													sp.species, sp_sub_tr.subspecies, sp_sub_tr.trans_species_id,
													sp_sub_tr.color,
													MAX(f.age_max) age_max, MIN(f.age_min) age_min, MIN(f.cc_min) cc_min, MAX(f.cc_max) cc_max,
													l.x, l.y, l.location_name, f.location_id
											FROM fossils f, locations l, species sp, spec_sub_trans sp_sub_tr
											WHERE	((f.age_max >= '$this->age_max' AND f.age_min <= '$this->age_max') OR
													(f.age_max >= '$this->age_min' AND f.age_min <= '$this->age_min') OR
													(f.age_max <= '$this->age_max' AND f.age_min >= '$this->age_min')) AND
													f.location_id=l.loc_id AND
													sp_sub_tr.sst_id=f.spec_sub_trans_id AND
													sp.sp_id=sp_sub_tr.species_id
											GROUP BY f.location_id, f.spec_sub_trans_id
											ORDER BY MAX(f.age_max) DESC, MIN(f.age_min) DESC")
									or die(mysqli_error($mysqli_site));

		//// SET POPULATION DATA
			$this->size = mysqli_num_rows($populationResult);
			$this->gene_flow = array();

			for($i=0; $populationRow = mysqli_fetch_assoc($populationResult); $i++) {
				$this->populations[$i] = array	(	//location info
													'location_id' =>		$populationRow['location_id'],
													'location' => 			$populationRow['location_name'],
													'position' =>			array(	'top' =>	( $populationRow['y'] - 9 ),
																					'left' =>	( $populationRow['x'] - 9 )	),
													//species info
													'color' =>				$populationRow['color'],
													'admixture' =>			$populationRow['admixture_id'],
													'species_id' =>			$populationRow['species_id'],
													'species' =>			$populationRow['species'],
													'subspecies' =>			$populationRow['subspecies'],
													'species_view' =>		ucfirst($populationRow['species']),
													//age info
													'age_max' =>			$populationRow['age_max'],
													'age_min' =>			$populationRow['age_min'],
													'age' =>				$this->getAgeString($populationRow['age_max'], $populationRow['age_min']),
													//other info
													'cc' =>					$this->getCCString($populationRow['cc_min'], $populationRow['cc_max'])
												);

				//COMPLETE SPECIES_VIEW
				if($populationRow['subspecies'] != null)	$this->populations[$i]['species_view'] .= ' '.ucfirst($this->populations[$i]['subspecies']);				
				if($populationRow['trans_species_id'] != null) {
					$trans_species_id = $populationRow['trans_species_id'];
					$transResult = mysqli_query($mysqli_site,
												"	SELECT sp.species FROM trans_species tr_sp, species sp, spec_sub_trans sp_sub_tr
													WHERE	tr_sp.tr_sp_id='$trans_species_id' AND
															sp_sub_tr.sst_id=tr_sp.species_id AND
															sp.sp_id=sp_sub_tr.species_id")
										or die(mysqli_error($mysqli_site));
										
					$transRow = mysqli_fetch_assoc($transResult);
					if($transRow['species'] != null) $this->populations[$i]['species_view'] .= '/'.ucfirst($transRow['species']);
				}

				// MOVE POSITION IF TAKEN
				if(isset( $positionUsed[ $this->populations[$i]['location_id'] ] )) {
					switch($positionUsed[ $this->populations[$i]['location_id'] ]) {
						case 1: //bottom left
							$this->populations[$i]['position']['left'] -= 10;
							$this->populations[$i]['position']['top'] += 10;
							break;
						case 2: //bottom right
							$this->populations[$i]['position']['left'] += 10;
							$this->populations[$i]['position']['top'] += 10;
							break;
						case 3: //top left
							$this->populations[$i]['position']['left'] -= 10;
							$this->populations[$i]['position']['top'] -= 10;
							break;
						case 4: //top right
							$this->populations[$i]['position']['left'] += 10;
							$this->populations[$i]['position']['top'] -= 10;
							break;
						case 5: //left
							$this->populations[$i]['position']['left'] -= 17;
							break;
						case 6: //right
							$this->populations[$i]['position']['left'] += 17;
							break;
						case 7: //top
							$this->populations[$i]['position']['top'] -= 17;
							break;
						case 8: //bottom
							$this->populations[$i]['position']['top'] += 17;
							break;
					}

					//count number of times used
					$positionUsed[ $this->populations[$i]['location_id'] ]++;
				}
				// SET POSITION TAKEN
				else $positionUsed[ $this->populations[$i]['location_id'] ] = 1;
			}
		//// END SET POPULATION DATA

		mysqli_free_result($populationResult);
	}

	//GET CURRENT POPULATION INCREMENTAL
	public function getNext() {
		//current population exists
		if($this->index >= 0 && $this->index < $this->size) {
			//SET POPULATION
			$this->current = $this->populations[$this->index];

			//next population
			$this->index++;
			
			return true;
		}
		//current population does not exist
		else {
			$this->index = 0;
			return false;
		}
		
	}

	//// GET MARKER BORDER
	public function getBorder() {
		global $mysqli_site;

		$border = null; //preset border

		//set query vars
			$qry_location_id = $this->current['location_id'];
			$qry_species_id = $this->current['species_id'];

		//GET ADMIXTURE FOR CURRENT POPULATION
		$populationResult = mysqli_query($mysqli_site,
										"	SELECT	f.admixture_id
											FROM fossils f
											WHERE	((f.age_max >= '$this->age_max' AND f.age_min <= '$this->age_max') OR
													(f.age_max >= '$this->age_min' AND f.age_min <= '$this->age_min') OR
													(f.age_max <= '$this->age_max' AND f.age_min >= '$this->age_min')) AND
													f.location_id='$qry_location_id' AND
													f.spec_sub_trans_id='$qry_species_id'
											ORDER BY age_min ASC, age_max ASC
											LIMIT 1")
									or die(mysqli_error($mysqli_site));
		//ADMIXTURE FOUND -> CONTINUE
		if($populationRow = mysqli_fetch_assoc($populationResult))  {
			//set vars
			$admixture_id = $populationRow['admixture_id'];
			$continue = true;
			
			$i=0;
			while($continue && $i<4) {
				//GET ADMIXTURE INFO
				$admixResult = mysqli_query($mysqli_site,
											"	SELECT	color, adm_2_id FROM spec_sub_trans sp_sub_tr, admixture adm, fossils fos
												WHERE	adm_id='$admixture_id' AND
														fos_id=fossil_id AND
														sp_sub_tr.sst_id=fos.spec_sub_trans_id")
									or die(mysqli_error($mysqli_site));
				
				$admixRow = mysqli_fetch_assoc($admixResult);

				//SET BORDER
				switch($i) {
					case 0: $border = 'border: 2px solid #'.$admixRow['color'].';';
						break;
					case 1: $border .= ' border-bottom: 2px solid #'.$admixRow['color'].'; border-right: 2px solid #'.$admixRow['color'].';';
						break;
					case 2: $border .= ' border-left: 2px solid #'.$admixRow['color'].'; border-bottom: 2px solid #'.$admixRow['color'].';';
						break;
					case 3: $border .= ' border-left: 2px solid #'.$admixRow['color'].';';
						break;
				}

				//NO MORE ADMIXTURE
				if($admixRow['adm_2_id'] == null) $continue = false;
				//SET NEXT ADMIXTURE
				else $admixture_id = $admixRow['adm_2_id'];
				
				mysqli_free_result($admixResult);
				
				$i++;
			}
		}
		
		return $border;
	}
	
	//// CONVERT RAW AGE TO STRING
	private function ageToString($age) {
		if($age == 0) return  'present';
		elseif($age < 10) return ($age * 1000).' BP';
		elseif($age >= 1000) return ($age / 1000).' Ma';
		else return $age.'k';
	}

	//// GET AGE RANGE STRING
	public function getAgeString($age_max=null, $age_min=null) {
		if($age_max == null) $age_max = $this->age_max;
		if($age_min == null) $age_min = $this->age_min;

		$ageString = $this->ageToString($age_max);
			
		if($age_max != $age_min) {
			$ageString .= ' &mdash; ';

			$ageString .= $this->ageToString($age_min);
		}
		
		return $ageString;
	}

	//// GET CRANIAL CAPACITY STRING
	public function getCCString($cc_min, $cc_max) {	
		if($cc_min != null && $cc_max != null) {
			if($cc_min != $cc_max) return $cc_min.'cc &mdash; '.$cc_max.'cc';
				else return $cc_min.'cc';
		} else return null;
	}
}