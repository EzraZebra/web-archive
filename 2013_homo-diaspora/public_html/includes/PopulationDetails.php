<?php
/*************************************************************/
/******		HOMO DIASPORA - MAP OF HUMAN EVOLUTION		******/
/******				BY EZRA HERTMANS					******/
/*************************************************************/

class PopulationDetails
{
	//// CONSTRUCTOR
	function __construct(&$populations) {
		//PREPARE VARS
		$this->details = null;
		$this->size = 0;
		$this->index = 0;
		$this->populations = $populations;

		$this->populate();
	}

	//// SET POPULATION DETAILS
	private function populate() {
		global $mysqli_site;
		//set vars for mysql injection
			$qry_species_id = $this->populations->current['species_id'];
			$qry_location_id = $this->populations->current['location_id'];
			$qry_age_max = $this->populations->age_max;
			$qry_age_min = $this->populations->age_min;

		// GET POPULATION DETAILS
		$detailResult = mysqli_query($mysqli_site,
									"	SELECT	fos_id, name, location_string, age_max, age_min, cc_min, cc_max,
												description, tools, culture, life, ancestry, kinship, progeny, citations
										FROM fossils
										WHERE	spec_sub_trans_id='$qry_species_id' AND
												location_id='$qry_location_id' AND
												((age_max >= '$qry_age_max' AND age_min <= '$qry_age_max') OR
												(age_max >= '$qry_age_min' AND age_min <= '$qry_age_min') OR
												(age_max <= '$qry_age_max' AND age_min >= '$qry_age_min'))
										ORDER BY age_max DESC, age_min DESC")
							or die(mysqli_error($mysqli_site));

		//reset population details	

		//// SET POPULATION DATA
			$this->size = mysqli_num_rows($detailResult);

			$imageCount = 0; //image index
			for($i=0; $detailRow = mysqli_fetch_assoc($detailResult); $i++) {
				$this->details[$i] = $detailRow; //save row
				$fossil_id = $detailRow['fos_id'];

				//GET GENE FLOW
					//get gene flow
						$geneflowResult = mysqli_query($mysqli_site,
													"	SELECT location_id, spec_sub_trans_id species_id FROM gene_flow, fossils
														WHERE	fossil_id_1='$fossil_id' AND
																fos_id=fossil_id_2 AND
																((age_max >= '$qry_age_max' AND age_min <= '$qry_age_max') OR
																(age_max >= '$qry_age_min' AND age_min <= '$qry_age_min') OR
																(age_max <= '$qry_age_max' AND age_min >= '$qry_age_min'))")
												or die(mysqli_error($mysqli_site));

						while($geneflowRow = mysqli_fetch_assoc($geneflowResult)) {
							foreach($this->populations->populations as $population) {
								if($population['location_id'] == $geneflowRow['location_id'] && $population['species_id'] == $geneflowRow['species_id'])
									$this->details[$i]['gene_flow'][] = $population['position'];
							}
						}

						mysqli_free_result($geneflowResult);	


					//get transitions
						$transResult = mysqli_query($mysqli_site,
													"	SELECT location_id, spec_sub_trans_id species_id FROM spec_sub_trans, trans_species, fossils
														WHERE	((fossil_src_id='$fossil_id' AND
																trans_species_id=tr_sp_id AND
																spec_sub_trans_id=sst_id) OR
																(sst_id='$qry_species_id' AND
																trans_species_id IS NOT NULL AND
																tr_sp_id=trans_species_id AND
																fos_id=fossil_trgt_id)) AND
																((age_max >= '$qry_age_max' AND age_min <= '$qry_age_max') OR
																(age_max >= '$qry_age_min' AND age_min <= '$qry_age_min') OR
																(age_max <= '$qry_age_max' AND age_min >= '$qry_age_min'))")
												or die(mysqli_error($mysqli_site));

						while($transRow = mysqli_fetch_assoc($transResult)) {
							foreach($this->populations->populations as $population) {
								if($population['location_id'] == $transRow['location_id'] && $population['species_id'] == $transRow['species_id'])
									$this->details[$i]['gene_flow'][] = $population['position'];
							}
						}

						mysqli_free_result($transResult);							
					
					//get admixture
						$admixResult = mysqli_query($mysqli_site,
													"	SELECT adm_id FROM admixture
														WHERE	fossil_id='$fossil_id'")
												or die(mysqli_error($mysqli_site));

						while($admixRow = mysqli_fetch_assoc($admixResult)) {
							$adm_id = $admixRow['adm_id'];

							//get admixture gene flow
							$geneflowResult = mysqli_query($mysqli_site,
														"	SELECT location_id, spec_sub_trans_id species_id FROM admixture, fossils
															WHERE	(adm_id='$adm_id' OR 
																	(adm_2_id='$adm_id' AND adm_2_id IS NOT NULL)) AND
																	admixture_id=adm_id AND
																	((age_max >= '$qry_age_max' AND age_min <= '$qry_age_max') OR
																	(age_max >= '$qry_age_min' AND age_min <= '$qry_age_min') OR
																	(age_max <= '$qry_age_max' AND age_min >= '$qry_age_min'))")
													or die(mysqli_error($mysqli_site));

							//add gene flow
							while($geneflowRow = mysqli_fetch_assoc($geneflowResult)) {
								foreach($this->populations->populations as $population) {
									if($population['location_id'] == $geneflowRow['location_id'] && $population['species_id'] == $geneflowRow['species_id'])
										$this->details[$i]['gene_flow'][] = $population['position'];
								}
							}
							mysqli_free_result($geneflowResult);
						}
						mysqli_free_result($admixResult);

				//GET IMAGES
					$imgResult = mysqli_query($mysqli_site,
											"	SELECT file FROM images
												WHERE fossil_id='$fossil_id'")
										or die(mysqli_error($mysqli_site));

					//SET IMAGES
					for($j=0; $imgRow = mysqli_fetch_assoc($imgResult); $j++) {
						$this->details['images'][$imageCount][$j] = $imgRow['file']; //images
						$this->details['imageIndexes'][$imageCount] = $i+1; //image indexes
					}
					
					//save index
					if(mysqli_num_rows($imgResult) > 0) $imageCount++;

					mysqli_free_result($imgResult);
			}
		mysqli_free_result($detailResult);

	}

	//// GET CURRENT DETAILS INCREMENTAL
	public function getNext() {
		if($this->index >= 0 && $this->index < $this->size) {
			//SET POPULATION
			$this->current = $this->details[$this->index];

			//next population
			$this->index++;
			
			return true;
		}
		//current population does not exist
		else {
			$this->index = 0;
			return false;
		}
	}

	//// GET GENE FLOW GRAPHICS
	public function getGeneFlow() {
		//GET DETAILS
		while($this->getNext()) {
			//FOR EACH GENE FLOW
			if(isset($this->current['gene_flow'])) foreach($this->current['gene_flow'] as $target) {
				$invert = null;
				//CURRENT IS LEFT
				if($this->populations->current['position']['left'] < $target['left']) {
					$x = $this->populations->current['position']['left'];
					$width = $target['left'];
				}
				//TARGET IS LEFT
				else {
					$x = $target['left'];
					$width = $this->populations->current['position']['left'];
				}
				//CURRENT IS TOP
				if($this->populations->current['position']['top'] < $target['top']) {
					$y = $this->populations->current['position']['top'];
					$height = $target['top'];
				}
				//TARGET IS TOP
				else {
					$y = $target['top'];
					$height = $this->populations->current['position']['top'];
				}
				
				if	(	($target['top'] != $y && $target['left'] == $x)
					||	($this->populations->current['position']['top'] != $y && $this->populations->current['position']['left'] == $x)
					) $invert = '&amp;invert';

				$width -= $x; $height -= $y; //calibrate size
				$x += 9; $y += 9; //adjust for marker size
				
				//OUTPUT GENE FLOW GRAPHICS
				echo '	<span style="	display:block; position: absolute;
										top: '.$y.'px;
										left: '.$x.'px;
										width: '.$width.'px; height: '.$height.'px;">
							<img class="route" src="routes.php&#63;width='.$width.'&amp;height='.$height.'&amp;color='.$this->populations->current['color'].$invert.'" alt="" />
						</span>';
			}
		}
	}

	//// FORMAT DETAILS FOR MARKER
	public function formatMarker() {
		//PREPARE VARS
		$marker = array	(	'cc' => '',
							'tools' => '',
							'culture' => '',
							'life' => '',
							'ancestry' => '',
							'kinship' => '',
							'progeny' => ''
						);

		//GET DETAILS
		while($this->getNext()) {
			// MORE THAN ONE IN GROUP -> BR/LOCATION PREFIX
			if($this->size > 1) {
				$prefix = '<br />';
				
				//ADD AGE
				if($this->current['age_max'] != $this->populations->age_max || $this->current['age_min'] != $this->populations->age_min) {
					$prefix .= '<i>'.$this->populations->getAgeString($this->current['age_max'], $this->current['age_min']).'</i>: ';
				}
			}
			// NO MORE THAN ONE IN GROUP -> NO BR/LOCATION PREFIX
			else $prefix = null;

			//ADD MARKER DATA
				$cc = $this->populations->getCCString($this->current['cc_min'], $this->current['cc_max']);
				
				if($cc != null) $marker['cc'] .= $prefix.$cc;
				if($this->current['tools'] != null) $marker['tools'] .= $prefix.$this->current['tools'];
				if($this->current['culture'] != null) $marker['culture'] .= $prefix.$this->current['culture'];
				if($this->current['life'] != null) $marker['life'] .= $prefix.$this->current['life'];
				if($this->current['ancestry'] != null) $marker['ancestry'] .= $prefix.$this->current['ancestry'];
				if($this->current['kinship'] != null) $marker['kinship'] .= $prefix.$this->current['kinship'];
				if($this->current['progeny'] != null) $marker['progeny'] .= $prefix.$this->current['progeny'];
		}
		
		//RETURN VALUES
		return array	(	'cc' =>		($marker['cc'] != "")
											? '<br /><b>Cranial capacity:</b> '.$marker['cc'].'<br />'
											: null,
							'tools' =>	($marker['tools'] != "")
										? '<br /><b>Tools:</b> '.$marker['tools'].'<br />'
										: null,
							'culture' =>	($marker['culture'] != "")
										? '<br /><b>Culture:</b> '.$marker['culture'].'<br />'
										: null,
							'life' =>	($marker['life'] != "")
										? '<br /><b>Life:</b> '.$marker['life'].'<br />'
										: null,
							'ancestry' =>	($marker['ancestry'] != "")
										? '<br /><b>Ancestry:</b> '.$marker['ancestry'].'<br />'
										: null,
							'kinship' =>	($marker['kinship'] != "")
										? '<br /><b>Kinship:</b> '.$marker['kinship'].'<br />'
										: null,
							'progeny' =>	($marker['progeny'] != "")
										? '<br /><b>Progeny:</b> '.$marker['progeny'].'<br />'
										: null
						);
	}
	
	//// FORMAT DETAILS FOR LEGEND
	public function formatLegend() {
		//PREPARE VARS
		$cc = $this->populations->getCCString($this->current['cc_min'], $this->current['cc_max']);
		
		//RETURN VALUES
		return array	(	'age' =>		$this->populations->getAgeString($this->current['age_max'], $this->current['age_min']).'<br />',
							'location' =>	($this->current['location_string'] != null)
												? '<b>Location:</b> '.$this->current['location_string'].'<br />'
												: null,
							'name' =>	($this->current['name'] != null)
												? '<b>Name:</b> '.$this->current['name'].'<br />'
												: null,
							'cc' =>	($cc != null)
												? '<b>Cranial capacity:</b> '.$cc.'<br />'
												: null,
							'description' =>	($this->current['description'] != null)
												? '<b>Description:</b> '.$this->current['description'].'<br />'
												: null,
							'tools' =>	($this->current['tools'] != null)
												? '<b>Tools:</b> '.$this->current['tools'].'<br />'
												: null,
							'culture' =>	($this->current['culture'] != null)
												? '<b>Culture:</b> '.$this->current['culture'].'<br />'
												: null,
							'life' =>	($this->current['life'] != null)
												? '<b>Life:</b> '.$this->current['life'].'<br />'
												: null,
							'ancestry' =>	($this->current['ancestry'] != null)
												? '<b>Ancestry:</b> '.$this->current['ancestry'].'<br />'
												: null,
							'kinship' =>	($this->current['kinship'] != null)
												? '<b>Kinship:</b> '.$this->current['kinship'].'<br />'
												: null,
							'progeny' =>	($this->current['progeny'] != null)
												? '<b>Progeny:</b> '.$this->current['progeny'].'<br />'
												: null
						);
	}
}