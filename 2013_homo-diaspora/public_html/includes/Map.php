<?php
/*************************************************************/
/******		HOMO DIASPORA - MAP OF HUMAN EVOLUTION		******/
/******				BY EZRA HERTMANS					******/
/*************************************************************/

class Map
{
	//// CONSTRUCTOR
	function __construct(&$populations) {
		$this->populations = $populations;
		
		$this->image = 'images/map.jpg';
		$imagesize = getimagesize($this->image);
		$this->totalSize = array( 'width' => $imagesize[0], 'height' => $imagesize[1] );

		//INITIALIZE
		$this->setMapSize();
	}

	//// SET MAP SIZE
	private function setMapSize() {
		global $mysqli_site;

		// GET BIGGEST DISTANCE
		$qry_age_max = $this->populations->age_max;
		$qry_age_min = $this->populations->age_min;
		$distanceResult = mysqli_query($mysqli_site,
									"	SELECT x FROM locations, fossils
										WHERE	((age_max >= '$qry_age_max' AND age_min <= '$qry_age_max') OR
												(age_max >= '$qry_age_min' AND age_min <= '$qry_age_min') OR
												(age_max <= '$qry_age_max' AND age_min >= '$qry_age_min')) AND
												loc_id=location_id
										ORDER BY x ASC")
						or die(mysqli_error($mysqli_site));

		//preset vars
		$biggestDistance = 0;
		$previous_x = null;
		$x = 0;
		$width = 0;
		while($distanceRow = mysqli_fetch_assoc($distanceResult)) {
			//first location -> preset vars
			if($previous_x === null) {
				$x = $distanceRow['x'];
				$width = $distanceRow['x'];
			}
			else {
				$distance = $distanceRow['x'] - $previous_x;
				//new distance > previous distance -> set new vars
				if($distance > $biggestDistance) {
					$biggestDistance = $distance;
					$x = $distanceRow['x'];
					$width = $previous_x;
				}

			}
			$previous_x = $distanceRow['x'];
		}
		
		mysqli_free_result($distanceResult);
		
		// GET MAP BOUNDS
		$mapSizeResult = mysqli_query($mysqli_site,
							"	SELECT MIN(x) x, MAX(x) width, MIN(y) y, MAX(y) height FROM locations, fossils
								WHERE	((age_max >= '$qry_age_max' AND age_min <= '$qry_age_max') OR
										(age_max >= '$qry_age_min' AND age_min <= '$qry_age_min') OR
										(age_max <= '$qry_age_max' AND age_min >= '$qry_age_min')) AND
										loc_id=location_id")
						or die(mysqli_error($mysqli_site));
		
		$mapSizeRow = mysqli_fetch_assoc($mapSizeResult);
		mysqli_free_result($mapSizeResult);

		// biggest distance does not exceed map bounds -> default
		if($mapSizeRow['x']+$mapSizeRow['width']-$this->totalSize['width'] > $biggestDistance) {
			$x = $mapSizeRow['x'];
			$width = $mapSizeRow['width'];
		}
		// else set map width
		else $width += $this->totalSize['width'];
		
		
		//add padding and calibrate size
		$x -= 50;
		$width -= $x-50;
		$y = $mapSizeRow['y']-50;
		$height = $mapSizeRow['height']-$y+50;
		
		//minimum 600x500
		if($width < 600) {
			$x -= (600-$width)/2;
			$width = 600;
		}
		if($height < 500) {
			$y -= (500-$height)/2;
			$height = 500;
		}

		//save data
		$this->x = $x;
		$this->y = $y;
		$this->width = $width;
		$this->height = $height;
		//calibrate position data
		$this->calibratePositions();
	}

	//// CALIBRATE POSITION DATA
	private function calibratePositions() {
		for($i=0; isset($this->populations->populations[$i]); $i++) {
			//recut map -> adjust pos
			if($this->populations->populations[$i]['position']['left'] < $this->x) $this->populations->populations[$i]['position']['left'] += $this->totalSize['width'];
			$this->populations->populations[$i]['position']['top'] -= $this->y + $this->height;
			$this->populations->populations[$i]['position']['left'] -= $this->x;
		}
	}
}