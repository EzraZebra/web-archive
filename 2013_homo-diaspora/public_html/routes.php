<?php
//GET VARS
$width = $_GET['width'];
$height = $_GET['height'];
$color_str = $_GET['color'];
if(isset($_GET['invert'])) $invert = true;
	else $invert = false;

//CREATE IMG
$image = imagecreatetruecolor($width, $height);
				
				imageantialias($image, true);

//SET COLORS
$color = imagecolorallocate($image, hexdec(substr($color_str, 0, 2)), hexdec(substr($color_str, 2, 2)), hexdec(substr($color_str, 4, 2)));
$grey = imagecolorallocate($image, 100, 100, 100);
$black = imagecolorallocate($image, 0, 0, 0);

//MAKE TRANSPARENT
imagecolortransparent($image, $black);

//center point = bottom-left corner
if($invert) {
	$cy = 0;
	$start = 0;
	$end = 90;
}
else {
	$cy = $height;
	$start = -90;
	$end = 0;
}

//DRAW ROUTE
	//bottom left border
	imagearc(	$image,
				-2, $cy+1, //center = bottom left
				$width*2, $height*2, //size = route size*2
				$start, $end, //start = top left, end = bottom right
				$grey);
	//route
	imagearc(	$image,
				-1, $cy, //center = bottom left
				$width*2, $height*2, //size = route size*2
				$start, $end, //start = top left, end = bottom right
				$color);
	//top right border
	imagearc(	$image,
				0, $cy-1, //center = bottom left + 1 right down
				$width*2, $height*2, //size = route size*2
				$start, $end, //start = top left, end = bottom right
				$grey);

//OUTPUT IMAGE
header('Content-Type: image/png');
imagepng($image);

//DESTROY IMAGE
imagedestroy($image);
?>