-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 19, 2019 at 01:07 AM
-- Server version: 10.0.38-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diaspora`
-- Required privileges: SELECT
--

-- --------------------------------------------------------

--
-- Table structure for table `admixture`
--

CREATE TABLE `admixture` (
  `adm_id` int(11) NOT NULL,
  `fossil_id` int(11) NOT NULL,
  `adm_2_id` int(11) DEFAULT NULL,
  `percentage_min` float DEFAULT NULL,
  `percentage_max` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admixture`
--

INSERT INTO `admixture` (`adm_id`, `fossil_id`, `adm_2_id`, `percentage_min`, `percentage_max`) VALUES
(1, 82, 3, 4, 6),
(2, 82, 3, 1, 3),
(3, 86, NULL, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `fossils`
--

CREATE TABLE `fossils` (
  `fos_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `spec_sub_trans_id` int(11) NOT NULL,
  `admixture_id` int(11) DEFAULT NULL,
  `location_id` int(11) NOT NULL,
  `location_string` varchar(255) DEFAULT NULL,
  `age_max` float NOT NULL,
  `age_min` float NOT NULL,
  `cc_min` int(11) DEFAULT NULL,
  `cc_max` int(11) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `tools` varchar(500) DEFAULT NULL,
  `culture` varchar(500) DEFAULT NULL,
  `life` varchar(500) DEFAULT NULL,
  `ancestry` varchar(500) DEFAULT NULL,
  `kinship` varchar(500) DEFAULT NULL,
  `progeny` varchar(500) DEFAULT NULL,
  `citations` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fossils`
--

INSERT INTO `fossils` (`fos_id`, `name`, `spec_sub_trans_id`, `admixture_id`, `location_id`, `location_string`, `age_max`, `age_min`, `cc_min`, `cc_max`, `description`, `tools`, `culture`, `life`, `ancestry`, `kinship`, `progeny`, `citations`) VALUES
(3, 'UR 501 (Uraha Man)', 1, NULL, 1, NULL, 2500, 2300, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'http://travelmag.co.uk/?p=1254'),
(4, 'SK 847', 2, NULL, 2, NULL, 2000, 1500, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'http://www.modernhumanorigins.net/sk847.html '),
(9, 'KNM-ER 1813', 2, NULL, 3, NULL, 1900, 1900, 510, 510, 'Adult skull', NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/KNM_ER_1813 '),
(10, 'KNM-ER 1805', 2, NULL, 3, NULL, 1740, 1740, 582, 582, 'Fragmented skull', NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/KNM_ER_1805 '),
(7, 'KNM-ER 1470', 1, NULL, 3, NULL, 1900, 1900, 700, 752, NULL, NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Homo_rudolfensis '),
(8, 'KNM-ER 60000, KNM-ER 62000, KNM-ER 62003', 1, NULL, 3, NULL, 1900, 1780, NULL, NULL, 'two jawbones with teeth, a juvenile face (62000)', NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Homo_rudolfensis '),
(11, 'D2700, D2280, D2282, D211, D2600', 3, NULL, 4, NULL, 1800, 1800, 600, 780, 'Teenage skull, braincase, cranium, three lower jaws', NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/D2700 '),
(20, 'KNM WT 15000 (Turkana Boy/Nariokotome Boy)', 3, NULL, 3, NULL, 1500, 1500, NULL, NULL, '7-15 years old, 1.6m tall, male skeleton', NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Turkana_Boy '),
(18, 'KNM ER 3733', 3, NULL, 3, NULL, 1700, 1700, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/KNM_ER_3733 '),
(19, 'KNM ER 3883', 3, NULL, 3, NULL, 1600, 1400, 804, 804, NULL, NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/KNM_ER_3883 '),
(15, 'OH 24 (Twiggy)', 2, NULL, 5, NULL, 1800, 1800, 600, 600, 'Adult cranium', NULL, NULL, NULL, '', '', '', 'http://humanorigins.si.edu/evidence/3d-collection/oh-24\r\n<br />\r\nhttp://en.wikipedia.org/wiki/OH_24'),
(16, 'OH 8', 2, NULL, 5, NULL, 1800, 1800, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'http://www.modernhumanorigins.net/oh8.html\r\n<br />\r\nhttp://en.wikipedia.org/wiki/Olduvai_Hominid_8 '),
(17, 'OH 7', 2, NULL, 5, NULL, 1750, 1750, 590, 710, 'fragmented lower mandible, maxillary molar, two 12-13-year-old male parietal bones, twenty-one finger, hand, and wrist bones', NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/OH_7 '),
(21, 'KNM ER 992', 3, NULL, 3, NULL, 1500, 1500, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/KNM_ER_992 '),
(23, 'STW 53', 4, NULL, 6, NULL, 1800, 1500, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Homo_gautengensis\r\n<br />\r\nhttp://www.modernhumanorigins.net/stw53.html '),
(24, 'Sangiran 2', 3, NULL, 7, 'Sangiran', 1600, 700, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Sangiran_2 '),
(25, 'Trinil 2 (Java Man)', 3, NULL, 7, 'Trinil', 1000, 700, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Trinil_2\r\n<br />\r\nhttp://humanorigins.si.edu/evidence/human-fossils/fossils/trinil-2 '),
(26, 'Sangiran 17', 3, NULL, 7, 'Sangiran', 700, 700, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'http://www.modernhumanorigins.net/sangiran17.html'),
(27, 'OH 9 (Chellean Man)', 3, NULL, 8, NULL, 1500, 1500, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'http://www.modernhumanorigins.net/oh9.html'),
(28, NULL, 3, NULL, 9, NULL, 1400, 600, 750, 800, NULL, NULL, NULL, NULL, '', '', '', ' http://archive.archaeology.org/9809/newsbriefs/eritrea.html'),
(29, NULL, 5, NULL, 10, 'Atapuerca', 1200, 1200, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'http://www.nature.com/news/2008/080326/full/news.2008.691.html '),
(30, ' BOU-VP-2/66 (Daka Skull)', 3, NULL, 11, NULL, 1000, 1000, 995, 995, 'Skull cap with cranial base, leg bones, cranial fragments, a toothless mandible', 'Acheulean industry stone tools', NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Daka_skull'),
(31, 'Bodo', 3, NULL, 11, NULL, 600, 600, 1250, 1250, NULL, NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Bodo_%28fossil%29\r\n<br />\r\nhttp://www.modernhumanorigins.net/bodo.html '),
(32, NULL, 6, NULL, 12, NULL, 800, 150, NULL, NULL, 'Skull, four teeth, two skeletons', 'Fire', NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Petralona_skull\r\n<br />\r\nhttp://humanorigins.si.edu/evidence/3d-collection/petralona-1'),
(33, 'Peking Man', 3, NULL, 13, NULL, 780, 300, 915, 1225, 'Lower jaw, skull fragments, 15 partial crania, 11 mandibles, many teeth, some skeletal bones', 'Large number of stone tools, fire', NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Peking_Man '),
(34, 'Mauer 1 (Heidelberg Man)', 6, NULL, 14, 'Mauer', 600, 500, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', ' http://en.wikipedia.org/wiki/Mauer_1 '),
(35, 'Steinheim Skull', 6, NULL, 14, 'Steinheim an der Murr', 350, 250, 950, 1280, NULL, NULL, NULL, NULL, '', '', '', ' http://en.wikipedia.org/wiki/Steinheim_Skull '),
(36, 'Boxgrove Man', 6, NULL, 15, 'Boxgrove Quarry, West Sussex', 524, 478, NULL, NULL, 'Tibia, two teeth', 'Flint tools', NULL, NULL, '', '', '', ' http://en.wikipedia.org/wiki/Eartham_Pit,_Boxgrove '),
(37, 'Swanscombe Man', 6, NULL, 15, 'Swanscombe, Kent', 400, 400, NULL, NULL, 'Woman, three skull fragments', 'Numerous Acheulean and Clactonian handaxes', NULL, NULL, '', '', '', ' http://en.wikipedia.org/wiki/Swanscombe_Man '),
(38, 'Saldanha Man', 7, NULL, 16, NULL, 500, 500, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Saldanha_man'),
(39, 'Arago 21 (Tautavel Man)', 6, NULL, 17, NULL, 450, 450, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Arago_21#Arago_XXI\r\n<br />http://www.modernhumanorigins.net/aragoxxi.html'),
(40, 'Skull 5 (Miguelón)', 6, NULL, 10, 'Sima de los Huesos, Atapuerca', 400, 400, 1100, NULL, 'More than 5500 fossils', NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Miguel%C3%B3n'),
(41, 'A.L. 444-2 (Ndutu)', 15, NULL, 5, 'Lake Ndutu', 350, 350, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'http://humanorigins.si.edu/evidence/3d-collection/ndutu'),
(42, 'Altamure Man', 8, NULL, 18, NULL, 400, 100, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Altamura_Man'),
(89, NULL, 6, NULL, 51, NULL, 1300, 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Sapiens, Neanderthalensis and Denisova', NULL),
(90, NULL, 15, NULL, 52, NULL, 516, 350, NULL, NULL, NULL, NULL, NULL, NULL, 'Heidelbergensis', NULL, 'Sapiens', NULL),
(91, NULL, 9, NULL, 53, NULL, 200, 100, NULL, NULL, NULL, NULL, NULL, NULL, 'Heidelbergensis', NULL, 'Sapiens', NULL),
(44, 'Bontnewydd', 8, NULL, 20, NULL, 230, 230, NULL, NULL, 'Eleven year old boy, teeth and jawbone', NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Bontnewydd_Palaeolithic_site'),
(45, 'Kabwe 1 (Broken Hill Skull)', 15, NULL, 21, NULL, 300, 110, 1230, 1300, 'a cranium, upper jaw from another individual, a sacrum, a tibia, and two femur fragments', NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Kabwe_Cranium\r\n<br />\r\nhttp://humanorigins.si.edu/evidence/human-fossils/fossils/kabwe-1'),
(46, 'Omo 1, Omo 2', 9, NULL, 22, NULL, 200, 190, NULL, NULL, 'Two partial skulls, four jaws, a legbone, around two hundred teeth, a tibia, a fibulan, and several other parts', NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Omo_remains\r\n<br />\r\nhttp://www.modernhumanorigins.net/omo1.html\r\n<br />\r\nhttp://www.modernhumanorigins.net/omo2.html'),
(47, 'Herto Remains', 12, NULL, 23, NULL, 160, 154, 1450, 1450, 'Five crania, including an adult male (BOU-VP-16/1), a partial adult male and a six-year-old child', NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Homo_sapiens_idaltu\r\n<br />\r\nhttp://news.bbc.co.uk/2/hi/science/nature/2978800.stm'),
(48, 'Irhoud 1, 2, 3 and 4', 9, NULL, 24, 'Jebel Irhoud, Sidi Moktar', 160, 160, NULL, NULL, 'long childhood, portions of two adult skulls (Irhoud 1 and Irhoud 2), a child’s mandible (Irhoud 3), and a child’s humerus (Irhoud 4)', NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Jebel_Irhoud'),
(49, NULL, 8, NULL, 25, 'Nahal Me\'arot Nature Reserve', 120, 120, NULL, NULL, 'Mousterian culture', 'Small flint tools, Levallois technique', NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Tabun_C1\r\n<br />\r\nhttp://www.modernhumanorigins.net/tabun1.html'),
(50, NULL, 8, NULL, 26, NULL, 127, 100, NULL, NULL, 'over eight hundred fossil remains', NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Krapina\r\n<br />\r\nhttp://www.modernhumanorigins.net/krapinac.html'),
(51, 'Skhul', 13, NULL, 25, 'Es Skul, Mount Carmel', 119, 81, NULL, NULL, 'seven adults and three children', NULL, 'possible delibirate burial and symbolism', NULL, '', '', '', 'http://en.wikipedia.org/wiki/Qafzeh'),
(52, 'Qafzeh', 13, NULL, 25, 'Qafzeh Cave, Precipice Mountain', 115, 92, NULL, NULL, '7 adults, 8 children', 'hearths, flint tools, sea shells, red ochre', 'six graves', NULL, '', '', '', 'http://en.wikipedia.org/wiki/Qafzeh\r\n<br />\r\nhttp://www.modernhumanorigins.net/qafzeh6.html'),
(53, NULL, 13, NULL, 27, 'Klasies River Caves', 125, 70, NULL, NULL, NULL, 'small game hunting, fishing, plants, roots and shellfish gathering, cooking, stone tools, land organisation', NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Klasies_River_Caves\r\n<br />\r\nhttp://www.modernhumanorigins.net/klasies.html'),
(54, 'Teshik-Tash Boy', 8, NULL, 28, NULL, 70, 70, NULL, NULL, '8-11-year old child', NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Teshik-Tash\r\n<br />\r\nhttp://humanorigins.si.edu/evidence/3d-collection/teshik-tash'),
(55, 'La Ferrassie', 8, NULL, 29, 'La Ferrassie, Dordogne', 70, 50, NULL, NULL, 'Eight individuals', NULL, 'intentionally buried', NULL, '', '', '', 'http://en.wikipedia.org/wiki/La_Ferrassie_1\r\n<br />\r\nhttp://humanorigins.si.edu/evidence/human-fossils/fossils/la-ferrassie'),
(56, 'Shanidar 1 (Nandy), Shanidar 2-4', 8, NULL, 30, NULL, 80, 60, NULL, NULL, '10 skeletons of varying ages', '', 'Implicit group concern, possible funeral ceremonies', NULL, '', '', '', 'http://en.wikipedia.org/wiki/Shanidar_1'),
(57, 'La Chapelle-aux-Saints 1 (The Old Man)', 8, NULL, 29, 'La Chapelle-aux-Saints', 60, 60, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/La_Chapelle-aux-Saints_1'),
(58, NULL, 8, NULL, 10, 'Sidrón Cave, Asturias', 49, 49, NULL, NULL, 'At least 12 individuals: three men, three adolescent boys, three women, and three infants', NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Sidr%C3%B3n_Cave'),
(59, NULL, 13, 3, 31, 'Kents Cavern, Devon, England', 44, 42, NULL, NULL, 'Upper jawbone', NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Kents_Cavern'),
(60, 'Lake Mungo 3 (Mungo Man)', 13, 3, 32, 'Lake Mungo, New South Wales', 68, 40, NULL, NULL, NULL, NULL, 'sprinkled with red ochre as artistic burial practice', NULL, '', '', '', 'http://en.wikipedia.org/wiki/Mungo_Man#Further_discoveries'),
(61, 'Mt. Circeo 1', 8, NULL, 33, NULL, 60, 40, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'http://www.modernhumanorigins.net/circeo.html'),
(62, 'Neanderthal 1', 8, NULL, 34, NULL, 40, 40, NULL, NULL, 'a skull cap, two femora, the three right arm bones, two of the left arm bones, ilium, and fragments of a scapula and ribs', NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Neanderthal_1'),
(63, 'Denisova hominin (X woman)', 11, NULL, 35, NULL, 41, 33.5, NULL, NULL, 'Finger bone fragment of a juvenile female (X woman, 41k), third upper molar from a young adult (34.5k)', NULL, 'Artifacts including a bracelet (40k)', NULL, 'Result of a migration out of Africa, before neanderthalensis and sapiens, and after erectus. Common ancestor with neanderthalensis 640k, with sapiens 804k.', '', '4-6% of Melanesian genome, also Austrialian Aborigines and smaller scattered groups of people in Southeast Asia', 'http://en.wikipedia.org/wiki/Denisova_hominin'),
(66, NULL, 8, NULL, 35, NULL, 41, 40, NULL, NULL, 'toe bone', NULL, NULL, NULL, '', '', '', 'http://en.wikipedia.org/wiki/Denisova_hominin'),
(67, 'Hofmeyr Skull', 13, NULL, 27, 'Vlekpoort River, near Hofmeyr', 39.2, 32.8, NULL, NULL, NULL, NULL, NULL, NULL, '', 'The skull\'s features were found to have a very close affinity with the Cro-magnon people in Eurasia', '', 'http://en.wikipedia.org/wiki/Hofmeyr_Skull'),
(68, 'Oase 1, 2', 13, 3, 36, 'Pestera cu Oase', 37.8, 37.8, NULL, NULL, 'mandible (Oase 1),  facial skeleton and temporal bone of a female (Oase 2)', NULL, NULL, NULL, '', 'has sapiens and neanderthalensis traits', '', 'http://en.wikipedia.org/wiki/Pe%C5%9Ftera_cu_Oase'),
(69, 'Red Lady of Paviland', 13, 3, 31, 'Goat\'s Hole Cave, Gower Peninsula, South Wales', 33, 33, NULL, NULL, 'fairly complete male skeleton, probably no older than 21', 'more than 4,000 flints, teeth and bones, and needles and bracelets', 'dyed in red ochre, ceremonial burial, possibly a tribal chieftain', 'Lived in a warmer period. Diet consisted of between 15% and 20% fish.', NULL, NULL, NULL, 'http://en.wikipedia.org/wiki/Red_Lady_of_Paviland'),
(70, 'Yamashita Cave Man', 13, 3, 38, 'Yamashita limestone cave near Naha', 33, 31, NULL, NULL, 'many bones', NULL, NULL, NULL, NULL, NULL, NULL, 'http://en.wikipedia.org/wiki/Yamashita_Cave_Man'),
(71, 'Gibraltar 2 (Devil\'s Tower Child)', 8, NULL, 39, 'Devil\'s Tower Mousterian rock shelter', 50, 30, NULL, NULL, 'Five skull fragments of a female child, about four years old', 'Flaked stone tools indicative of Mousterian industry', NULL, 'May have been primarily carnivorous', NULL, NULL, NULL, 'http://en.wikipedia.org/wiki/Gibraltar_2'),
(72, ' Le Moustier skull', 8, NULL, 29, ' Peyzac-le-Moustier, Dordogne', 45, 45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'http://en.wikipedia.org/wiki/Le_Moustier'),
(73, 'Cro-Magnon 1', 13, 3, 29, 'Les Eyzies, Dordogne', 27.95, 27.41, 1600, 1600, 'skull', NULL, NULL, NULL, NULL, NULL, NULL, 'http://en.wikipedia.org/wiki/Cro-Magnon_1'),
(74, 'Flores Man, hobbit, LB1 (Little Lady of Flores, Flo)', 10, NULL, 40, NULL, 95, 12, 426, 426, 'Partial skeletons of nine individuals, including one complete cranium. 30-year-old female (LB1).', 'Sophisticated stone tools', NULL, 'About three feet tall', 'Homo erectus', 'More similar to early humans and apes than modern humans', NULL, 'http://en.wikipedia.org/wiki/Homo_floresiensis'),
(75, 'Minatogawa Man', 13, 3, 38, 'Minatogawa limestone quarry, near Naha', 16, 14, NULL, NULL, 'Between 5 and 9 individuals, two males, the rest females, four skeletons and some isolated bones', NULL, NULL, NULL, NULL, NULL, NULL, 'http://en.wikipedia.org/wiki/Minatogawa_Man\r\n<br />\r\nhttp://www.modernhumanorigins.net/minatogawa1.html'),
(76, 'Cheddar Man', 13, 3, 31, 'Gough\'s Cave, Cheddar, Somerset', 14.7, 9.15, NULL, NULL, 'Complete male skeleton', NULL, 'Carving of a mammoth (13k), human skulls possibly fashioned into ritual drinking cups or bowls', NULL, NULL, NULL, 'Adrian Targett, a man living in the local area', 'http://en.wikipedia.org/wiki/Human\r\n<br />\r\nhttp://www.ubss.org.uk/resources/proceedings/vol17/UBSS_Proc_17_2_145-152.pdf'),
(77, 'La Brea Woman', 13, 3, 41, NULL, 10, 10, NULL, NULL, 'Partial skeleton of a woman, 17 to 25 years old', NULL, 'domestic dog, possibly ceremonially buried', NULL, NULL, NULL, NULL, 'http://en.wikipedia.org/wiki/La_Brea_Woman'),
(78, NULL, 9, NULL, 42, NULL, 13, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Its nearest neighbour is the Ngaloba (Laetoli 18) skull from Tanzania, about 140k', NULL, 'http://www.nhm.ac.uk/about-us/news/2011/september/mystery-of-a-west-african-skull-from-13000-years-ago103799.html'),
(79, NULL, 13, 3, 29, 'Combe-Capelle, Couze valley, Périgord region', 7.575, 7.575, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'http://en.wikipedia.org/wiki/Combe_Capelle'),
(80, 'Cohuna Cranium', 13, 2, 32, 'Kow Swamp, Murray Valley, Victoria', 13.28, 6.5, NULL, NULL, 'More than 22 individuals', NULL, 'Possible artificial cranial deformation, particularly Kow Swamp 5', NULL, NULL, NULL, NULL, 'http://en.wikipedia.org/wiki/Kow_Swamp_Archaeological_Site'),
(81, 'Tepexpan Man', 13, 3, 43, NULL, 10, 4.7, NULL, NULL, 'Possibly a woman', NULL, NULL, NULL, NULL, NULL, NULL, 'http://en.wikipedia.org/wiki/Tepexpan_man'),
(82, 'Interbreeding event', 11, NULL, 45, NULL, 41, 20.5, NULL, NULL, 'Date is approximate', NULL, NULL, NULL, 'Denisovan population in Siberia', NULL, 'Interbreeding event for 4-6% of Melanesian genome, 1-3% Austrialian Aborigines and smaller scattered groups of people in Southeast Asia', 'http://en.wikipedia.org/wiki/Denisova_hominin'),
(83, 'Melanesian population', 13, 1, 44, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '4-6% of genome from Denisova population in Siberia. Interbreeding event in Southeastern Asia.\n<br />\n1-4% of genome from neanderthalensis after sapiens\' migration from Africa.', 'Predominantly Melanesian areas include the Moluccas of Eastern Indonesia, the New Guinea and surrounding islands, the Solomon Islands, Vanuatu and Fiji.<br />Neanderthalensis ancestry for all non-Africans.', NULL, 'http://en.wikipedia.org/wiki/Denisova_hominin\r\n<br />\r\nhttp://en.wikipedia.org/wiki/Melanesians\r\n<br />\r\nhttp://en.wikipedia.org/wiki/Neanderthal_admixture_theory#Neanderthals'),
(84, 'Aboriginal population', 13, 2, 46, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1-3% of genome from Denisova population in Siberia. Interbreeding event in Southeastern Asia.', 'Aboriginal populations across Australia', NULL, 'http://en.wikipedia.org/wiki/Denisova_hominin\r\n<br />\r\nhttp://en.wikipedia.org/wiki/Australian_Aborigines'),
(85, 'Mamanwa population', 13, 2, 47, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1-3% of genome from Denisova population in Siberia. Interbreeding event in Southeastern Asia.', 'Mamanwa people in Philippines and some other Negrito people', NULL, 'http://en.wikipedia.org/wiki/Denisova_hominin\r\n<br />\r\nhttp://s452.photobucket.com/user/oditous/profile/'),
(86, 'Interbreeding event', 8, NULL, 50, NULL, 80, 50, NULL, NULL, 'Date is approximate', NULL, NULL, NULL, NULL, NULL, '1-4% of non-African genome', 'http://en.wikipedia.org/wiki/Neanderthal_admixture_theory#Neanderthals\r\n<br />\r\nhttp://en.wikipedia.org/wiki/Neanderthal'),
(87, NULL, 13, 3, 48, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1-4% of genome from neanderthalensis after sapiens\' migration from Africa', 'All non-Africans', NULL, 'http://en.wikipedia.org/wiki/Neanderthal_admixture_theory#Neanderthals\r\n<br />\r\nhttp://en.wikipedia.org/wiki/File:Fran%C3%A7ois_Hollande_%28Journ%C3%A9es_de_Nantes_2012%29.jpg'),
(88, NULL, 13, 3, 49, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1-4% of genome from neanderthalensis after sapiens\' migration from Africa', 'All non-Africans', NULL, 'http://en.wikipedia.org/wiki/Neanderthal_admixture_theory#Neanderthals\r\n<br />\r\nen.wikipedia.org/wiki/File:Tdlee_ccast.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `gene_flow`
--

CREATE TABLE `gene_flow` (
  `gn_fl_id` int(11) NOT NULL,
  `fossil_id_1` int(11) NOT NULL,
  `fossil_id_2` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gene_flow`
--

INSERT INTO `gene_flow` (`gn_fl_id`, `fossil_id_1`, `fossil_id_2`) VALUES
(1, 63, 82);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `img_id` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `fossil_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`img_id`, `file`, `fossil_id`) VALUES
(1, 'rudolfensis1.jpg', 3),
(2, 'habilis5.jpg', 4),
(3, 'rudolfensis2.jpg', 7),
(4, 'habilis1.jpg', 9),
(5, 'erectus1.jpg', 11),
(6, 'habilis2.jpg', 15),
(7, 'habilis3.jpg', 16),
(8, 'habilis4.jpg', 17),
(9, 'gautengensis1.jpg', 23),
(10, 'erectus2.jpg', 18),
(11, 'erectus6.jpg', 19),
(12, 'erectus3a.jpg', 20),
(13, 'erectus3b.jpg', 20),
(14, 'erectus5.jpg', 21),
(15, 'erectus8.jpg', 24),
(16, 'erectus9.jpg', 25),
(17, 'erectus10.jpg', 26),
(18, 'erectus4.jpg', 27),
(19, 'erectus12.jpg', 28),
(20, 'antecessor1.jpg', 29),
(21, 'erectus7.jpg', 30),
(22, 'erectus13.jpg', 31),
(23, 'heidelbergensis8.jpg', 32),
(24, 'erectus11.jpg', 33),
(25, 'erectus15.png', 33),
(26, 'heidelbergensis1.jpg', 34),
(27, 'heidelbergensis7.jpg', 35),
(28, 'heidelbergensis2.jpg', 36),
(29, 'heidelbergensis4.jpg', 37),
(30, 'heidelbergensis5.jpg', 39),
(31, 'heidelbergensis3.jpg', 40),
(32, 'heidelbergensis6.png', 41),
(33, 'neanderthalensis1.jpg', 42),
(34, 'erectussapiens1.jpg', 43),
(35, 'heidelbergensis_Kabwe_jddh_3qtr_l.jpg', 45),
(36, 'omo1rside.jpg', 46),
(37, 'omo2lside.jpg', 46),
(38, '_39333327_skull_white_a203.jpg', 47),
(39, '1.jpg', 47),
(40, 'Irhoud1face.jpg', 48),
(41, 'NA_mpg.jpg', 48),
(42, 'Rhodesian_Men.jpg', 45),
(46, 'tabun1rside.jpg', 49),
(47, 'krapinacrangle.jpg', 50),
(48, 'Skhul.JPG', 51),
(49, 'qafzeh6rside.jpg', 52),
(50, 'Qafzeh.JPG', 52),
(51, 'klasies.jpg', 53),
(52, 'Skull_of_Teshik-Tash_Boy.jpg', 54),
(53, 'Teshik-Tash_Boy.jpg', 54),
(54, 'Ferrassie_skull.jpg', 55),
(55, 'neanderthalensis_laferrassie_skull_3qtr_l.jpg', 55),
(56, 'Shanidarf.jpg', 56),
(57, 'Kermanshah_Pal_Museum-Neanderthal.jpg', 56),
(58, 'Homo_sapiens_neanderthalensis.jpg', 57),
(59, 'Mungo_Man.jpg', 60),
(60, 'mountcirceo.jpg', 61),
(61, 'neanderthal1langle.jpg', 62),
(62, 'Hofmeyr_Skull.jpg', 67),
(63, 'Experts-Recreate-the-Face-of-the-First-European-2.jpg', 68),
(64, 'Homo_neanderthalensis_face-top_(University_of_Zurich).JPG', 71),
(65, 'Homo_neanderthalensis_reconstruction2_(University_of_Zurich).JPG', 71),
(66, 'Lemousf.jpg', 72),
(67, 'Cro-Magnon.jpg', 73),
(68, 'Cromagf.jpg', 73),
(69, 'Homo_floresiensis_-_steps_of_forensic_facial_reconstruction.png', 74),
(70, 'Homo_floresiensis.jpg', 74),
(71, 'minatogawa1front.jpg', 75),
(72, 'minatogawa1.jpg', 75),
(73, 'Minatogawa_Man_Restoration_model.jpg', 75),
(74, 'cheddarman.jpg', 76),
(75, 'La_Brea_woman.jpg', 77),
(76, 'iwo-eleru-490_103803_2.jpg', 78),
(77, 'Homo_sapiens_Combe_Capelle.jpg', 79),
(78, 'Kowswamp1.JPG', 80),
(79, 'Vanuatu_blonde.jpg', 83),
(80, 'David_Gulpilil.jpg', 84),
(81, 'mamanwaPH2.jpg', 85),
(82, 'Tdlee_ccast.jpg', 88),
(83, 'Francois_Hollande__Journees_de_Nantes_2012_.jpg', 87);

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `loc_id` int(11) NOT NULL,
  `x` int(4) NOT NULL,
  `y` int(4) NOT NULL,
  `location_name` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`loc_id`, `x`, `y`, `location_name`) VALUES
(1, 1190, 580, 'Uraha Village, Malawi'),
(2, 1145, 705, 'South Africa'),
(3, 1215, 500, 'Koobi Fora, Kenya'),
(4, 1245, 270, 'Dmanisi, Georgia'),
(5, 1205, 535, 'Olduvai Gorge, Tanzania'),
(6, 1130, 685, 'Sterkfontein, South Africa'),
(7, 1605, 565, 'Java, Indonesia'),
(8, 1195, 555, 'Tanzania'),
(9, 1213, 437, 'Afar Region, Ethiopia'),
(10, 982, 263, 'Northern Spain'),
(11, 1220, 475, 'Awash Valley, Ethiopia'),
(12, 1130, 275, 'Petralona, Greece'),
(13, 1645, 285, 'Zhoukoudian, China'),
(14, 1055, 225, 'South Germany'),
(15, 997, 213, 'South England'),
(16, 1105, 720, 'Elandsfontein, South Africa'),
(17, 1013, 265, 'Arago Cave, Tautavel, France'),
(18, 1095, 280, 'Altamure, Italy'),
(19, 1605, 310, 'Dali County, Shaanxi Province, China'),
(20, 982, 195, 'North Wales'),
(21, 1155, 610, 'Kabwe, Tanzania'),
(22, 1200, 485, 'Omo River, Ethiopia'),
(23, 1235, 460, 'Middle Awash, Ehtiopia'),
(24, 957, 327, 'Marrakesh, Morocco'),
(25, 1195, 330, 'Haifa, Israel'),
(26, 1090, 240, 'Krapina, Croatia'),
(27, 1135, 725, 'Eastern Cape Province, South Africa'),
(28, 1380, 300, 'Teshik-Tash, Uzbekistan'),
(29, 1005, 250, 'South West France'),
(30, 1260, 310, 'Shanidar Cave, Zagros Mountains, Iraq'),
(31, 977, 212, 'South West UK'),
(32, 1795, 725, 'South Australia'),
(33, 1075, 270, 'Mount Circeo, Italy'),
(34, 1045, 210, 'Neander Valley, Germany'),
(35, 1445, 215, 'Denisova Cave,  Altai Mountains, Siberia, Russia'),
(36, 1130, 250, 'Southwestern Romania'),
(40, 1675, 575, 'Flores, Indonesia'),
(38, 1705, 365, 'Okinawa, Japan'),
(39, 970, 305, 'Gibraltar'),
(41, 340, 315, 'La Brea Tar Pits, Los Angeles, United States'),
(42, 1035, 475, 'Iwo-Eleru, Nigera'),
(43, 450, 405, 'Lake Texcoco, Mexico'),
(44, 1810, 560, 'Papa New Guinea'),
(45, 1570, 385, 'Southeast Asia'),
(46, 1750, 685, 'Australia'),
(47, 1695, 480, 'Mindanao, Philippines'),
(48, 1010, 240, 'France'),
(49, 1570, 310, 'China'),
(50, 1215, 320, 'Middle East'),
(51, 1130, 405, 'North East Africa'),
(52, 1134, 550, 'South East Africa'),
(53, 1235, 490, 'East Africa');

-- --------------------------------------------------------

--
-- Table structure for table `species`
--

CREATE TABLE `species` (
  `sp_id` int(11) NOT NULL,
  `species` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `species`
--

INSERT INTO `species` (`sp_id`, `species`) VALUES
(5, 'antecessor'),
(11, 'denisova'),
(3, 'erectus'),
(10, 'floresiensis'),
(4, 'gautengensis'),
(2, 'habilis'),
(6, 'heidelbergensis'),
(8, 'neanderthalensis'),
(7, 'rhodesiensis'),
(1, 'rudolfensis'),
(9, 'sapiens');

-- --------------------------------------------------------

--
-- Table structure for table `spec_sub_trans`
--

CREATE TABLE `spec_sub_trans` (
  `sst_id` int(11) NOT NULL,
  `species_id` int(11) NOT NULL,
  `subspecies` varchar(255) DEFAULT NULL,
  `trans_species_id` int(11) DEFAULT NULL,
  `color` varchar(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spec_sub_trans`
--

INSERT INTO `spec_sub_trans` (`sst_id`, `species_id`, `subspecies`, `trans_species_id`, `color`) VALUES
(9, 9, NULL, NULL, '62a2b2'),
(8, 8, NULL, NULL, 'b2563d'),
(7, 7, NULL, NULL, 'b2b162'),
(6, 6, NULL, NULL, 'dda466'),
(5, 5, NULL, NULL, 'af7883'),
(3, 3, NULL, NULL, '6662b2'),
(2, 2, NULL, NULL, '8bb262'),
(1, 1, NULL, NULL, 'b262a8'),
(4, 4, NULL, NULL, 'b2a962'),
(10, 10, NULL, NULL, 'bd69c0'),
(11, 11, NULL, NULL, 'ff7e3e'),
(12, 9, 'idaltu', NULL, '62b297'),
(13, 9, 'sapiens', NULL, '6db7e7'),
(15, 6, NULL, 1, '92a27d');

-- --------------------------------------------------------

--
-- Table structure for table `trans_species`
--

CREATE TABLE `trans_species` (
  `tr_sp_id` int(11) NOT NULL,
  `species_id` int(11) NOT NULL,
  `fossil_src_id` int(11) DEFAULT NULL,
  `fossil_trgt_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_species`
--

INSERT INTO `trans_species` (`tr_sp_id`, `species_id`, `fossil_src_id`, `fossil_trgt_id`) VALUES
(1, 9, 89, 91);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admixture`
--
ALTER TABLE `admixture`
  ADD PRIMARY KEY (`adm_id`);

--
-- Indexes for table `fossils`
--
ALTER TABLE `fossils`
  ADD PRIMARY KEY (`fos_id`);

--
-- Indexes for table `gene_flow`
--
ALTER TABLE `gene_flow`
  ADD PRIMARY KEY (`gn_fl_id`),
  ADD UNIQUE KEY `fossil_id_1` (`fossil_id_1`,`fossil_id_2`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`img_id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`loc_id`);

--
-- Indexes for table `species`
--
ALTER TABLE `species`
  ADD PRIMARY KEY (`sp_id`),
  ADD UNIQUE KEY `species` (`species`);

--
-- Indexes for table `spec_sub_trans`
--
ALTER TABLE `spec_sub_trans`
  ADD PRIMARY KEY (`sst_id`),
  ADD UNIQUE KEY `color` (`color`),
  ADD UNIQUE KEY `species_id` (`species_id`,`subspecies`);

--
-- Indexes for table `trans_species`
--
ALTER TABLE `trans_species`
  ADD PRIMARY KEY (`tr_sp_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fossils`
--
ALTER TABLE `fossils`
  MODIFY `fos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `gene_flow`
--
ALTER TABLE `gene_flow`
  MODIFY `gn_fl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `img_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `loc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `species`
--
ALTER TABLE `species`
  MODIFY `sp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `spec_sub_trans`
--
ALTER TABLE `spec_sub_trans`
  MODIFY `sst_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `trans_species`
--
ALTER TABLE `trans_species`
  MODIFY `tr_sp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
