<?php
//GET VARS
$main['width'] = $_GET['width'];
$main['x'] = $_GET['x'];
$main['dx'] = 0;
$main['height'] = $_GET['height'];
$main['y'] = $_GET['y'];

//CREATE IMGS
$newImg = imagecreatetruecolor($main['width'], $main['height']);
$fullImg = imagecreatefromjpeg('images/map.jpg');

// SET VARS
	//GET MAP SIZE
	$mapSize['width'] = imagesx($fullImg);
	$mapSize['height'] = imagesy($fullImg);

	//DONT COPY UNLESS...
	$left['copy'] = false;
	$right['copy'] = false;

	// OFF MAP LEFT
	if($main['x'] < 0) {
		//SET LEFT SIZE AND POSITION
		$left['width'] = abs($main['x']);
		$left['x'] = $main['x']+$mapSize['width'];

		//ADJUST MAIN
		$main['width'] += $main['x'];
		$main['x'] = 0;
		$main['dx'] = $left['width'];
		
		//COPY LEFT
		$left['copy'] = true;
	}
	//OFF MAP RIGHT
	elseif($main['x']+$main['width'] > $mapSize['width']) {
		//SET RIGHT SIZE AND POSITION
		$right['width'] = $main['x']+$main['width']-$mapSize['width'];
		$right['dx'] = $main['width']-$right['width'];

		//ADJUST MAIN
		$main['width'] -= $right['width'];
		
		//COPY RIGHT
		$right['copy'] = true;
	}

	//OFF MAP TOP
	if($main['y'] < 0) $main['y'] = 0;
	//OFF MAP BOTTOM
	elseif($main['y']+$main['height'] > $mapSize['height']) $main['y'] = $mapSize['height']-$main['height'];

//CREATE IMAGE
imagecopy($newImg, $fullImg, $main['dx'], 0, $main['x'], $main['y'], $main['width'], $main['height']); //main
if($left['copy']) imagecopy($newImg, $fullImg, 0, 0, $left['x'], $main['y'], $left['width'], $main['height']); //copy left
if($right['copy']) imagecopy($newImg, $fullImg, $right['dx'], 0, 0, $main['y'], $right['width'], $main['height']); //copy right

//DESTROY FULL IMAGE
imagedestroy($fullImg);

//OUTPUT IMAGE
header('Content-Type: image/jpeg');
imagejpeg($newImg);

//DESTROY IMAGE
imagedestroy($newImg);
?>