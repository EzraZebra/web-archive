<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$root_path = '';

// Pickr: https://github.com/Simonwep/pickr
$config['color_picker'] = [
	//'styles'		=> [ 'https://cdn.jsdelivr.net/npm/@simonwep/pickr/dist/themes/monolith.min.css' ],
	//'scripts'		=> [ 'https://cdn.jsdelivr.net/npm/@simonwep/pickr/dist/pickr.min.js' ],
	'styles'		=> [ $root_path . '/plugins/pickr/themes/monolith.min.css' ],
	'scripts'		=> [ $root_path . '/plugins/pickr/pickr.min.js' ],
	'scripts_dom'	=> [ $root_path . '/js/pickr.js' ]
];

// Toast UI Editor: https://github.com/nhn/tui.editor
$config['text_editor'] = [
	'styles'		=> [
		//'https://uicdn.toast.com/tui-editor/latest/tui-editor.min.css',
		//'https://uicdn.toast.com/tui-editor/latest/tui-editor-contents.min.css',
		//'https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.33.0/codemirror.min.css',
		//'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.8/styles/default.min.css'
		$root_path . '/plugins/tui-editor/tui-editor.min.css',
		$root_path . '/plugins/tui-editor/tui-editor-contents.min.css',
		$root_path . '/plugins/codemirror/codemirror.min.css',
		$root_path . '/plugins/highlightjs/styles/default.min.css'
	],
	//'scripts'		=> [ 'https://uicdn.toast.com/tui-editor/latest/tui-editor-Editor-full.min.js' ],
	'scripts'		=> [ $root_path . '/plugins/tui-editor/tui-editor-Editor-full.min.js' ],
	'scripts_dom'	=> [ $root_path . '/js/tui_editor.js' ]
];
$config['text_viewer'] = [
	'styles'		=> [
		$config['text_editor']['styles'][1],
		$config['text_editor']['styles'][3],
	],
	//'scripts'		=> [ 'https://uicdn.toast.com/tui-editor/latest/tui-editor-Viewer-full.min.js' ],
	'scripts'		=> [ $root_path . '/plugins/tui-editor/tui-editor-Viewer-full.min.js' ],
	'scripts_dom'	=> [ $root_path . '/js/tui_viewer.js' ]
];

// Day.js: https://github.com/iamkun/dayjs
$config['datetime'] = [
	//'scripts'		=> [ 'https://unpkg.com/dayjs' ],
	'scripts'		=> [ $root_path . '/plugins/dayjs/dayjs.min.js' ],
	'scripts_dom'	=> [ $root_path . '/js/dayjs.js' ]
];
$config['datetime_form'] = [
	'scripts'		=> $config['datetime']['scripts'],
	'scripts_dom'	=> [ $root_path . '/js/dayjs_form.js' ]
];