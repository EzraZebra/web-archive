<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('data_add_script'))
{
	function data_add_script($script, &$data)
	{
		get_instance()->config->load('ez_cms');
		$script = get_instance()->config->item($script);
		
		if (is_array($script)) $data = array_merge_recursive($data, $script);
	}
}

if ( ! function_exists('datetime'))
{
	function datetime($timestamp, $format, $class = null)
	{
		return	'<time datetime="'.date('c', $timestamp).'" data-format="'.$format.'" class="'.$class.'">'
					.date(dayjs_to_php($format), $timestamp).
				'</time>';
	}
}

if ( ! function_exists('dayjs_to_php'))
{
	function dayjs_to_php($format)
	{
		return strtr($format, [
			'[YY]' => '\Y\Y',
			'[YYYY]' => '\Y\Y\Y\Y',
			'YY' => 'y',
			'YYYY' => 'Y',
			'[M]' => '\M',
			'[MM]' => '\M\M',
			'[MMM]' => '\M\M\M',
			'[MMMM]' => '\M\M\M\M',
			'M' => 'n',
			'MM' => 'm',
			'MMM' => 'M',
			'MMMM' => 'F',
			'[D]' => '\D',
			'[DD]' => '\D\D',
			'D' => 'j',
			'DD' => 'd',
			'[d]' => '\d',
			'[dd]' => '\d\d',
			'[ddd]' => '\d\d\d',
			'[dddd]' => '\d\d\d\d',
			'd' => 'w',
			'dd' => 'D',
			'ddd' => 'D',
			'dddd' => 'l',
			'[H]' => '\H',
			'[HH]' => '\H\H',
			'H' => 'G',
			'HH' => 'H',
			'[h]' => '\h',
			'[hh]' => '\h\h',
			'h' => 'g',
			'hh' => 'h',
			'[m]' => '\m',
			'[mm]' => '\m\m',
			'm' => 'i',
			'mm' => 'i',
			'[s]' => '\s',
			'[ss]' => '\s\s',
			//'s' => 's',
			'ss' => 's',
			'[SSS]' => '\S\S\S',
			'SSS' => 'v',
			'[Z]' => '\Z',
			'[ZZ]' => '\Z\Z',
			'Z' => 'P',
			'ZZ' => 'O',
			'[A]' => '\A',
			//'A' => 'A',
			'[a]' => '\a',
			//'a' => 'a',
		]);
	}
}