<?php
class Theme_model extends CI_Model {

	public function __construct()
	{
		// the database is loaded in the controller
		// $this->load->database();
	}
	
	/**
	 * Get all theme names & ids
	 */
	public function get_list()
	{
		$this->db->select('name, id');
		$query = $this->db->get_where('themes', array('name !=' => '__active'));
		$result = $query->result_array();
		$query->free_result();
		
		return $result;
	}
	
	/**
	 * Get theme fields (not id)
	 */
	public function get_fields()
	{
		$fields = $this->db->list_fields('themes');
		unset($fields[0]); // remove id
		return $fields;
	}
	
	/**
	 * Get theme
	 */
	public function get($id)
	{
		$query = $this->db->get_where('themes', [ 'id' => $id ]);
		
		// Get default or first theme if theme doesn't exist
		if (!$query->num_rows())
		{
			$query->free_result();
			$query = $this->db->get_where('themes', [ 'name' => 'default' ]);
			
			if (!$query->num_rows())
			{
				$query->free_result();
				$this->db->limit(1);
				$query = $this->db->get('themes');
			}
		}
		
		$result = $query->row_array();
		$query->free_result();
		
		return $result;
	}
	
	/**
	 * Get id from name
	 */
	public function get_id($name)
	{
		$this->db->select('id');
		$query = $this->db->get_where('themes', [ 'name' => $name ]);
		
		$result = $query->result()[0]->id;
		$query->free_result();
		
		return $result;
	}
	
	/**
	 * Check if theme exists
	 */
	public function exists($id)
	{
		$query = $this->db->get_where('themes', [ 'id' => $id ]);
		$result = $query->num_rows() > 0;
		$query->free_result();
		return $result;
	}
	
	/**
	 * Update theme
	 */
	public function update($id, $data)
	{
		if ($this->exists($id))
		{
			$data = $this->filter_data($data);
			return $data !== FALSE && $this->db->update('themes', $data, [ 'id' => $id ]);
		}
		
		return FALSE;
	}
	
	/**
	 * Add theme
	 */
	public function add($data)
	{
		$data = $this->filter_data($data);
		return $data !== FALSE && $this->db->insert('themes', $data);
	}
	
	/**
	 * Delete theme
	 */
	public function delete($id)
	{
		return $this->exists($id) && count($this->get_list()) > 1 && $this->db->delete('themes', array('id' => $id)) !== FALSE;
	}
	
	/**
	 * Filter non existing fields from data
	*/
	private function filter_data($data)
	{
		if (is_array($data) && !empty($data))
		{
			foreach (array_keys($data) as $field)
			{
				if ($field == 'id' || !$this->db->field_exists($field, 'themes'))
				{
					unset($data[$field]);
				}
			}
			
			if (!empty($data)) return $data;
		}
		
		return FALSE;
	}
}