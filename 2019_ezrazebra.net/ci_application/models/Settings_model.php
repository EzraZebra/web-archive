<?php
class Settings_model extends CI_Model {

	public function __construct()
	{
		$query = $this->db->get('settings');
		$this->settings = $query->row_array();
		$query->free_result();
		
		if ($this->settings['date_format'] == null) $this->settings['date_format'] = '%Y-%m-%d';
		if ($this->settings['timezone'] == null) $this->settings['timezone'] = 'UTC';
		
		if ($this->settings['icon_styles'] == null) $this->settings['icon_styles'] = array();
		else $this->settings['icon_styles'] = explode(';', $this->settings['icon_styles']);
		
		if ($this->settings['icon_codes'] == null) $this->settings['icon_codes'] = array();
		else $this->settings['icon_codes'] = explode(';', $this->settings['icon_codes']);
	}
	
	public function update($data)
	{
		// filter the data
		foreach (array_keys($data) as $k)
		{
			if (!$this->db->field_exists($k, 'settings')) unset($data[$k]);
		}
		
		if (!empty($data)) 
		{
			if (isset($data['icon_styles'])) $data['icon_styles'] = implode(';', $data['icon_styles']);
			if (isset($data['icon_codes'])) $data['icon_codes'] = implode(';', $data['icon_codes']);
			
			if ($this->db->update('settings', $data))
			{
				$this->settings = array_merge($this->settings, $data);
				return TRUE;
			}
		}
		
		return FALSE;
	}
}