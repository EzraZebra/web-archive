<?php
class Content_model extends CI_Model {

	public function __construct()
	{
		// Loading DB in controller
		
		$this->types = array('pages', 'posts');
	}
	
	/**
	 * Get content list
	 */
	public function get_list($table='pages')
	{
		if (in_array($table, $this->types))
		{
			if ($table == 'pages')
			{
				$this->db->order_by('pos', 'ASC');
				$this->db->order_by('id', 'ASC');
			}
			else
			{
				$this->db->order_by('publish_date', 'DESC');
				$this->db->order_by('create_date', 'DESC');
				$this->db->order_by('id', 'DESC');
				$this->db->select('content.id, uri, title, create_date, publish_date');
				$this->db->join('content', 'content_id = content.id');
			}
			
			$query = $this->db->get($table);
			$result = $query->result();
			$query->free_result();
				
			return $result;
		}
		
		return FALSE;
	}
	
	/**
	 * Get content
	 */
	public function get($uri, $table='pages')
	{
		if ($this->exists($uri, $table))
		{
			if ($table != 'pages')
			{
				if ($table == 'posts') $this->db->select('text');
			
				$this->db->select('content.*, uri');
				$this->db->join('content', 'content_id = content.id');
			}
			
			$query = $this->db->get_where($table, [ 'uri' => $uri ]);
			$result = $query->row();
			$query->free_result();
			
			return $result;
		}
		
		return FALSE;
	}
	
	/**
	 * Check if content exists
	 */
	public function exists($uri, $table='pages')
	{
		if (in_array($table, $this->types))
		{
			$query = $this->db->get_where($table, [ 'uri' => $uri ]);
			$result = $query->num_rows() > 0;
			$query->free_result();
			return $result;
		}
		
		return FALSE;
	}
	
	/**
	 * Update content item
	 */
	public function update($uri, $data, $table='pages')
	{
		if ($this->exists($uri, $table))
		{
			$this->db->trans_start();
			
			$filter_data = $this->filter_data($table, $data);
			
			if (($filter_data === FALSE || $this->db->update($table, $filter_data, [ 'uri' => $uri ])) && $table != 'pages')
			{
				if (isset($filter_data['uri'])) $uri = $filter_data['uri'];
				$filter_data = $this->filter_data('content', $data);
				
				if ($filter_data !== FALSE)
				{
					$this->db->select('content_id');
					$query = $this->db->get_where($table, [ 'uri' => $uri ]);
					
					$result = $this->db->update('content', $filter_data, [ 'id' => $query->row()->content_id ]);
					$query->free_result();
				}
			}
			
			$this->db->trans_complete();
			
			return $this->db->trans_status() !== FALSE;
		}
		
		return FALSE;
	}
	
	/**
	 * Update multiple
	 */
	public function update_batch($data, $table='pages')
	{
		if (in_array($table, $this->types) && is_array($data) && !empty($data))
		{
			$errors = FALSE;
			
			foreach ($data as $uri => $d)
			{
				$errors = !$this->update($uri, $d, $table) || $errors;
			}
			
			return !$errors;
		}
		
		return FALSE;
	}
	
	/**
	 * Add content
	 */
	public function add($data, $table='pages')
	{
		if (in_array($table, $this->types))
		{
			if ($table != 'pages')
			{
				$type = $table;
				$table = 'content';
			}
			
			$filter_data = $this->filter_data($table, $data);
			
			if ($filter_data !== FALSE)
			{
				$this->db->trans_start();
				
				if (isset($type)) $filter_data['type'] = $type;
				if ($this->db->insert($table, $filter_data))
				{
					$id = $this->db->insert_id();
					
					if (isset($type))
					{
						$filter_data = $this->filter_data($type, $data);
						
						if ($filter_data !== FALSE)
						{
							$filter_data['content_id'] = $id;
							
							$this->db->insert($type, $filter_data);
						}
					}
				}
				
				$this->db->trans_complete();
					
				if ($this->db->trans_status() !== FALSE)
				{
					return $id;
				}
			}
		}
		
		return FALSE;
	}
	
	/**
	 * Delete content
	 */
	public function delete($uri, $table='pages')
	{
		if ($this->exists($uri, $table))
		{
			if ($table == 'pages') $this->db->select('id');
			else $this->db->select('content_id');
			
			$query = $this->db->get_where($table, [ 'uri' => $uri ]);
			
			if ($query->num_rows())
			{
				$row = $query->row();
				$query->free_result();
				
				$this->db->trans_start();
				
				if ($table == 'pages')
				{
					$this->db->delete('pages_content', array('page_id' => $row->id));
				}
				else
				{
					$this->db->delete('pages_content', array('content_id' => $row->content_id));
					$this->db->delete('content', array('id' => $row->content_id));
				}
				
				$this->db->delete($table, array('uri' => $uri));
				
				$this->db->trans_complete();
				
				return $this->db->trans_status() !== FALSE;
			}
		}
		
		return FALSE;
	}
	
	/**
	 * Get page content list
	 */
	public function get_page_content($uri, $admin_list = FALSE)
	{
		$this->db->select('id, all_content, all_posts');
		$query = $this->db->get_where('pages', [ 'uri' => $uri ]);
		
		if ($query->num_rows())
		{
			$page = $query->row();
			$query->free_result();
			
			if ($admin_list) $this->db->select('content.id, added, hidden');
			else $this->db->select('text');
			$this->db->select('uri, type, title, create_date, publish_date');
			
			$this->db->join('pages_content', 'pages_content.content_id = content.id AND page_id = '.$page->id, 'left');
			$this->db->join('posts', 'posts.content_id = content.id', 'left');
			
			if (!$page->all_content)
			{
				$this->db	->group_start()
								->where('page_id', $page->id)
								->where('added', 1)
							->group_end();
							
				if ($page->all_posts) // || other types
				{
					$this->db->or_group_start();
					
					if (!$admin_list) $this->db->where('hidden', null);
					
					// $this->db->	group_start();
					/*if ($page->all_posts)*/ $this->db->where('type', 'posts');
					// $this->db->	group_end();
					
					$this->db->group_end();
				}
			}
			elseif (!$admin_list) $this->db->where('hidden', null);
			
			$this->db->order_by('publish_date', 'DESC');
			
			$query = $this->db->get('content');
			$result = $query->result();
			$query->free_result();
			
			return $result;
		}
		
		return FALSE;
	}
	
	/**
	 * Get content page list
	 */
	public function get_content_pages($id_or_type)
	{
		if (in_array($id_or_type, $this->types)) $type = $id_or_type;
		else
		{
			$id = $id_or_type;
			$this->db->select('type');
			$query = $this->db->get_where('content', [ 'id' => $id ]);
			
			if ($query->num_rows()) $type = $query->row()->type;
			$query->free_result();
		}
		
		if (isset($type))
		{
			if (isset($id))
			{
				$this->db->join('pages_content', 'pages_content.content_id = '.$id.' AND page_id = pages.id', 'left');
				$this->db->select('pages.id, uri, added, hidden, title, all_content, all_'.$type.' AS all_type');
			}
			else $this->db->select('pages.id, uri, title, all_content, all_'.$type.' AS all_type');
			
			$this->db->order_by('pos', 'ASC');
			
			$query = $this->db->get('pages');
			$result = $query->result();
			$query->free_result();
			
			return $result;
		}
		
		return FALSE;
	}
	
	/**
	 * Hide/Show Page Content
	 */
	public function update_page_content($data)
	{
		$errors = FALSE;
		
		foreach ($data as $item)
		{
			$where = [ 'page_id' => $item['page_id'], 'content_id' => $item['content_id'] ];
			
			$this->db->select('added, hidden');
			$query = $this->db->get_where('pages_content', $where);
			
			if ($query->num_rows())
			{
				$row = $query->row();
				$query->free_result();
				
				if (!isset($item['hidden'])) $item['hidden'] = $row->hidden;
				if (!isset($item['added'])) $item['added'] = $row->added;
			
				if (!$item['hidden'] && !$item['added'])
				{
					$errors = $this->db->delete('pages_content', $where) === FALSE || $errors;
				}
				else
				{
					if ($item['hidden'] != $row->hidden)
					{
						$errors = !$this->db->update('pages_content', [ 'hidden' => $item['hidden'] ], $where) || $errors;
					}
					if ($item['added'] != $row->added)
					{
						$errors = !$this->db->update('pages_content', [ 'added' => $item['added'] ], $where) || $errors;
					}
				}
			}
			elseif ((isset($item['hidden']) && $item['hidden']) || (isset($item['added']) && $item['added']))
			{
				$insert = $where;
				$insert['added'] = isset($item['added']) ? $item['added'] : 0;
				$insert['hidden'] = isset($item['hidden']) ? $item['hidden'] : 0;
				$errors = !$this->db->insert('pages_content', $insert) || $errors;
			}
		}
		
		return !$errors;
	}
	
	/**
	 * Get next page position
	 */
	public function next_page_pos()
	{
		$this->db->select_max('pos');
		$query = $this->db->get('pages');
		$result = $query->row()->pos + 1;
		$query->free_result();
		return $result;
	}
	
	/**
	 * Filter non existing fields from data
	*/
	private function filter_data($table, $data)
	{
		if (is_array($data) && !empty($data))
		{
			foreach (array_keys($data) as $field)
			{
				if ($field == 'id' || $field == 'content_id' || $field == 'type' || !$this->db->field_exists($field, $table))
				{
					unset($data[$field]);
				}
			}
			
			if (!empty($data)) return $data;
		}
		
		return FALSE;
	}
}
?>