<?php

$lang['error_404_heading'] = '404 Page Not Found';
$lang['error_404_message'] = 'Oops, can\'t find that page.';