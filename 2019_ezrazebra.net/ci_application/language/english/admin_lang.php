<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// Permissions

// General
$lang['none']				= 'None';
$lang['nothing_here']		= 'Nothing here.';
$lang['never']				= 'Never';

// Validation
$lang['alphadash']	= 'alphanumeric characters, dashes or underscores';
$lang['error_csrf']	= 'Invalid form.';

// Actions
$lang['action_apply']		= 'Apply';
$lang['action_delete']		= 'Delete';
$lang['action_save']		= 'Save';
$lang['action_add']			= 'Add';
$lang['action_remove']		= 'Remove';
$lang['toggle_hide']		= 'Show or Hide';

// Results
$lang['something_wrong']	= 'Something went wrong.';
$lang['not_exist']			= 'That doesn\'t exist.';
$lang['file_not_exist']		= 'That file doesn\'t exist.';
$lang['save_success']		= 'Successfully saved.';
$lang['save_failed']		= 'Failed to save.';
$lang['save_errors']		= 'Errors occurred while saving.';
$lang['add_success']		= 'Successfully added.';
$lang['add_exists']			= 'That already exists.';
$lang['add_failed']			= 'Failed to add.';
$lang['apply_success']		= 'Successfully applied.';
$lang['apply_failed']		= 'Failed to apply.';
$lang['delete_success']		= 'Successfully deleted.';
$lang['delete_failed']		= 'Failed to delete.';
$lang['field_invalid_date'] = 'Invalid {field} date.';
$lang['file_not_css']		= 'That\'s not a CSS file.';

// Auth
$lang['title_noadmin']	 		= 'Insufficient priveleges.';
$lang['noadmin_message'] 		= 'You must be an administrator to view this page.';
$lang['title_control_panel']	= 'Control Panel';
$lang['title_login']			= 'Login';
$lang['auth_username']			= 'Username';
$lang['auth_email']				= 'Email';
$lang['auth_password']			= 'Password';
$lang['auth_password_confirm']	= 'Confirm Password';
$lang['action_login']			= 'Login';
$lang['action_logout']			= 'Logout';

// General Titles
$lang['title_edit']	= 'Edit ';
$lang['title_new']	= 'New ';

// Settings
$lang['title_settings']	= 'Settings';
$lang['title_general']	= 'General';
$lang['title_theme'] 	= 'Theme';
$lang['title_icons'] 	= 'Icon Fonts';

// Settings: date & time
$lang['general_date_format'] = 'Date Format';

// Settings: theme
$lang['theme_delete_last']				= 'Can\'t delete the last theme.';
$lang['theme_name']						= 'Name';
$lang['theme_font']						= 'Font';
$lang['theme_font_size']				= 'Font size';
$lang['theme_font_color']				= 'Font color';
$lang['theme_font_color_links']			= 'Links color';
$lang['theme_font_color_links_hover']	= 'Links hover color';
$lang['theme_ui_font']					= 'UI font';
$lang['theme_color_bg']					= 'Background color';
$lang['theme_header_color']				= 'Header color';
$lang['theme_header_font_color']		= 'Header font color';
$lang['theme_header_font_color_hover']	= 'Header font hover color';
$lang['theme_logo']						= 'Logo CSS';
$lang['theme_logo_content']				= 'Logo content';

// Settings: icons
$lang['icons_styles']			= 'Styles';
$lang['icons_codes']			= 'Codes';
$lang['icons_style']			= 'Style';
$lang['icons_code']				= 'Code';

// Content
$lang['content_uri']			= 'URI';
$lang['content_title']			= 'Title';
$lang['content_date_publish']	= 'Published';
$lang['content_date_created']	= 'Created';
$lang['content_visibility']		= 'Visibility';
$lang['content_type']			= 'Type';

// Content types
$lang['title_pages']	= 'Pages';
$lang['type_pages']		= 'Page';
$lang['title_content']	= 'Content';
$lang['title_posts']	= 'Posts';
$lang['type_posts']		= 'Post';

// Pages
$lang['pages_position']		= 'Position';
$lang['pages_icon']			= 'Icon';
$lang['pages_all_content']	= 'All Content';
$lang['pages_all_posts']	= 'All Posts';

// Users
$lang['title_user']						= 'User';
$lang['title_users']					= 'Users';
$lang['users_groups']					= 'Groups';
$lang['users_last_login']				= 'Last Login';
$lang['users_created']					= 'Created';
$lang['users_active']					= 'Active';
$lang['users_password_change_message']	= 'Leave blank to keep unchanged.';
$lang['users_delete_self']   			= 'Can\'t delete yourself.';
?>