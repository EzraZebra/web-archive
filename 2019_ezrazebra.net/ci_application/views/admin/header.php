<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
	
	<title><?= $title; ?></title>
	
	<style><?php $this->load->view('admin/style.php'); ?></style>
	
	<?php foreach ($styles as $style) : ?>
		<link rel="stylesheet" href="<?= $style; ?>"></link>
	<?php endforeach; ?>
	
	<?php foreach ($scripts as $script) : ?>
		<script src="<?= $script; ?>"></script>
	<?php endforeach; ?>
</head>
<body>