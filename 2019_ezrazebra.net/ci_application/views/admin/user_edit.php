<div id="content-wrapper">
	<h1><?= $title; ?></h1>
	
	<div class="info-message"><?php echo $message;?></div>
	
	<?php echo form_open(site_url('admin/users/'.$user_edit));?>

		<div class="sticky-submit">
			<span class="button-icon">
				<button	name="user_save" type="submit" class="icon-apply" title="<?= lang('action_save'); ?>"></button>
			</span>
			<?php if ($user_edit != 'new' && !$user_self) : ?>
				<span class="button-icon">
					<button	name="user_delete" value="delete" type="submit" class="icon-delete" title="<?= lang('action_delete'); ?>"></button>
				</span>
			<? endif ?>
		</div>

		<?php if ($user_edit != 'new') : ?>
			<div id="user-dates">
				<div>
					<label><?= lang('users_last_login'); ?></label>
					<?= $last_login == null ? lang('never') : datetime($last_login, $date_format); ?>
				</div>
				<div>
					<label><?= lang('users_created'); ?></label> 
					<?= datetime($created, $date_format); ?>
				</div>
			</div>
			<?= form_hidden('id', $id); ?>
		<?php endif; ?>

		<div class="table table-simple">
			<div>
				<span><?= lang('auth_username', 'username'); ?></span>
				<span><?= form_input($username_input); ?><?= form_error('username'); ?></span>
			</div>

			<div>
				<span><?= lang('auth_email', 'email'); ?></span>
				<span><?= form_input($email); ?><?= form_error('email'); ?></span>
			</div>

			<div>
				<span>
					<?= lang('auth_password', 'password'); ?>
				</span>
				<span>
					<?= form_input($password); ?><?= form_error('password'); ?>
					<?php if ($user_edit != 'new') : ?>
						<div class="form-comment"><?= lang('users_password_change_message'); ?></div>
					<?php endif ?>
				</span>
			</div>

			<div>
				<span><?= lang('auth_password_confirm', 'password_confirm'); ?></span>
				<span><?= form_input($password_confirm); ?><?= form_error('password_confirm'); ?></span>
			</div>

			<div><span>
				<?= form_input($active); ?><?= form_error('active'); ?>
				<?= lang('users_active', 'active'); ?>
			</span></div>
			
			<?php foreach ($groups as $group) : ?>
				<div><span>
					<?= form_input($group); ?><?= form_error($group['value']); ?>
					<?= form_label($group_names[$group['value']], $group['id']); ?>
				</span></div>
			<?php endforeach ?>
		</div>

	<?php echo form_close();?>
</div>