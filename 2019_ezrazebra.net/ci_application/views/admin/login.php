<div id="content-wrapper" class="noaccess">
	<h1><?= $title; ?></h1>

	<div class="info-message"><?php echo $message;?></div>
	<br />

	<?php echo form_open(current_url());?>

		<div class="table table-simple">
		  <p>
			<span><?php echo lang($lang_identity, 'identity');?></span>
			<span><?php echo form_input($identity);?></span>
		  </p>

		  <p>
			<span><?php echo lang('auth_password', 'password');?></span>
			<span><?php echo form_input($password);?></span>
		  </p>
		</div>
		
		<div><?php echo form_submit('submit', lang('action_login'));?></div>

	<?php echo form_close();?>
</div>