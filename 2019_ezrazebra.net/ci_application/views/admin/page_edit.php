<div id="content-wrapper">
	<h1><?= $title; ?></h1>
	
	<div class="info-message"><?php echo $message;?></div>
	
	<?php echo form_open(uri_string());?>

		<div class="sticky-submit">
			<span class="button-icon">
				<button	name="content_save" type="submit" class="icon-apply" title="<?= lang('action_save'); ?>"></button>
			</span>
			<?php if ($content_edit != 'new') : ?>
				<span class="button-icon">
					<button	name="content_delete" value="delete" type="submit"
							class="icon-delete" title="<?= lang('action_delete'); ?>"></button>
				</span>
			<? endif ?>
		</div>
		
		<section class="subsection">
			<div class="flex-row-wrap main-input">
				<span>
					<?= lang('pages_position', $page_pos['id']); ?><br />
					<?= form_input($page_pos); ?>
				</span>
				<span>
					<?= lang('content_uri', $content_uri['id']); ?><br />
					<?= form_input($content_uri); ?>
				</span>
				<span>
					<?= lang('content_title', $content_title['id']); ?><br />
					<?= form_input($content_title); ?>
				</span>
				<span>
					<label><?= lang('content_visibility'); ?></label><br />
					<span>
						<span class="cbx-icon" title="<?= lang('toggle_hide'); ?>">
							<?= form_checkbox($show_title_cbx); ?>
							<span class="icon-show checked"></span>
							<span class="icon-hide"></span>
						</span><?=
						form_label(lang('content_title'), $show_title_cbx['id'], ['title' => lang('toggle_hide') ]); ?>
						
						<span class="cbx-icon" title="<?= lang('toggle_hide'); ?>">
							<?= form_checkbox($show_icon_cbx); ?>
							<span class="icon-show checked"></span>
							<span class="icon-hide"></span>
						</span><?=
						form_label(lang('pages_icon'), $show_icon_cbx['id'], ['title' => lang('toggle_hide') ]); ?>
					</span>
				</span>
			</div>
			
			<fieldset class="fieldset">
				<legend><?= lang('pages_icon'); ?></legend>
				<div class="flex-row-wrap">
					<?php foreach ($select_icons as $k => $icon) : ?>
						<span class="flex-row-center">
							<?= form_radio($icon)
							. form_label($icon['value'] == null ? lang('none') : null, $icon['id'], [ 'class' => $icon['value'] ]); ?>
						</span>
					<?php endforeach; ?>
				</div>
			</fieldset>
		</section>
	
		<!-- PAGE CONTENT -->
		<h2><?= lang('title_content'); ?></h2>
		
		<section class="subsection">
			<div class="content-cbxs">
				<span class="cbx-icon" title="<?= lang('toggle_hide'); ?>">
					<?= form_checkbox($all_content_cbx); ?>
					<span class="icon-show checked"></span>
					<span class="icon-hide"></span>
				</span><?=
				form_label(lang('pages_all_content'), $all_content_cbx['id'], ['title' => lang('toggle_hide') ]); ?>
				
				<span class="cbx-icon" title="<?= lang('toggle_hide'); ?>">
					<?= form_checkbox($all_posts_cbx); ?>
					<span class="icon-show checked"></span>
					<span class="icon-hide"></span>
				</span><?=
				form_label(lang('pages_all_posts'), $all_posts_cbx['id'], ['title' => lang('toggle_hide') ]); ?>
			</div>
		</section>
		
		<?php if (!empty($content_list)) : ?>
			<div class="table">
				<div class="table-title">
					<span></span>
					<span></span>
					<span><?= lang('content_type'); ?></span>
					<span><?= lang('content_date_created'); ?></span>
					<span><?= lang('content_date_publish'); ?></span>
				</div>
				
				<?php foreach ($content_list as $content) : ?>
					<div <?php if (isset($content->content_cbx['checked']) && $content->content_cbx['value'] == 'toggle_hide') : ?>
							class="content-hidden" <?php endif; ?>>
							
						<?= form_hidden($content->id_hidden); ?>
						
						<span class="content-hidden-vis" title="<?= lang($content->content_cbx['value']); ?>"><span class="cbx-icon">
							<?= form_checkbox($content->content_cbx); ?>
							<?php if ($content->content_cbx['value'] == 'toggle_hide') : ?>
								<span class="icon-show"></span>
								<span class="icon-hide checked"></span>
							<?php elseif ($content->content_cbx['value'] == 'action_remove') : ?>
								<span class="icon-remove"></span>
								<span class="icon-remove checked"></span>
							<?php endif; ?>
						</span></span>
						
						<span><a href="<?= site_url('admin/'.$content->type.'/'.$content->uri); ?>">
							<?= $content->title; ?>
						</a></span>
						
						<span><?= lang('type_'.$content->type); ?></span>
						<span><?= $content->create_date; ?></span>
						<span><?= $content->publish_date; ?></span>
					</div>
				<?php endforeach ?>
			</div>
		<?php endif; ?>

	<?php echo form_close();?>
</div>

<script>
	const allContent = document.getElementById('<?= $all_content_cbx['id']; ?>');
	const checkboxes = document.querySelectorAll('.content-cbxs input[type=checkbox]');
	
	for (var i=0; i<checkboxes.length; i++) {
		if(checkboxes[i].addEventListener){
			checkboxes[i].addEventListener('change', toggleCheckboxes, false);  //Modern browsers
		}else if(checkboxes[i].attachEvent){
			checkboxes[i].attachEvent('onchange', toggleCheckboxes);            //Old IE
		}
	}

	function toggleCheckboxes() {
		if (this == allContent) {
			for (var i=0; i<checkboxes.length; i++) {
				checkboxes[i].checked = allContent.checked;
			}
		}
		else if (!this.checked) allContent.checked = false;
	}
</script>