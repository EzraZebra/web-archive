<div id="content-wrapper">
	<h1><?= $title; ?></h1>
	<div class="info-message"><?php echo $message;?></div>
	
	<!-- DATE-TIME -->
	<h2 id="<?= url_title(lang('title_general')); ?>"><?= lang('title_general'); ?></h2>
	<div class="info-message"><?php echo $general_message;?></div>

	<section class="subsection">
	
		<?= form_open(uri_string(), 'class="fieldset"'); ?>
			<div>
				<?= lang('general_date_format', 'date_format') . form_input($date_format); ?>
				<a 	href="https://github.com/iamkun/dayjs/blob/dev/docs/en/API-reference.md#list-of-all-available-formats"
					class="icon-help" target="_blank"></a>
			</div>
			
			<span class="button-cont">
				<span class="button-icon">
					<button name="dateformat_apply" id="dateformat_apply" type="submit"
							class="icon-apply" value="apply" title="<?= lang('action_apply'); ?>"></button>
				</span>
			</span>
		<?= form_close();?>
		
	</section>
	
	<!-- THEME -->
	<h2 id="<?= url_title(lang('title_theme')); ?>"><?= lang('title_theme'); ?></h2>
	<div class="info-message"><?php echo $theme_message;?></div>
	
	<section class="subsection">
	
		<!-- THEME SELECT -->
		<?= form_open(uri_string()); ?>
			<?= form_dropdown($theme_select); ?>
			<span class="button-cont">
				<span class="button-icon">
					<button name="theme_apply" id="theme_apply" type="submit"
							class="icon-apply" value="apply" title="<?= lang('action_apply'); ?>"></button>
				</span>
				<span class="button-icon">
					<button name="theme_delete" id="theme_delete" type="submit"
							class="icon-delete" value="delete" title="<?= lang('action_delete'); ?>"></button>
				</span>
			</span>
			<?= form_error('theme_select'); ?>
		<?php echo form_close();?>
		
		<!-- THEME SETTINGS -->
		<?= form_open(uri_string(), 'class="fieldset"'); ?>
			<div class="flex-row-wrap fixed-width-large">
				<?php foreach ($theme_edit as $field => $input) : ?>
					<div><?php
						if (isset($input['class']) && $input['class'] == 'color-input')
						{
							echo form_label(lang('theme_'.$field), $input['name'], [ 'class' => 'color-label' ])
								. '<div class="color-picker"></div>';
						}
						else echo lang('theme_'.$field, $input['name']);
						
						if ($input['type'] == 'textarea')
						{
							echo form_textarea($input);
						}
						elseif ($input['type'] == 'number')
						{
							echo '<br />'.form_input($input).'px';
						}
						else echo form_input($input);
						
						echo form_error($input['id']);
					?></div>
				<?php endforeach ?>
				
				<datalist id="font-list">
					<option value='Georgia, serif'>
					<option value='"Palatino Linotype", "Book Antiqua", Palatino, serif'>
					<option value='"Times New Roman", Times, serif'>
					<option value='Arial, Helvetica, sans-serif'>
					<option value='"Arial Black", Gadget, sans-serif'>
					<option value='"Comic Sans MS", cursive, sans-serif'>
					<option value='Impact, Charcoal, sans-serif'>
					<option value='"Lucida Sans Unicode", "Lucida Grande", sans-serif'>
					<option value='Tahoma, Geneva, sans-serif'>
					<option value='"Trebuchet MS", Helvetica, sans-serif'>
					<option value='Verdana, Geneva, sans-serif'>
					<option value='"Courier New", Courier, monospace'>
					<option value='"Lucida Console", Monaco, monospace'>
				</datalist>
			</div>
			
			<span class="button-cont">
				<span class="button-icon">
					<button name="theme_save" id="theme_save" type="submit"
							class="icon-apply" value="save" title="<?= lang('action_apply'); ?>"></button>
				</span>
				<span class="button-icon">
					<button name="theme_add" id="theme_add" type="submit"
							class="icon-create" value="add" title="<?= lang('action_add'); ?>"></button>
				</span>
			</span>
		<?= form_close();?>
	</section>
	
	<!-- ICON FONTS -->
	<h2 id="<?= url_title(lang('title_icons')); ?>"><?= lang('title_icons'); ?></h2>
	<div class="info-message"><?php echo $icons_message;?></div>
	
	<section class="subsection">
		<!-- STYLES -->
		<fieldset class="fieldset">
			<legend><?= lang('icons_styles', 'icon_style_add'); ?></legend>
			
			<?= form_open(uri_string()) ?>
				<?= form_input($icon_style_add); ?>
				<span class="button-cont"><span class="button-icon">
					<button	name="icon_style_save" id="icon_style_save" type="submit"
							class="icon-create" value="add" title="<?= lang('action_add'); ?>"></button>
				</span></span>
				<?= form_error($icon_style_add['id']); ?>
			<?= form_close(); ?>
			
			<?php if (isset($icon_styles_select['options']) && !empty($icon_styles_select['options'])) : ?>
				<?= form_open(uri_string()); ?>
					<?= form_dropdown($icon_styles_select); ?>
					<span class="button-cont"><span class="button-icon">
						<button	name="icon_style_delete" id="icon_style_delete" type="submit"
								class="icon-delete" value="delete" title="<?= lang('action_delete'); ?>"></button>
					</span></span>
				<?= form_close(); ?>
			<?php endif; ?>
		</fieldset>
		<br />
		
		<!-- CODES -->
		<fieldset class="fieldset">
			<legend><?= lang('icons_codes', 'icon_code_add'); ?></legend>
			
			<?= form_open(uri_string()); ?>
				<?= form_input($icon_code_add); ?>
				<span class="button-cont"><span class="button-icon">
					<button	name="icon_code_save" id="icon_code_save" type="submit"
							class="icon-create" value="add" title="<?= lang('action_add'); ?>"></button>
				</span></span>
				<?= form_error('icon_code_add'); ?>
			<?= form_close(); ?>
			
			<?php if (isset($icon_codes_cbx) && !empty($icon_codes_cbx)) : ?>
				<?= form_open(uri_string()); ?>
					<div class="flex-row-wrap fixed-width">
						<?php foreach ($icon_codes_cbx as $cbx) : ?>
							<span class="flex-row-baseline">
								<?= form_checkbox($cbx) . form_label($cbx['value'], $cbx['id'], [ 'class' => $cbx['value'] ]); ?>
							</span>
						<?php endforeach; ?>
					</div>
					<span class="button-cont"><span class="button-icon">
						<button	name="icon_code_delete" id="icon_code_delete" type="submit"
								class="icon-delete" value="delete" title="<?= lang('action_delete'); ?>"></button>
					</span></span>
				<?= form_close(); ?>
			<?php endif; ?>
		</fieldset>
	</section>
</div>