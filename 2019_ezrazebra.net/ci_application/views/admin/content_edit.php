<div id="content-wrapper">
	<h1><?= $title; ?></h1>
	
	<div class="info-message"><?php echo $message; ?></div>

	<?php echo form_open(site_url('admin/'.$content_type.'/'.$content_edit), 'id="contentForm"');?>

		<div class="sticky-submit">
			<span class="button-icon">
				<button	name="content_save" type="submit" class="icon-apply" title="<?= lang('action_save'); ?>"></button>
			</span>
			<?php if ($content_edit != 'new') : ?>
				<span class="button-icon">
					<button	name="content_delete" value="delete" type="submit"
							class="icon-delete" title="<?= lang('action_delete'); ?>"></button>
				</span>
			<? endif; ?>
		</div>

		<section class="subsection">
			<div class="flex-row-wrap main-input">
				<span>
					<?= lang('content_uri', $content_uri['id']); ?><br />
					<?= form_input($content_uri); ?>
				</span>
				<span>
					<?= lang('content_title', $content_title['id']); ?><br />
					<?= form_input($content_title); ?>
				</span>
				<span>
					<?= lang('content_date_created', $content_create_date['id']); ?><br />
					<?= form_input($content_create_date); ?>
				</span>
				<span>
					<?= lang('content_date_publish', $content_publish_date['id']); ?><br />
					<?= form_input($content_publish_date); ?>
					<?= form_input($content_publish_time); ?>
				</span>
			</div>
			
			<fieldset class="fieldset">
				<legend><?= lang('title_pages'); ?></legend>
				<div class="flex-row-wrap">
					<?php foreach ($pages_list as $page) : ?>
						<div <?php if (isset($page->pages_cbx['checked']) && $page->pages_cbx['value'] == 'hide') : ?>
								class="page-hidden" <?php endif; ?>>
								
							<?= form_hidden($page->id_hidden); ?>
							
							<span class="flex-row-center">
								<span class="cbx-icon" title="<?= lang($page->pages_cbx['value']); ?>">
									<?= form_checkbox($page->pages_cbx); ?>
									<?php if ($page->pages_cbx['value'] == 'toggle_hide') : ?>
										<span class="icon-show"></span>
										<span class="icon-hide checked"></span>
									<?php elseif ($page->pages_cbx['value'] == 'action_remove') : ?>
										<span class="icon-remove"></span>
										<span class="icon-remove checked"></span>
									<?php elseif ($page->pages_cbx['value'] == 'action_add') : ?>
										<span class="icon-add"></span>
										<span class="icon-add checked"></span>
									<?php endif; ?>
								</span>
							
								<a href="<?= site_url('admin/pages/'.$page->uri); ?>"><?= $page->title; ?></a>
							</span>
						</div>
					<?php endforeach; ?>
				</div>
			</fieldset>
		</section>
		
		<section class="subsection">
			<?php if ($content_type == 'posts') : ?>
				<?= form_textarea($content_text); ?>
				<div id="editSection"></div>
			<?php endif; ?>
		</section>

	<?= form_close();?>
</div>