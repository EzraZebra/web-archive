	<nav><div>
		<div>
			<a href="<?= base_url(); ?>" class="icon-home"></a>
			<a href="<?= site_url('admin/logout'); ?>" class="icon-logout" title="<?= lang('action_logout'); ?>"></a>
		</div>
		<div><a href="<?= site_url('admin/settings'); ?>"><?= lang('title_settings'); ?></a></div>
		<?php if (uri_string() == 'admin/settings' || uri_string() == 'admin') : ?>
			<div class="nav-sub"><a href="#<?= url_title(lang('title_general')); ?>"><?= lang('title_general'); ?></a></div>
			<div class="nav-sub"><a href="#<?= url_title(lang('title_theme')); ?>"><?= lang('title_theme'); ?></a></div>
			<div class="nav-sub"><a href="#<?= url_title(lang('title_icons')); ?>"><?= lang('title_icons'); ?></a></div>
		<?php endif; ?>
		<div><a href="<?= site_url('admin/pages'); ?>"><?= lang('title_pages'); ?></a></div>
		<div><a href="<?= site_url('admin/posts'); ?>"><?= lang('title_posts'); ?></a></div>
		<div><a href="<?= site_url('admin/users'); ?>"><?= lang('title_users'); ?></a></div>
	</div></nav>