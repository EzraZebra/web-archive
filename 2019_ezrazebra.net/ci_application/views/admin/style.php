html {
	height: 100%; width: 100%;
	overflow: auto;
	
	font-family: <?= $theme['font']; ?>;
	font-size: <?= $theme['font_size']; ?>px;
	background-color: <?= $theme['color_bg']; ?>;
	color: <?= $theme['font_color']; ?>;
}

body {
	display: flex;
	flex-direction: row;
	margin: 0;
	min-height: 100%; min-width: 100%;
}

a {
	color: <?= $theme['font_color_links']; ?>;
	font-weight: bold; text-decoration: none;
}

a:hover {
	color: <?= $theme['font_color_links_hover']; ?>;
}

h1 {
	margin: 5px 0;
}

h2 {
	margin: 0; margin-bottom: 5px;
}

/* GENERAL */
	#content-wrapper {
		flex-grow: 1;
		padding: 20px;
	}

	.subsection {
		margin-bottom: 25px; margin-left: 5px;
	}
	
/* CONTENT */
	.content-hidden > :not(.content-hidden-vis) {
		opacity: 0.5;
	}

/* USERS */
	#user-dates {
		margin-bottom: 5px;
	}

	#user-dates, #user-dates > div {
		display: flex;
		align-items: flex-start;
		flex-direction: row;
		margin-right: 10px;
		font-size: 0.875rem;
	}

	#user-dates span {
		margin-right: 2px;
		font-weight: bold;
	}

/* AUTH */
	.noaccess {
		text-align: center;
	}
	
	.noaccess form {
		display: inline-block;
	}

/* SIDEBAR */
	nav {
		font-size: 1.25rem; font-weight: bold;
		color: <?= $theme['header_font_color']; ?>;
		background-color: <?= $theme['header_color']; ?>;
	}
	
	nav > div {
		display: flex;
		align-items: flex-end;
		flex-direction: column;
		
		padding: 12px 20px;
		position: sticky; top: 0;
	}
	
	nav > div > div:first-child {
		font-size: 1rem;
	}

	nav a {
		color: <?= $theme['header_font_color']; ?>;
		text-decoration: none;
	}

	nav a:hover {
		color: <?= $theme['header_font_color_hover']; ?>;
	}
	
	.nav-sub {
		white-space: nowrap;
		font-size: 1rem !important;
	}
	
/* FORMS */
	form {
		margin-bottom: 5px;
	}
	
	form > * {
		vertical-align: middle;
	}

	form > div:not(.error) {
		margin-bottom: 5px;
	}
	
	label:after {
		content: ':';
		position: relative; left: -1px;
	}
	
	input[type=checkbox] + label:after,
	input[type=radio] + label:after,
	.cbx-icon + label:after {
		content: '';
		position: initial; left: auto;
		width: 0; height: 0;
	}

	label, legend {
		font-weight: bold;
	}
	
	input:disabled + label {
		opacity: 0.5;
	}
	
	input[type=number][size="2"] {
		width: 2rem;
	}
	
	input[type=checkbox], input[type=radio] {
		margin: 3px; margin-left: 5px;
	}
	
	textarea.font-code {
		font-family: "Courier New", Courier, monospace !important;
		font-size: 12px;
	}
	
	.error {
		display: inline-block;
		color: #cc1111;
		font-size: 0.9rem;
	}

	.info-message, .info-message .error {
		font-size: 1rem;
	}
	
	.info-message {
		margin-bottom: 5px;
		padding: 3px 5px;
		color: #11cc11; font-weight: bold;
	}
	
	.info-message:empty {
		display: none;
	}
	
	.info-message p {
		margin: 0;
	}
	
	.form-comment {
		font-size: 0.75rem;
	}
	
	.button-cont, .sticky-submit, .info-message {
		display: inline-block;
		background-color: #C9E9F9;
		border-radius: 5px;
	}
	
	.button-cont {
		padding: 5px; height: 1rem;
		line-height: 1rem;
		font-size: 1rem;
	}
	
	.button-cont > * {
		vertical-align: middle;
	}
	
	.sticky-submit {
		position: sticky; top: 10px;
		height: 1.2rem;
		line-height: 1.2rem;
		padding: 5px; margin-bottom: 3px;
		z-index: 100000;
	}
	
	.sticky-submit > * {
		width: 1.2rem !important; height: 1.2rem !important;
		font-size: 1.2rem !important;
		vertical-align: middle;
	}
	
	.main-input > * {
		margin: 5px;
	}
	
	.fieldset {
		display: inline-block;
		max-width: 1155px; /* 7*(150+15) */
		padding: 5px; margin: 5px 0;
		font-size: 0.9rem;
		border: 1px solid <?= $theme['font_color']; ?>;
		border-radius: 3px;
	}
	
	.fieldset button {
		font-size: 1rem;
	}
	
/* ICON BUTTONS */
	span.button-icon,
	span.cbx-icon,
	a.button-icon {
		position: relative;
		display: inline-block;
		width: 1rem; height: 1rem;
		line-height: 1rem;
		overflow: visible;
		font-size: 1rem;
		vertical-align: middle; text-align: center;
		white-space: nowrap;
	}
	
	span.cbx-icon {
		margin-right: 0.2em;
	}
	
	span.button-icon > button[class^="icon-"],
	span.button-icon > button[class*=" icon-"],
	span.cbx-icon > span,
	span.cbx-icon > input[type=checkbox] {
		position: absolute; top: 0; left: 0;
		display: inline-block;
		width: 1em !important; height: 1em !important;
		padding: 0;
		overflow: visible;
		font-size: 1em !important;
		line-height: 1em !important;
		text-align: center;
		cursor: pointer;
		border: none; background: none;
	}
	
	span.button-icon > button[class^="icon-"]:before,
	span.button-icon > button[class*=" icon-"]:before,
	span.cbx-icon > span:before,
	span.cbx-icon > span,
	span.cbx-icon > input[type=checkbox],
	a.button-icon:before {
		position: absolute; top: 0; left: 0;
		display: inline-block;
		width: 1em !important; height: 1em !important;
		overflow: visible;
		font-size: 1em !important;
		line-height: 1em !important;
		text-align: center;
		margin: 0;
	}
	
	span.cbx-icon > input[type=checkbox]:checked ~ span:not(.checked),
	span.cbx-icon > input[type=checkbox]:not(:checked) ~ span.checked {
		display: none;
	}
	
	span.cbx-icon > span {
		z-index: 1;
	}
	
	span.cbx-icon > input[type=checkbox] {
		z-index: 2;
		opacity: 0;
	}
	
	span.button-icon > button[class^="icon-"]:active:hover,
	span.button-icon > button[class*=" icon-"]:active:hover,
	span.cbx-icon > input[type=checkbox]:active:hover ~ span,
	a.button-icon:active:hover {
		top: 1px; left: 1px;
	}
	
	button.icon-apply, button.icon-create,
	span.cbx-icon > span.icon-show,
	span.cbx-icon > span.icon-add,
	a.button-icon.icon-apply, a.button-icon.icon-create,
	a.button-icon.icon-user-create {
		color: #11cc11;
	}
	
	button.icon-delete,
	a.button-icon.icon-delete,
	span.cbx-icon > span.icon-remove,
	span.cbx-icon > span.icon-hide {
		color: #cc1111;
	}
	
	span.cbx-icon > span.icon-remove:not(.checked),
	span.cbx-icon > span.icon-add:not(.checked)	{
		opacity: 0.25;
	}

/* TABLES */
	.table {
		display: table;
	}

	.table > * {
		display: table-row;
	}

	.table > .table-title {
		font-weight: bold;
	}

	.table:not(.table-simple) > *:nth-child(2n) {
		background-color: rgba(255, 255, 255, 0.05);
	}

	.table:not(.table-simple) > *:nth-child(2n+1) {
		background-color: rgba(0, 0, 0, 0.05);
	}

	.table:not(.table-simple) > .table-title {
		color: <?= $theme['header_font_color']; ?>;
		background-color: <?= $theme['header_color']; ?>;
	}

	.table > * > * {
		display: table-cell;
		padding: 3px 5px;
		vertical-align: middle;
	}
	
/* FLEX */
	.flex-row-baseline, .flex-row-center, .flex-row-wrap {
		display: flex !important;
		flex-direction: row;
		margin-right: 5px;
	}
	.flex-row-baseline { align-items: baseline; }
	.flex-row-center { align-items: center; }
	.flex-row-wrap { flex-wrap: wrap; }
	.flex-row-wrap > * { margin: 2.5px; }
	
	.fixed-width > * {
		box-sizing: border-box;
		width: 135px; max-width: 135px;
		margin-right: 5px; margin-top: 5px;
	}
	
	.fixed-width-large > * {
		box-sizing: border-box;
		width: 150px; max-width: 150px;
		margin-right: 10px; margin-top: 10px;
		padding: 3px;
	}
	
	.fixed-width-large textarea {
		max-width: 144px; height: 50px;
	}
	
/* COLOR PICKERS */
	.pickr {
		float: right;
	}
	
	.pcr-button {
		border: 1px dashed black !important;
		background-color: white !important;
	}
	
	.color-label {
		display: inline-block;
		max-width: calc(140px - 2rem);
	}
	
	.color-input {
		display: none;
	}

/* "FONT" ICONS */
	.icon-empty:before { content: ' '; } /* '' */