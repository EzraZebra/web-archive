<div id="content-wrapper">
	<h1><?= $title; ?></h1>
	
	<div class="info-message"><?php echo $message;?></div>
	
	<?= form_open(current_url());?>
		<div class="sticky-submit">
			<?php if ($content_list !== FALSE) : ?>
				<span class="button-icon">
					<button	name="content_list_apply" type="submit" class="icon-apply" title="<?= lang('action_apply'); ?>"></button>
				</span>
			<?php endif; ?>
			<a	href="<?= current_url().'/new'; ?>" class="icon-create button-icon"
				title="<?= lang('title_new').lang('type_'.$content_type); ?>"></a>
		</div>
		
		<?php if ($content_list !== FALSE) : ?>
			<div class="table">
				<div class="table-title">
					<span></span>
					<span><?= lang('content_date_created'); ?></span>
					<span><?= lang('content_date_publish'); ?></span>
				</div>
				
				<?php foreach ($content_list as $content) : ?>
					<div>
						<?= form_hidden($content->uri_hidden); ?>
						<span><a href="<?= current_url().'/'.htmlspecialchars($content->uri, ENT_QUOTES, 'UTF-8'); ?>">
							<?= htmlspecialchars($content->title, ENT_QUOTES, 'UTF-8'); ?>
						</a></span>
						
						<span><?= form_input($content->create_date_input); ?></span>
						<span>
							<?= form_input($content->publish_date_input); ?>
							<?= form_input($content->publish_time_input); ?>
						</span>
					</div>
				<?php endforeach; ?>
			</div>
			
		<?php else: echo lang('nothing_here'); endif; ?>
	<?php echo form_close();?>
</div>