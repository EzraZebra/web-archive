<div id="content-wrapper">
	<h1><?= $title; ?></h1>
	
	<div class="info-message"><?php echo $message;?></div>
	
	<div class="sticky-submit">
		<a href="<?= current_url().'/new'; ?>" class="icon-user-create button-icon" title="<?= lang('title_new').lang('title_user'); ?>"></a>
	</div>
	
	<div class="table">
		<div class="table-title">
			<div><?= lang('auth_username'); ?></div>
			<div><?= lang('users_groups'); ?></div>
			<div><?= lang('users_last_login'); ?></div>
		</div>
		<?php foreach ($users as $user) : ?>
			<div>
				<div><a href="<?= current_url().'/'.htmlspecialchars($user->username, ENT_QUOTES, 'UTF-8'); ?>">
					<?= htmlspecialchars($user->username, ENT_QUOTES, 'UTF-8'); ?>
				</a></div>
				<div><?php foreach ($user->groups as $group) echo htmlspecialchars($group->name, ENT_QUOTES, 'UTF-8').'<br />'; ?></div>
				<div><?= $user->last_login === null ? lang('never') : datetime($user->last_login, $date_format); ?></div>
			</div>
		<?php endforeach ?>
	</div>
</div>