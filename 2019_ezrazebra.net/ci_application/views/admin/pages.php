<div id="content-wrapper">
	<h1><?= $title; ?></h1>
	
	<div class="info-message"><?php echo $message;?></div>
	
	<?= form_open(uri_string());?>
	
		<div class="sticky-submit">
			<?php if ($pages !== FALSE) : ?>
				<span class="button-icon">
					<button	name="pages_apply" type="submit" class="icon-apply" title="<?= lang('action_apply'); ?>"></button>
				</span>
			<?php endif; ?>
			<a href="<?= current_url().'/new'; ?>" class="icon-create button-icon" title="<?= lang('title_new').lang('type_pages'); ?>"></a>
		</div>
		
		<?php if ($pages !== FALSE) : ?>
		
			<div class="table">
				<?php foreach ($pages as $page) : ?>
					<div>
						<?= form_hidden($page->uri_hidden); ?>
						
						<div><?= form_input($page->pos_input); ?></div>
						
						<div><a	href="<?= current_url().'/'.$page->uri; ?>"
								class="<?= $page->icon == null ? 'icon-empty' : $page->icon; ?>"><?=
							$page->title; ?>
						</a></div>
						
						<div>
							<span class="cbx-icon" title="<?= lang('toggle_hide'); ?>">
								<?= form_checkbox($page->show_title_cbx); ?>
								<span class="icon-show checked"></span>
								<span class="icon-hide"></span>
							</span><?=
							form_label(lang('content_title'), $page->show_title_cbx['id'], ['title' => lang('toggle_hide') ]); ?>
							
							<span class="cbx-icon" title="<?= lang('toggle_hide'); ?>">
								<?= form_checkbox($page->show_icon_cbx); ?>
								<span class="icon-show checked"></span>
								<span class="icon-hide"></span>
							</span><?=
							form_label(lang('pages_icon'), $page->show_icon_cbx['id'], ['title' => lang('toggle_hide') ]); ?>
						</div>
					</div>
				<?php endforeach ?>
			</div>
			
		<?php else : echo '<br />' . lang('nothing_here'); endif; ?>
		
	<?php echo form_close();?>
</div>