body {
	font-family: <?= $theme['font']; ?>;
	font-size: <?= $theme['font_size']; ?>px;
	color: <?= $theme['font_color']; ?>;
	margin: 0;
	background-color: <?= $theme['color_bg']; ?>;
}

header {
	padding: 12px 15px;
	background-color: <?= $theme['header_color']; ?>;
}

header, header > nav {
	display: flex;
}

header > nav {
	flex: auto;
	flex-wrap: wrap;
	align-items: flex-end;
}

h2 { margin-bottom: 0; }

.h2date {
	margin-bottom: 0.75em;
	font-style: italic; font-size: 0.77em;
}

.header-item {
	margin-right: 0.5em;
	font-size: 1rem;
}

.header-user {
	float: right;
	font-size: 1rem;
}

.header-item > a {
	color: <?= $theme['header_font_color']; ?>; text-align: center;
	font-weight: bold; text-decoration: none;
}

.header-item > a:hover {
	color: <?= $theme['header_font_color_hover']; ?>;
	border-color: <?= $theme['header_font_color_hover']; ?> !important;
}

#logo {
	display: inline-block;
	margin-right: 12px; padding: 0 4px; padding-top: 1px;
	border: 3px solid <?= $theme['header_font_color']; ?>; border-radius: 6px;
	font-size: 2.125rem; line-height: 80%;
	<?= $theme['logo']; ?>
}

#logo:before {
	content: '<?= $theme['logo_content']; ?>';
}

#content-wrapper {
	width: 85%;
	margin: 20px auto;
}

.content-div {
	opacity: 0.25;
	border-bottom: 1px solid <?= $theme['font_color']; ?>;
}

article {
	padding: 2px 10px;
	background-color: rgba(0, 0, 0, 0.03);
}