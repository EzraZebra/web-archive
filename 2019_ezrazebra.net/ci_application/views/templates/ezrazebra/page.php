<div id="content-wrapper">
	<h1><?= $title; ?></h1>
	
	<section class="page-content">
	
		<?php foreach ($content as $item) : ?>
			<h2><?= $item->title; ?></h2>
			<?= datetime($item->publish_date, $date_format, 'h2date'); ?>
			
			<?php if ($item->type == 'posts') : ?>
				<article class="post-text"><?= $item->text; ?></article>
			<?php endif; ?>
			<div class="content-div"></div>
		<?php endforeach; ?>
	
	</section>
</div>