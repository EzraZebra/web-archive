<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
	
	<title><?= $title; ?></title>
	
	<style><?php $this->load->view('templates/ezrazebra/style.php'); ?></style>
	
	<?php foreach ($styles as $style) : ?>
		<link rel="stylesheet" href="<?= $style; ?>"></link>
	<?php endforeach; ?>
	
	<?php foreach ($scripts as $script) : ?>
		<script src="<?= $script; ?>"></script>
	<?php endforeach; ?>
</head>
<body>
<header>
	<div class="header-item"><a id="logo" href="<?= base_url(); ?>"></a></div>
	<nav>
		<?php foreach ($pages as $page) : ?>
			<?php if ($page->show_title || ($page->show_icon && $page->icon != null)): ?>
				<div class="header-item">
					<a href="<?= base_url($page->uri); ?>"
						<?php if (!$page->show_title) echo 'title="'.$page->title.'"'; ?>
						<?php if ($page->show_icon && $page->icon != null) echo 'class="'.$page->icon.'"'; ?>
					><?php if ($page->show_title) echo $page->title; ?></a>
				</div>
			<?php endif; ?>
		<?php endforeach; ?>
	</nav>
	<?php if ($this->ion_auth->logged_in()) : ?>
		<?php if ($this->ion_auth->is_admin()) : ?>
			<div class="header-item header-user"><a href="<?= base_url('admin'); ?>" class="icon-config" title="<?= lang('title_control_panel'); ?>"></a></div>
		<? endif ?>
		<div class="header-item header-user"><a href="<?= base_url('admin/logout'); ?>" class="icon-logout" title="<?= lang('action_logout'); ?>"></a></div>
	<? endif ?>
</header>