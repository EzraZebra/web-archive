<?php
class Admin extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		
		$this->load->database();
		
		$this->load->library(['ion_auth', 'form_validation']);
		$this->load->model([ 'settings_model', 'theme_model', 'content_model' ]);
		$this->load->helper([ 'url', 'language', 'date', 'ez_cms']);

		$this->lang->load('admin');
		$this->lang->load('misc');
		
		$this->form_validation->set_error_delimiters(	$this->config->item('error_start_delimiter', 'ion_auth'),
														$this->config->item('error_end_delimiter', 'ion_auth')	);

		$this->alphadash_pattern = '[a-zA-Z0-9_-]+';
		$this->uri_rules = 'trim|required|alpha_dash';
		
		$this->redirect_method = 'refresh';
		
		$this->data['theme'] = $this->theme_model->get($this->settings_model->settings['theme']);
		$this->data['styles'] = $this->settings_model->settings['icon_styles'];
		$this->data['scripts'] = array();
		$this->data['scripts_dom'] = array();
	}
	
	/**
	 * Main controller
	 * --> Settings page
	 */
	public function index()
	{
		$this->settings();
	}
	
	/**
	 * Logout Controller
	 */
	public function logout()
	{
		$this->ion_auth->logout();
		redirect(base_url(), $this->redirect_method);
	}
	
	/**
	 * Settings Controller
	 */
	public function settings()
	{
		// Only proceed if user has permission
		if (!$this->permissions_check()) return;
		
		// Valid form submitted?
		if ($this->form_check())
		{
			$redirect = TRUE;
			
			// Apply Date Format
			if ($this->input->post('dateformat_apply'))
			{
				$section = 'general';
				
				// Set rules & validate
				$this->form_validation->set_rules('date_format', lang('general_date_format'), 'trim|required');
				if ($this->form_validation->run() === TRUE)
				{
					// Update
					if ($this->settings_model->update([ 'date_format' => $this->input->post('date_format') ]))
					{
						$message = lang('apply_success');
					}
					
					// Failed
					else $message = $this->error('apply_failed');
				}
				else $message = validation_errors();
			}
			
			// Apply Theme
			elseif ($this->input->post('theme_apply'))
			{
				$section = 'theme';
				
				// Doesn't exist
				if (!$this->theme_model->exists($this->input->post('theme_select')))
				{
					$message = $this->error('not_exist');
				}
				
				// Success
				elseif ($this->settings_model->update([ 'theme' => $this->input->post('theme_select') ]))
				{
					$message = lang('apply_success');
				}
				
				// Failed
				else $message = $this->error('apply_failed');
			}
			
			// Delete theme
			elseif ($this->input->post('theme_delete'))
			{
				$section = 'theme';
				
				// Doesn't exist
				if (!$this->theme_model->exists($this->input->post('theme_select')))
				{
					$message = $this->error('not_exist');
				}
				
				// Last theme
				elseif (count($this->theme_model->get_list()) == 1)
				{
					$message = $this->error('theme_delete_last');
				}
				
				// Success
				elseif ($this->theme_model->delete($this->input->post('theme_select')))
				{
					$message = lang('delete_success');
				}
				
				// Failed
				else $message = $this->error('delete_failed');
			}
			
			// Theme Save/Add
			elseif ($this->input->post('theme_save') || $this->input->post('theme_add'))
			{
				$section = 'theme';
				$fields = $this->theme_model->get_fields();
				
				// Set rules
				foreach ($fields as $field)
				{
					if ($field != 'logo')
					{
						$this->form_validation->set_rules('theme_'.$field, lang('theme_'.$field), 'trim|required');
					}
				}
					
				if ($this->input->post('theme_add') || $this->input->post('theme_name') != $this->data['theme']['name'])
				{
					$this->form_validation->set_rules(	'theme_name',
														lang('theme_name'),
														'trim|required|is_unique[themes.name]');
				}
					
				// Validate form
				if ($this->form_validation->run() === TRUE)
				{
					// Get data
					foreach ($fields as $field)
					{
						$data[$field] = $this->input->post('theme_'.$field);
					}
						
					// Save
					if ($this->input->post('theme_save'))
					{
						$success = $this->theme_model->update($this->data['theme']['id'], $data);
					}
					
					// Add & Set new as current
					elseif ($this->input->post('theme_add'))
					{
						$success = $this->theme_model->add($data);
						if ($success) $this->settings_model->update([ 'theme' => $this->theme_model->get_id($data['name']) ]);
					}
				
					// Success
					if ($success) $message = lang('save_success');
					else $message = $this->error('save_failed');
				}
				
				$redirect = isset($success) && $success;
			}
			
			// Add Icon Style/Code
			elseif ($this->input->post('icon_style_save') || $this->input->post('icon_code_save'))
			{
				$section = 'icons';
				$type = $this->input->post('icon_style_save') ? 'style' : 'code';
				
				// Define rules
				$this->tmp_list = $this->settings_model->settings['icon_'.$type.'s'];
				$rules = [
					'trim', 'required',
					[	'is_unique',
						function($str)
						{
							if (in_array($str, $this->tmp_list))
							{
								$this->form_validation->set_message('is_unique', $this->error('add_exists'));
								return FALSE;
							}
							return TRUE;
						}
					]
				];
					
				// Style -> must be css file
				if ($type == 'style')
				{
					$rules[] = [
						'css_file',
						function($str)
						{
							$path = $_SERVER['DOCUMENT_ROOT'] . $str;
							
							// Local file doesn't exist
							if (!file_exists($path))
							{
								$scheme = parse_url($str, PHP_URL_SCHEME);
								$path = parse_url($str, PHP_URL_PATH); // Override $path
								
								// Valid http url with path -> get mime type
								if (($scheme == 'https' || $scheme == 'http')
									&& $path != null
									&& filter_var($str, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED))
								{
									$type = get_headers(
										$str, 1,
										stream_context_create(array('http' => array('method' => 'HEAD')))
									)['Content-Type'];
								}
								
								// Not a valid http url/no path
								else
								{
									$this->form_validation->set_message('css_file', $this->error('file_not_exist'));
									return FALSE;
								}
							}
							
							// Local file exists -> get mime type
							else $type = mime_content_type($path);
							
							// Implode type if it's an array
							if (is_array($type)) $type = implode('/', $type);
							
							// Check if it's a css file
							if ($type != 'text/css' && ($type != 'text/plain' || pathinfo($path)['extension'] != 'css'))
							{
								$this->form_validation->set_message('css_file', $this->error('file_not_css'));
								return FALSE;
							}
							
							return TRUE;
						}
					];
				}
				
				// Icon -> must be alphadash (alphanumeric/dash/underscore)
				else $rules[] = 'alpha_dash';
				
				// Set the rules
				$this->form_validation->set_rules('icon_'.$type.'_add', lang('icons_'.$type), $rules);									
				
				// Validate form
				if ($this->form_validation->run() === TRUE)
				{
					// Get current list and append
					$data = $this->settings_model->settings['icon_'.$type.'s'];
					$data[] = $this->input->post('icon_'.$type.'_add');
					
					// Success
					if ($this->settings_model->update([ 'icon_'.$type.'s' => $data ]))
					{
						$message = lang('add_success');
						$success = TRUE;
					}
					
					// Failed
					else $message = $this->error('add_failed');
				}
				
				$redirect = isset($success) && $success;
				unset($this->tmp_list);
			}
			
			// Delete Icon Style
			elseif ($this->input->post('icon_style_delete'))
			{
				$section = 'icons';
				
				$key = array_search($this->input->post('icon_styles_select'), $this->settings_model->settings['icon_styles']);
				
				// Doesn't exist
				if ($key === FALSE) $message = $this->error('not_exist');
				else
				{
					// Get data
					$data = $this->settings_model->settings['icon_styles'];
					unset($data[$key]);
					
					// Success
					if ($this->settings_model->update([ 'icon_styles' => $data ]))
					{
						$message = lang('delete_success');
					}
					
					// Failed
					else $message = $this->error('delete_failed');
				}
			}
			
			// Delete Icon Codes
			elseif ($this->input->post('icon_code_delete') && $this->input->post('icon_codes'))
			{
				$section = 'icons';
				
				// Get data
				$data = $this->settings_model->settings['icon_codes'];
				$codes = $this->input->post('icon_codes');
				$deleted = FALSE;
				foreach ($codes as $code)
				{
					$key = array_search($code, $data);
					if ($key !== FALSE)
					{
						unset($data[$key]);
						$deleted = TRUE;
					}
				}
				
				// None of the codes exist
				if (!$deleted) $message = $this->error('not_exist');
				
				// Success
				elseif ($this->settings_model->update([ 'icon_codes' => $data ]))
				{
					$message = lang('delete_success');
				}
				
				// Failed
				else $message = $this->error('delete_failed');
			}
			
			// Set flashdata & redirect
			$location = uri_string();
			if (isset($section))
			{
				if ($this->redirect_method == 'location') $location .= '#'.url_title(lang('title_'.$section));
				if (isset($message)) $this->session->set_flashdata($section.'_message', $message);
			}
			
			if ($redirect) redirect($location, $this->redirect_method);
		}
		
		// Date format
		$value = $this->form_validation->set_value('date_format');
		if ($value == null) $value = $this->settings_model->settings['date_format'];
		$this->data['date_format'] = [
			'name' => 'date_format',
			'id' => 'date_format',
			'type' => 'text',
			'value' => $value,
			'required' => 'required'
		];
		
		// Theme select
		$value = $this->form_validation->set_value('theme_select');
		if ($value == null) $value = $this->data['theme']['id'];
		$this->data['theme_select'] = [
			'name' => 'theme_select',
			'id' => 'theme_select',
			'type' => 'select',
			'selected' => $value
		];
		
		// Color picker script
		data_add_script('color_picker', $this->data);
		
		// Populate theme select & current theme form
		if (!isset($fields)) $fields = $this->theme_model->get_fields();
		foreach ($this->theme_model->get_list() as $theme)
		{
			$this->data['theme_select']['options'][$theme['id']] = $theme['name'];
			// Current theme form
			if ($theme['id'] === $this->data['theme_select']['selected'])
			{
				foreach ($fields as $field)
				{
					$value = $this->form_validation->set_value('theme_'.$field);
					if ($value == null) $value = $this->data['theme'][$field];
					$this->data['theme_edit'][$field] = [
						'name' => 'theme_'.$field,
						'id' => 'theme_'.$field,
						'value' => $value,
					];
					
					if ($field == 'logo')
					{
						$this->data['theme_edit'][$field]['type'] = 'textarea';
						$this->data['theme_edit'][$field]['class'] = 'font-code';
					}
					else
					{
						$this->data['theme_edit'][$field]['required'] = 'required';
						
						if ($field == 'font_size')
						{
							$this->data['theme_edit'][$field]['type'] = 'number';
							$this->data['theme_edit'][$field]['size'] = 2;
						}
						else
						{
							$this->data['theme_edit'][$field]['type'] = 'text';
							if ($field == 'font')
							{
								$this->data['theme_edit'][$field]['list'] = 'font-list';
							}
							elseif (strpos($field, 'color') !== FALSE)
							{
								$this->data['theme_edit'][$field]['class'] = 'color-input';
							}
						}
					}
				}
			}
		}
		
		// Icon styles select
		$this->data['icon_styles_select'] = [
			'name' => 'icon_styles_select',
			'id' => 'icon_styles_select',
			'type' => 'select',
			'selected' => $this->form_validation->set_value('theme_select')
		];
		
		foreach ($this->settings_model->settings['icon_styles'] as $style)
		{
			$this->data['icon_styles_select']['options'][$style] = $style;
		}
			
		$this->data['icon_style_add'] = [
			'name' => 'icon_style_add',
			'id' => 'icon_style_add',
			'type' => 'text',
			'value' => $this->form_validation->set_value('icon_style_add'),
			//'required' => 'required'
		];
		
		// Icon codes checkboxes
		$this->data['icon_codes_cbx'] = array();
		if (!isset($codes) && $this->input->post('icon_codes[]'))
		{
			$codes = $this->input->post('icon_codes[]');
		}
		foreach ($this->settings_model->settings['icon_codes'] as $code)
		{
			array_push($this->data['icon_codes_cbx'], [
				'name' => 'icon_codes[]',
				'id' => 'icon_code_'.$code,
				'type' => 'checkbox',
				'value' => $code,
			]);
			if (isset($codes) && in_array($code, $codes))
			{
				$this->data['icon_codes_cbx']['checked'] = 'checked';
			}
		}
			
		$this->data['icon_code_add'] = [
			'name' => 'icon_code_add',
			'id' => 'icon_code_add',
			'type' => 'text',
			'value' => $this->form_validation->set_value('icon_code_add'),
			'required' => 'required',
			'pattern' => $this->alphadash_pattern,
			'title' => lang('alphadash')
		];
		
		// Messages
		$this->data['general_message'] = $this->session->flashdata('general_message');
		$this->data['theme_message'] = $this->session->flashdata('theme_message');
		$this->data['icons_message'] = $this->session->flashdata('icons_message');
		$this->session->set_flashdata('general_message', null);
		$this->session->set_flashdata('theme_message', null);
		$this->session->set_flashdata('icons_message', null);
		
		// Output the page
		$this->output('settings');
	}
	
	/**
	 * Pages Controller
	 */
	public function pages($slug = FALSE)
	{
		// slug set -> show edit page
		if ($slug !== FALSE)
		{
			$this->data['content_type'] = 'pages';
			$this->content_edit($slug);
			return;
		}
		
		// Only proceed if user has permission
		if (!$this->permissions_check()) return;
		
		// Valid form submitted?
		if ($this->input->post('uris') && $this->form_check())
		{
			// Set validation rules
			foreach ($this->input->post('uris') as $uri)
			{
				$this->form_validation->set_rules('position_'.$uri, lang('pages_position'), 'trim|required|integer');
			}
			
			if ($this->form_validation->run() === TRUE)
			{
				// Get Data
				foreach ($this->input->post('uris') as $uri)
				{
					$data[$uri]['pos'] = $this->input->post('position_'.$uri);
					$data[$uri]['show_title'] = (bool)$this->input->post('show_title_'.$uri);
					$data[$uri]['show_icon'] = (bool)$this->input->post('show_icon_'.$uri);
				}
				
				// Update
				if (isset($data) && $this->content_model->update_batch($data))
				{
					// Success: Save message and redirect
					$this->session->set_flashdata('message', lang('save_success'));
					redirect(uri_string(), $this->redirect_method);
				}
				// One or more errors
				else $this->session->set_flashdata('message', $this->error('save_errors'));
			}
			
			// Validation errors
			else $this->session->set_flashdata('message', validation_errors());
		}
		
		// Get the list of content
		$this->data['pages'] = $this->content_model->get_list();
		
		// Errors retrieving content
		if ($this->data['pages'] === FALSE)
		{
			$this->data['message'] = $this->error('something_wrong');
		}
		
		// No content found
		elseif (empty($this->data['pages'])) $this->data['pages'] = FALSE;
		
		// Get form data
		else
		{
			foreach ($this->data['pages'] as $k => $page)
			{
				$this->data['pages'][$k]->uri_hidden = [
					'uris[]' => $page->uri
				];
				
				$value = $this->form_validation->set_value('position_'.$page->uri);
				if ($value == null) $value = $page->pos;
				$this->data['pages'][$k]->pos_input = [
					'name' => 'position_'.$page->uri,
					'type' => 'number',
					'value' => $value,
					'size' => '2',
					'required' => 'required'
				];
				
				$this->data['pages'][$k]->show_title_cbx = [
					'name' => 'show_title_'.$page->uri,
					'id' => 'show_title_'.$page->uri,
					'type' => 'checkbox',
					'value' => $page->uri,
				];
				
				$this->data['pages'][$k]->show_icon_cbx = [
					'name' => 'show_icon_'.$page->uri,
					'id' => 'show_icon_'.$page->uri,
					'type' => 'checkbox',
					'value' => $page->uri
				];
				
				// Fill checkboxes
				if ($this->input->post('show_title_'.$page->uri) || $page->show_title)
				{
					$this->data['pages'][$k]->show_title_cbx['checked'] = 'checked';
				}
					
				if ($this->input->post('show_icon_'.$page->uri) || $page->show_icon)
				{
					$this->data['pages'][$k]->show_icon_cbx['checked'] = 'checked';
				}
			}
		}
		
		// Output the page
		$this->output('pages');
	}
	
	/**
	 * Posts Controller
	 */
	public function posts($slug = FALSE)
	{
		$this->data['content_type'] = 'posts';
			
		// Show edit page if there a slug, else show the list
		if ($slug === FALSE) $this->content_list();
		else $this->content_edit($slug);
	}
	
	/**
	 * Content List
	 */
	private function content_list()
	{
		// Only proceed if user has permission
		if (!$this->permissions_check()) return;
		
		// Valid form submitted?
		if ($this->input->post('uris') && $this->form_check())
		{
			foreach ($this->input->post('uris') as $uri)
			{
				$this->form_validation->set_rules(	'create_date_'.$uri,
													lang('content_date_created'), 'trim|required|callback_valid_date');
				$this->form_validation->set_rules(	'publish_date_'.$uri,
													lang('content_date_publish'), 'trim|required|callback_valid_date');
				$this->form_validation->set_rules(	'publish_time_'.$uri,
													lang('content_date_publish'), 'trim|required|callback_valid_date');
			}
	
			if ($this->form_validation->run() === TRUE)
			{
				foreach ($this->input->post('uris') as $uri)
				{
					$data[$uri]['create_date'] = strtotime($this->input->post('create_date_'.$uri));
					$data[$uri]['publish_date'] = strtotime($this->input->post('publish_date_'.$uri)
																. ' ' . $this->input->post('publish_time_'.$uri));
					if ($this->input->post('utcOffset'))
					{
						$data[$uri]['publish_date'] -= (int)$this->input->post('utcOffset') * 60;
					}
				}
				
				if (isset($data) && $this->content_model->update_batch($data, $this->data['content_type']))
				{
					$this->session->set_flashdata('message', lang('save_success'));
					redirect(uri_string(), $this->redirect_method);
				}
				else $this->session->set_flashdata('message', $this->error('save_errors'));
			}
			else $this->session->set_flashdata('message', validation_errors());
		}
		
		$this->data['content_list'] = $this->content_model->get_list($this->data['content_type']);
		
		if ($this->data['content_list'] === FALSE)
		{
			$this->data['message'] = $this->error('something_wrong');
		}
		elseif (empty($this->data['content_list']))
		{
			$this->data['content_list'] = FALSE;
		}
		else
		{
			// Add datetime script for publish date input
			data_add_script('datetime_form', $this->data);
			
			foreach ($this->data['content_list'] as $k => $content)
			{
				$this->data['content_list'][$k]->uri_hidden = [
					 'uris[]' => $content->uri,
				];
				
				$date = $this->form_validation->set_value('create_date_'.$content->uri);
				if ($date == null) $date = date('Y-m-d', $content->create_date);
				$this->data['content_list'][$k]->create_date_input = [
					'name' => 'create_date_'.$content->uri,
					'id' => 'create_date_'.$content->uri,
					'type' => 'date',
					'value' => $date,
					'max' => date('Y-m-d'),
					'required' => 'required'
				];
				
				$date = $this->form_validation->set_value('publish_date_'.$content->uri);
				if ($date == null) $date = date('Y-m-d', $content->publish_date);
				$this->data['content_list'][$k]->publish_date_input = [
					'name' => 'publish_date_'.$content->uri,
					'id' => 'publish_date_'.$content->uri,
					'type' => 'date',
					'value' => $date,
					'required' => 'required'
				];
			
				$date = $this->form_validation->set_value('publish_time_'.$content->uri);
				if ($date == null) $date = date('H:i', $content->publish_date);
				$this->data['content_list'][$k]->publish_time_input = [
					'name' => 'publish_time_'.$content->uri,
					'id' => 'publish_time_'.$content->uri,
					'type' => 'time',
					'value' => $date,
					'required' => 'required',
					'data-dateid' => 'publish_date_'.$content->uri,
				];
			}
		}
		
		$this->output('content');
	}
	
	/**
	 * Content Edit
	 */
	private function content_edit($slug)
	{
		// Only proceed if user has permission
		if (!$this->permissions_check()) return;
		
		// Invalid slug -> new content
		if ($slug != 'new' && !$this->content_model->exists($slug, $this->data['content_type']))
		{
			$this->data['content_edit'] = 'new';
		}
		else $this->data['content_edit'] = $slug;
		
		// Get content & title
		if ($this->data['content_edit'] == 'new')
		{
			$content = FALSE;
			$this->data['title'] = lang('title_new').lang('type_'.$this->data['content_type']);
		}
		else
		{
			$content = $this->content_model->get($slug, $this->data['content_type']);
			$this->data['title'] = lang('title_edit').lang('type_'.$this->data['content_type']);
			
			if ($content === FALSE) $this->session->set_flashdata('message', $this->error('something_wrong'));
			else $this->data['title'] .= ': ' . $content->title;
		}
		
		// Valid form submitted?
		if (($this->data['content_edit'] == 'new' || $content !== FALSE) && $this->form_check())
		{
			// Delete content
			if ($this->input->post('content_delete'))
			{
				if ($content !== FALSE) // Doesn't exist -> msg already set
				{
					// Success
					if ($this->content_model->delete($slug, $this->data['content_type']))
					{
						$this->session->set_flashdata('message', lang('delete_success'));
					
						// Redirect to main page
						redirect(site_url('admin/'.$this->data['content_type']), $this->redirect_method);
					}
					
					// Failed
					else
					{
						$this->session->set_flashdata('message', $this->error('delete_failed'));
					
						// Redirect to content page
						redirect(site_url('admin/'.$this->data['content_type']).'/'.$content->uri, $this->redirect_method);
					}
				}
				else $this->session->set_flashdata('message', $this->error('not_exist'));
			}
			
			// Edit/Create content
			else
			{
				// Set validation rules
				$this->form_validation->set_rules('title',lang('content_title'), 'trim|required');
				
				if ($content === FALSE || trim($this->input->post('uri')) !== $content->uri)
				{
					$data['uri'] = $this->input->post('uri');
					$this->form_validation->set_rules(	'uri',
														lang('content_uri'),
														$this->uri_rules.'|is_unique['.$this->data['content_type'].'.uri]');
				}
				
				// Pages rules
				if ($this->data['content_type'] == 'pages')
				{
					$this->form_validation->set_rules('position', lang('pages_position'), 'trim|required|integer');
				}
				// Content rules
				else
				{
					$this->form_validation->set_rules(	'create_date',
														lang('content_date_created'),
														'trim|required|callback_valid_date');
					$this->form_validation->set_rules(	'publish_date',
														lang('content_date_publish'),
														'trim|required|callback_valid_date');
					$this->form_validation->set_rules(	'publish_time',
														lang('content_date_publish'),
														'trim|required|callback_valid_date');
				}
				
				// Validate
				if ($this->form_validation->run() === TRUE)
				{
					// Get Data
					$data['title'] = $this->input->post('title');
					
					// Page data
					if ($this->data['content_type'] == 'pages')
					{
						$data['pos'] = $this->input->post('position');
						$data['show_title'] = (bool)$this->input->post('show_title');
						$data['show_icon'] = (bool)$this->input->post('show_icon');
						$data['icon'] = $this->input->post('select_icon[]')[0];
						$data['all_content'] = (bool)$this->input->post('all_content');
						$data['all_posts'] = (bool)$this->input->post('all_posts');
					}
					// Content data
					else
					{
						$data['create_date'] = strtotime($this->input->post('create_date'));
						$data['publish_date'] = strtotime($this->input->post('publish_date')
															. ' ' . $this->input->post('publish_time'));
						if ($this->input->post('utcOffset'))
						{
							$data['publish_date'] -= (int)$this->input->post('utcOffset') * 60;
						}
					
						if ($this->data['content_type'] == 'posts') $data['text'] = $this->input->post('content_text');
					}
					
					// Add new content
					if ($content === FALSE)
					{
						$new_id = $this->content_model->add($data, $this->data['content_type']);
						$success = $new_id !== FALSE;
					}
					
					// Update content
					else $success = $this->content_model->update($content->uri, $data, $this->data['content_type']);
					
					// Update page_content
					$content_success = TRUE;
					if ($success && ($content !== FALSE || $this->data['content_type'] != 'pages'))
					{
						if ($this->data['content_type'] == 'pages')
						{
							$main_index = 'page_id';
							$loop_index = 'content_id';
						}
						else
						{
							$main_index = 'content_id';
							$loop_index = 'page_id';
						}
						$main_id = $content !== FALSE ? $content->id : $new_id;
						
						if ($this->input->post('hide_ids[]'))
						{
							foreach ($this->input->post('hide_ids[]') as $id)
							{
								$page_content_data[$id][$main_index] = $main_id;
								$page_content_data[$id][$loop_index] = $id;
								$page_content_data[$id]['hidden'] = (bool)$this->input->post('content_hide_'.$id);
							}
						}
						if ($this->input->post('remove_ids[]'))
						{
							foreach ($this->input->post('remove_ids[]') as $id)
							{
								$page_content_data[$id][$main_index] = $main_id;
								$page_content_data[$id][$loop_index] = $id;
								$page_content_data[$id]['added'] = !(bool)$this->input->post('content_remove_'.$id);
							}
						}
						if ($this->input->post('add_ids[]'))
						{
							foreach ($this->input->post('add_ids[]') as $id)
							{
								$page_content_data[$id][$main_index] = $main_id;
								$page_content_data[$id][$loop_index] = $id;
								$page_content_data[$id]['added'] = (bool)$this->input->post('content_add_'.$id);
							}
						}
						
						if (isset($page_content_data))
						{
							$content_success = $this->content_model->update_page_content($page_content_data);
						}
					}
					
					// Set flashdata & redirect
					if ($success)
					{
						if ($content_success) $this->session->set_flashdata('message', lang('save_success'));
						else $this->session->set_flashdata('message', $this->error('save_errors'));
						redirect(	site_url('admin/'.$this->data['content_type'].'/'.(isset($data['uri']) ? $data['uri'] : $content->uri)),
									$this->redirect_method);
					}
					else $this->session->set_flashdata('message', $this->error('save_failed'));
				}
				
				// Invalid form
				else $this->session->set_flashdata('message', validation_errors());
			}
		}
		
		// Prepare form
		$value = $this->form_validation->set_value('uri');
		if ($value == null)
		{
			if ($slug == 'new' || $slug == null)
			{
				if ($content !== FALSE) $value = $content->uri;
			}
			else $value = $slug;
		}
		$this->data['content_uri'] = [
			'name' => 'uri',
			'id' => 'uri',
			'type' => 'text',
			'value' => $value,
			'required' => 'required',
			'pattern' => $this->alphadash_pattern,
			'title' => lang('alphadash')
		];
		
		$value = $this->form_validation->set_value('title');
		if ($value == null && $content !== FALSE) $value = $content->title;
		$this->data['content_title'] = [
			'name' => 'title',
			'id' => 'title',
			'type' => 'text',
			'value' => $value,
			'required' => 'required'
		];
	
		// Pages
		if ($this->data['content_type'] == 'pages')
		{
			$value = $this->form_validation->set_value('position');
			if ($value == null) $value = $content !== FALSE ? $content->pos : $this->content_model->next_page_pos();
			$this->data['page_pos'] = [
				'name' => 'position',
				'id' => 'position',
				'type' => 'number',
				'value' => $value,
				'size' => '2',
				'required' => 'required'
			];
			
			$this->data['show_title_cbx'] = [
				'name' => 'show_title',
				'id' => 'show_title',
				'type' => 'checkbox',
				'value' => 'show_title',
			];
			if ($content === FALSE || $content->show_title) $this->data['show_title_cbx']['checked'] = 'checked';
			
			$this->data['show_icon_cbx'] = [
				'name' => 'show_icon',
				'id' => 'show_icon',
				'type' => 'checkbox',
				'value' => 'show_icon',
			];
			if ($content === FALSE || $content->show_icon) $this->data['show_icon_cbx']['checked'] = 'checked';
			
			$this->data['select_icons'] = array([
				'name' => 'select_icon[]',
				'id' => 'select_icon_none',
				'type' => 'radio',
				'value' => null
			]);
			
			// Get icons
			if ($this->input->post('select_icon[]')) $value = $this->input->post('select_icon[]')[0];
			else $value = $content !== FALSE ? $content->icon : null;
			foreach ($this->settings_model->settings['icon_codes'] as $k => $icon)
			{
				$this->data['select_icons'][$k+1] = [
					'name' => 'select_icon[]',
					'id' => 'select_icon_'.$icon,
					'type' => 'radio',
					'value' => $icon
				];
				
				// Get title/icon checked state
				if ($value == $icon)
				{
					$this->data['select_icons'][$k+1]['checked'] = 'checked';
					$icon_checked = TRUE;
				}
			}
			if (!isset($icon_checked)) $this->data['select_icons'][0]['checked'] = 'checked';
			
			$this->data['all_content_cbx'] = [
				'name' => 'all_content',
				'id' => 'all_content',
				'type' => 'checkbox',
				'value' => 'all_content',
			];
			if ($content !== FALSE && $content->all_content) $this->data['all_content_cbx']['checked'] = 'checked';
			
			$this->data['all_posts_cbx'] = [
				'name' => 'all_posts',
				'id' => 'all_posts',
				'type' => 'checkbox',
				'value' => 'all_posts',
			];
			if ($content !== FALSE && $content->all_posts) $this->data['all_posts_cbx']['checked'] = 'checked';
			
			// Get content list
			if ($content !== FALSE)
			{
				$this->data['content_list'] = $this->content_model->get_page_content($content->uri, TRUE);
				if ($this->data['content_list'] === FALSE) $this->data['content_list'] = array();
				
				foreach ($this->data['content_list'] as $k => $item)
				{
					// Auto content
					if ($content->all_content || ($item->type == 'posts' && $content->all_posts))
					{
						$this->data['content_list'][$k]->id_hidden = [ 'hide_ids[]' => $item->id ];
						
						$this->data['content_list'][$k]->content_cbx = [
							'name' => 'content_hide_'.$item->id,
							'id' => 'content_hide_'.$item->id,
							'type' => 'checkbox',
							'value' => 'toggle_hide',
						];
						if ($item->hidden) $this->data['content_list'][$k]->content_cbx['checked'] = 'checked';
					}
					// Added content
					elseif ($item->added)
					{
						$this->data['content_list'][$k]->id_hidden = [ 'remove_ids[]' => $item->id ];
						
						$this->data['content_list'][$k]->content_cbx = [
							'name' => 'content_remove_'.$item->id,
							'id' => 'content_remove_'.$item->id,
							'type' => 'checkbox',
							'value' => 'action_remove',
						];
					}
			
					// Format dates
					$this->data['content_list'][$k]->create_date = date('Y-m-d', $item->create_date);
					$this->data['content_list'][$k]->publish_date = date('Y-m-d', $item->publish_date);
				}
			}
		}
		
		// Content (not pages) form
		else
		{
			$value = $this->form_validation->set_value('create_date');
			if ($value == null) $value = $content !== FALSE ? date('Y-m-d', $content->create_date) : date('Y-m-d');
			$this->data['content_create_date'] = [
				'name' => 'create_date',
				'id' => 'create_date',
				'type' => 'date',
				'value' => $value,
				'max' => date('Y-m-d'),
				'required' => 'required'
			];
			
			// Add datetime script for publish date input
			data_add_script('datetime_form', $this->data);
			
			$value = $this->form_validation->set_value('publish_date');
			if ($value == null) $value = $content !== FALSE ? date('Y-m-d', $content->publish_date) : date('Y-m-d');
			$this->data['content_publish_date'] = [
				'name' => 'publish_date',
				'id' => 'publish_date',
				'type' => 'date',
				'value' => $value,
				'required' => 'required',
			];
			
			$value = $this->form_validation->set_value('publish_time');
			if ($value == null) $value = $content !== FALSE ? date('H:i', $content->publish_date) : date('H:i');
			$this->data['content_publish_time'] = [
				'name' => 'publish_time',
				'id' => 'publish_time',
				'type' => 'time',
				'value' => $value,
				'required' => 'required',
				'data-dateid' => 'publish_date',
			];
			
			$this->data['pages_list'] = $this->content_model->get_content_pages($content === FALSE ? $this->data['content_type'] : $content->id);
			if ($this->data['pages_list'] === FALSE) $this->data['pages_list'] = array();
			
			// Get pages list
			foreach ($this->data['pages_list'] as $k => $page)
			{
				if ($page->all_content || $page->all_type)
				{
					$this->data['pages_list'][$k]->id_hidden = [ 'hide_ids[]' => $page->id ];
					
					$this->data['pages_list'][$k]->pages_cbx = [
						'name' => 'content_hide_'.$page->id,
						'id' => 'content_hide_'.$page->id,
						'type' => 'checkbox',
						'value' => 'toggle_hide',
					];
					if (isset($page->hidden) && $page->hidden) $this->data['pages_list'][$k]->pages_cbx['checked'] = 'checked';
				}
				elseif (isset($page->added) && $page->added)
				{
					$this->data['pages_list'][$k]->id_hidden = [ 'remove_ids[]' => $page->id ];
					
					$this->data['pages_list'][$k]->pages_cbx = [
						'name' => 'content_remove_'.$page->id,
						'id' => 'content_remove_'.$page->id,
						'type' => 'checkbox',
						'value' => 'action_remove',
					];
				}
				else
				{
					$this->data['pages_list'][$k]->id_hidden = [ 'add_ids[]' => $page->id ];
					
					$this->data['pages_list'][$k]->pages_cbx = [
						'name' => 'content_add_'.$page->id,
						'id' => 'content_add_'.$page->id,
						'type' => 'checkbox',
						'value' => 'action_add',
					];
				}
			}
			
			// POSTS
			if ($this->data['content_type'] == 'posts')
			{
				// Add Editor script
				data_add_script('text_editor', $this->data);
				
				$value = $this->form_validation->set_value('content_text');
				if ($value == null && $content !== FALSE) $value = $content->text;
				$this->data['content_text'] = [
					'name' => 'content_text',
					'id' => 'content_text',
					'type' => 'textarea',
					'value' => $this->security->xss_clean($value),
				];
			}
		}
		
		// Output the edit page
		if ($this->data['content_type'] == 'pages') $this->output('page_edit');
		else $this->output('content_edit');
	}
	
	/**
	 * Users
	 */
	public function users($username = FALSE)
	{
		// Only proceed if user has permission
		if (!$this->permissions_check()) return;
		
		// No username -> show Users page
		if ($username === FALSE)
		{
			data_add_script('datetime', $this->data);
			$this->data['date_format'] = $this->settings_model->settings['date_format'];
			
			$this->data['users'] = $this->ion_auth->users()->result();
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}
			
			$this->output('users');
			
			return;
		}
		
		// Invalid username -> new user
		if ($username != 'new' && !$this->ion_auth->username_check($username))
		{
			$this->data['user_edit'] = 'new';
		}
		else $this->data['user_edit'] = $username;
		
		// Get user & title
		if ($this->data['user_edit'] == 'new')
		{
			$user = FALSE;
			$this->data['title'] = lang('title_new').lang('title_users');
		}
		else
		{
			$result = $this->ion_auth->where([ 'username' => $username ])->users()->result();
			$this->data['title'] = lang('title_edit').lang('title_users');
			
			if (!is_array($result) || empty($result))
			{
				$user = FALSE;
				$this->data['message'] = $this->error('something_wrong');
			}
			else
			{
				$user = $result[0];
				
				// Get data for view
				$this->data['title'] .= ': ' . $user->username;
				$this->data['last_login'] = $user->last_login;
				$this->data['created'] = $user->created_on;
				$this->data['id'] = $user->id;
				$this->data['user_self'] = $user->username == $this->ion_auth->user()->row()->username;
			}
		}
		
		// Valid form submitted?
		if ($this->form_check())
		{
			// Delete user
			if ($this->input->post('user_delete'))
			{
				if ($user !== FALSE)
				{
					// Trying to delete self
					if ($this->data['user_self'])
					{
						$this->session->set_flashdata('message', $this->error('users_delete_self'));
					}
					
					// Delete user
					elseif ($this->ion_auth->delete_user($user->id))
					{
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						redirect(site_url('admin/users'), $this->redirect_method);
					}
					
					// Delete failed
					else $this->session->set_flashdata('message', $this->ion_auth->errors());
				}
				
				// User doesn't exist
				else $this->session->set_flashdata('message', $this->error('not_exist'));
			}
		
			// Edit/Create user
			elseif ($user !== false || $this->data['user_edit'] == 'new')
			{
				// Set validation rules
				$tables = $this->config->item('tables', 'ion_auth');
				$form_changed = FALSE;
				
				if ($user === FALSE || trim($this->input->post('username')) !== $user->username)
				{
					$form_changed = TRUE;
					$data['username'] = $this->input->post('username');
					
					$this->form_validation->set_rules(
						'username', lang('auth_username'),
						'trim|required|alpha_dash|min_length[5]|max_length[12]|is_unique['.$tables['users'].'.username'.']');
				}
														
				if ($user === FALSE || trim($this->input->post('email')) !== $user->email)
				{
					$form_changed = TRUE;
					$data['email'] = $this->input->post('email');
					
					$this->form_validation->set_rules(
						'email', lang('auth_email'),
						'trim|required|valid_email|is_unique[' . $tables['users'] . '.email]');
				}

				if ($user === FALSE || $this->input->post('password'))
				{
					$form_changed = TRUE;
					$data['password'] = $this->input->post('password');
					
					$this->form_validation->set_rules(
						'password', lang('auth_password'),
						'required|min_length['.$this->config->item('min_password_length', 'ion_auth').']|matches[password_confirm]');
						
					$this->form_validation->set_rules('password_confirm', lang('auth_password_confirm'), 'required');
				}
				
				// Validate
				if (!$form_changed || $this->form_validation->run() === TRUE)
				{
					// Register new user
					if ($user === FALSE)
					{
						$reg_result = $this->ion_auth->register($data['username'],
																$data['password'],
																$data['email'],
																null, // additional data
																$this->input->post('groups'));
																
						// Returns an array if email activation is enabled
						if (is_array($reg_result)) $reg_result = $reg_result['id'];
						
						$success = $reg_result !== FALSE;
						if ($success) $user = $this->ion_auth->user($reg_result)->row();
					}
					
					// Update user
					else
					{
						$success = !$form_changed || $this->ion_auth->update($user->id, $data);
						
						if ($success)
						{
							// Set groups
							$groupsAdd = array();
							$groupsRemove = array();
							
							$allGroups = $this->ion_auth->groups()->result_array();
							$postGroups = $this->input->post('groups');
							
							foreach ($allGroups as $group)
							{
								if ($postGroups && in_array($group['id'], $postGroups))
								{
									if (!$this->ion_auth->in_group($group['id'], $user->id))
									{
										array_push($groupsAdd, $group['id']);
									}
								}
								elseif ($this->ion_auth->in_group($group['id'], $user->id))
								{
									array_push($groupsRemove, $group['id']);
								}
							}
							
							if (count($groupsAdd))
							{
								$success = $this->ion_auth->add_to_group($groupsAdd, $user->id) == count($groupsAdd);
								$groups_only = !$form_changed;
							}
							if (count($groupsRemove))
							{
								$success = $this->ion_auth->remove_from_group($groupsRemove, $user->id) == count($groupsRemove)
											&& $success;
								$groups_only = !$form_changed;
							}
						}
					}
							
					// Activation
					if ($success)
					{
						if ($this->input->post('active'))
						{
							if (!$user->active)
							{
								$success = $this->ion_auth->activate($user->id);
								$groups_only = FALSE;
							}
						}
						elseif ($user->active)
						{
							$success = $this->ion_auth->deactivate($user->id);
							$groups_only = FALSE;
						}
					}
					
					// Set flashdata
					if (isset($groups_only) && $groups_only)
					{
						if ($success) $this->session->set_flashdata('message', lang('save_success'));
						else $this->session->set_flashdata('message', $this->error('save_failed'));
					}
					else
					{
						if ($success) $this->session->set_flashdata('message', $this->ion_auth->messages());
						else $this->session->set_flashdata('message', $this->ion_auth->errors());
					}
						
					// Redirect 
					if ($success) redirect(site_url('admin/users/'.$user->username), $this->redirect_method);
				}
			}
		}
		
		data_add_script('datetime', $this->data);
		$this->data['date_format'] = $this->settings_model->settings['date_format'];
			
		// Prepare form
		$value = $this->form_validation->set_value('username');
		if ($value == null && $username != 'new')
		{
			if ($username == null && $user !== FALSE) $value = $user->username;
			else $value = $username;
		}
		$this->data['username_input'] = [
			'name' => 'username',
			'id' => 'username',
			'type' => 'text',
			'value' => $value,
			'required' => 'required',
			'maxlength' => 12,
			'pattern' => '^[a-zA-Z0-9-_]{5,12}$',
			'title' => 'five or more '.lang('alphadash')
		];
		
		$value = $this->form_validation->set_value('email');
		if ($value == null && $user !== FALSE) $value = $user->email;
		$this->data['email'] = [
			'name' => 'email',
			'id' => 'email',
			'type' => 'email',
			'value' => $value,
			'required' => 'required'
		];
		
		$this->data['password'] = [
			'name' => 'password',
			'id' => 'password',
			'type' => 'password',
			'autocomplete' => 'new-password',
			'pattern' => '.{8,}',
			'title' => 'eight or more characters'
		];
		if ($user === FALSE) $this->data['password']['required'] = 'required';
		
		$this->data['password_confirm'] = [
			'name' => 'password_confirm',
			'id' => 'password_confirm',
			'type' => 'password',
		];
		if ($user === FALSE) $this->data['password_confirm']['required'] = 'required';
		
		$this->data['active'] = [
			'name' => 'active',
			'id' => 'active',
			'type' => 'checkbox',
			'value' => 'active',
		];
		if (isset($_POST) && !empty($_POST))
		{
			if ($this->input->post('active')) $this->data['active']['checked'] = 'checked';
		}
		elseif ($user !== FALSE)
		{
			if ($user->active) $this->data['active']['checked'] = 'checked';
		}
		elseif (!$this->config->item('email_activation', 'ion_auth') && !$this->config->item('manual_activation', 'ion_auth'))
		{
			$this->data['active']['checked'] = 'checked';
		}
		
		// Group checkboxes 
		$groups = $this->ion_auth->groups()->result_array();
		if (!isset($postGroups) && $this->input->post('groups')) $postGroups = $this->input->post('groups');
		foreach ($groups as $k => $group)
		{
			$this->data['group_names'][$group['id']] = $group['description'];
			$this->data['groups'][$group['id']] = [
				'name' => 'groups[]',
				'id' => 'group_'.$group['id'],
				'type' => 'checkbox',
				'value' => $group['id'],
			];

			// Recheck if form was submitted
			if (isset($postGroups))
			{
				if (isset($postGroups[$k])) $this->data['groups'][$group['id']]['checked'] = 'checked';
			}
			// Else check defaults
			elseif ($user === FALSE)
			{
				if ($group['name'] == $this->config->item('default_group', 'ion_auth'))
				{
					$this->data['groups'][$group['id']]['checked'] = 'checked';
				}
			}
			elseif ($this->ion_auth->in_group($group['id'], $user->id))
			{
				$this->data['groups'][$group['id']]['checked'] = 'checked';
			}
		}
		
		$this->output('user_edit');
	}
	
	/**
	 * Output the page
	 */
	private function output($page)
	{
		if (!file_exists(APPPATH . '/views/admin/'.$page . '.php'))
		{
			show_404();
		}
		else
		{
			// Get a title if there isn't one already
			if (!isset($this->data['title']))
			{
				$this->data['title'] = lang('title_' . (isset($this->data['content_type']) ? $this->data['content_type'] : $page));
			}
			// Make sure message if set is there is one
			if (!isset($this->data['message'])) $this->data['message'] = $this->session->flashdata('message');
			// Add csrf nonce to data
			//$this->data['csrf'] = $this->_get_csrf_nonce();
			
			$this->load->view('admin/header', $this->data);
			
			if ($this->ion_auth->logged_in())
			{
				// Navigation bar
				if ($this->ion_auth->is_admin()) $this->load->view('admin/navbar');
			}
		
			// Main page view
			$this->load->view('admin/'.$page);
			
			// Remove flashdata message so we don't show it again
			$this->session->set_flashdata('message', null);
			
			$this->load->view('admin/footer');
		}
	}
	
	/**
	 * Permissions Check
	 */
	private function permissions_check()
	{
		if (!$this->ion_auth->logged_in())
		{
			$this->data['lang_identity'] = 'auth_'.$this->config->item('identity', 'ion_auth');
			
			// Attempt Login
			if ($this->form_check())
			{
				$this->form_validation->set_rules('identity', lang($this->data['lang_identity']), 'required');
				$this->form_validation->set_rules('password', lang('auth_password'), 'required');

				if ($this->form_validation->run() === TRUE)
				{
					if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password')))
					{
						$this->session->set_flashdata('message', $this->ion_auth->messages());
					}
					else $this->session->set_flashdata('message', $this->ion_auth->errors());
					
					redirect(uri_string(), $this->redirect_method);
				}
				else $this->session->set_flashdata('message', validation_errors());
			}
			
			$this->data['title'] = lang('title_login');
			
			// Show login page
			$this->data['identity'] = [
				'name' => 'identity',
				'id' => 'identity',
				'type' => 'text',
				'value' => $this->form_validation->set_value('identity'),
				'required' => 'required',
				'autofocus' => 'autofocus'
			];

			$this->data['password'] = [
				'name' => 'password',
				'id' => 'password',
				'type' => 'password',
				'required' => 'required'
			];
			
			$this->output('login');
		}
		
		// Logged in -> get username & perform additional checks
		else
		{
			$user = $this->ion_auth->user()->row();
			if (isset($user->username))
			{
				$this->data['username'] = $user->username;
				
				// Not an admin -> send to 'noadmin' page
				if (!$this->ion_auth->is_admin())
				{
					$this->output('noadmin');
				}
				else return TRUE;
			}
			
			// Logged in user does not exist -> logout
			else redirect(site_url('admin/logout'), $this->redirect_method);
		}
		
		return FALSE;
	}
	
	/**
	 * Valid form check
	 */
	private function form_check()
	{
		if (isset($_POST) && !empty($_POST))
		{
			/*if ($this->_valid_csrf_nonce() === FALSE)
			{
				$this->session->set_flashdata('message', $this->error('error_csrf'));
				redirect(uri_string(), $this->redirect_method);
			}
			else */return TRUE;
		}
		
		//return FALSE;
	}
	
	private function error($lang_index)
	{
		return $this->config->item('error_start_delimiter', 'ion_auth')
				. lang($lang_index)
				. $this->config->item('error_end_delimiter', 'ion_auth');
	}
	
	/**
	 * Form validation callback: date
	 */
		public function valid_date($date)
		{
			if (strtotime($date) === FALSE)
			{
				$this->form_validation->set_message('valid_date', $this->error('field_invalid_date'));
				return FALSE;
			}
			
			return TRUE;
		}

	/**
	 * @return array A CSRF key-value pair
	 */
	private function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);
		return [$key => $value];
	}

	/**
	 * @return bool Whether the posted CSRF token matches
	 */
	private function _valid_csrf_nonce(){
		$csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
		return $csrfkey && $csrfkey === $this->session->flashdata('csrfvalue');
	}
}
?>