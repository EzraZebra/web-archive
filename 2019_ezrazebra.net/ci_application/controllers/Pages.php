<?php
class Pages extends CI_Controller {
	public function __construct()
{
		parent::__construct();
		$this->load->database();
		$this->load->model([ 'settings_model', 'theme_model', 'content_model' ]);
		$this->load->library('ion_auth');
		$this->load->helper([ 'url', 'ez_cms' ]);
		
		/*if ($this->ion_auth->logged_in() || $this->ion_auth->is_admin())
			$this->lang->load('admin');*/
	}

	public function index($slug = 'home')
	{
		$page = $this->content_model->get($slug);
		if ($page === FALSE) show_404();
		else
		{
			$data['title'] = $page->title;
		
			$data['theme'] = $this->theme_model->get($this->settings_model->settings['theme']);
			$data['styles'] = $this->settings_model->settings['icon_styles'];
			
			$data['pages'] = $this->content_model->get_list();
			$data['content'] = $this->content_model->get_page_content($slug);
			
			$script_added['text_viewer'] = FALSE;
			foreach ($data['content'] as $k => $item)
			{
				if ($item->type == 'posts')
				{
					//$data['content'][$k]->text = $this->security->xss_clean($item->text);
					if (!$script_added['text_viewer']) 
					{
						data_add_script('text_viewer', $data);
						$script_added['text_viewer'] = TRUE;
					}
				}
			}
			
			$data['date_format'] = $this->settings_model->settings['date_format'];
			data_add_script('datetime', $data);
			
			$this->load->view('templates/ezrazebra/header', $data);
			$this->load->view('templates/ezrazebra/page');
			$this->load->view('templates/ezrazebra/footer');
		}
	}
}
?>