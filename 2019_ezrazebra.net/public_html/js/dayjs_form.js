/**
 * Adjust time inputs and their associated date inputs to local time
 * Add UTC offset to forms as hidden input
 */
(function() {
	for (var i=0; i<document.forms.length; i++) {
		let inputs = document.forms[i].querySelectorAll('input[type=time][data-dateid]');

		if (inputs.length > 0) {
			for (var j=0; j<inputs.length; j++) {
				// Get datetime in UTC
				let date_input = document.getElementById(inputs[j].getAttribute('data-dateid'));
				let date = dayjs(date_input.value + 'T' + inputs[j].value + ':00+00:00');
				
				// Set inputs in local time
				date_input.value = date.format('YYYY-MM-DD');
				inputs[j].value = date.format('HH:mm');
			}
			
			// Add hidden input with utc offset -> subtract in controller
			let offset_input = document.createElement('input');
			offset_input.name = 'utcOffset';
			offset_input.type = 'hidden';
			offset_input.value = dayjs().utcOffset();
			document.forms[i].appendChild(offset_input);
		}
	}
})();