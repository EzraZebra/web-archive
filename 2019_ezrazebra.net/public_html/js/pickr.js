/**
 * Replace color inputs with Pickr instances
 */
(function() {
	const els = document.querySelectorAll('.color-picker');
	const inputs = document.querySelectorAll('.color-input');
	var pickrs = new Array();
	var colors = new Array();

	for (var i=0; i<els.length; i++) {
		colors[i] = inputs[i].value;
		
		pickrs[i] = Pickr.create({
			el: els[i],
			theme: 'monolith',
			
			default: colors[i],

			components: {

				// Main components
				palette: true,
				preview: true,
				opacity: true,
				hue: true,

				// Input / output Options
				interaction: {
					hex: true,
					rgba: true,
					input: true,
					save: true,
				}
			}
		});
		
		pickrs[i].input = inputs[i];
		pickrs[i].on('save', (color, instance) => {
			instance.input.value = color.toHEXA().toString();
		})
	}

	for (var i=0; i<pickrs.length; i++) {
		for (var j=0; j<colors.length; j++) {
			pickrs[i].addSwatch(colors[j]);
		}
	}
})();