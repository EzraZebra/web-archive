/**
 * Adjust time tags with a format to local time
 */
(function() {
	let dates = document.querySelectorAll('time[data-format]');

	for (var i=0; i<dates.length; i++) {
		dates[i].innerHTML = dayjs(dates[i].getAttribute('datetime'))
								.format(dates[i].getAttribute('data-format'));
	}
})();