/**
 * Replace .post-text elements with TUI Viewers
 */
(function() {
	const posts = document.querySelectorAll('.post-text');

	for (var i=0; i<posts.length; i++) {
		let value = posts[i].textContent;
		posts[i].innerHTML = null;
		new tui.Editor({
			el: posts[i],
			initialValue: value,
			usageStatistics: false,
		});
	}
})();