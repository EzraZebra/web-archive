/**
 * Replace #content_text elements with TUI Editors
**/
(function() {
	var content = document.getElementById('content_text');

	var editor = new tui.Editor({
		el: document.getElementById('editSection'),
		initialEditType: 'markdown',
		previewStyle: 'tab',
		height: '500px',
		initialValue: content.value,
		usageStatistics: false
	});

	content.style = 'display: none;';

	var content_form = document.getElementById('contentForm');
	if (content_form.addEventListener) {
		content_form.addEventListener("submit", postSubmit, false);  //Modern browsers
	} else if (content_form.attachEvent) {
		content_form.attachEvent('onsubmit', postSubmit);            //Old IE
	}

	function postSubmit() {
		content.innerHTML = editor.getValue();
	}
})();
