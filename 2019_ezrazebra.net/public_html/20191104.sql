-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 04, 2019 at 03:29 AM
-- Server version: 10.3.18-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ezrazebra`
--

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int(11) UNSIGNED NOT NULL,
  `type` varchar(25) NOT NULL,
  `title` varchar(100) NOT NULL,
  `create_date` int(11) UNSIGNED NOT NULL,
  `publish_date` int(11) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `type`, `title`, `create_date`, `publish_date`) VALUES
(23, 'posts', 'Markdown Tests', 1563580800, 1563584400),
(13, 'posts', 'Hidden Post', 1563321600, 1562374800),
(12, 'posts', 'Example Page 2 Post', 1562284800, 1562292000),
(15, 'posts', 'Example Post', 1562457600, 1562497200),
(30, 'posts', 'Blockquote', 1564444800, 1564525740),
(31, 'posts', 'Example Post', 1564531200, 1564609620);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) UNSIGNED NOT NULL,
  `pos` int(11) NOT NULL,
  `uri` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `show_title` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `show_icon` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `all_content` tinyint(1) UNSIGNED DEFAULT 0,
  `all_posts` tinyint(1) UNSIGNED DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `pos`, `uri`, `title`, `icon`, `show_title`, `show_icon`, `all_content`, `all_posts`) VALUES
(24, 0, 'home', 'Home', 'icon-home', 0, 1, 1, 1),
(25, 1, 'example-1', 'Example Page', 'icon-write', 1, 1, 0, 0),
(34, 2, 'example-2', 'Example Page 2', 'icon-photo', 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pages_content`
--

CREATE TABLE `pages_content` (
  `id` int(11) UNSIGNED NOT NULL,
  `page_id` int(11) UNSIGNED NOT NULL,
  `content_id` int(11) UNSIGNED NOT NULL,
  `added` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `hidden` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages_content`
--

INSERT INTO `pages_content` (`id`, `page_id`, `content_id`, `added`, `hidden`) VALUES
(34, 24, 15, 0, 1),
(26, 34, 12, 1, 0),
(38, 24, 13, 0, 1),
(32, 25, 15, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) UNSIGNED NOT NULL,
  `content_id` int(11) UNSIGNED NOT NULL,
  `uri` varchar(100) NOT NULL,
  `text` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `content_id`, `uri`, `text`) VALUES
(12, 15, 'example-post-2', '# Example\r\n\r\n1. this\r\n2. is\r\n3. an\r\n4. example\r\n'),
(9, 12, 'example-post-3', '# Example Page 2 Post\r\n\r\n* this\r\n* post\r\n\r\n> is for\r\n> example page 2\r\n> '),
(10, 13, 'hidden-post', 'This post isn\'t visible on any page.'),
(19, 23, 'markdown-tests', 'trhrh\r\n\r\n`echo $lol;`\r\n\r\n``` php\r\nfunction($param) {\r\n    echo $param;\r\n}\r\n```\r\n\r\n``` javascript\r\nvar a = \'lol\';\r\nconsole.log(a);\r\n```\r\n\r\n> blabla\r\n\r\n> bla\r\n> \r\n\r\n> blabla\r\n\r\n**bold**\r\n*italic*\r\n~~strikethrough~~\r\n\r\n* * *\r\n\r\n> \r\n\r\n* 1\r\n* 2\r\n\r\n1. 1\r\n2. 2\r\n\r\n* [ ] check\r\n\r\n    \r\n    \r\n\r\n| t | t |\r\n| --- | --- |\r\n| c | c |'),
(22, 30, 'blockquote', '> This is a\r\n> blockquote'),
(23, 31, 'example-post', '# Example\r\nThis is an example post.');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) UNSIGNED NOT NULL,
  `date_format` varchar(255) NOT NULL,
  `timezone` varchar(6) NOT NULL,
  `theme` int(11) UNSIGNED NOT NULL,
  `icon_styles` text DEFAULT NULL,
  `icon_codes` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `date_format`, `timezone`, `theme`, `icon_styles`, `icon_codes`) VALUES
(1, 'D MMM YY HH:mm', '', 1, '/icon-fonts/css/fontello.css;/icon-fonts/icons.css', 'icon-music;icon-photo;icon-user;icon-logout;icon-user-create;icon-config;icon-delete;icon-add;icon-code;icon-home;icon-write;icon-apply;icon-show;icon-hide;icon-create;icon-remove;icon-help');

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE `themes` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `font` varchar(100) NOT NULL,
  `font_size` tinyint(2) UNSIGNED NOT NULL,
  `font_color` varchar(25) NOT NULL,
  `font_color_links` varchar(25) NOT NULL,
  `font_color_links_hover` varchar(25) NOT NULL,
  `color_bg` varchar(25) NOT NULL,
  `header_color` varchar(25) NOT NULL,
  `header_font_color` varchar(25) NOT NULL,
  `header_font_color_hover` varchar(25) NOT NULL,
  `logo` text DEFAULT NULL,
  `logo_content` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `name`, `font`, `font_size`, `font_color`, `font_color_links`, `font_color_links_hover`, `color_bg`, `header_color`, `header_font_color`, `header_font_color_hover`, `logo`, `logo_content`) VALUES
(1, 'default', '\"Courier New\", Courier, monospace', 16, '#24292e', '#24292E', '#3D4044', '#e6f2ff', '#24292e', '#FFFFFF', '#b3bcc6', '', 'E'),
(40, 'Example 1', 'Arial, Helvetica, sans-serif', 20, '#E6F2FF', '#24292e', '#54606c', '#8E6A97', '#4A4693', '#e6f2ff', '#b3bcc6', '', 'E'),
(42, 'Example 2', '\"Comic Sans MS\", cursive, sans-serif', 20, '#E6F2FF', '#24292e', '#54606c', '#21B44C', '#24292e', '#e6f2ff', '#b3bcc6', '', 'E');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`) VALUES
(65, NULL, 'admin', '$2y$12$N4qFHe6MgTF8Ncit8JEA4uNC//6KBJdKzVeQ5ClfVXv9RnufLyLq6', 'admin@ezrazebra.net', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1572795922, 1572834513, 1),
(66, NULL, 'demo1', '$2y$10$j43IpJK2RedO3JBWcGG7..3qytYGLr0U6.jwOZkGZq6y40cv0QX/.', 'demo1@ezrazebra.net', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1572795960, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(176, 65, 1),
(177, 65, 2),
(178, 66, 1),
(179, 66, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`uri`);

--
-- Indexes for table `pages_content`
--
ALTER TABLE `pages_content`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `page_id` (`page_id`,`content_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `content_id` (`content_id`),
  ADD UNIQUE KEY `uri` (`uri`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_email` (`email`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  ADD UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  ADD UNIQUE KEY `uc_remember_selector` (`remember_selector`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `pages_content`
--
ALTER TABLE `pages_content`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=180;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
