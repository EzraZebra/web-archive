### ezrazebra.net (2019)
Personal website with CMS.  
[Live demo](https://ezrazebra.net/archive/2019_ezrazebra.net/) ([login](https://ezrazebra.net/archive/2019_ezrazebra.net/admin): `demo1` / `demodemo`)

`jQuery 3.3.1` [`Day.js 1.8.16`](https://github.com/iamkun/dayjs) [`Pickr 1.4.6`](https://simonwep.github.io/pickr/) [`TUI Editor 1.4.7`](https://ui.toast.com/tui-editor/)  
`PHP 7.2` [`CodeIgniter 3.1.10`](https://codeigniter.com/) [`Ion Auth 3`](http://benedmunds.com/ion_auth/)  
`MySQL`
