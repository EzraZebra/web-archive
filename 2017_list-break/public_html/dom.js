if (typeof(Storage) === 'undefined') $('#lists').html('This browser doesn\'t support localStorage :(');
else {	
	$(document).ready(function() {
		/** $LIST-BREAK **/
		var $lb = new function() {
			var lb = new Lb();
			 
			/** $LISTS **/
			this.lists = new function() {
				//CLASSES
				this.classes = {
					autolist: 'auto-list',
					showInput: 'add-list-exp',
					showError: 'float-error-vis'
				};
					
				//$OUTPUT
					var $listsOutput = $("#lists").tabs();
					$listsOutput.$tabs = $listsOutput.children('ul');
					
					//ADD TAB
					$listsOutput.$tabs.add = function(list, index = this.children('li').length) {						
						this.append('<li><a'+(list.auto ? ' class="'+$lb.lists.classes.autolist+'"' : '')+' href="#'+index+'">'+list.name+'</a></li>');
						$listsOutput.append('<div id="'+index+'">'+index+' bottles of beer on the wall</div>');
					};
						
					//LOAD $OUTPUT
					this.output = function() {
						var lists = lb.getStorage(true, false).lists,
							maxIndex = lists.length-1;
							
						lists.forEach(function(list, index, array) {
							$listsOutput.$tabs.add(list, index);
							if(index >= maxIndex) $listsOutput.tabs('refresh').tabs('option', 'active', 0);
						});
					};
					
				//$SORT
					this.$sort = $('#listsSortDiag').dialog({
						autoOpen: false, modal: true,
						draggable: false, resizable: false,
						hide: true, show: true,
						width: "auto",
						position: { my: "center top", at: "center-1% top+6%", of: window }
					});
					this.$sort.$list = $('#listsSortList').scrollable();
					this.$sort.$autosort = $('#listsAutosort').checkboxradio({ icon: false });		
					var $listsSortListItems = this.$sort.$list.children('ul').sortable({ placeholder: 'ui-state-highlight' });
					
					//OUTPUT $SORT DIALOG
					this.$sort.output = function() {
						var lbStorage = lb.getStorage();
						$listsSortListItems.empty();
						this.$autosort.prop('checked', lbStorage.settings.autosort.enabled).checkboxradio('refresh');
						
						lbStorage.lists.forEach(function(list, index) {
							$listsSortListItems
								.append(	'<li class="ui-state-default'+(list.auto ? ' '+$lb.lists.classes.autolist : '')+'" data-id="'+index+'">'
										+		'<span>'+list.name+'</span>'
										+		(list.auto ? ' (autolist)' : '')
										+	'</li>');
						});
						
						$listsSortListItems.sortable('refresh');
						
						return this
					};
					
					//$SORT LIST ITEMS
					this.$sort.listItems = function(event) {
						var $btn = $(event.target),
							val = $btn.attr('value'),
							settings = { };
							
						switch($btn.attr('name')) {
							case 'byName':	
								settings.byName = {	func: function(list) { return $(list).text() },
													dir: val === 'desc' ? -1 : 1	};
								break;
							case 'autolists':
								settings.autolists = {	func: function(list) { return $(list).hasClass($lb.lists.classes.autolist) },
														pos: val === 'last' ? -1 : 1	};
								break;
							default: return false
						}
							
						lb.lists.sortBy($listsSortListItems.children('li'), settings).each(function() { $(this).appendTo($listsSortListItems); }); 
					};
					
					//SAVE $SORT ORDER
					this.$sort.save = function() {
						lb.settings.save({ autosort: { enabled: this.$autosort.prop('checked'), byName: -1, autolists: 1 } });
						lb.lists.sortOrder(function(callback) { //save sort & return new lists
							$listsSortListItems.children('li').each(function() {
								callback($(this).attr('data-id'));
							});
						}).forEach(function(list) {
							$listsOutput.$tabs.children('li').filter(function() { return $(this).children('a').text() === list.name }).appendTo($listsOutput.$tabs);
						});
						
						return this
					};
				//$END $SORT
					
				//$ADD
					this.$add = $('#listsAdd');
					this.$add.$input = this.$add.children('input');
					this.$add.$error = this.$add.children('.float-error');	
					
					//CHECK LIST NAME
					this.$add.check = function() {
						var check = lb.lists.check($lb.lists.$add.$input.val().trim());
						
						$lb.lists.$add.$error.removeClass($lb.lists.classes.showError);
						
						if(check.valid) return check.listName
						else {
							if(check.error === 'duplicate') $lb.lists.$add.$error.addClass($lb.lists.classes.showError);
							return false
						}
					};
					
					//SAVE NEW LIST
					this.$add.save = function() {
						var listName = this.check(this.$input.val().trim());
						
						if(listName !== false)	{
							var newList = new lb.List(listName, false);
								
							lb.lists.add(newList);
							$listsOutput.$tabs.add(newList);	
							//if autosort: reorder (see save sort order)
						
							$listsOutput.tabs('refresh').tabs('option', 'active', -1);
							this.$input.val('');
						}
					};
			};
			/** END $LISTS **/
		};
		/** END $LIST-BREAK **/
		
		/** INIT **/
		$lb.lists.output();
	
		/** EVENT HANDLERS **/
		$('form').submit(false);
		
		//SORT LISTS DIALOG		
			//OPEN DIALOG
			$('#listsSort').click(function() {
				$lb.lists.$sort.output().dialog('open').$list.scrollable('refresh');

				//close dialog
				$('.ui-widget-overlay').click(function() { $lb.lists.$sort.dialog('close'); });
			});
			
			//SORT LIST ITEMS
			$('#listsSortOptions > span > button').click(function(event) {
				$lb.lists.$sort.listItems(event);
			});
			
			//BUTTONS
			$lb.lists.$sort.dialog("option", "buttons", [
				//cancel
				{ class: 'cancel-btn',	click: function() {	$(this).dialog('close'); } },
				//apply (save)
				{ class: 'apply-btn',	click: function() {	$lb.lists.$sort.save().dialog('close'); } }
			]);
		
		//ADD LIST
			//BUTTON
			$lb.lists.$add.children('button').mousedown(function(event) {
				event.preventDefault();
				$lb.lists.$add.$input.focus();
				setTimeout($lb.lists.$add.check, 300);
			});
			
			//INPUT
			$lb.lists.$add.$input
				.bind('input propertychange', $lb.lists.$add.check)
				.blur(function() {
					$(this).removeClass($lb.lists.classes.showInput);
					$lb.lists.$add.$error.removeClass($lb.lists.classes.showError);
				});
			
			//SUBMIT
			$lb.lists.$add.submit(function() {
				if($lb.lists.$add.$input.hasClass($lb.lists.classes.showInput)) $lb.lists.$add.save();
				else $lb.lists.$add.$input.addClass($lb.lists.classes.showInput);
			});
			
		$("#del").click(function() {
			localStorage.clear();
		});
		/** END EVENT HANDLERS **/
	});//docready
}//storage defined