function Scrollable($wrapper) {
	var scrollbarClass = 'scrollbar',
		containerClass = 'scroll-container';
		
	$wrapper.addClass('scroll-wrapper').append('<div class="'+scrollbarClass+'"><span></span></div>').children().first().addClass(containerClass);
	
	var $container = $wrapper.children('.'+containerClass);
	this.$scrollbar = $wrapper.children('div.'+scrollbarClass);
	this.$scrollhandle = this.$scrollbar.children('span');
	
	var containerMaxOffset = 0,
		scrollhandleMaxOffset = 0;
	this.scrollhandleHeight = 0;
	this.wrapperPos = $wrapper.offset().top;

	/* *** GET OFFSETS *** */
	this.getOffsets = function() {
		return {
			container: parseFloat($container.css('top')),
			scrollhandle: parseFloat(this.$scrollhandle.css('top'))
		}
	};

	/* *** SETUP INFO SCROLLBAR *** */
	this.refresh = function() {
		var offsets = this.getOffsets(),
			objP = parseFloat($wrapper.css("padding-right")),
			barW = this.$scrollbar.outerWidth(),
			visH = $wrapper.height(),
			totH = $container.outerHeight(true)+5, //5px margin of error
			barH = this.$scrollbar.height(),
			overflow = totH > visH;
	
		this.wrapperPos = $wrapper.offset().top;
		this.scrollhandleHeight = overflow ? visH/totH*visH : barH;
		scrollhandleMaxOffset = overflow ? barH-this.scrollhandleHeight : 0;
		containerMaxOffset = overflow ? visH-totH : 0;
			
		this.scrollhandleHeight += this.$scrollhandle.height()-this.$scrollhandle.outerHeight(true); //subtract padding, border & margin
			
		//Draw scrollbar
		if(overflow) {
			this.$scrollhandle.css('height', this.scrollhandleHeight);
			if(!this.$scrollbar.is(':visible')) {
				this.$scrollbar.fadeIn();
				$wrapper.animate({ "padding-right": objP+barW });
			}
		}
		else if(this.$scrollbar.is(':visible')) {
			this.$scrollbar.fadeOut();
			$wrapper.animate({ "padding-right": objP-barW });
		}

		//Check scroll pos
		if(offsets.scrollhandle <= 0 || offsets.container >= 0) {
			this.$scrollhandle.animate({ top: '0' }, { queue: false });
			$container.animate({ top: '0' }, { queue: false });
		}
		else if(offsets.scrollhandle >= scrollhandleMaxOffset || offsets.container <= containerMaxOffset) {
			this.$scrollhandle.animate({ top: scrollhandleMaxOffset+'px' }, { queue: false });
			$container.animate({ top: containerMaxOffset+'px' }, { queue: false });
		}
	};
			
	/* *** HANDLE SCROLL *** */
	this.scroll = function(scrollOffset) {
		var offsets = this.getOffsets();

		if((scrollOffset < 0 && offsets.scrollhandle > 0) || (scrollOffset > 0 && offsets.scrollhandle < scrollhandleMaxOffset)) {
			var scrollbarTop = offsets.scrollhandle+scrollOffset,
				containerTop = offsets.container+scrollOffset*(containerMaxOffset/scrollhandleMaxOffset);

			this.$scrollhandle.css('top', scrollbarTop <= 0 ? 0 : scrollbarTop > scrollhandleMaxOffset ? scrollhandleMaxOffset: scrollbarTop);
			$container.css('top', containerTop >= 0 ? 0 : containerTop < containerMaxOffset ? containerMaxOffset: containerTop);
		}
	};
}

jQuery.fn.extend({
    scrollable: function (arg) {
		var $wrapper = this;
		if(typeof $wrapper.scrollable.scrollbar === 'undefined' && typeof arg === 'undefined') {
			$wrapper.scrollable.scrollbar = new Scrollable($wrapper);
			
			/** EVENT HANDLERS **/
			//MOUSEWHEEL SCROLL
			$wrapper.bind('wheel', function(e) {
				e.preventDefault();
				$wrapper.scrollable.scrollbar.scroll(e.originalEvent.deltaY*3);
			});
			
			//DRAG SCROLL
				var autoScroll = {
					intervId: null,
					offset: 0,
					toggle: function(offset) {
						autoScroll.offset = offset*.01;
						if(autoScroll.offset == 0) {
							clearInterval(autoScroll.intervId);
							autoScroll.intervId = null;
						}
						else if(autoScroll.intervId === null) intervId = setInterval(function() { $wrapper.scrollable.scrollbar.scroll(autoScroll.offset) }, 400);
					}
				};
						
				$wrapper.mousedown(function(e) {
					var wrapperBottom = $wrapper.scrollable.scrollbar.wrapperPos+$wrapper.outerHeight();
						
					$(window).on('mousemove.scrollablewrapper', function(eMove) {
						autoScroll.toggle(	eMove.pageY
										-	(	eMove.pageY < $wrapper.scrollable.scrollbar.wrapperPos ? $wrapper.scrollable.scrollbar.wrapperPos
											:	eMove.pageY > wrapperBottom ? wrapperBottom : eMove.pageY	)	);

						$(window).mouseup(function() {
							$(window).off('mousemove.scrollablewrapper');
							autoScroll.toggle(0);
						});	
					});
				});
			
				$wrapper.scrollable.scrollbar.$scrollbar.mousedown(function(e) {
					e.stopPropagation();
				});
				
				$wrapper.scrollable.scrollbar.$scrollhandle.mousedown(function(e) {
					e.stopPropagation();
				});
			
			//CLICK SCROLLBAR SCROLL
				$wrapper.scrollable.scrollbar.$scrollbar.click(function(e) {
					$wrapper.scrollable.scrollbar.scroll( e.pageY - $wrapper.scrollable.scrollbar.wrapperPos //click pos
													- $wrapper.scrollable.scrollbar.getOffsets().scrollhandle //scroll pos
													- $wrapper.scrollable.scrollbar.scrollhandleHeight / 2 ); //adjust for middle of scrollbar
				});
				
				$wrapper.scrollable.scrollbar.$scrollhandle.click(function(e) {
					e.stopPropagation();
				});
			
			//DRAG HANDLE SCROLL
			$wrapper.scrollable.scrollbar.$scrollhandle.mousedown(function(e) {
				e.preventDefault();
				$(this).addClass('mousedown')
				
				var mouseY = null;
				$(window).on('mousemove.scrollablehandle', function(eMove) {
					if(mouseY !== null && mouseY !== eMove.pageY) $wrapper.scrollable.scrollbar.scroll(eMove.pageY-mouseY);
					mouseY = eMove.pageY;

					$(window).mouseup(function() {
						$(window).off('mousemove.scrollablehandle');
						$wrapper.scrollable.scrollbar.$scrollhandle.removeClass('mousedown');
					});
				});
			});
			
			//REDRAW SCROLLBAR WHEN WINDOW RESIZES
			$(window).on('resize', function() {
				$wrapper.scrollable.scrollbar.refresh();
			});
			/** END EVENT HANDLERS **/
		}
		else if(typeof $wrapper.scrollable.scrollbar !== 'undefined' && arg === 'refresh') $wrapper.scrollable.scrollbar.refresh();
		else console.log('scrollable: invalid argument');
		
		return $wrapper
    }
});