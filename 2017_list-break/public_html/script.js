if (typeof(Storage) === 'undefined') $('#lists').html('This browser doesn\'t support localStorage :(');
else {
	/** LIST-BREAK **/
	var Lb = function() {
		var Lb = this,
		
		//LB STORAGE
			lbStorage = { },
			
		//PARSE STRING FOR LOCAL STORAGE
			tryParse = function(string) {
				try { string = JSON.parse(string); }
				catch(error) { string = ''; }
				finally { return string }
			};
			
		//GET LISTS
		Lb.getStorage = function(lists=true, settings=true) {
			var r = { };
			if(lists) r.lists = lbStorage.lists;
			if(settings) r.settings = lbStorage.settings;
			
			return r
		};
		
		/** SETTINGS **/
		Lb.settings = new function() {
			//SETTINGS STORAGE
			lbStorage.settings = tryParse(localStorage.settings);
			if(typeof lbStorage.settings !== 'object')
				lbStorage.settings = { autosort: { enabled: false, byName: 1, autolists: 1 } };
			
			//SAVE SETTINGS
			this.save = function(newSettings) {
				if(typeof newSettings.autosort !== 'undefined') lbStorage.settings.autosort = newSettings.autosort;
				
				localStorage.settings = JSON.stringify(lbStorage.settings);
			};
		};
			
		/** LISTS **/
		Lb.List = function(name = 'My list', auto = false) { this.name = name; this.auto = auto; };
		Lb.lists = new function() {
			//LISTS STORAGE
			lbStorage.lists = tryParse(localStorage.lists);
			if(lbStorage.lists.constructor !== Array)
				lbStorage.lists = [ new Lb.List('All', true), new Lb.List() ];
			
			//SAVE LISTS
			var	storeLists = function() {
					if(lbStorage.settings.autosort.enabled)
						Lb.lists.sortBy(	lbStorage.lists,
										params = {	byName: { func: function(list) { return list.name }, dir: lbStorage.settings.autosort.byName },
													autolists: { func: function(list) { return list.auto }, pos: lbStorage.settings.autosort.autolists }
										}	);
				
					localStorage.lists = JSON.stringify(lbStorage.lists);
				};
			
			//SORT LISTS
			this.sortBy = function(lists, params) {
				var byName = false, autolists = false;
				
				if(	typeof params.byName !== 'undefined' && typeof params.byName.func === 'function'
				&&	(params.byName.dir === 1 || params.byName.dir === -1)	) byName = true;
				
				if(	typeof params.autolists !== 'undefined' && typeof params.autolists.func === 'function'
				&&	(params.autolists.pos === 1 || params.autolists.pos === -1)	) autolists = true;
				
				if(byName || autolists)
					lists.sort(function(a, b) {
						var r = 0;
						
						if(autolists) {
							var aF = params.autolists.func(a), bF = params.autolists.func(b);
							if(aF) { if(!bF) r = -params.autolists.pos; }
							else if(bF) r = params.autolists.pos;
						}
						
						if(r === 0 && byName) r = params.byName.dir*params.byName.func(a).localeCompare(params.byName.func(b));
						
						return r
					});

				return lists
			};
			
			//SAVE SORT ORDER
			this.sortOrder = function(eachIndex) {
				var tempLists = [];
				
				eachIndex(function(index) {
					tempLists.push(lbStorage.lists[index]);
				});
				
				lbStorage.lists = tempLists;
				
				storeLists();
				return lbStorage.lists
			};
			
			//CHECK LIST NAME
			this.check = function(listName) {
				if(typeof listName === 'string' && listName != '') {
					listName = listName.length > 25 ? listName.substring(0, 24) + '&hellip;' : listName;
					
					var duplicate = false;
					lbStorage.lists.every(function(list) {
						duplicate = list.name == listName;
						return !duplicate
					});
					
					if(!duplicate) return { valid: true, listName: listName }
					else return { valid: false, error: 'duplicate' }		
				}
				else return { valid: false, error: 'nostring' }
			};
			
			//SAVE LISTS
			this.add = function(newList) {
				lbStorage.lists.push(newList);
				
				storeLists();
			};
		};
		/** END LISTS **/
	};
	/** END LIST-BREAK **/
}//storage defined