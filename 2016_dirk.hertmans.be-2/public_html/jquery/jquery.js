/* ************************** */
/* *** GLOBAL VARS OBJECT *** */
/* ************************** */
let gVars = {
	durFast: 1000,
	durSlow: 2000
};

/* ************************** */
/* *** MEDIA CACHE OBJECT *** */
/* ************************** */
let media = {
	pageVisited: [],
	media: 'img',
	filter: ':not(#slideShowWrapper *, .noCache)',

	/* *** CACHE MEDIA *** */
	cache: function() {
		$(this.media).filter(this.filter).each(function() {
			let $media = $(this), width = $media.width(), height = $media.height(), bgSize = 96,
				$mediaWrapper = $media.parent(), minWidth = parseFloat($mediaWrapper.css('min-width')), minHeight = parseFloat($mediaWrapper.css('min-height'));

			if(width < bgSize || height < bgSize) bgSize = height < width ? height : width;
			if(width > minWidth) minWidth = width;
			if(height > minHeight) minHeight = height;
			
			$media	.data('mediasrc', $media.attr('src'))
					.hide().attr('src', '')
					.before('<span class="loading" style="width: '+width+'px; height: '+height+'px; background-size: '+bgSize+'px;"></span>');
			$mediaWrapper.addClass('loadingWrapper').css({ 'min-width': minWidth, 'min-height': minHeight });
		});
	},

	/* *** LOAD MEDIA *** */
	load: function(pageId) {
		if(typeof this.pageVisited[pageId] === 'undefined' || !this.pageVisited[pageId]) {
			this.pageVisited[pageId] = true;
			
			$(pageId).find(this.media).filter(this.filter).each(function() {
				let $media = $(this);

				$media.attr('src', $media.data('mediasrc')).on('load', function() {
					$media.fadeIn(gVars.durFast, 'linear').prev('.loading').fadeOut(gVars.durFast*0.75, 'linear', function() {
						$media.parent().removeClass('loadingWrapper').css({ 'min-width': '', 'min-height': '' });
						$(this).remove();
					});
				});
			});
		}
	}
};

/* ************************ */
/* *** SCROLLBAR OBJECT *** */
/* ************************ */
let scrollbar = {
	$scrollbar: $('#infoScroll'),
	$scroller: $('#infoScroll span'),

	/* *** GET VARS *** */
		getOffsets: function($container) {
			return {
				scroller: parseFloat(this.$scroller.css('top')),
				container: parseFloat($container.css('top'))
			};
		},

		getVars: function() {
			let $container = $('.infoBlock.currentPage .infoBlockContainer'),
				totH = $container.height(),
				visH = $('.infoBlock.currentPage').height();

			return {
				$container: $container,
				totH: totH,
				visH: visH,
				barH: this.$scrollbar.height(),
				containerMaxOffset: totH <= visH ? 0 : visH-totH
			};
		},

	/* *** SETUP INFO SCROLLBAR *** */
	draw: function() {
		let vars = scrollbar.getVars(),
			offsets = scrollbar.getOffsets(vars.$container),
			scrollerHeight = vars.visH/vars.totH*vars.visH;

		//Draw
		if(vars.totH > vars.visH) {
			scrollbar.$scroller.css('height', scrollerHeight);
			scrollbar.$scrollbar.fadeIn(gVars.durFast);
		}
		else scrollbar.$scrollbar.fadeOut(gVars.durFast);

		//Check
		if(offsets.scroller <= 0 || offsets.container >= 0) {
			scrollbar.$scroller.animate({ top: '0' }, { queue: false, duration: gVars.durFast });
			vars.$container.animate({ top: '0' }, { queue: false, duration: gVars.durFast });
		}
		else {
			let scrollerMaxOffset = vars.barH-scrollerHeight;

			if(offsets.scroller >= scrollerMaxOffset || offsets.container <= vars.containerMaxOffset) {
				scrollbar.$scroller.animate({ top: scrollerMaxOffset+'px' }, { queue: false, duration: gVars.durFast });
				vars.$container.animate({ top: vars.containerMaxOffset+'px' }, { queue: false, duration: gVars.durFast });
			}
		}
	},

	/* *** DO SCROLL *** */
	doScroll: function(scrollOffset, scrollRatio, scrollerMaxOffset, $container, containerMaxOffset) {
		let offsets = this.getOffsets($container);

		if((scrollOffset < 0 && offsets.scroller > 0) || (scrollOffset > 0 && offsets.scroller < scrollerMaxOffset)) {
			let scrollbarTop = offsets.scroller+scrollOffset,
				containerTop = offsets.container-scrollOffset*scrollRatio;

			this.$scroller.css('top', scrollbarTop <= 0 ? 0 : scrollbarTop > scrollerMaxOffset ? scrollerMaxOffset: scrollbarTop);
			$container.css('top', containerTop >= 0 ? 0 : containerTop < containerMaxOffset ? containerMaxOffset: containerTop);
		}
	},

	/* *** HANDLE SCROLL *** */
	scroll: function(e) {
		e.preventDefault();
		let vars = this.getVars(),
			scrollerMaxOffset = vars.barH-this.$scroller.height(),
			scrollRatio = -vars.containerMaxOffset/scrollerMaxOffset;

		if(e.type === 'mousedown') {
			let mouseY = null;

			$(window).on('mousemove', function(eMove) {
				if(mouseY !== null && mouseY !== eMove.pageY)
					scrollbar.doScroll(eMove.pageY-mouseY, scrollRatio, scrollerMaxOffset, vars.$container, vars.containerMaxOffset);

				mouseY = eMove.pageY;
			});
		}
		else if(e.type === 'wheel')
			this.doScroll(e.originalEvent.deltaY*3, scrollRatio, scrollerMaxOffset, vars.$container, vars.containerMaxOffset);
	}
};

/* **************** */
/* *** PAGELOAD *** */
/* **************** */
function pageLoad() {
		/* *** GET HASH DATA *** */
			//Defaults
			let title = '#Welkom',
				id = '#welkomContent',
				$info = $('#welkomInfo'),
				$slideShow = null,
				index = 0,
				iTotal = 0,
				isSlideShow = false;

			if(window.location.hash) {
				let hashArray = window.location.hash.split('_');
				title = hashArray[0];
				let titleLower = title.toLowerCase();
				id = titleLower+'Content';
				$info = $(titleLower+'Info');

				//Page found
				if($(id)[0]) {
					$slideShow = $(titleLower+'Slide');

					//Slideshow exists
					if(hashArray.length === 2 && $slideShow[0]) {
						iTotal = $slideShow.children('div').length;

						//Slideshow has slides
						if(iTotal > 0) {
							index = parseInt(hashArray[1]);

							if(index > iTotal) index = iTotal;
							else if(index < 1) index = 1;

							isSlideShow = true;
						}
					}
					//Slideshow doesn't exist or doesn't have slides
					if(!isSlideShow) $slideShow = null;
				}
				//Page not found
				else {
					title = '#NietGevonden';
					id = '#nietgevondenContent';
					$info = $('#nietgevondenInfo');
				}
			}

		/* *** LOAD PAGE & SLIDESHOW VARS * ***/
		let siteTitle = 'Dirk Hertmans', titleDiv = ' \u003A\u003A ';

		/* *** LOAD PAGE *** */			
			//Page has changed
			if(!$(id).hasClass('currentPage')) {
				/* SET DOC TITLE */
					document.title = siteTitle+titleDiv+$(id+' > h1').text();

				/* UPDATE PAGE & TITLE */
					$('.currentPage').removeClass('currentPage');
					$(id).addClass('currentPage');
					$('#navigation a[href="'+title+'"]').addClass('currentPage');
					$info.addClass('currentPage');
					scrollbar.draw();

				/* LOAD OBJECTS */
					setTimeout(media.load(id), gVars.durSlow);
			}

		/* *** SLIDESHOW *** */
			let $slideShowWrapper = $('#slideShowWrapper');

			/* *** ACTIVE: SETUP AND SHOW *** */
			if(isSlideShow) {
				let $originSlide = $('.currentSlide'),
					currentHash = title+'_'+index,
					$previousSlide = $('#previousSlide'),
					$nextSlide = $('#nextSlide');

				//Slide has changed
				if(!$originSlide.is($(currentHash))) {
					//Load media ASAP
					//media.load(currentHash); (currently not caching slideshow imgs because theyre loaded in gallery page)

					//Set doc title
					if(iTotal > 1) document.title = siteTitle+titleDiv+$(id+' > h1').text()+titleDiv+index;

					//Set slide positions
					$originSlide.removeClass('currentSlide');
					$(currentHash).removeClass('leftSlide').removeClass('rightSlide').addClass('currentSlide');
					$(currentHash).nextAll().removeClass('leftSlide').addClass('rightSlide');
					$(currentHash).prevAll().removeClass('rightSlide').addClass('leftSlide');

					//Set close button & previous & next button indexes
					$('#closeSlideShow').attr('href', title);
					if(iTotal === 1) {
						$previousSlide.attr('href', currentHash).hide();
						$nextSlide.attr('href', currentHash).hide();
					}
					else {
						let iPrevious = index-1,
							iNext = index+1;

						if(iPrevious < 1) iPrevious = iTotal;
						if(iNext > iTotal) iNext = 1;

						$previousSlide.attr('href', title+'_'+iPrevious).show();
						$nextSlide.attr('href', title+'_'+iNext).show();
					}
				}
				
				if(!$slideShowWrapper.hasClass('showSlideShow')) {
					//disable kb shortcuts for PDFs
					//if($(currentHash+' > embed[type="application/pdf"]')[0]) $(document).off('keyup.fullSlide');
					//enable kb shortcuts
					//else	
							$(document).on('keyup.fullSlide', function(e) {
								if(e.keyCode === 70) fullSlide.toggle();   // F 
								if(e.keyCode === 27 && !fullSlide.check()) window.location = title; //	esc
								if(e.keyCode === 37) window.location = $previousSlide.attr('href');   // left
								if(e.keyCode === 39) window.location = $nextSlide.attr('href');   // right 
							});

					//Show Slideshow
					$slideShowWrapper.removeClass('hideSlideShow').addClass('showSlideShow');
					$slideShow.addClass('showSlideShow');
				}
			}
			/* *** INACTIVE: HIDE *** */
			else if($slideShowWrapper.hasClass('showSlideShow')) {
				$('.showSlideShow').removeClass('showSlideShow');
				$slideShowWrapper.addClass('hideSlideShow');
				
				//Turn off keyboard events
				$(document).off('keyup.fullSlide');
			}
		/* *** END SLIDESHOW *** */
}
/* *** END PAGELOAD *** */

/* *** INIT *** */
$(document).ready(function() {
	/* *** PREVENT OBJECTS FROM LOADING *** */
		media.cache();

	/* *** LOAD DATA *** */
		pageLoad();
		$(window).on('hashchange', pageLoad);

	/* *** SWITCH ROTATION *** */
		if($(window).height() > $(window).width()) $('body').addClass('vertical');

		$('#rotateBtn').on('click', function() {
			$('body').toggleClass('vertical');

			//Wait for transition to complete & update scrollbar
			setTimeout(scrollbar.draw, gVars.durSlow);
		});

	/* *** SWITCH HAND *** */
		$('#switchHandBtn').on('click', function() {
			$('body').toggleClass('leftHand');
		});

	/* *** RESIZE WINDOW -> UPDATE SCROLLBAR *** */
		$(window).on('resize', function() {
			setTimeout(scrollbar.draw, gVars.durSlow);
		});

	/* *** INFO SCROLLING *** */
		scrollbar.$scrollbar.on('mousedown', function(e) {
			scrollbar.scroll(e);
		});

		$(window).on('mouseup', function() {
			$(window).off('mousemove');
		});

		$('#infoContainer').on('wheel', function(e) {
			scrollbar.scroll(e);
		});
});