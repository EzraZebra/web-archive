/* ************************** */
/* *** GLOBAL VARS OBJECT *** */
/* ************************** */
let gVars = {
	durFast: 1000,
	durVeryFast: 500,
	durSlow: 2000
};

/* ************************** */
/* *** MEDIA CACHE OBJECT *** */
/* ************************** */
let media = {
	pageVisited: [],
	media: 'img, embed, iframe',
	filter: ':not(#slideShowWrapper *, .noCache)',

	/* *** CACHE MEDIA *** */
	cache: function() {
		$(this.media).filter(this.filter).each(function() {
			let $media = $(this), width = $media.width(), height = $media.height(), bgSize = 96,
				$mediaWrapper = $media.parent(), minWidth = parseFloat($mediaWrapper.css('min-width')), minHeight = parseFloat($mediaWrapper.css('min-height'));

			if(width < bgSize || height < bgSize) bgSize = height < width ? height : width;
			if(width > minWidth) minWidth = width;
			if(height > minHeight) minHeight = height;
			
			$media	.data('mediasrc', $media.attr('src'))
					.hide().attr('src', '')
					.before('<span class="loading" style="width: '+width+'px; height: '+height+'px; background-size: '+bgSize+'px;"></span>');
			$mediaWrapper.addClass('loadingWrapper').css({ 'min-width': minWidth, 'min-height': minHeight });
		});
	},

	/* *** LOAD MEDIA *** */
	load: function(pageId) {
		if(typeof this.pageVisited[pageId] === 'undefined' || !this.pageVisited[pageId]) {
			this.pageVisited[pageId] = true;
			
			$(pageId).find(this.media).filter(this.filter).each(function() {
				let $media = $(this);

				$media.attr('src', $media.data('mediasrc')).on('load', function() {
					$media.fadeIn(gVars.durFast, 'linear').prev('.loading').fadeOut(gVars.durFast*0.75, 'linear', function() {
						$media.parent().removeClass('loadingWrapper').css({ 'min-width': '', 'min-height': '' });
						$(this).remove();
					});
				});
			});
		}
	}
};

/* *************************** */
/* *** HEADER FLOAT OBJECT *** */
/* *************************** */
let headerFloat = {
	headerMainHeight: 170,
	pageTitleTop: 150,
	contentTop: 225,
	floating: false,
	$headerMain: $('#headerMain'),

	/* *** GET FLOATING CONTENT HEADER *** */
	$getContentHeader: function(pageId) {
		switch(pageId) {
			case '#cvContent': return $('#cvContent header');
			case '#softwareContent': return $('#softwareContent nav');
			default: return null;
		}
	},

	/* *** TOGGLE FLOAT *** */
	toggle: function() {
		let floatOffset = 95;

		if(	(headerFloat.floating && $(window).scrollTop() < floatOffset) ||
			(!headerFloat.floating && $(window).scrollTop() >= floatOffset) ) {
			//Update floating bool
			headerFloat.floating = $(window).scrollTop() >= floatOffset;

				//Header
			let $pageTitle = $('.currentPage > h1'), 
				pos = '', headerMainHeight = '', contentPadding = '', pageTitleTop = '',
				//Content header
				$contentHeader = $('section.currentPage').find('.contentHeaderFloat');
				contentHeaderTop = '';
			
			if(headerFloat.floating) {
				pos = 'fixed';
				headerMainHeight = (headerFloat.headerMainHeight-floatOffset)+'px';
				contentPadding = headerFloat.contentTop+'px';
				pageTitleTop = (headerFloat.pageTitleTop-floatOffset)+'px';
				contentHeaderTop = (headerFloat.contentTop-floatOffset)+'px';
			}

			//Header
			$('#pageHeader').css('position', pos);
			headerFloat.$headerMain.css('height', headerMainHeight);
			$('#content').css('padding-top', contentPadding);
			$pageTitle.css({ 'transition': 'all 0s', 'position': pos, 'top': pageTitleTop });
			$pageTitle[0].offsetHeight; //force "reflow" of element -- stackoverflow.com/questions/11131875/#16575811
			$pageTitle.css('transition', '');

			//Content header
			if($contentHeader !== null) $contentHeader.css({ 'position': pos, 'top': contentHeaderTop });
		}
	},

	/* *** ANIMATED HEADER RESET *** */
	reset: function() {
		if(this.floating) {
			$(window).off('scroll.float',  headerFloat.toggle);

			let $pageTitle = $('.previousPage > h1'),
				//$contentHeader = this.$getContentHeader('#'+$('section.previousPage').attr('id'));
				$contentHeader = $('section.previousPage').find('.contentHeaderFloat');

			this.$headerMain.animate({ height: this.headerMainHeight+'px' }, {
				queue: false,
				duration: gVars.durFast
			});

			$pageTitle.css({ 'transition': 'all 0s' });
			$pageTitle.animate({ top: this.pageTitleTop+'px' }, {
				queue: false,
				duration: gVars.durFast,
				always: function() {
					$(this).css({ 'position': '', 'top': '', 'transition': '' });
					$(window).on('scroll.float',  headerFloat.toggle);
				}
			});

			if($contentHeader !== null) {
				$contentHeader.animate({ top: this.contentTop+'px', left: '75%' }, {
					queue: false,
					duration: gVars.durFast,
					always: function() {
						$(this).css({ 'position': '', 'top': '', 'left': '' });
					}
				});
			}
		}
	}
};
/* *** END HEADER FLOAT OBJECT *** */

/* **************** */
/* *** PAGELOAD *** */
/* **************** */
function pageLoad() {
	/* *** PAGELOAD GLOBAL VARS *** */
		scrolling = false;

		//Hash data
		let hash = window.location.hash;
		if(!hash) hash = '#Home';

		let hashArray = hash.split('/'),
			tabArray = hashArray[0].split(':'),
			title = tabArray[0],
			titleLower = title.toLowerCase(),
			id = titleLower+'Content';

		if(!$(id)[0]) {
			title = '#NotFound';
			titleLower = '#notfound'
			id = titleLower+'Content';
		}

		//Pagetitle data
		let titleDiv = ' \u003A\u003A ',
			siteTitle = 'Ezra Hertmans',
			pageTitle = $(id).find('h1').text();

			if(typeof pageTitle !== 'undefined') pageTitle = siteTitle+titleDiv+pageTitle;
			else pageTitle = siteTitle;

	/* *** LOAD PAGE *** */			
		//Page has changed
		if(!$(id).hasClass('currentPage')) {
			/* LOAD OBJECTS */
				setTimeout(media.load(id), gVars.durSlow);

			/* SET DOC TITLE */
				document.title = pageTitle;

			/* UPDATE PAGE */
				$('.previousPage').removeClass('previousPage');
				$('.currentPage').removeClass('currentPage').filter('#content > section').addClass('previousPage');
				setTimeout(function() { //make sure the css resets after .removeClass('previousPage') (previous overrides current)
					$(id).addClass('currentPage');
				 }, 1);

			/* UPDATE TITLE */
				let $headerButtons = $('.headerButtons');
				$headerButtons.find('a[href="'+title+'"]').addClass('currentPage');

				//toggle home menu
				if(title === '#Home') $headerButtons.addClass('homeNav');
				else $headerButtons.removeClass('homeNav');

			/* RESET SCROLL POS */
				if($(window).scrollTop() > 0) {
					scrolling = true;

					$('html').animate({ scrollTop: 0 }, {
						queue: false,
						duration: gVars.durFast
					});
					
					headerFloat.reset();
				}
		}

	/* *** LOAD TAB *** */
		let $tabBtns = $(id).find('.tabList > a');

		//Tablist exists
		if($tabBtns.length > 0) {
			/* GET CURRENT TAB */
				let $currentTabBtn = $tabBtns.filter('[href="'+hashArray[0]+'"]');
				if(tabArray.length === 1 || !$currentTabBtn[0]) $currentTabBtn = $tabBtns.first();

			//Tab has changed
			if(!$currentTabBtn.hasClass('activeTab')) {
				/* UPDATE TAB BUTTON */
					$tabBtns.filter('.activeTab').removeClass('activeTab');
					$currentTabBtn.addClass('activeTab');
			
				/* UPDATE TAB CONTENTS */
					let tabContents = $(id).find('.tabContent'),
						tabFilter = '[data-nav="'+$currentTabBtn.attr('data-nav')+'"]';

					tabContents.not(tabFilter).stop().fadeOut({
						queue: false,
						duration: gVars.durVeryFast/2,
						always: function() {
							tabContents.filter(tabFilter).stop().fadeIn({
								queue: false,
								duration: gVars.durVeryFast/2
							});
						}
					});

				/* RESET SCROLL POS */
					if(!scrolling && $(window).scrollTop() > 95) {
						$('html').animate({ scrollTop: 95 }, {
							queue: false,
							duration: gVars.durFast
						});
					}
			}
		}

	/* *** SLIDES SETUP *** */
		let isSlideShow = false,
			$slideShowWrapper = $('#slideShowWrapper');

		//Slideshow requested
		if(hashArray.length > 1) {
			let $slideShow = $(titleLower+'Slide');

			//Slideshow exists
			if($slideShow[0]) {
				let $slides = $slideShow.children('div'),
					iTotal = $slides.length;

				//Slideshow has slides
				if(iTotal > 0) {
					isSlideShow = true;

					let $currentSlide = $slides.filter('[id="'+window.location.hash+'"]'),
						$originSlide = $('.currentSlide');

					if(!$currentSlide[0]) $currentSlide = $slides.first().attr('id');

					//Slide has changed
					if(!$originSlide.is($currentSlide)) {
						/* LOAD OBJECTS */
							//media.load(currentHash); (currently not caching slideshow imgs because theyre loaded as thumbs)

						/* SET DOC TITLE */
							let slideTitle = $currentSlide.attr('title');
							if(typeof slideTitle !== 'undefined') document.title = pageTitle+titleDiv+slideTitle;

						/* UPDATE SLIDES */
							$originSlide.removeClass('currentSlide');
							$currentSlide.removeClass('leftSlide').removeClass('rightSlide').addClass('currentSlide');
							$currentSlide.nextAll().removeClass('leftSlide').addClass('rightSlide');
							$currentSlide.prevAll().removeClass('rightSlide').addClass('leftSlide');

						/* UPDATE BUTTONS */
							let $previousSlideBtn = $('#previousSlide'),
								$nextSlideBtn = $('#nextSlide');

							$('#closeSlideShow').attr('href', title);
							if(iTotal === 1) {
								$previousSlideBtn.attr('href', window.location.hash).hide();
								$nextSlideBtn.attr('href', window.location.hash).hide();
							}
							else {
								let $previousSlide = $currentSlide.prev(),
									$nextSlide = $currentSlide.next();

								if(!$previousSlide[0]) $previousSlide = $slides.last();
								if(!$nextSlide[0]) $nextSlide = $slides.first();

								$previousSlideBtn.attr('href', $previousSlide.attr('id')).show();
								$nextSlideBtn.attr('href', $nextSlide.attr('id')).show();
							}

							//Update kb shortcuts
							$(document).on('keyup.fullSlide', function(e) {
								if(e.keyCode === 70) fullSlide.toggle();   // F 
								if(e.keyCode === 27 && !fullSlide.check()) window.location = title; //	esc
								if(e.keyCode === 37) window.location = $previousSlideBtn.attr('href');   // left
								if(e.keyCode === 39) window.location = $nextSlideBtn.attr('href');   // right 
							});
					}

					/* *** OPEN SLIDESHOW *** */
					if(!$slideShowWrapper.hasClass('showSlideShow')) {
						$slideShowWrapper.removeClass('hideSlideShow').addClass('showSlideShow');
						$slideShow.addClass('showSlideShow');
					}
				}
			}
		}

		/* *** NO SLIDESHOW -> CLOSE *** */
		if(!isSlideShow && $slideShowWrapper.hasClass('showSlideShow')) {
			$('.showSlideShow').removeClass('showSlideShow');
			$slideShowWrapper.addClass('hideSlideShow');
			
			//Turn off keyboard events
			$(document).off('keyup.fullSlide');
		}
	/* *** END SLIDES SETUP *** */
};
/* *** END PAGELOAD *** */
	
/* ********************************* */
/* *** FULL SCREEN SLIDES OBJECT *** */
/* ********************************* */
let fullSlide = {
	/* *** CHECK FULLSCREEN *** */
	check: function() {
		return (	document.fullscreenElement ||    // alternative standard method
					document.mozFullScreenElement || document.webkitFullscreenElement || document.msFullscreenElement	)  // current working methods
	},

	/* *** ENTER FULLSCREEN *** */
	enter: function() {
		if(!this.check()) {
			if (document.documentElement.requestFullscreen) document.documentElement.requestFullscreen();
			else if (document.documentElement.msRequestFullscreen) document.documentElement.msRequestFullscreen();
			else if (document.documentElement.mozRequestFullScreen) document.documentElement.mozRequestFullScreen();
			else if (document.documentElement.webkitRequestFullscreen) document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);

			return true;
		}
		else return false;
	},

	/* *** EXIT FULLSCREEN *** */
	exit: function() {		
		if (document.exitFullscreen) document.exitFullscreen();
		else if (document.msExitFullscreen) document.msExitFullscreen();
		else if (document.mozCancelFullScreen) document.mozCancelFullScreen();
		else if (document.webkitExitFullscreen) document.webkitExitFullscreen();
	},

	/* *** TOGGLE FULLSCREEN *** */
	toggle: function() {
		if(!this.enter()) this.exit();
	},
	
	toggleBtn: function() {
		if(!fullSlide.check()) $('#fullScreenBtn').removeClass('exitFullBtn').addClass('enterFullBtn');
		else $('#fullScreenBtn').removeClass('enterFullBtn').addClass('exitFullBtn');
	}
};

/* *** INIT *** */
$(document).ready(function() {
	/* *** PREVENT OBJECTS FROM LOADING *** */
		media.cache();

	/* *** LOAD DATA *** */
		pageLoad();
		
	/* *** EVENT BINDERS *** */
		/* PAGE CHANGE */
		$(window).on('hashchange', pageLoad);

		/* SCROLL -> FLOAT HEADER */
		$(window).on('scroll.float', headerFloat.toggle);

		/* SLIDESHOW: TOGGLE FULLSCREEN BTN ON CHANGE */
		$(document).on('webkitfullscreenchange mozfullscreenchange msfullscreenchange fullscreenchange', fullSlide.toggleBtn);

		/* SOFTWARE PAGE: NAV CLICK SCROLL */
		$('#softwareContent .softDropDown li, #softwareContent .softMainNav').on('click', function() {
			let target = $(this).attr('data-nav'),
				tab = $(this).closest('.tabContent').attr('data-nav');
			
			$('html').animate({
				scrollTop: $('#softwareContent section.tabContent[data-nav="'+tab+'"] [data-nav="'+target+'"]').offset().top-205
			}, {
				queue: false,
				duration: gVars.durFast
			});
		});

	/* *** TEMP: LANGUAGE SELECTOR *** */
		$('#langSelect li').click(function() {
			if(!$(this).hasClass('selected')) {
				let $selected = $(this),
					lang = $selected.attr('id');

				$('.selected').removeClass('selected');
				$selected.addClass('selected').parent().prepend($selected);
				$('html').attr('lang', lang);
			}
		});
});