#### Schizo's Playground (2013)
~~Creative sharing platform~~, with CMS.  
[Live demo](https://ezrazebra.net/archive/2013-2012_schizosplayground.com/) (login: `demo1` / `demo1`)

`jQuery 1.8.2` [`History.js 1.7.1`](https://github.com/balupton/history.js) [`markItUp! 1.1.13`](http://markitup.jaysalvat.com)  
`PHP (2018 => 7)` [`jBBCode 1.0.3`](http://jbbcode.com/) [`Securimage 3.0.1`](http://www.phpcaptcha.org)  
`MySQL`

_design &copy;2013 Dirk Hertmans, Ezra Hertmans_
