<?php
	header("Content-type: text/css");
	include '../includes/var_dir.php';
?>

/*MAIN*/
	/*DOCUMENT BACKGROUND*/
	@font-face {
        font-family: "Arabic Typesetting Custom";
        src: url('<?php echo $cssfolder."arabtype.ttf"; ?>'); /*if IE */
        src: url('<?php echo $cssfolder."arabtype.ttf"; ?>') format("truetype"); /* non-IE */
	}
	
	
	html, body {
	height: 100%;
	margin: 0;
	background: #000000;
	font-family: Arabic Typesetting, Arabic Typesetting Custom; font-weight: bold; color: #000000;
	}

	/*BACKGROUND*/
	#bg {
	width: 1200px; min-height: 100%;
	margin: 0 auto;
	overflow: hidden;
	background: url('<?php echo $img['main']['bg']; ?>') no-repeat #ebebff;
	}

	/*LEFT COLUMN*/
		#left {
		width: 120px; 
		float: left;
		border-left: 1px solid gray; border-right: 1px solid gray;
		margin-bottom: -5000px; padding-bottom: 5000px; 
		background-color: #f6f3ea;
		opacity: 0.7;
		}

		#left ul {
		width: 120px;
		margin: 0; padding: 0; border: 0;
		list-style: none;
		}

		#left li {
		position: relative;
		width: 120px;
		opacity: 0.10;
		}

		#left a { 
		width: 120px; 
		display: block;
		}
	/**/

	/*USERBAR*/
		#userbar {
			position: relative;
			width: 1078px; height: 0px;
			margin-left: 122px;
			z-index: 4;
		}
	/**/

	/*HEADER*/
		#header {
		position: relative;
		width: 1078px; height: 179px;
		margin-left: 122px;
		background: url('<?php echo $img['main']['header']; ?>') no-repeat;
		}

		#header:hover {
		background-position: 0 -179px;
		}

		#header a {
		position: absolute;
		width: 100%; height: 100%;
		}
	/**/
/*END MAIN*/

/*MAIN RIGHT */
	#infocontainer {
	width: 211px;
	margin-top: 114px; margin-right: 40px;
	float: right;
	font-size: 20px; font-weight: normal; font-style: italic;
	margin-bottom: -5000px; padding-bottom: 5000px;
	}

	/*NAVB*/
		#navb {
		margin-bottom: 20px;
		font-size: 18px;
		}

		#navb span {
		font-weight: bold;
		}

		#forum, #pinboard, #channels, #channelsborder, #channelsbordertop {
		border: 1px solid gray;
		opacity: 0.90;
		}

		#forum a, #pinboard a, #channels a {
		position: absolute; top: 0; left: 0;
		width: 100%; height: 100%;
		}
		
		/*FORUM*/
			#forum {
			position: relative;
			width: 130px; height: 150px;
			margin-left: -73px;
			background: url('<?php echo $img['main']['forum']; ?>') no-repeat;
			z-index: 3;
			}

			#forum:hover {
			background-position: -132px 0;
			}

			#forum span {
			position: absolute;
			margin-left: 5px;
			}

		/*PINBOARD*/
			#pinboard {
			position: relative;
			width: 120px; height: 120px;
			margin-top: -122px; margin-left: 91px;
			background: url('<?php echo $img['main']['pinboard']; ?>') no-repeat;
			z-index: 1;
			}

			#pinboard:hover {
			background-position: -120px 0;
			}

			#pinboard span {
			position: absolute; right: 0;
			margin-right: 10px;
			}

		/*CHANNELS*/
			#channels {
			position: relative;
			width: 150px; height: 130px;
			margin-top: -19px; margin-left: 1px;
			background: url('<?php echo $img['main']['channels']; ?>') no-repeat;
			border-top: 0; border-right: 0;
			z-index: 2;
			}

			#channels:hover {
			background-position: -150px 0;
			}

			#channels span {
			position: absolute; bottom: 0;
			margin-left: 5px;
			}

			#channelsborder {
			position: relative; top: 18px;
			width: 100%; height: 113px;
			margin-left: -1px;
			border-top: 0; border-bottom: 0; border-left: 0;
			}

			#channelsbordertop {
			position: relative; top: -19px;
			width: 33px;
			margin-left: 58px;
			border-bottom: 0; border-left: 0; border-right: 0;
			}
	/*END NAVB*/
/*END MAIN RIGHT*/

/*CONTENT*/
	#inhoud {
	width: 785px;  height: 100%;
	margin-left: 142px; margin-top: 90px;
	}
	/*TITLE*/
		#title {
		width: 698px; height: 80px;
		margin-top: -85px; padding-left: 25px; padding-bottom: 15px;
		font-style: italic; text-align: left; font-size: 24px;
		cursor: default;
		}

		#title > span {
		font-size: 46px; font-style: normal;
		}

		.settitle {
		position: relative; top: -4px; left: -23px;
		height: 28px;
		padding-left: 7px; padding-bottom: 3px;
		border: 1px solid #71839a; border-bottom: 1px solid #505050; border-radius: 5px;
		background-color: #d0d8d9;
		font-size: 30px;
		opacity: 0.8;
		cursor: default;
		}
	/**/
/*END CONTENT*/

/*GENERAL CRAP*/
	fieldset {
	border: 0;
	}
	
	.inputSmall {
	height: 12px; font-size: 10px; padding: 0;
	}
		
	.error {
	font-size: 30px; font-style: italic; font-weight: normal; text-align: center; 
	cursor: default; line-height: 30px;
	}
	
	.noError {
	display: none;
	}

	a {
	font-style: italic; color: #000000; text-decoration: none; font-weight: bold;
	cursor: pointer;
	}

	a img {
	border: 0;
	}

	a:hover {
	color: #505050;
	}

	.hiddenoption {
	display: none;
	}

	/*LOGIN STUFF*/
		.resetPwdMsg {
		padding-right: 15px;
		font-size: 18px; font-style: italic; text-align: right; font-weight: normal;
		}

		.hideLoginFailedMsg {
		display: none;
		}
		
		#login_user, #login_pwd {
		width: 112px;
		}
		
		.adminTitle, .root_adminTitle {
		font-weight: bold;
		color: #961c00;
		}
		
		.inactiveTitle, .root_inactiveTitle {
		color: #7b9ccc;
		}
		
		.bannedTitle, .root_bannedTitle{
		padding-right: 3px;
		color: grey; font-style: italic; text-decoration: line-through;
		}
		
		.bannedTitle .ipFilter, .root_bannedTitle .ipFilter {
		color: grey;
		}
		
		.bannedTitle:hover, .bannedTitle a.ipFilter:hover, .root_bannedTitle:hover, .root_bannedTitle a.ipFilter:hover {
		color: #666666;
		}

		/*LOGIN BUTTON*/
			#login_btn {
			margin-right: 15px;
			border: 0;
			float: right;
			cursor: pointer;
			}

			#login_btn:disabled  {
			cursor: progress;
			}
			
			#login_btn {
			width: 23px; height: 19px;
			display: inline-block;
			background: url('<?php echo $img['form']['enter']; ?>') no-repeat;
			}

			#login_btn:hover {
			background-position: -23px 0;
			}
		/**/
	/*END LOGIN STUFF*/

	/*ERROR HANDLING*/
		#errorpopup {
		width: 75%;
		margin: 5px auto; padding-top: 3px;
		display: none;
		background-color: #bc604d;
		border: 1px solid #808080;
		font-size: 18px; font-weight: normal; line-height: 18px;
		border-radius: 5px;
		}

		#successpopup {
		width: 75%;
		margin: 5px auto; padding-top: 3px;
		display: none;
		background-color: #4dbc59;
		border: 1px solid #808080;
		font-size: 18px; font-weight: normal; line-height: 18px;
		border-radius: 5px;
		}

		.inputerrorpopup {
		border: 1px solid red !important;
		}

		.inputcorrectpopup {
		border: 1px solid green !important;
		}

		.forminfo td, .forminfo div, span.forminfo {
		font-family: Lucida Console; font-size: 10px; font-weight: normal; line-height: 10px;
		}

		.forminfo td span {
		position: relative; top: -5px; left: 5px;
		}
		
		.forminfo .usertaken, .forminfo .emailtaken, .forminfo .captchaerror, .forminfo .activationerror {
		display: none;
		color: red;
		}

		.forminfo td .usertaken {
		position: relative; top: -9px;
		}

		.forminfo td .emailtaken {
		position: relative; top: -4px;
		}

		.forminfo td .captchaerror , .forminfo td .activationerror {
		position: relative; top: -4px;
		}
	/*END ERROR HANDLING*/
	
	/*FORM BUTTONS*/
		input[type="text"], input[type="password"] {
		border: 1px solid lightgrey;
		}
		
		#form_btnbar div {
		width: 75%; height: 30px;
		margin: auto; padding-top: 8px;
		text-align: center;
		}

		#form_btn {
		width: 110px; height: 29px;
		background-color: #f6f3ea;
		border: 1px solid #808080; border-radius: 3px;
		font-family: Lucida Console; font-size: 20px; font-weight: normal; color: #505050;
		cursor: pointer;
		}

		#noform_btn {
		width: 108px; height: 27px;
		margin: 10px auto;
		background-color: #f6f3ea;
		border: 1px solid #808080; border-radius: 3px;
		font-family: Lucida Console; font-size: 20px; font-weight: normal; color: #505050; text-align: center;
		cursor: pointer;
		}

		#noform_btn span {  line-height: 29px; }

		#form_btn:hover, #noform_btn:hover {
		background-color: #f6f3cb;
		}

		#form_btn:disabled  {
		background-color: #f6f3da;
		color: #808080;
		cursor: progress;
		}
			
		#form_btn.form_btn_small, #noform_btn.form_btn_small {
		width: 55px; height: 25px;
		display: inline-block;
		font-size: 12px !important; font-weight: bold;
		}
			
		#noform_btn.form_btn_small {
		width: 54px; height: 23px;
		margin: 5px auto;
		}

		#noform_btn.form_btn_small span {  line-height: 25px; }
	/*END FORM BUTTONS
/*END GENERAL CRAP*/

/*INFO/ADS COLUMN (RIGHT)*/
	#reklam {
	margin-bottom: 40px;
	width: 211px;
	border: 1px solid #c4c4c4;
	background-color: #e0e8e9;
	text-align: center;
	opacity: 0.8;
	}
	
	#reklam ul {
	list-style: none;
	}
	#reklam li {
	margin: 0; margin-left: -40px;
	width: 211px;
	}

	#reklam a {
	position: relative;
	width: 211px;
	display: table-cell;
	vertical-align: middle;
	border-radius: 4px;
	}
	
	#reklam li:not(.googleAd):not(.ad_divider) a {
	padding: 11px 0;
	}
	
	#reklam li.googleAd {
	padding: 11px 0;
	}
	
	#reklam a img {
	display: inline-block;
	margin-bottom: -10px;
	}

	#reklam a:hover {
	background-color: #d2e6e9;
	}
	
	#reklam .ad_divider {
	margin: 0; margin-left: 5px;
	width: 119px; height: 0;
	border-bottom: 1px dotted #c4c4c4;
	}

	#reklam .ad_divider:first-child {
	margin-top: -10px;
	}
	
	#reklam.reklamBanner {
	margin-left: 15px; margin-top: 198px; margin-bottom: 20px;
	width: 748px; height: 112px;
	}
	
	#reklam.reklamBanner li{
	margin-top: -16px;
	width: 748px;
	}
	
	#reklam.reklamBanner li:not(.googleAd){
	height: 90px;
	}
	
	#reklam.reklamBanner a{
	width: 748px; height: 90px;
	}
/*END INFO/ADS COLUMN (RIGHT)*/

/*UI BUTTONS*/
	/*ADD, DELETE, EDIT, SAVE*/		
		.uibtnAdd, .uibtnAddGoogle, .uibtnDelete, .uibtnCancel, .uibtnEdit, .uibtnSave, .uibtnBan, .uibtnUnban, .uibtnUpArrowBlue, .uibtnDownArrowBlue {
		cursor: pointer;
		}
		
		.uibtnAdd span, .uibtnAddGoogle span,  .uibtnDelete span, .uibtnCancel span, .uibtnEdit span, .uibtnSave span, .uibtnBan span, .uibtnUnban span {
		width: 10px; height: 10px;
		margin-right: 3px;
		display: inline-block;
		}

		.uibtnAdd:hover  span, .uibtnAddGoogle:hover span,  .uibtnDelete:hover span, .uibtnCancel:hover span, .uibtnEdit:hover span, .uibtnSave:hover span, .uibtnBan:hover span, .uibtnUnban:hover span {
		background-position: -10px 0;
		}

		.uibtnAdd:hover, .uibtnAddGoogle:hover,  .uibtnDelete:hover, .uibtnCancel:hover, .uibtnEdit:hover, .uibtnSave:hover, .uibtnBan:hover, .uibtnUnban:hover, .uibtnUpArrowBlue:hover, .uibtnDownArrowBlue:hover {
		color: #505050;
		}

		.uibtnAdd span {
		background: url('<?php echo $img['ui']['add']; ?>') no-repeat;
		}

		.uibtnAddGoogle span {
		background: url('<?php echo $img['ui']['googleadd']; ?>') no-repeat;
		}

		.uibtnDelete span, .uibtnCancel span {
		background: url('<?php echo $img['ui']['delete']; ?>') no-repeat;
		}

		.uibtnEdit span {
		background: url('<?php echo $img['ui']['edit']; ?>') no-repeat;
		}

		.uibtnSave span {
		background: url('<?php echo $img['ui']['save']; ?>') no-repeat;
		}

		.uibtnBan span {
		background: url('<?php echo $img['ui']['ban']; ?>') no-repeat;
		}

		.uibtnUnban span {
		background: url('<?php echo $img['ui']['unban']; ?>') no-repeat;
		}
		
		.uibtnSave, .uibtnCancel, .uibtnHidden {
		display: none;
		}
		
		.uibtnShown {
		display: inline;
		}
	/**/
	
	/*LEFT, RIGHT ARROWS*/
		.uibtnLeftArrow span, .uibtnRightArrow span {
		width: 5px; height: 10px;
		display: inline-block;
		background: url('<?php echo $img['ui']['arrowx']; ?>') no-repeat;
		}

		.uibtnLeftArrow span {
		background-position: 0 -10px;
		}

		.uibtnLeftArrow:hover span, .uibtnLeftArrow.disabled span {
		background-position: -5px -10px;
		}

		.uibtnRightArrow:hover span, .uibtnRightArrow.disabled span {
		background-position: -5px 0;
		}
	/**/
	
	/*DOWN, UP ARROWS*/
		.uibtnDownArrow, .uibtnUpArrow {		
		cursor: pointer;
		}
	
		.uibtnDownArrow span, .uibtnUpArrow span {
		width: 10px; height: 5px;
		display: inline-block;
		background: url('<?php echo $img['ui']['arrowy']; ?>') no-repeat;
		}

		.uibtnUpArrow span {
		background-position: 0 -5px;
		}

		.uibtnDownArrow:hover span {
		background-position: -10px 0;
		}

		.uibtnUpArrow:hover span {
		background-position: -10px -5px;
		}
		
		/*BLUE DOWN, UP ARROWS*/
		.uibtnDownArrowBlue span, .uibtnUpArrowBlue span {
		width: 10px; height: 7px; margin-right: 3px;
		display: inline-block;
		background: url('<?php echo $img['ui']['arrowyblue']; ?>') no-repeat;
		}

		.uibtnUpArrowBlue span {
		background-position: 0 -7px;
		}

		.uibtnDownArrowBlue:hover span {
		background-position: -10px 0;
		}

		.uibtnUpArrowBlue:hover span {
		background-position: -10px -7px;
		}
	/**/
/*END UI BUTTONS*/

/*BBCODE*/
	.bbimg {
	max-width: 730px;
	}

	.bbquote {
	margin: 0 50px; padding: 15px;
	border: 1px solid #808080;
	font-style: italic;
	}

	.bbquote span {
	padding-left: 10px;
	font-style: normal; font-size: 10px;
	}

	ol.bblist {
	margin: 3px 20px; padding: 0 15px;
	}

	ul.bblist {
	margin: 3px 10px; padding: 0 15px;
	}
	
	.bbleft {
	text-align: left;
	}
	
	.bbcenter {
	text-align: center;
	}
	
	.bbright {
	text-align: right;
	}
/*END BBCODE*/

/*OVERLAY*/
	#overlaybg {
	position: fixed; top: 0; left: 0;
	height: 100%; width: 100%;
	background-color: black;
	z-index: 4;
	display: none;
	}

	#overlaywrapper {
	position: fixed; top: 0; left: 0;
	height: 100%; width: 100%;
	z-index: 5;
	overflow: auto;
	display: none;
	}

	#overlay {
	position: relative;
	margin: 100px auto 50px;
	width: 800px;
	border: 2px solid gray;
	border-radius: 10px;
	z-index: 6;
	display: none;
	}
	
	#overlay.login {
	width: 275px;
	}

	#overlay.register {
	width: 400px;
	}
/*END OVERLAY*/

/*LOADING*/
	.loading {
	position: absolute; top: 0; left: 0;
	width: 100%; height: 100%;
	opacity: 0.7;
	background-color: lightgrey;
	border-radius: 10px;
	}

	.loading div {
	position: absolute; top: 12%;
	height: 20px; width: 100%;
	font-size: 20px; line-height: 20px; font-weight: bold; text-align: center;
	}
/**/