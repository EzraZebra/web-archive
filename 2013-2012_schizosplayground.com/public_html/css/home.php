<?php
	header("Content-type: text/css");
	include '../includes/var_dir.php';
?>

/*HOME*/
	#homewrapper {
	position: relative; top: -41px; left: -50px;
	width: 745px; height: 500px;
	font-size: 20px;
	}

	/*CATEGORY LINKS*/
		#wordb, #soundb, #imageb, #performanceb {
		position: relative;
		border: 1px solid gray;
		}

		#wordb a, #soundb a, #imageb a, #performanceb a {
		position: absolute; left: 0;
		width: 100%; height: 100%;
		}

		#wordb span, #imageb span, #performanceb span {
		position: absolute; bottom: -22px; right: 5px;
		}

		/*SOUND*/
			#soundb {
			margin-top: 5px; margin-left: 140px;
			width: 190px; height: 190px;
			background: url('<?php echo $img['home']['soundb']; ?>') no-repeat;
			}

			#soundb span {
			position: absolute; bottom: -22px; left: 5px;
			}

			#soundb:hover {
			background-position: -190px 0;
			}

		/*WORD*/
			#wordb {
			margin-top: 43px; margin-left: 239px;
			width: 180px; height: 165px;
			background: url('<?php echo $img['home']['wordb']; ?>') no-repeat;
			}

			#wordb:hover {
			background-position: -180px 0;
			}

			
		/*IMAGE*/
			#imageb {
			margin-top: -302px; margin-left: 426px;
			width: 190px; height: 175px;
			background: url('<?php echo $img['home']['imageb']; ?>') no-repeat;
			}

			#imageb:hover {
			background-position: -190px 0;
			}
			
		/*PERFORMANCE*/
			#performanceb {
			margin-top: 5px; margin-left: 337px;
			width: 175px; height: 165px;
			background: url('<?php echo $img['home']['performanceb']; ?>') no-repeat;
			}

			#performanceb:hover {
			background-position: -175px 0;
			}
	/*END CATEGORY LINKS*/
/*END HOME*/