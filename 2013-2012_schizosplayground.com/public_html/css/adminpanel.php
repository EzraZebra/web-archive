<?php
	header("Content-type: text/css");
	include '../includes/var_dir.php';
?>

/*ADMIN PANEL*/
	#admincontentwrapper {
	position: relative;
	}
	/*LIST & MENU MARKUP*/
		.adminlist, .adminUserForm fieldset > div {
		border: 1px solid #71839a; border-radius: 5px;
		background: #e0e8e9;
		opacity: 0.8;
		cursor: default;
		}

		.adminlist tr:nth-child(odd) {
		background: #d0d8d9;
		}

		.adminlist tr:hover, .adminlist .itemSelected {
		background-color: #d2e6e9 !important;
		}
		
		.adminUserForm {
		margin-bottom: 25px;
		text-align: left; font-size: 20px;
		}

		.adminUserForm fieldset > div {
		position: relative;
		width: 350px;
		margin: auto;
		padding: 5px;
		}

		.adminUserForm td, .adminlist td, th {
		padding: 3px;
		}

		.adminUserForm tr:last-child td {
		padding-bottom: 0px;
		text-align: center;
		}

		.adminUserForm  .forminfo span {
		margin-right: 10px;
		}
	/*END MENU MARKUP*/
	/*ADMIN MENU*/
		#adminmenu {
		position: absolute; top: 120px; right: -174px;
		margin-left: 1px; width: 150px;
		float: right;
		}

		#adminmenu ul { position: relative;
		margin: 0; padding: 0;
		list-style:none;
		}

		#adminmenu li {
		padding-left: 5px;
		border: 1px solid #505050; border-radius: 3px 0 0 3px;
		background-color: #e0e8e9;
		font-size: 26px;
		}

		#adminmenu .adminsubmenu {
		width: 136px; margin-left: 7px;
		background: #d0d8d9;
		}

		#adminmenu li:hover {
		background-color: #d2e6e9 !important;
		}

		#adminmenu li a {
		height: 30px;
		padding-top: 3px;
		display: block;
		}

		#adminmenu .adminsubmenu a {
		height: 28px;
		padding-top: 1px;
		}

		#adminmenu li:last-child {
		border-bottom: 2px solid #505050;
		}

		#adminmenu li:hover {
		border-color: #808080;
		}
	/*END ADMIN MENU*/

	/*ADMIN CONTENT*/
		#admincontent fieldset {
		padding: 0;
		}
		
		#admincontent .loading {
		background-color: #c4cbcc; border-radius: 5px;
		}
		
		#admincontent .settitle {
		left: 0;
		margin-bottom: 10px;
		}

		/*ADMIN SETTINGS*/
			#adminsettings {
			margin-bottom: 15px; padding-left: 5px;
			font-size: 20px;
			cursor: default;
			}

			#adminsettings input[type=text] {
			margin-left: 5px;
			text-align: right;
			}

			#adminsettings .admin_editLength {
			width: 20px;
			}
			
			#admin_editSettingsSuccess {
			cursor: default;
			}
		/*END ADMIN SETTINGS*/

		/*ADMIN TOOLS*/
			.admintools {
			position: relative; margin-left: 7px; margin-right: 9px;
			font-size: 20px; line-height: 16px; text-align: right; 
			}

			.admintools .uibtnAdd, .admintools .newItemContainer {
			float: left;
			}
			
			.admintools .newItemContainer .uibtnAdd {
			float: none;
			}
			
			.admintools #ipFilterTools {
			position: absolute; left: 0; top: -2px;
			}
			
			.admintools #ipFilterTools input {
			}
			
			span.forminfo {
			float: left; margin-left: 10px;
			}

			.admintools .uibtnSave, .admintools .uibtnCancel, .adminlist .checklist, #adminNew .uibtnCancel {
			display: inline;
			}

			#adminNew {
			display: none;
			}
			/**/
		/*END ADMIN TOOLS
				
		/*ADMIN LIST*/
			.adminlist {
			position: relative;
			width: 100%; padding: 0; margin: 0; margin-bottom: 30px;
			font-size: 20px; text-align: left; line-height: 16px;
			}

			.adminlist th{
			background: #c4cbcc;
			}

			.adminlist .intcell {
			text-align: right;
			}

			.adminlist .imgcell {
			text-align: center;
			}

			/*INPUTS*/
				.adminlist .registerinput, .admin_editInput, #newItem, #itemPrototype {
				display: none;
				}
				
				.admin_editInputSmall {
				width: 15px; text-align: right;
				}

				.adminlist #toolscol {
				width: 125px; min-width: 125px;
				}

				.adminlist input[type=checkbox] {
				float: right;
				}
			/*END INPUTS*/
			/*LOGIN*/
				#loginlist #toolscol {
				width: 100px !important; min-width: 100px !important;
				}
				
				th#ipCol {
				width: 180px;
				}
				
				#ipFilterLabel {
				display: none;
				}
			/*END LOGIN*/
			/*ADS*/				
				img.admin_googleAdForm {
				margin-right: 3px;
				float: left;
				}
				
				#newItem img.admin_googleAdForm, .editing img.admin_googleAdForm {
				margin-top: 2px;
				}
			/*END ADS*/
			/*FILE UPLOAD*/
				.adminlist .fileUploadCell {
				width: 150px; height: 90px; min-width: 150px;
				padding: 0; padding-top: 5px;
				}
				
				.adminlist iframe {
				width: 100%; height: 100%;
				border: 0;
				}
				
				.adminEditSubmit {
				display: none;
				}
				
				.admin_banlistGroupItemIP {
				width: 103px; height: 15px;
				padding-top: 3px;
				display: inline-block;
				border-left: 2px solid #7b9ccc; border-bottom: 1px dotted #7b9ccc;
				}
			/*END FILE UPLOAD*/
		/*END ADMIN LIST*/
	/*END ADMIN CONTENT*/
/*END ADMIN PANEL*/