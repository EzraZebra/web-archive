<?php
	header("Content-type: text/css");
	include '../includes/var_dir.php';
?>

/*POPUP HEADER*/
	#closebtnpopup {
	position: absolute; right: 0; top: 0;
	width: 28px; height: 28px;
	border-radius: 0 6px 0 8px;
	cursor: pointer;
	background: url('<?php echo $img['popups']['closebtn']; ?>') no-repeat;
	z-index: 1;
	}

	#closebtnpopup:hover {
	background-position: -28px 0;
	}

	#bgpopup {
	width: 100%; min-height: 50px;
	background: url('<?php echo $img['popups']['bg']['main']; ?>') no-repeat #ebebed;
	border-radius: 8px;
	text-align: center; font-size: 22px;
	overflow: hidden;
	cursor: default;
	}

	#inhoudpopup {
	width: 100%; height: 100%;
	}

	#headerpopup {
	margin: 3px 40px;
	border-bottom: 1px solid black;
	}

	#titlepopup {
	margin-bottom: -5px;
	height: 30px; padding: 0;
	line-height: 30px; font-size: 40px; 
	}

	#titlepopup.hidden {
	display: none;
	}

	/*PAGES*/
		#pagespopup img {
		cursor: pointer;
		}

		.pagesBtnPopup {
		cursor: pointer;
		}

		#selectpopup {
		width: 90px;
		}

		.expand {
		width: auto;
		}
		
		.itemDate {
		display: none;
		}
	/**/

	/*TOGGLE BUTTON*/
		#toggleBtnPopup {
		position: relative; margin-top: -3px; margin-bottom: -1px;
		width: 100%; height: 10px;
		cursor: pointer; 
		border-bottom: 1px solid black;
		}

		#toggleBtnPopup:hover {
		border-bottom: 1px solid #7f7f7f;
		}

		#toggleBtnPopup span {
		position: absolute; bottom: -1px;
		}

		#toggleBtnPopup span:first-of-type {
		left: -4px;
		}

		#toggleBtnPopup span:last-of-type {
		right: -4px;
		}
	/**/
/*END POPUP HEADER*/

/*POPUP MAIN*/
	#mainpopup {
	position: relative;
	}

	/*TITLE BAR*/
		#itemtitlepopup {
		position: relative;
		margin: 5px;
		font-style: italic; font-size: 22px;
		}
		
		#itemtitlespan {
		display: block;
		max-width: 430px;
		margin: auto;
		line-height: 20px;
		overflow: hidden;
		}

		#itemtitleinput {
		display: none;
		}

		/*EDIT BUTTONS*/
			#itemeditspan, #itemcancelspan, #itemsavespan, #itemdeletespan, #itemnewspan {
			position: absolute; right: 25px; bottom: -5px;
			font-style: normal; font-size: 20px;
			cursor: pointer;
			}

			#itemeditspan:hover, #itemcancelspan:hover, #itemsavespan:hover, #itemdeletespan:hover, #itemnewspan:hover {
			color: #505050;
			}

			#itemeditspan {
			margin-right: 60px;
			}
			
			#itemcancelspan {
			display: none;
			}

			#itemsavespan {
			margin-right: 65px;
			display: none;
			}

			#itemnewspan {
			margin-right: 103px;
			}
		/**/
	/*END TITLE BAR*/

	/*POPUP CONTENT*/
		#contentpopup {
		padding: 10px; margin: 20px; margin-top: -3px;
		overflow: auto;
		border: solid black; border-width: 2px 3px;
		text-align: left; font-weight: normal;
		cursor: auto;
		}

		#contentpopup textarea {
		width: 715px; height: 500px;
		margin-left: 4px;
		}

		#contenttxt {
		margin-top: -3px; padding-bottom: 5px;
		font-family: Lucida Console; font-size: 12px; font-weight: normal;
		}

		#contentedit {
		display: none;
		}

		#user {
		font-size: 20px; font-style: italic; font-weight: bold; cursor: default;
		}
	/**/
	
	/*POPUP FORMS*/
		.formpopup {
		font-size: 20px; font-style: normal; text-align: left;
		}

		.formpopup td {
		width: 50%;
		}

		.formpopup table {
		border-collapse: collapse;
		}

		.formpopup fieldset {
		border: 0;
		}

		/*LOGIN MODULE*/
			#loginform #loginbar {
			padding: 5px;
			border-top: 1px solid black;
			}

			#loginform  {
			margin: auto; margin-bottom: -15px;
			}

			#loginpopup a {
			font-weight: bold;
			}
		/**/

		/*RESET MODULE*/
			#resetCont, #resetBack {
			margin-top: 3px; margin-bottom: -3px;
			font-weight: normal; font-size: 18px;
			}
			
			.resetinput {
			width: 112px;
			}
		/**/
		
		/*REGISTER MODULE*/
			#registermsg {
			width: 85%;
			margin: auto; margin-top: 10px;
			border-bottom: 1px solid black;
			font-size: 10px;
			}

			#register #form_btnbar div {
			border-top: 1px solid black;
			}
			
			.registerinput {
			width: 175px;
			}
		/**/

		/*ACTIVATE MODULE*/
			#activation_code {
			width: 112px;
			}

			#activateform table {
			width: 250px;
			margin: auto;
			}

			.activationsuccess {
			border: 0 !important;
			}
		/**/
		
		/*CAPTCHA MODULE*/
			#captcha {
			border: 1px solid black;
			cursor: pointer;
			}

			#captchaimg {
			text-align: center;
			}

			#captchaimg div {
			position: relative;
			}

			#changecaptcha {
			position: absolute; bottom: 3px;
			margin-left: 5px;
			cursor: pointer;
			}

			#changecaptcha span {
			width: 20px; height: 20px;
			display: inline-block;
			background: url('<?php echo $img['form']['refresh']; ?>') no-repeat;
			}

			#changecaptcha:hover span {
			background-position: -20px 0;
			}
			
			#captcha_code {
			width: 77px;
			}
		/**/
	/*END POPUP FORMS*/
/*END POPUP MAIN*/
/*NEWS*/
	#bgpopup[class=home] {
	background-image: url('<?php echo $img['popups']['bg']['news']; ?>');
	}
/**/