<?php
	header("Content-type: text/css");
	include '../includes/var_dir.php';
?>

/*FILE UPLOAD*/
@font-face {
	font-family: "Arabic Typesetting Custom";
	src: url('<?php echo $cssfolder."arabtype.ttf"; ?>'); /*if IE */
	src: url('<?php echo $cssfolder."arabtype.ttf"; ?>') format("truetype"); /* non-IE */
}

body {
margin: 0;
text-align: center;
overflow: hidden;
font-family: Arabic Typesetting, Arabic Typesetting Custom; font-weight: bold; color: #000000; font-size: 20px;
}

input {
border: 1px solid lightgrey;
}

#error {
display: none;
}

/*IMAGE*/
	#imagecontainer {
	position: relative;
	width: 150px; height: 90px;
	padding: 0;
	display: table-cell;
	vertical-align: middle; 
	}

	#imagecontainer img {
	max-width: 140px;  max-height: 80px; 
	margin-bottom: -10px;
	}

	#img_Delete_container {
	position: absolute; top: 0; left: 0;
	width: 150px; height: 60px;
	padding-top: 30px;
	background: #d2e6e9;
	opacity: 0;
	color: black;
	}
/*END IMAGE*/
/*FORM*/			
	#fileUpload_file {
	display: none;
	}
	
	#fileUpload_path{
	}
	
	#fileUpload_pathContainer{
	position: relative;
	}

	#fileUpload_pathClicker {
	position: absolute; left: 0; top: 0;
	width: 135px; height: 19px;
	display: block;
	cursor: pointer;
	}

	#fileUploadForm.moveForm{
	margin-top: 10px;
	}

	#fileUploadForm.hideForm {
	margin-top: 0px;
	display: none;
	}

	#img_Delete_form {
	display: none;
	}
/*END FORM*/

/*TOOLS UI*/
	#img_Delete {
	display: none;
	}

	#img_Choose, #img_Delete_container.editing {
	cursor: pointer;
	}

	#img_Choose span, #img_Delete span {
	width: 10px; height: 10px;
	margin-right: 3px;
	display: inline-block;
	}

	#img_Choose span {
	background: url('<?php echo $img['ui']['add']; ?>') no-repeat;
	}

	#img_Delete span {
	background: url('<?php echo $img['ui']['delete']; ?>') no-repeat;
	}

	#img_Delete_container.editing:hover {
	opacity: 0.9;
	}

	#img_Delete_container.editing:hover #img_Delete {
	display: block;
	}

	#img_Choose:hover span, #img_Delete_container:hover #img_Delete span {
	background-position: -10px 0;
	}

	#img_Choose:hover, #img_Delete_container:hover #img_Delete {
	color: #505050;
	}
/*END TOOLS UI*/

/*LOADING*/
	.loading {
	position: absolute; top: 0; left: 0;
	width: 100%; height: 100%;
	opacity: 0.7;
	background-color: #d2e6e9;
	}

	.loading div {
	position: absolute; top: 30%;
	height: 20px; width: 100%;
	font-size: 20px; line-height: 20px; font-weight: bold; text-align: center;
	}
/*END LOADING*/