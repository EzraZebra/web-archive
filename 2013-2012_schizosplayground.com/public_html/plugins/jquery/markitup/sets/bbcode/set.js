// ----------------------------------------------------------------------------
// markItUp!
// ----------------------------------------------------------------------------
// Copyright (C) 2008 Jay Salvat
// http://markitup.jaysalvat.com/
// ----------------------------------------------------------------------------

var myBbcodeSettings = {
  nameSpace:          "bbcode", // Useful to prevent multi-instances CSS conflict
  markupSet: [
      {name:'Bold', key:'B', openWith:'[b]', closeWith:'[/b]'}, 
      {name:'Italic', key:'I', openWith:'[i]', closeWith:'[/i]'}, 
      {name:'Underline', key:'U', openWith:'[u]', closeWith:'[/u]'}, 
      {separator:'---------------' },
	  {name:'Align Left', openWith:'[left]', closeWith:'[/left]'},
	  {name:'Align Center', openWith:'[center]', closeWith:'[/center]'},
	  {name:'Align Right', openWith:'[right]', closeWith:'[/right]'}, 
      {separator:'---------------' },
      {name:'Size', key:'S', openWith:'[size=[![Text size]!]]', closeWith:'[/size]', dropMenu :[
          {name:'Very Big', openWith:'[size=18]', closeWith:'[/size]' },
          {name:'Big', openWith:'[size=14]', closeWith:'[/size]' },
          {name:'Normal', openWith:'[size=12]', closeWith:'[/size]' },
          {name:'Small', openWith:'[size=10]', closeWith:'[/size]' },
          {name:'Very Small', openWith:'[size=8]', closeWith:'[/size]' }
      ]},
      {name:'Colors', openWith:'[color=[![Color]!]]', closeWith:'[/color]', dropMenu: [
          {name:'Yellow', openWith:'[color=#dcdc00]', closeWith:'[/color]', className:"col1-1" },
          {name:'Orange', openWith:'[color=#ddb04a]', closeWith:'[/color]', className:"col1-2" },
          {name:'Red', openWith:'[color=#d25441]', closeWith:'[/color]', className:"col1-3" },
          {name:'Blue', openWith:'[color=#5f8bca]', closeWith:'[/color]', className:"col2-1" },
          {name:'Purple', openWith:'[color=#9860be]', closeWith:'[/color]', className:"col2-2" },
          {name:'Green', openWith:'[color=#8bb262]', closeWith:'[/color]', className:"col2-3" },
          {name:'White', openWith:'[color=white]', closeWith:'[/color]', className:"col3-1" },
          {name:'Grey', openWith:'[color=#808080]', closeWith:'[/color]', className:"col3-2" },
          {name:'Black', openWith:'[color=black]', closeWith:'[/color]', className:"col3-3" }
      ]},
      {separator:'---------------' },
      {name:'List', openWith:'\n[list]\n', closeWith:'\n[/list]\n'}, 
      {name:'Unordered List item', openWith:'[*] '}, 
      {name:'Ordered List item', openWith:'[#] '}, 
      {name:'Indent Text', openWith:'[indent]', closeWith:'[/indent]'}, 
      {separator:'---------------' },
      {name:'Quote', openWith:'\n[quote=[![Who are you quoting?]!]]', closeWith:'[/quote]\n'},
      {separator:'---------------' },
      {name:'Image', key:'M', openWith:'[img=[![Please nter the image\'s URL:]!]]', closeWith:'[/img]', placeHolder:'Image Title'},
      {name:'Link', key:'L', openWith:'[url=[![Please enter the URL:]!]]', closeWith:'[/url]', placeHolder:'Link text'}
   ]
}