-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 03, 2019 at 04:07 PM
-- Server version: 10.3.18-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `schizo`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `ad_id` int(11) NOT NULL,
  `widget` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `url` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `img` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `priority` int(1) NOT NULL,
  `googleAd` tinyint(1) NOT NULL DEFAULT 0,
  `slot` bigint(10) DEFAULT NULL,
  `width` int(3) DEFAULT NULL,
  `height` int(3) DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT 0,
  `clicks` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`ad_id`, `widget`, `name`, `url`, `img`, `priority`, `googleAd`, `slot`, `width`, `height`, `views`, `clicks`) VALUES
(1, 'rightcolumn', 'Notepad++: free source code editor', 'http://notepad-plus-plus.org/', '450215437.png', 5, 0, 0, 0, 0, 137, 5),
(2, 'rightcolumn', 'GIMP: free image manipulation program', 'http://www.gimp.org/', '881237713.png', 5, 0, 0, 0, 0, 135, 1),
(3, 'rightcolumn', 'REAPER: digital audio workstation', 'http://reaper.fm/', '1121159252.png', 3, 0, 0, 0, 0, 297, 7),
(4, 'rightcolumn', 'Firefox: free web browser', 'http://www.mozilla.org/firefox/', '1553525360.png', 1, 0, 0, 0, 0, 156, 16),
(5, 'rightcolumn', 'Photoscape: free photo editing software', 'http://www.photoscape.org/ps/main/index.php/advertise/', '1897944492.jpg', 4, 0, 0, 0, 0, 181, 3),
(6, 'rightcolumn', 'foobar2000: advanced freeware audio player', 'http://www.foobar2000.org/', '3483249191.jpg', 2, 0, 0, 0, 0, 391, 6),
(7, 'rightcolumn', 'VLC: free multimedia player', 'http://www.videolan.org', '3049947993.png', 3, 0, 0, 0, 0, 249, 3),
(8, 'rightcolumn', 'FileZilla: free FTP solution', 'https://filezilla-project.org/', '3229335726.png', 4, 0, 0, 0, 0, 225, 10),
(9, 'rightcolumn', 'Audacity: free audio editor and recorder', 'http://audacity.sourceforge.net/', '1880605548.png', 1, 0, 0, 0, 0, 129, 3),
(10, 'rightcolumn', 'WampServer: Apache, PHP, MySQL on Windows', 'http://www.wampserver.com', '4108050209.png', 2, 0, 0, 0, 0, 263, 4),
(11, 'rightcolumn', 'Exact Audio Copy:audio grabber foraudio CDs', 'http://exactaudiocopy.de/', '2305878314.png', 4, 0, 0, 0, 0, 227, 6),
(26, 'rightcolumn', 'Schizo Right Column', '', NULL, 9, 1, 8140250233, 180, 150, 0, 0),
(27, 'rightcolumn', 'Schizos Playground Square Image', '', NULL, 9, 1, 1814515034, 200, 200, 0, 0),
(14, 'rightcolumn', 'IXQuick: the world\'s most private browser', 'https://ixquick.com', '4045638317.jpg', 4, 0, 0, 0, 0, 149, 2),
(20, 'rightcolumn', 'Soulseek: free file sharing application', 'http://www.soulseekqt.net', '807724274.png', 1, 0, 0, 0, 0, 176, 6),
(25, 'rightcolumn', 'Schizo Right Column', '', NULL, 9, 1, 8140250233, 180, 150, 0, 0),
(24, 'rightcolumn', 'Schizos Playground Square Image', '', NULL, 4, 1, 1814515034, 200, 200, 37, 0),
(28, 'rightcolumn', 'Schizos Playground Square Image', '', NULL, 4, 1, 1814515034, 200, 200, 0, 0),
(29, 'bottombanner', 'Schizo Banner', '', NULL, 1, 1, 1917089832, 728, 90, 0, 0),
(32, 'bottombanner', 'zefezf', 'http://', '2212294583.png', 1, 0, 0, 0, 0, 17834, 2837);

-- --------------------------------------------------------

--
-- Table structure for table `ad_settings`
--

CREATE TABLE `ad_settings` (
  `googlePub_id` bigint(16) DEFAULT NULL,
  `rightcolumn_length` int(1) NOT NULL DEFAULT 3
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ad_settings`
--

INSERT INTO `ad_settings` (`googlePub_id`, `rightcolumn_length`) VALUES
(7516392572486384, 3);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(25) CHARACTER SET latin1 NOT NULL,
  `cat_url` varchar(25) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_name`, `cat_url`) VALUES
(5, 'Fun', 'fun'),
(3, 'Image', 'image'),
(1, 'News', 'home'),
(4, 'Sound', 'sound'),
(2, 'Word', 'word');

-- --------------------------------------------------------

--
-- Table structure for table `categories_items`
--

CREATE TABLE `categories_items` (
  `cat_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories_items`
--

INSERT INTO `categories_items` (`cat_id`, `item_id`) VALUES
(1, 27),
(1, 28),
(1, 29),
(1, 30);

-- --------------------------------------------------------

--
-- Table structure for table `clearance`
--

CREATE TABLE `clearance` (
  `clearance_id` int(11) NOT NULL,
  `title` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `special_perm` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_news` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `clearance`
--

INSERT INTO `clearance` (`clearance_id`, `title`, `special_perm`, `post_news`) VALUES
(1, 'Schizophrenic', 'root_user', 0),
(2, 'Lobotobmized', 'root_banned', 0),
(3, 'Undiagnosed', 'root_inactive', 0),
(4, 'Headshrink', 'root_admin', 1),
(6, 'Orderly', 'admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `global_settings`
--

CREATE TABLE `global_settings` (
  `newspanel_length` int(2) NOT NULL DEFAULT 3,
  `login_remember` int(2) NOT NULL DEFAULT 10,
  `login_time` int(2) NOT NULL DEFAULT 30,
  `activity_threshold` int(2) NOT NULL DEFAULT 5,
  `left_length` int(2) NOT NULL DEFAULT 7
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `global_settings`
--

INSERT INTO `global_settings` (`newspanel_length`, `login_remember`, `login_time`, `activity_threshold`, `left_length`) VALUES
(5, 5, 30, 5, 7);

-- --------------------------------------------------------

--
-- Table structure for table `ipbans`
--

CREATE TABLE `ipbans` (
  `ip` int(10) UNSIGNED NOT NULL,
  `ip_range` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `item_id` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET latin1 NOT NULL,
  `filename` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `text` text CHARACTER SET latin1 DEFAULT NULL,
  `descr` text CHARACTER SET latin1 DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `collection_id` int(11) DEFAULT NULL,
  `post_date` int(10) NOT NULL,
  `channel_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `title`, `filename`, `text`, `descr`, `user_id`, `collection_id`, `post_date`, `channel_id`) VALUES
(4, 'Github Archive', NULL, 'Archive has been moved and added to [url=https://github.com/EzraZebra/web-archive]Github[/url].\n\n\n[quote=README.md]Schizo\'s Playground abandoned (2011–2012)[/quote]\n', NULL, 69, NULL, 1550361600, NULL),
(1, 'e/&#%\\\\?', NULL, '€\n²\n³\n&\n|\né\n@\n\"\n#\n\'\n(\n§\n^\nè\n!\nç\n{\nà\n}\n)\n°\n-\n_\n^\n¨\n[\n$\n*\n]\nù\n%\n´\nµ\n`\n£\n`\n<\n>\n\\\n,\n?\n;\n.\n:\n-\n=\n+\n~', NULL, 69, NULL, 1350882000, NULL),
(3, 'Archived', NULL, 'Schizo\'s Playground has been archived.', NULL, 69, NULL, 1520488800, NULL),
(2, 'BBCode Test', NULL, '[indent]This text is indented.[/indent]\n[left]This text is aligned to the left.[/left]\n[center]This text is centered.[/center]\n[right]This text is aligned to the right.[/right]\n\n[color=#dcdc00]yellow[/color]\n[color=#ddb04a]orange[/color]\n[color=#d25441]red[/color]\n[color=#5f8bca]blue[/color]\n[color=#9860be]purple[/color]\n[color=#8bb262]green[/color]\n[color=white]white[/color]\n[color=#808080]grey[/color]\n[color=black]black[/color]\nUnordered:\n[list]\n[*] aaa\n[*] aaaaaa\n[*] aaaaaaaaa\n[/list]\nOrdered:\n[list]\n[#] aaa\n[#] aaaaaa\n[#] aaaaaaaaa\n[/list]\n[size=18]Very Big[/size]\n[size=14]Big[/size]\n[size=12]Normal[/size]\n[size=10]Small[/size]\n[size=8]Very Small[/size]\n\n[quote=A. Einstein]Schizo\'s Playground is the best website ever made.[/quote]\n[quote]Everything Einstein has ever said is factual.[/quote]\n\n[url]http://www.schizosplayground.com[/url]\n[url=http://www.schizosplayground.com]best site everrrr[/url]\n\n[b]bold[/b]\n[i]italic[/i]\n[u]underlined[/u]\n\n[img=https://fc07.deviantart.net/fs71/f/2009/347/1/9/Star_Trek_Demo_Picard_by_Trekkie_By_Birth.jpg]Picard - Defying Red Shirt Policy since 1987[/img]\n\ns', NULL, 69, NULL, 1389333600, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `leftcolumn`
--

CREATE TABLE `leftcolumn` (
  `left_id` int(11) NOT NULL,
  `url` varchar(255) CHARACTER SET latin1 NOT NULL,
  `url_mo` varchar(255) CHARACTER SET latin1 NOT NULL,
  `target` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT 999,
  `colour` varchar(10) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `leftcolumn`
--

INSERT INTO `leftcolumn` (`left_id`, `url`, `url_mo`, `target`, `position`, `colour`) VALUES
(23, 'left102.jpg', 'left102_mo.jpg', 'monkey_bars', 999, 'red'),
(22, 'left101.jpg', 'left101_mo.jpg', 'monkey_bars', 999, 'yellow'),
(21, 'left94.jpg', 'left94_mo.jpg', 'slide', 999, 'blue'),
(20, 'left93.jpg', 'left93_mo.jpg', 'slide', 999, 'green'),
(19, 'left92.jpg', 'left92_mo.jpg', 'slide', 999, 'red'),
(18, 'left91.jpg', 'left91_mo.jpg', 'slide', 999, 'yellow'),
(17, 'left84.jpg', 'left84_mo.jpg', 'swing', 999, 'blue'),
(16, 'left83.jpg', 'left83_mo.jpg', 'swing', 999, 'green'),
(15, 'left82.jpg', 'left82_mo.jpg', 'swing', 999, 'red'),
(14, 'left74.jpg', 'left74_mo.jpg', 'merry_go_round', 999, 'blue'),
(13, 'left73.jpg', 'left73_mo.jpg', 'merry_go_round', 999, 'green'),
(12, 'left72.jpg', 'left72_mo.jpg', 'merry_go_round', 999, 'red'),
(11, 'left81.jpg', 'left81_mo.jpg', 'swing', 999, 'yellow'),
(10, 'left71.jpg', 'left71_mo.jpg', 'merry_go_round', 999, 'yellow'),
(9, 'left64.jpg', 'left64_mo.jpg', 'maze', 999, 'yellow'),
(8, 'left63.jpg', 'left63_mo.jpg', 'maze', 999, 'red'),
(7, 'left62.jpg', 'left62_mo.jpg', 'maze', 999, 'green'),
(6, 'left61.jpg', 'left61_mo.jpg', 'maze', 999, 'blue'),
(4, 'blow.jpg', 'blow_mo.jpg', 'new_word_fun', 4, 'blue'),
(3, 'speakers.jpg', 'speakers_mo.jpg', 'new_sound', 3, 'green'),
(2, 'eye.jpg', 'eye_mo.jpg', 'new_image', 2, 'yellow'),
(1, 'skull.jpg', 'skull_mo.jpg', 'new_channels', 1, 'red'),
(24, 'left103.jpg', 'left103_mo.jpg', 'monkey_bars', 999, 'green'),
(25, 'left104.jpg', 'left104_mo.jpg', 'monkey_bars', 999, 'blue'),
(26, 'left111.jpg', 'left111_mo.jpg', 'trapeze', 999, 'yellow'),
(27, 'left112.jpg', 'left112_mo.jpg', 'trapeze', 999, 'red'),
(28, 'left113.jpg', 'left113_mo.jpg', 'trapeze', 999, 'green'),
(29, 'left114.jpg', 'left114_mo.jpg', 'trapeze', 999, 'blue'),
(30, 'left121.jpg', 'left121_mo.jpg', 'playhouse', 999, 'green'),
(31, 'left122.jpg', 'left122_mo.jpg', 'playhouse', 999, 'blue'),
(32, 'left123.jpg', 'left123_mo.jpg', 'playhouse', 999, 'yellow'),
(33, 'left124.jpg', 'left124_mo.jpg', 'playhouse', 999, 'red'),
(34, 'left131.jpg', 'left131_mo.jpg', 'spring_rider', 999, 'green'),
(35, 'left132.jpg', 'left132_mo.jpg', 'spring_rider', 999, 'blue'),
(36, 'left133.jpg', 'left133_mo.jpg', 'spring_rider', 999, 'yellow'),
(37, 'left134.jpg', 'left134_mo.jpg', 'spring_rider', 999, 'red'),
(38, 'left141.jpg', 'left141_mo.jpg', 'sandbox', 999, 'green'),
(39, 'left142.jpg', 'left142_mo.jpg', 'sandbox', 999, 'blue'),
(40, 'left143.jpg', 'left143_mo.jpg', 'sandbox', 999, 'yellow'),
(41, 'left144.jpg', 'left144_mo.jpg', 'sandbox', 999, 'red'),
(42, 'left151.jpg', 'left151_mo.jpg', 'jungle_gym', 999, 'green'),
(43, 'left152.jpg', 'left152_mo.jpg', 'jungle_gym', 999, 'blue'),
(44, 'left153.jpg', 'left153_mo.jpg', 'jungle_gym', 999, 'yellow'),
(45, 'left154.jpg', 'left154_mo.jpg', 'jungle_gym', 999, 'red');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `user_id` int(11) NOT NULL,
  `session_id` varchar(64) CHARACTER SET latin1 NOT NULL,
  `time` int(10) NOT NULL,
  `ip` int(10) UNSIGNED NOT NULL,
  `expires` int(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(25) CHARACTER SET latin1 NOT NULL,
  `password` varchar(74) CHARACTER SET latin1 NOT NULL,
  `clearance` int(11) NOT NULL,
  `join_date` int(10) NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 NOT NULL,
  `activation` varchar(74) CHARACTER SET latin1 DEFAULT NULL,
  `reset` varchar(74) CHARACTER SET latin1 DEFAULT NULL,
  `resetTime` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `password`, `clearance`, `join_date`, `email`, `activation`, `reset`, `resetTime`) VALUES
(48, 'demo1', 'a8dcda1ba344c71c96421e234ed98556de5a8525ff142d76eda2aa29f8904b13bae7905d8c', 6, 1520622451, 'demo@ezrazebra.net', NULL, NULL, NULL),
(69, 'admin', '5bed8937f16cda44c01c74981a5ead05fd0eb7f5bdd11d529ec5462301029d642bfec48d67', 4, 1572792326, 'admin@schizosplayground.com', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_activity`
--

CREATE TABLE `user_activity` (
  `user_id` int(11) NOT NULL,
  `ip` int(10) UNSIGNED NOT NULL,
  `time` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`ad_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`),
  ADD UNIQUE KEY `cat_name` (`cat_name`),
  ADD UNIQUE KEY `cat_url` (`cat_url`);

--
-- Indexes for table `categories_items`
--
ALTER TABLE `categories_items`
  ADD PRIMARY KEY (`cat_id`,`item_id`);

--
-- Indexes for table `clearance`
--
ALTER TABLE `clearance`
  ADD PRIMARY KEY (`clearance_id`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Indexes for table `global_settings`
--
ALTER TABLE `global_settings`
  ADD KEY `newspanel_length` (`newspanel_length`),
  ADD KEY `logins_remember` (`login_remember`),
  ADD KEY `left_length` (`left_length`),
  ADD KEY `login_time` (`login_time`);

--
-- Indexes for table `ipbans`
--
ALTER TABLE `ipbans`
  ADD PRIMARY KEY (`ip`,`ip_range`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `leftcolumn`
--
ALTER TABLE `leftcolumn`
  ADD PRIMARY KEY (`left_id`),
  ADD UNIQUE KEY `url` (`url`,`url_mo`),
  ADD UNIQUE KEY `url_mo` (`url_mo`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`user_id`,`session_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `activation` (`activation`);

--
-- Indexes for table `user_activity`
--
ALTER TABLE `user_activity`
  ADD PRIMARY KEY (`user_id`,`ip`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `ad_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `clearance`
--
ALTER TABLE `clearance`
  MODIFY `clearance_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `leftcolumn`
--
ALTER TABLE `leftcolumn`
  MODIFY `left_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
