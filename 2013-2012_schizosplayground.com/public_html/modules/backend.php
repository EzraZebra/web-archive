<?php
session_start();

include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include_once $incl['func']['misc'];

if(isset($_POST['valid']) && $_POST['valid'] == 1) {
	if(isset($_POST['var'])) $var = secure_get($_POST['var'], true);
	if(isset($_POST['val'])) $val = secure_get($_POST['val'], true);
	
	$_SESSION[$var] = $val;
	session_write_close();
} else header('Location: '.$main_dir['home']);
?>