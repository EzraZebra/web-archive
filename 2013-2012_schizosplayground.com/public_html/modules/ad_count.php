<?php
//////////////////////////////////
///// AUTH ACTIVATION MODULE /////
//////////////////////////////////
//// INCLUDES
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include_once $incl['db_connect'];
include_once $incl['func']['misc'];

if(isset($_GET['id'])) $id = secure_sql($_GET['id'], true);
	else $id = null;
	
$result = mysqli_query($sqldb, "SELECT url, clicks FROM ads WHERE CRC32(ad_id) = '$id'")
				or die (header('Location: '.$main_dir['home']));

$row = mysqli_fetch_assoc($result);
mysqli_free_result($result);
$url = secure_get($row['url'], false);
if(substr($url, 0, 7) != 'http://' && substr($url, 0, 8) != 'https://') $url = 'http://'.$url;
$clicks = $row['clicks']+1;

mysqli_query($sqldb, "UPDATE ads SET clicks = '$clicks' WHERE CRC32(ad_id) = '$id'");

header('Location: '.$url);