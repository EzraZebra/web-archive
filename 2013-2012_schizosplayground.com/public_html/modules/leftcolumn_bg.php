<?php
////////////////////////////////////
///// LEFT COLUMN BG GENERATOR /////
////////////////////////////////////
//// INCLUDES
include $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include $incl['db_connect'];
include $incl['func']['misc'];

//// GET IMG ID's
$validId = false;
if(isset($_GET['idstring'])) {
	$ids = secure_get($_GET["idstring"], true);
	$ids = explode('_', $ids);
	
	if(is_array($ids) && count($ids) == 10 && is_numeric_array($ids)) $validId = true;
}
//// ID's ARE VALID -> CONTINUE	
if($validId) {
	//get img info
		$result = mysqli_query($sqldb, "SELECT left_id, url FROM leftcolumn")
					or die('Database error.');  
		for($i=0; $row = mysqli_fetch_assoc($result); $i++) {
			$left_id[$i] = $row['left_id'];
			$url[$i] = $row['url'];
		}

		mysqli_free_result($result);
				
	//START IMG GENERATION
	$finalbg = null;
	for($i=0; $i<10; $i++) {
		$current_id = array_search($ids[$i], $left_id); //get img index

		//GET RELATIVE PATH TO IMG
			$from = $modules;
			$to = $img['leftdir_php'].$url[$current_id];
			$relative = '';
			
			$from = explode('/', rtrim($from, '/'));
			for($j=0; $j<count($from); $j++)
				$relative .= '../';
			$relative .= $to;
			
		//GENERATE ADDED IMG
			$addbg = imagecreatefromjpeg($relative); //create image to add

		//GET IMG SIZES
			$addsize = imagesy($addbg); //get size of of img
			
			if($finalbg != null) $basesize = imagesy($finalbg); else $basesize = 0; //set base size
			$newsize = $addsize+$basesize; //set new size

		//CREATE TEMPORARY IMAGE
			$tempbg = imagecreatetruecolor(120, $newsize); //create temporary img
			if($finalbg != null) imagecopy($tempbg, $finalbg, 0, 0, 0, 0, 120, $basesize); //copy final img on temp img
			imagecopy($tempbg, $addbg, 0, $basesize, 0, 0, 120, $addsize); //copy added img to temp img
			
		//DESTROY IMAGES
			imagedestroy($addbg); //destroy added img
			if($finalbg != null) imagedestroy($finalbg);  //destroy final image
			
		//SAVE TEMPORARY IMG
			$finalbg = $tempbg;
	}

	//// OUTPUT IMAGE
		header('Content-Type: image/jpeg');
		imagejpeg($finalbg);

	//// DESTROY IMAGES
		imagedestroy($tempbg);
		//imagedestroy($finalbg);
}
//// ID's ARE INVALID
else echo 'Invalid image.';
?>