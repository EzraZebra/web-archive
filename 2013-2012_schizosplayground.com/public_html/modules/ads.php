<?php
//////////////////////
///// ADS MODULE /////
//////////////////////
function outputAds($root, $dir, $page, $widget) {
	global $sqldb;

	if($page != 'adminpanel' && ($widget === 'bottombanner' || $widget === 'rightcolumn')) {
		// GET AD INFO
			$result = mysqli_query($sqldb,
								"	SELECT ad_id, name, url, img, views, priority, googleAd, slot, width, height FROM ads
									WHERE widget='$widget'
									ORDER BY priority ASC")
				or die('Database error.');  

			for($i=0; $row = mysqli_fetch_assoc($result); $i++) {
				$ad_ids[$i] = $row['ad_id'];
				$name[$i] = $row['name'];
				$ad_url[$i] = $row['url'];
				$ad_img[$i] = $row['img'];
				$views[$i] = $row['views'];
				$googleAd[$i] = $row['googleAd'];
				$slot[$i] = $row['slot'];
				$width[$i] = $row['width'];
				$height[$i] = $row['height'];
				$used[$i] = false;
				
				if($widget == 'rightcolumn') {
					if(isset($priority[$row['priority']]) && is_array($priority[$row['priority']])) array_push($priority[$row['priority']], $i);
						else $priority[$row['priority']][0] = $i;
				}
			}

			$nads = mysqli_num_rows($result);
			mysqli_free_result($result);		

			$result = mysqli_query($sqldb, "SELECT googlePub_id, rightcolumn_length FROM ad_settings")
				or die('Database error.');
				
			$row = mysqli_fetch_assoc($result);

			if($widget == 'rightcolumn') {
				$ads_length = $row['rightcolumn_length'];
				$containerClass = null;
			}
			elseif($widget == 'bottombanner') {
				$ads_length = 1;
				$containerClass = 'class="reklamBanner"';
			}
			$googlePub_id = $row['googlePub_id'];
			mysqli_free_result($result);

		// OUTPUT ADS
		if($nads != 0 && $ads_length != 0) {
			echo '			<div id="reklam" '.$containerClass.'><ul>';
			if($widget == 'rightcolumn')
				echo '			<li class="ad_divider"></li>';
			for($i=1; $i<=$ads_length; $i++) {
				if($widget == 'rightcolumn') $ad_id = $priority[$i][mt_rand(0, sizeof($priority[$i])-1)];
				elseif($widget == 'bottombanner') $ad_id = mt_rand(0, sizeof($ad_ids)-1);
				
				if(!$googleAd[$ad_id]) {
					$cur_views = $views[$ad_id]+1;
					$cur_ad_id = $ad_ids[$ad_id];
					mysqli_query($sqldb,
								"	UPDATE ads SET views = '$cur_views'
									WHERE ad_id = '$cur_ad_id'")
							or die('Database error.');
				}

				if($googleAd[$ad_id])
					echo '	<li class="googleAd" style="height: '.$height[$ad_id].'px;">
								<script type="text/javascript"><!--
								google_ad_client = "ca-pub-'.$googlePub_id.'";
								/* '.$name[$ad_id].' */
								google_ad_slot = "'.$slot[$ad_id].'";
								google_ad_width = '.$width[$ad_id].';
								google_ad_height = '.$height[$ad_id].';
								//-->
								</script>
								<script type="text/javascript"
								src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
								</script>
							</li>';
				else echo '	<li><a href="'.$root.'ad/'.enc_item_id($ad_ids[$ad_id]).'/'.$ad_url[$ad_id].'">
								<img src="'.$dir.$ad_img[$ad_id].'?'.time().'" alt="'.$name[$ad_id].'" title="'.$name[$ad_id].'">
							</a></li>';
				if($widget == 'rightcolumn')
					echo '	<li class="ad_divider"></li>';
			}
			echo '			</ul></div>';
		}
	}
}
?>