<?php
/////////////////////////
///// AUTH LOGIN UI /////
/////////////////////////
//// INCLUDES
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include_once $incl['func']['misc'];
include $mod['auth']['common_ui'];

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj
//// OUTPUT UI
	echo '				<div id="loginpopup">';
	// BANNED -> DISPLAY MSG
	if($userValidation->isBanned()) echo $commonUi['bannedMsg'];
	// NOT LOGGED IN -> LOGIN FORM
	elseif(!$userValidation->loggedIn) {
		echo '				<form id="loginform" class="formpopup" accept-charset="utf-8"><fieldset><table>'.
								$commonUi['login_form'];

		if($userValidation->getFailed() >= 3)
			include				$mod['auth']['captcha_ui'];

		echo '					<tr id="form_btnbar">
									<td colspan="2" id="loginbar">
										<input type="checkbox" name="login_remember" id="login_remember" /><label for="login_remember">Remember</label>
										<input name="submit" type="submit" id="login_btn" value="" />
									</td>
								</tr>
							</table></fieldset></form>';
	}
	// LOGGED IN -> SHOW MSG
	else echo 				$commonUi['alreadyLoggedIn'];
	
	echo '				</div>
						<script type="text/javascript" src="'.$jquery['auth']['login_ui'].'"></script>';
?>