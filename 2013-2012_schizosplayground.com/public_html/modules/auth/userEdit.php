<?php
/////////////////////////////////
///// AUTH USER EDIT MODULE /////
/////////////////////////////////
//// INCLUDES
include $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include $incl['func']['misc'];
include $incl['func']['login'];

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj

//// LOGGED IN & ADMIN -> CONTINUE
if($userValidation->isAdmin()) {
	//// EDIT USER -> CONTINUE
	if(isset($_POST['editUser']) && secure_get($_POST['editUser'], true) == 1) {
		//// INFO SET -> CONTINUE
		if(isset($_POST['user_id']) && isset($_POST['user_name']) && isset($_POST['clearance']) && isset($_POST['email'])) {
			//SET VARS
			$user_id = secure_sql(urldecodeslashes($_POST['user_id']), true);	
			$user_name = secure_sql(urldecodeslashes($_POST['user_name']), false);
			$clearance = secure_sql(urldecodeslashes($_POST['clearance']), true);
			$email = secure_sql(urldecodeslashes($_POST['email']), false);
		
			//// VALID USERNAME -> CONTINUE
			if(preg_match($regex['user'], $user_name) !== 0) {
				//// VALID EMAIL -> CONTINUE
				if(preg_match($regex['email'], $email) !== 0) {
					
					//CHECK FOR DUPLICATE
						$duplicate = 'noDuplicate';
						//user
						$result = mysqli_query($sqldb,
											"	SELECT	user_id FROM users
												WHERE	user_name = '$user_name' AND
														user_id != '$user_id'")
							or die('Database error.');
						if(mysqli_num_rows($result) != 0) $duplicate = 'duplicateUser';
						mysqli_free_result($result);
						
						//email
						$result = mysqli_query($sqldb,
											"	SELECT	user_id FROM users
												WHERE	email = '$email' AND
														user_id != '$user_id'")
							or die('Database error.');
						if(mysqli_num_rows($result) != 0) {
							if($duplicate == 'noDuplicate') $duplicate = 'duplicateEmail';
								else $duplicate = 'duplicateError';
						}
						mysqli_free_result($result);
					
					//// NO DUPLICATE -> CONTINUE
					if($duplicate == 'noDuplicate') {
						//SET PWD VARS
							$error = "Invalid password.";
							$queryClause = null;
							
							//no pwd info
							if(!isset($_POST['pwd']) && !isset($_POST['pwdc'])) $error = null;
							//all pwd info
							elseif(isset($_POST['pwd']) && isset($_POST['pwdc'])) {
								$pwd = secure_sql(urldecodeslashes($_POST['pwd']), false);
								$pwdc = secure_sql(urldecodeslashes($_POST['pwdc']), false);

								//valid pwd
								if(preg_match($regex['pwd'], $pwd)) {
									//pwds match
									if($pwd == $pwdc) {
										$pwd = generateHash($pwd);
										$error = null;
										$queryClause = ", password = '$pwd'";
									}
									//pwds do not match
									else $error = 'Passwords do not match.';
								}
							}

						//NO PWD ERROR -> CONTINUE
						if($error == null) {
							//CHECK USERID
							$result = mysqli_query($sqldb,
												"	SELECT user_id FROM users
													WHERE	user_id = '$user_id'") 
								or die('Database error.');

							//// USERID FOUND -> CONTINUE
							if(mysqli_num_rows($result) == 1) {
								//GET USERID FROM DB
								$row = mysqli_fetch_assoc($result);
								$user_id = $row['user_id'];
								
								//// CAN EDIT -> CONTINUE
								if($userValidation->canEdit($user_id)) {		
									//// EDIT USER
									mysqli_query($sqldb,
												"	UPDATE users SET user_name = '$user_name', clearance = '$clearance', email = '$email' $queryClause
													WHERE user_id = '$user_id'")
											or die('Database error.');
									echo 'success'; //success
								} else echo 'You do not have permission to edit this user.'; //can't edit
							} else echo 'That user does not exist.';							
							mysqli_free_result($result);
						} else echo $error;
					} else { //duplicate user and/or email
						if($duplicate == 'duplicateError') $error = 'Username and e-mail address have already been taken.';
							elseif($duplicate == 'duplicateUser') $error = 'Username has already been taken.';
								elseif($duplicate == 'duplicateEmail') $error = 'E-mail address has already been taken.';
						echo $error;
					}
				} else echo 'Invalid e-mail address.'; //invalid email
			} else echo 'Invalid username.'; //invalid user
		} else echo 'There was an error in your submission.'; //form error
	}
	//// DELETE USER -> CONTINUE
	elseif(isset($_POST['deleteUser']) && secure_get($_POST['deleteUser'], true) == 1) {
		//// ID SET -> CONTINUE
		if(isset($_POST['user_id'])) {
			//SET USER ID
			$user_id = secure_sql($_POST['user_id'], true);

			//CHECK USER ID
			$result = mysqli_query($sqldb,
								"	SELECT	user_id FROM users
									WHERE	user_id = '$user_id'") 
				or die('Database error');   

			//// USER FOUND -> CONTINUE
			if(mysqli_num_rows($result) == 1) {
				mysqli_free_result($result);

				//GET ITEMS
				$result = mysqli_query($sqldb,
									"	SELECT	item_id FROM items
										WHERE	user_id = '$user_id'") 
					or die('dbError');   

				//SET ITEMS
				for($i=0; $row = mysqli_fetch_assoc($result); $i++) {
					if($i==0) $item_ids = $row['item_id'];
						else $item_ids .= ",".$row['item_id'];
				}			
				mysqli_free_result($result);					

				//// CAN EDIT -> CONTINUE
				if($userValidation->canEdit($user_id)) {
					//// DELETE LOGIN INFO
					mysqli_query($sqldb,
								"	DELETE FROM login
									WHERE 	user_id='$user_id'")
						or die('Database error.');	

					if(isset($item_ids)) {
						//// DELETE CAT/ITEMS LINKS
						mysqli_query($sqldb,
									"	DELETE FROM categories_items
										WHERE 	item_id IN ($item_ids)")
							or die('Database error.');	
						//// DELETE ITEMS
						mysqli_query($sqldb,
									"	DELETE FROM items
										WHERE 	user_id='$user_id'")
							or die('Database error.');
					}

					//// DELETE USER
					mysqli_query($sqldb,
								"	DELETE FROM users
									WHERE 	user_id='$user_id'")
						or die('Database error.');							
					
					echo 'success'; //success
				} else echo 'You are not allowed to delete this item.'; //can't edit
			} else echo 'The item you are trying to delete does not exist.'; //item not found	
		} else echo 'No item specified.'; //no id
	} else header('Location: '.$main_dir['home']); //invalid form
} else echo 'You do not have permission to perform this action.'; //clearance error
?>