<?php
/////////////////////////////
///// AUTH RESET MODULE /////
/////////////////////////////
//// INCLUDES
include $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include $incl['db_connect'];
include $incl['func']['misc'];
include $incl['func']['login'];
include_once $plugins['php']['securimage'];

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj

if(!$userValidation->loggedIn && !$userValidation->isIPBanned()) {
	//// SEND VERIFICATION EMAIL
	if(isset($_POST['sendVer']) && secure_get($_POST['sendVer'], true) == 1) {
		// GET USER INFO
			if(isset($_POST["reset_user"])) $username = secure_sql($_POST['reset_user'], false);
				else $username = null;

			if(isset($_POST['reset_email'])) $email = secure_sql($_POST['reset_email'], false);
				else $email = null;

			$usermatch = false;
			$emailmatch = false;
			if(preg_match($regex['user'], $username)) $usermatch = true;
			if(preg_match($regex['email'], $email)) $emailmatch = true;
		
		//USER OR EMAIL VALID _> CONTINUE
		if($usermatch || $emailmatch) {
			//CHECK FOR MATCHING ACCOUNT
			if($usermatch && $emailmatch) $queryclause = 'AND';
				else $queryclause = 'OR';
				
			$result = mysqli_query($sqldb,
								"	SELECT	reset, resetTime, email FROM users
									WHERE	user_name = '$username' $queryclause
											email = '$email'")
						or die('dbError');	

			//ACCOUNT FOUND -> CONTINUE
			if(mysqli_num_rows($result) == 1) {
				$row = mysqli_fetch_assoc($result);
				mysqli_free_result($result);

				//NOT YET RESET -> CONTINUE
				if($row['reset'] == null || $row['resetTime'] < time()-60*60*24) {
					$email = $row['email'];
					//set reset code
					$reset = genRandomString(10);
						
					//email message
					$msg = "<html>
							<head>
							  <title>".$title_info['title']." password reset</title>
							</head>
							<body>
								Dear <b>".$username."</b>,<br />
								<br />
								Someone requested a password reset for your account at <a href='http://www.schizosplayground.com'>".$title_info['title']."</a>.<br />
								If this wasn't you, please ignore this e-mail. Otherwise, to reset your password, you can:<br />
								<ul>
									<li>copy this reset code into the form on the website: <b>".$reset."</b></li>
									<li>
										click this link, or copy it into your browser's address bar: 
										<a href='http://www.schizosplayground.com/reset/".$username."/".$reset."/'>
											www.schizosplayground.com/reset/".$username."/".$reset."/
										</a>
									</li>
								</ul>
								
								The reset code is valid for twenty-four hours.<br />
								<br />									
								Your password will be reset and e-mailed to you.<br />
								<br />
								Regards,<br />
								".$title_info['title']."
							</body>
							</html>";
					
					//set headers for html
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
					
					//send mail; success -> continue
					if(@mail($email, $title_info['title']." password reset", $msg, $headers)) {
						//set reset code hash
						$reset = generateHash($reset);
						$time = time();
						
						//create db entry
						mysqli_query($sqldb,
									"	UPDATE users SET reset='$reset', resetTime='$time'
										WHERE user_name = '$username'")
							or die('dbError');
						echo 'success'; //success
					} else echo 'mailFailed'; //failed to send mail
				} else echo 'alreadySent';
			} else echo 'noMatchError'; //no match
		} else echo 'formError'; //form error
	}
	//// RESET PASSWORD
	elseif(isset($_POST['reset']) && secure_get($_POST['reset'], true) == 1) {
		// GET CAPTCHA INFO	
			if(isset($_POST['captcha_code'])) $captcha_code = secure_get($_POST['captcha_code'], true);
				else $captcha_code = null;
		
			$securimage = new Securimage();
		
		// CAPTCHA CORRECT -> CONTINUE
		if($userValidation->getFailed()<3 || $securimage->check($captcha_code) != false) {
			// GET INFO
				if(isset($_POST["reset_user"])) $username = secure_sql($_POST['reset_user'], false);
					else $username = null;

				if(isset($_POST['reset_code'])) $reset = secure_sql($_POST['reset_code'], false);
					else $reset = null;
					
				$error = 'error';
					
			// USER INFO IS VALID -> CONTINUE
			if(preg_match($regex['user'], $username) && preg_match($regex['code'], $reset)) {
				//CHECK FOR MATCHING ACCOUNT
					$result = mysqli_query($sqldb,
										"	SELECT	email, reset, resetTime FROM users
											WHERE	user_name = '$username'")
								or die('dbError');	
						
					if(mysqli_num_rows($result) == 1) { //SUCCESS -> RESET
						$row = mysqli_fetch_assoc($result);

						$email = $row['email'];
						$resetdb = $row['reset'];
						$resetTime = $row['resetTime'];
						$reset = generateHash(strtoupper($reset), $resetdb);
						
						if($reset == $resetdb && $resetTime > time()-24*60*60) {
							//set pwd
							$pwd = genRandomString(10);
								
							//email message
							$msg = "<html>
									<head>
									  <title>".$title_info['title']." password reset</title>
									</head>
									<body>
										Dear <b>".$username."</b>,<br />
										<br />
										Your password at <a href='http://www.schizosplayground.com'>".$title_info['title']."</a> has been reset.<br />
										You can now <a href='http://www.schizosplayground.com/login/'>log in</a> with this password:<br />
										<ul><li><b>".$pwd."</b></li></ul>

										We recommend that you change your password to something of your own choosing.<br />
										<br />
										Regards,<br />
										".$title_info['title']."
									</body>
									</html>";
							
							//set headers for html
							$headers  = 'MIME-Version: 1.0' . "\r\n";
							$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
							
							//send mail; success -> continue
							if(@mail($email, $title_info['title']." password reset", $msg, $headers)) {
								//set reset code hash
								$pwd = generateHash($pwd);
								
								//create db entry
								mysqli_query($sqldb,
											"	UPDATE users SET password='$pwd', reset=NULL, resetTime=NULL
												WHERE user_name = '$username'")
									or die('dbError');
								
								$userValidation->resetFailed();
								
								$error = 'success'; //success
							} else $error = 'mailFailed'; //failed to send mail
						}
					}
					mysqli_free_result($result);
			}
			
			if($error == 'error' && $userValidation->loginFailed() == 3) $error = 'captchaMissing'; //captcha missing

			echo $error; //output error
		} else echo 'captchaError'; //captcha error
	}
	//// INVALID FORM -> REDIRECT
	else header('Location: '.$main_dir['home']); //invalid form
} else echo 'clearanceError';  //clearance error
?>