<?php
/////////////////////////
///// AUTH LOGIN UI /////
/////////////////////////
//// INCLUDES
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include_once $incl['func']['misc'];
include $mod['auth']['common_ui'];

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj

//// OUTPUT ACTIVATION UI
	echo '				<div id="activationpopup">';
	
	//BANNED -> DISPLAY MSG
	if($userValidation->isBanned()) echo $commonUi['bannedMsg'];
	//NOT ACTIVE -> OUTPUT ACTIVATION FORM
	elseif(!$userValidation->isActive()) {
		// GET ACTIVATION CODE
			if(isset($_GET['id'])) $activation = secure_get(trim($_GET["id"], "/"), true);
				else $activation = null;
			if($activation == '_noid') $activation = null;
		
		// ACTIVATION FORM
		echo '				<div id="registermsg">
								<div id="contenttxt">';
		if($userValidation->loggedIn) echo '		Your account is currently inactive.<br />'; //logged in -> display msg
		echo '						An activation code was sent to your e-mail address.<br />';
		if($userValidation->loggedIn) echo '		If you did not receive it, click <a id="resendActivation">here</a> to send it again.<br />'; //logged in -> display email msg
		echo '						You can copy the activation code into the form below, or follow the instructions in the e-mail.
								</div>
								<div id="errorpopup" class="msgpopup"></div>
								<div id="successpopup" class="msgpopup"></div>
							</div>
							<form id="activateform" class="formpopup" accept-charset="utf-8"><fieldset><table>';
		if(!$userValidation->loggedIn) echo	$commonUi['login_form']; //not logged in -> display login form
		echo '					<tr>
									<td><label for="activation_code">Activation code: </label></td>
									<td><input type="text" id="activation_code" name="activation_code" maxlength="10" autocomplete="off" value="'.$activation.'" /></td>
								</tr>
								<tr class="forminfo">
									<td></td>
									<td><span class="activationerror">Code is incorrect.</span></td>
								</tr>';

		//include captcha if necessary
		if($userValidation->getFailed() >= 3)
			include				$mod['auth']['captcha_ui'];

		echo '					<tr id="form_btnbar"><td colspan="2"><div>
									<input type="submit" name="submit" id="form_btn" value="Activate" />
									<input type="hidden" name="activation_form" id="activation_form" value="1" />
								</div></td></tr>
							</table></fieldset></form>';
	}
	//ACTIVE -> DISPLAY MSG
	else echo '				Your account has been activated!
							<div id="noform_btn" title="Close activation window"><span>Close</span></div>';

	echo '				</div>
						<script type="text/javascript" src="'.$jquery['auth']['activation_ui'].'"></script>';
?>