<?php
/////////////////////////////
///// AUTH LOGIN MODULE /////
/////////////////////////////
//// INCLUDES
include $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include $incl['db_connect'];
include $incl['func']['misc'];
include $incl['func']['login'];
include_once $plugins['php']['securimage'];

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj

//// LOG OUT
if(isset($_POST['logout']) && secure_get($_POST['logout'], true) == 1) {
	if($userValidation->loggedIn) echo $userValidation->logout();
		else echo 'notLoggedIn';
}
//// LOG IN
elseif(isset($_POST['login']) && secure_get($_POST['login'], true) == 1) {
	if(!$userValidation->loggedIn && !$userValidation->isIPBanned()) {
		// GET CAPTCHA INFO	
			if(isset($_POST['captcha_code'])) $captcha_code = secure_get($_POST['captcha_code'], true);
				else $captcha_code = null;
		
			$securimage = new Securimage();
		
		// CAPTCHA CORRECT -> CONTINUE
		if($userValidation->getFailed()<3 || $securimage->check($captcha_code) != false) {
			// GET LOGIN INFO
				if(isset($_POST["login_user"])) $username = secure_sql($_POST['login_user'], false);
					else $username = null;

				if(isset($_POST['login_pwd'])) $pwd = secure_sql($_POST['login_pwd'], false);
					else $pwd = null;
					
				if(isset($_POST['login_remember']) && $_POST['login_remember'] == 'true') $remember = true;
					else $remember = false;
					
			// LOGIN IS VALID -> CONTINUE
			if(preg_match($regex['user'], $username) && preg_match($regex['pwd'], $pwd)) {
				//GET PWD HASH
					$result = mysqli_query($sqldb,
										"	SELECT	password FROM users
											WHERE	user_name = '$username'")
								or die('dbError');
											
					$row = mysqli_fetch_assoc($result);
					mysqli_free_result($result);
					
					$pwd = generateHash($pwd, $row['password']);

				//CHECK LOGIN
					$result = mysqli_query($sqldb,
										"	SELECT	user_id FROM users
											WHERE	user_name = '$username' AND
													password = '$pwd'")
								or die('dbError');
					
					//USER FOUND -> CONTINUE
					if(mysqli_num_rows($result) == 1) {
						$row = mysqli_fetch_assoc($result);
						mysqli_free_result($result);
						$user_id = $row['user_id'];

						//USER NOT BANNED -> CONTINUE
						if(!$userValidation->isBanned($user_id)) {
							$userValidation->validateUser($user_id, $remember);
							echo 'success'; //success
						} else echo 'bannedError';
					} else { //USER NOT FOUND -> ERROR
						$newFailed = $userValidation->loginFailed();
						if($newFailed == 3) echo 'captchaMissing'; //captcha missing
							else echo $newFailed; //failed 1x or 2x
					}
			} else echo 'formError'; //form error
		} else echo 'captchaError'; //captcha error
	} else echo 'clearanceError';  //clearance error
} else header('Location: '.$main_dir['home']); //invalid form
?>