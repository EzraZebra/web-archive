<?php
//////////////////////////////////
///// AUTH ACTIVATION MODULE /////
//////////////////////////////////
//// INCLUDES
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include_once $incl['func']['misc'];
include $incl['func']['login'];

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj

// NOT RESTRICTED -> CONTINUE
if(!$userValidation->isBanned()) {
	// USER NOT ACTIVE -> CONTINUE
	if(!$userValidation->isActive()) {
		//// ACTIVATE
		if(isset($_POST["activate"]) && secure_get($_POST["activate"], true) == 1) {
			//get activation code
			if(isset($_POST['activation'])) $activation = secure_get($_POST['activation'], true);
				else $activation = null;
			
			// VALID ACTIVATION CODE -> CONTINUE
			if(preg_match($regex['code'], $activation)) {
				//get user clearance and activation code
					$result = mysqli_query($sqldb,
										"	SELECT	activation FROM users
											WHERE	user_id = '$userValidation->userid'")
						or die('dbError');										
					$row = mysqli_fetch_assoc($result);
					mysqli_free_result($result);

					$activationdb = $row['activation'];
					$activation = generateHash(strtoupper($activation), $activationdb);

				// ACTIVATION CODE IS CORRECT -> CONTINUE
				if($activation == $activationdb) {
					$clearance = $userValidation->getRootClearance();
					// ACTIVATE
					mysqli_query($sqldb,
										"	UPDATE users SET clearance='$clearance', activation=NULL
											WHERE user_id = '$userValidation->userid'")
						or die('dbError');
					echo 'success'; //success
				} else echo 'activationError'; //incorrect activation code
			} else echo 'formError'; //invalid activation code
		}
		//// SEND ACTIVATION MAIL
		elseif(isset($_POST["sendAct"]) && secure_get($_POST["sendAct"], true) == 1) {
				$result = mysqli_query($sqldb,
									"	SELECT email FROM users
										WHERE user_id = '$userValidation->userid'")
							or die('dbError');
							
				// USER FOUND -> CONTINUE
				if(mysqli_num_rows($result) == 1) {
					$row = mysqli_fetch_assoc($result);
					
					$activation = genRandomString(10);
					
					//email message
					$msg = "<html>
							<head>
							  <title>".$title_info['title']." account activation</title>
							</head>
							<body>
								Dear <b>".$userValidation->username."</b>,<br />
								<br />
								Thank you for registering at <a href='http://www.schizosplayground.com'>".$title_info['title']."</a>!<br />
								Before you can start using your account, you'll have to activate it. You can:<br />
								<ul>
									<li>copy this activation code into the form on the website: <b>".$activation."</b></li>
									<li>
										click this link, or copy it into your browser's address bar: 
										<a href='http://www.schizosplayground.com/activation/".$activation."/'>www.schizosplayground.com/activation/".$activation."/</a>
									</li>
								</ul>
								If you didn't create an account, feel free to ignore this e-mail.<br /><br />
								<i>Note: any previously sent activation code is no longer valid.</i><br />
								<br />
								Regards,<br />
								".$title_info['title']."
							</body>
							</html>
							";
					
					//set headers for html
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
					
					//send mail; success -> continue
					if(@mail($row['email'], $title_info['title']." account activation", $msg, $headers)) {
						//set activation code and pwd hash
						$activation = generateHash($activation);
						
						//update db
						mysqli_query($sqldb,
									"	UPDATE users SET activation='$activation'
										WHERE user_id='$userValidation->userid'")
							or die('dbError');
						
						echo 'success'; //success
					} else echo 'mailFailed'; //failed to send mail
				} else echo 'userError'; //user not found
		} else header('Location: '.$main_dir['home']); //invalid form
	} else echo 'alreadyActive'; //already active
} else echo 'clearanceError'; //restricted
?>