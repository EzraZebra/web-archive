<?php
/////////////////////////////////
///// AUTH ITEM EDIT MODULE /////
/////////////////////////////////
//// INCLUDES
include $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include $incl['func']['misc'];
include $incl['func']['login'];

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj

////IS LOGGED IN -> CONTINUE
if(!$userValidation->isRestricted()) {
	if(		(isset($_POST['saveEdit']) || isset($_POST['saveNew']))
		&&	isset($_POST['day']) && isset($_POST['month']) && isset($_POST['year'])
		&&	isset($_POST['title']) && isset($_POST['content'])) {
			
		//SET VARS
			$date = mktime(0, 0, 0, intval(secure_sql($_POST['month'], true)),
									intval(secure_sql($_POST['day'], true)),
									intval(secure_sql($_POST['year'], true)));
			$title = secure_sql(urldecodeslashes($_POST['title']), false);
			$content = secure_sql(urldecodeslashes($_POST['content']), false);

		////EDIT ITEM
		if(isset($_POST['saveEdit']) && secure_get($_POST['saveEdit'], true) == 1 && isset($_POST['id'])) {
			$id = secure_sql($_POST['id'], true); //get item id

			//get item user id
			$result = mysqli_query($sqldb,
								"	SELECT	user_id FROM items
									WHERE	CRC32(item_id) = '$id'") 
				or die('Database error.');   

			//ITEM FOUND -> CONTINUE
			if(mysqli_num_rows($result) == 1) {
				$row = mysqli_fetch_assoc($result);
				
				//USER CAN EDIT -> CONTINUE
				if($userValidation->canEdit($row['user_id'])) {
					//EDIT ITEM
					mysqli_query($sqldb,
								"	UPDATE items SET post_date='$date', title='$title', text='$content'
									WHERE CRC32(item_id) = '$id'")
							or die('Database error.');
					echo 'success'; //success
				} else echo 'clearanceError'; //user can't edit
			} else echo 'The item you are trying to edit does not exist.'; //item doesn't exit
					
			mysqli_free_result($result);	
		}
		////NEW ITEM
		elseif(isset($_POST['saveNew']) && secure_get($_POST['saveNew'], true) == 1 && isset($_POST['module'])) {
			$module = secure_sql($_POST['module'], false); //get module
			
			//USER CAN POST -> CONTINUE
			if($module != 'home' || $userValidation->canPostNews()) {
				//get cat id
				$result = mysqli_query($sqldb,
									"	SELECT cat_id FROM categories
										WHERE cat_url = '$module'")
					or die('Database error.');

				$row = mysqli_fetch_assoc($result);
				mysqli_free_result($result);

				//ADD ITEM
				mysqli_query($sqldb,
							"	INSERT INTO items (post_date, title, text, user_id)
								VALUES ('$date', '$title', '$content', '$userValidation->userid')")
					or die('Database error.');

				//get new item id
				$id = enc_item_id(mysqli_insert_id($sqldb));
					
				//ADD CAT LINK
				mysqli_query($sqldb,
							"	INSERT INTO categories_items (cat_id, item_id)
								VALUES ('".$row['cat_id']."', LAST_INSERT_ID())")
					or die('Database error.');

				echo 'success'.$id; //success
			} else echo 'clearanceError.'; //can't post
		} else header('Location: '.$main_dir['home']); //invalid form
	}
	//DELETE ITEM
	elseif(isset($_POST['delete']) && secure_get($_POST['delete'], true) == 1) {
		if(isset($_POST['id'])) {
			$id = secure_sql($_POST['id'], true); //get id

			//get item id, user id
			$result = mysqli_query($sqldb,
								"	SELECT item_id, user_id FROM items
									WHERE	CRC32(item_id) = '$id'") 
				or die('dbError');   

			//ITEM FOUND -> CONTINUE
			if(mysqli_num_rows($result) == 1) {
				$row = mysqli_fetch_assoc($result);

				//USER CAN EDIT -> CONTINUE
				if($userValidation->canEdit($row['user_id'])) {
					//delete item
					mysqli_query($sqldb,
								"	DELETE FROM items
									WHERE 	item_id='".$row['item_id']."'")
						or die('Database error.');
					//delete cat link
					mysqli_query($sqldb,
								"	DELETE FROM categories_items
									WHERE 	item_id='".$row['item_id']."'")
						or die('Database error.');			
					
					echo 'success'; //success
				} else echo 'You are not allowed to delete this item.'; //can't edit
			} else echo 'The item you are trying to delete does not exist.'; //item does not exit
					
			mysqli_free_result($result);		
		} else echo 'No item specified.'; //item id missing
	} else header('Location: '.$main_dir['home']); //invalid form
} else echo 'clearanceError'; //clearance error
?>