<?php
/////////////////////////////
///// AUTH REGISTER UI //////
/////////////////////////////
//// INCLUDES
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include $mod['auth']['common_ui'];

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj

//// OUTPUT UI
	echo '	<div id="registerpopup">';
	// BANNED -> DISPLAY MSG
	if($userValidation->isBanned()) echo $commonUi['bannedMsg'];
	// NOT LOGGED IN -> REGISTER FORM
	elseif(!$userValidation->loggedIn) {
		echo '	<div id="registermsg">
					<div id="contenttxt">
						Welcome to <b>Schizo\'s Playground</b>!<br />
						You can register by filling out the form below.
					</div>
					<div id="errorpopup"></div>
				</div>
				<form id="register" class="formpopup" accept-charset="utf-8"><fieldset><table>
					<tr>
						<td><label for="register_user">Username: </label></td>
						<td><input type="text" id="register_user" name="register_user" maxlength="25" class="registerinput" /></td>
					</tr>
					<tr class="forminfo">
						<td><span>Five or more characters<br />Allowed: _, a-Z,  0-9</span></td>
						<td><span class="usertaken">Username already in use.</span></td>
					</tr>
					<tr>
						<td><label for="register_email">E-mail address: </label></td>
						<td><input type="text" id="register_email" name="register_email" maxlength="255" class="registerinput" /></td>
					</tr>
					<tr class="forminfo">
						<td></td>
						<td><span class="emailtaken">E-mail already in use.</span></td>
					</tr>
					<tr>
						<td><label for="register_emailc">Confirm e-mail address: </label></td>
						<td><input type="text" id="register_emailc" name="register_emailc" maxlength="255" autocomplete="off" class="registerinput" /></td>
					</tr>
					<tr>
						<td><label for="register_pwd">Password: </label></td>
						<td><input type="password" id="register_pwd" name="register_pwd" maxlength="74" class="registerinput" /></td>
					</tr>
					<tr class="forminfo">
						<td><span>Five or more characters<br />Allowed: !._-, a-Z,  0-9</span></td>
						<td></td>
					</tr>
					<tr>
						<td><label for="register_pwdc">Confirm password: </label></td>
						<td><input type="password" id="register_pwdc" name="register_pwdc" maxlength="74" class="registerinput" /></td>
					</tr>';
		include		$mod['auth']['captcha_ui']; //include captcha
		echo '		<tr id="form_btnbar">
						<td colspan="2"><div>
							<input type="submit" name="submit" id="form_btn" value="Register" />
							<input type="hidden" name="register_form" id="register_form" value="1" />
						</div></td>
					</tr>
				</table></fieldset></form>';
	}
	// LOGGED IN -> DISPLAY MSG
	else echo	$commonUi['alreadyLoggedIn'];
	
	echo '	</div>
			<script type="text/javascript" src="'.$jquery['auth']['register_ui'].'"></script>';
?>