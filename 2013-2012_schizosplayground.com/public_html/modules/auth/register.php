<?php
////////////////////////////////
///// AUTH REGISTER MODULE /////
////////////////////////////////
//// INCLUDES
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include_once $incl['func']['misc'];
include $incl['func']['login'];

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj

// NOT LOGGED IN OR ADMIN -> CONTINUE
if((!$userValidation->loggedIn && !$userValidation->isIPBanned()) || $userValidation->isAdmin()) {
	//// REGISTER
	if(isset($_POST["register"]) && secure_get($_POST["register"], true) == 1) {
		// GET VARS
			include_once $plugins['php']['securimage'];
			$securimage = new Securimage();
			
			if(isset($_POST["register_user"])) $username = secure_sql($_POST['register_user'], false);
				else $username = null;

			if(isset($_POST['register_email'])) $email = secure_sql($_POST['register_email'], false);
				else $email = null;

			if(isset($_POST['register_emailc'])) $emailc = secure_get($_POST['register_emailc'], true);
				else $emailc = null;

			if(isset($_POST['register_pwd'])) $pwd = secure_sql($_POST['register_pwd'], false);
				else $pwd = null;

			if(isset($_POST['register_pwdc'])) $pwdc = secure_get($_POST['register_pwdc'], true);
				else $pwdc = null;

			//ADMIN PANEL
			if($userValidation->isAdmin() && isset($_POST['register_clearance'])) {
				$clearance = secure_sql($_POST['register_clearance'], true);
				
				$result = mysqli_query($sqldb,
									"	SELECT	special_perm FROM clearance
										WHERE	clearance_id = '$clearance'")
					or die(mysqli_error($sqldb));
										
				$row = mysqli_fetch_assoc($result);
				mysqli_free_result($result);
				
				if($userValidation->getSpecialPermName($row['special_perm']) == 'inactive') $sendActivation = true;
					else $sendActivation = false;
			}
			//SET TO ROOT_INACTIVE CLEARANCE
			else {
					$clearance = $userValidation->getRootClearance('inactive');
					$sendActivation = true;
			}
				
			if(isset($_POST['captcha_code'])) $captcha_code = secure_get($_POST['captcha_code'], true);
				else $captcha_code = null;
				
			$join_date = time();
			
		//CAPTCHA IS CORRECT -> CONTINUE
		if($securimage->check($captcha_code) != false || $userValidation->isAdmin()) {
			//INFO IS VALID -> CONTINUE
			if(		preg_match($regex['user'], $username) !== 0
				&&	preg_match($regex['email'], $email) !== 0
				&&	$email == $emailc
				&&	preg_match($regex['pwd'], $pwd) !== 0
				&&	$pwd == $pwdc) {

				//CHECK FOR DUPLICATE
					$duplicate = 'noDuplicate';
					$result = mysqli_query($sqldb,
										"	SELECT	user_id FROM users
											WHERE	user_name = '$username'")
						or die(mysqli_error($sqldb));
					if(mysqli_num_rows($result) != 0) $duplicate = 'duplicateUser';
					mysqli_free_result($result);
					
					$result = mysqli_query($sqldb,
										"	SELECT	user_id FROM users
											WHERE	email = '$email'")
						or die(mysqli_error($sqldb));
					if(mysqli_num_rows($result) != 0) {
						if($duplicate == 'noDuplicate') $duplicate = 'duplicateEmail';
							else $duplicate = 'duplicateError';
					}
					mysqli_free_result($result);
					
				//NO DUPLICATE -> CONTINUE
				if($duplicate == 'noDuplicate') {
					//preset title
					$title = $title_info['title'];
					
					//set activation code/title
					if($sendActivation) {
						$activation = genRandomString(10);
						$title .= " account activation";
					} else $title .= " account registration";
		
					//email message
					$msg = "	<html>
									<head><title>
										".$title_info['title']." account ";
					if($sendActivation) $msg .= "activation";
						else $msg .= "registration";
					$msg .= "		</title></head>
									<body>
										Dear <b>".$username."</b>,<br />
										<br />";
					if($sendActivation)
						$msg .= "		Thank you for registering at <a href='http://www.schizosplayground.com'>".$title_info['title']."</a>!<br />
										<br />
										Before you can start using your account, you'll have to activate it. You can:<br />
										<ul>
											<li>copy this activation code into the form on the website: <b>".$activation."</b></li>
											<li>
												click this link, or copy it into your browser's address bar: 
												<a href='http://www.schizosplayground.com/activation/".$activation."/'>www.schizosplayground.com/activation/".$activation."/</a>
											</li>
										</ul>
										
										If you didn't create an account, feel free to ignore this e-mail.<br />";
						else $msg .= "	An account has been registered with this e-mail address at <a href='http://www.schizosplayground.com'>".$title_info['title']."</a>.<br />
										<br />
										You can start using your account immediately! All you have to do is log in:
										<ul>
											<li><b>Username:</b> ".$username."</li>
											<li><b>Password:</b> ".$pwd."</li>
											<li>Log in at <a href='http://www.schizosplayground.com/login/'>http://www.schizosplayground.com/login/</a></li>
										</ul>
										
										If you did not create an account and don't want one, you can remove it through the user settings.<br />";
					$msg .= "			<br />
										Regards,<br />
										".$title_info['title']."
									</body>
								</html>";
					
					//set headers for html
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
					
					//send mail; success -> continue
					if(@mail($email, $title, $msg, $headers)) {
						//set pwd hash
						$pwd = generateHash($pwd);
						
						//set activation code
						if($sendActivation) $activation = "'".generateHash($activation)."'";
							else $activation = "NULL";
							
							
						//create db enttry
						mysqli_query($sqldb,
									"	INSERT INTO users (user_name, password, email, clearance, join_date, activation)
										VALUES ('$username', '$pwd', '$email', '$clearance', '$join_date', $activation)")
							or die(mysqli_error($sqldb));

						$userValidation->resetFailed();
						
						echo 'success'; //success 
					} else echo 'mailFailed'; //failed to send mail
				} else echo $duplicate; //duplicate username or email
			} else echo 'formError'; //form error
		} else echo 'captchaError'; //captcha error
	}
	//// CHECK FOR DUPLICATES
	elseif(isset($_POST['register_check']) &&  secure_get($_POST['register_check'], true) == 1) {
		// CHECK USER
		if(isset($_POST["register_user"])) {
			$username = secure_sql($_POST['register_user'], false);
			$result = mysqli_query($sqldb,
								"	SELECT	user_name FROM users
									WHERE	user_name = '$username'")
				or die(mysqli_error($sqldb));
			
			if(mysqli_num_rows($result) == 1) echo 'alreadyTaken';
				else echo 'notTaken';
			mysqli_free_result($result);
		//CHECK EMAIL
		} elseif(isset($_POST["register_email"])) {
			$email = secure_sql($_POST['register_email'], false);
			$result = mysqli_query($sqldb,
								"	SELECT	email FROM users
									WHERE	email = '$email'")
			or die(mysqli_error($sqldb));
			
			if(mysqli_num_rows($result) == 1) echo 'alreadyTaken';
				else echo 'notTaken';
			mysqli_free_result($result);
		}
	}
	//// INVALID FORM
	else header('Location: '.$main_dir['home']);
} else echo 'clearanceError'; //clearance error
?>