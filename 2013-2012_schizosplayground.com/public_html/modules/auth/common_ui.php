<?php
include_once $incl['func']['misc'];
include_once $incl['func']['login'];

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj
//// BANNED MSG
if($userValidation->isBanned()) {
	//SET ALREADY LOGGED IN MSG
	$commonUi['bannedMsg'] = '			Your access to this page has been restricted.';

	$commonUi['alreadyLoggedIn'] = $commonUi['bannedMsg'];
	$commonUi['login_form'] = $commonUi['bannedMsg'];
}
//// ALREADY LOGGED IN MSG
elseif($userValidation->loggedIn) {
	//SET ALREADY LOGGED IN MSG
	$commonUi['alreadyLoggedIn'] = '	You are already logged in as '.$userValidation->username.'.
										<div id="noform_btn" title="Log out"><span>Log out</span></div>';
	
	$commonUi['login_form'] = $commonUi['alreadyLoggedIn'];
}
//// LOGIN FORM
else {
	//GET FAILED STATE
		if($userValidation->getFailed() == 0) $hideLoginFailedMsg = 'class="hideLoginFailedMsg"'; //hide login failed msg if failed == 0
			else $hideLoginFailedMsg = null;

	//SET LOGIN FORM
	$commonUi['login_form'] = '	<tr class="logininput">
									<td><label for="login_user">Username: </label></td>
									<td><input type="text" id="login_user" name="login_user" maxlength="25" /></td>
								</tr>
								<tr class="logininput">
									<td><label for="login_pwd">Password: </label></td>
									<td>
										<input type="password" id="login_pwd" name="login_pwd" maxlength="74" />
										<input type="hidden" name="login_form" id="login_form" value="1" />
									</td>
								</tr>
								<tr class="logininput"><td colspan="2" class="resetPwdMsg">
									<span '.$hideLoginFailedMsg.'>Login failed. </span>
									<a href="'.$rootdoc.'reset/" class="DiagLnk reset">Forgot</a> password?
								</td></tr>';
	
	$commonUi['alreadyLoggedIn'] = $commonUi['login_form'];
}
?>