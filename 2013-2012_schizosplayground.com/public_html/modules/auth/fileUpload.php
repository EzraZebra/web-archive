<?php
header('Content-Type: text/html; charset=utf-8');

include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include_once $incl['func']['login'];
include_once $incl['func']['fileUpload'];
include_once $incl['func']['misc'];

if(!isset($userValidation)) $userValidation = new UserValidation();

if(!$userValidation->isRestricted() && $userValidation->isAdmin() && isset($_GET['m']) && isset($_GET['w'])) {
	$module = secure_get($_GET['m'], false);
	$widget = secure_get($_GET['w'], false);
	$id = null;
	$action = $mod['auth']['fileUpload'].'/upload/'.$module.'/'.$widget.'/';
	$error = null;
	$is_deleted = false;
	
	if($module == 'admin_ads') {
		if(isset($_GET['new']) && secure_sql($_GET['new'], true) == 1) $is_new = true;
			else $is_new = false;

		if(isset($_GET['id'])) {
			$id = secure_sql($_GET['id'], true);
			if(!$is_new) $action .= $id.'/';
		}
		
		$result = mysqli_query($sqldb,
							"	SELECT img FROM ads
								WHERE ad_id = '$id'");
		
		$row = mysqli_fetch_assoc($result);
		mysqli_free_result($result);
		$cur_img = $row['img'];

		$uploaddir = '../../images/reklam/';
		if($widget == 'rightcolumn') $dimensions = array( 'width' => 180, 'height' => 150);
		elseif($widget == 'bottombanner') $dimensions = array( 'width' => 728, 'height' => 90);
	}
	
	if(isset($_GET['upload']) && secure_sql($_GET['upload'], true) == 1) {
		if(!isset($fileUpload)) $fileUpload = new FileUpload();
			
		$uploadResult = $fileUpload->imageUpload($_FILES['fileUpload_file'], $uploaddir, $dimensions);
		if(isset($uploadResult[0]) && $uploadResult[0] == 'success' && isset($uploadResult[1])) {		
			if($cur_img != null && $cur_img != $uploadResult[1] && file_exists($uploaddir.$cur_img)) unlink($uploaddir.$cur_img);
			
			$cur_img = $uploadResult[1];
			
			mysqli_query($sqldb,
						"	UPDATE ads SET img='$cur_img'
							WHERE ad_id = '$id'")
					or die('Database error.');
		} else $error = '<span id="error">'.$uploadResult.'</span>';
	}
	elseif(isset($_GET['delete']) && secure_sql($_GET['delete'], true) == 1) {	
		if($cur_img != null) {
			if(file_exists($uploaddir.$cur_img)) unlink($uploaddir.$cur_img);
			
			$cur_img = null;
			
			mysqli_query($sqldb,
						"	UPDATE ads SET img=NULL
							WHERE ad_id = '$id'")
					or die('Database error.');
		} else $error = '<span id="error">That ad has no image.</span>';
		
		$is_deleted = true;
	}

	if($cur_img == null) $cur_img = "<div id='imagecontainer'>none</div>";
		else $cur_img = "<div id='imagecontainer'>
							<form id='img_Delete_form' accept-charset='utf-8' action='".$mod['auth']['fileUpload']."delete/".$module."/".$id."/'></form>
							<div id='img_Delete_container'><span id='img_Delete'><span></span>Delete image</span></div>
							<img src='".$img['main']['adfolder'].$cur_img."?".time()."' alt='missing image'>
						</div>";
		
	if($id == null || $is_new) {
		$formClass = 'moveForm';
		$cur_img = null;
	} elseif($is_deleted) $formClass = null;
		else $formClass = 'hideForm';
	?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>File Upload</title>
		<link href="<?php echo $css['fileUpload']; ?>" rel="stylesheet" type="text/css" />
	</head>
	<body><div id="fileUpload_wrapper">
		<div class="loading"><div>Loading. . .</div></div>
		<?php echo $cur_img.$error; ?>
		<form id="fileUploadForm" class="<?php echo $formClass; ?>" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
			<span id="img_Choose"><span></span>Choose image</span><br />
			<span id="fileUpload_pathContainer">
				<span id="fileUpload_pathClicker"></span>
				<input type="text" id="fileUpload_path" name="fileUpload_path" disabled="disabled" />
			</span>
			<input type="file" id="fileUpload_file" name="fileUpload_file" />
		</form>
	</div></body>
	</html>
	<?php
}
?>