<?php
/////////////////////////
///// AUTH LOGIN UI /////
/////////////////////////
//// INCLUDES
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include $mod['auth']['common_ui'];

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj

//// OUTPUT UI
	echo '		<div id="resetpopup">';
	
	// BANNED -> DISPLAY MSG
	if($userValidation->isBanned()) echo $commonUi['bannedMsg'];
	// NOT LOGGED IN -> CONTINUE
	elseif(!$userValidation->loggedIn) {
		// SUCCESS -> OUTPUT MSG
		if(isset($_GET['status']) && $_GET['status'] == 'success')
			echo '	Your password has been reset.
					<div id="noform_btn" title="Continue to log-in"><span>Log in</span></div>';
		// ELSE CONTINUE
		else {
			//set user
			if(isset($_GET['user'])) $user = secure_get($_GET['user'], true);
				else $user = '_nouser';
				
			// MESSAGE
				echo '	<div id="registermsg">
							<div id="contenttxt">';
					//VERIFICATION MSG
					if($user == '_nouser')
						echo '	If you have forgotten your password, please fill in your username or e-mail address below.<br />
								An e-mail will be sent to confirm that you want to reset your password.';
					//RESET MSG
					else
						echo '	An e-mail was sent with a reset code for your password.<br />
								Please fill it in below, or follow the instructions in the e-mail.';

				echo '		</div>
							<div id="errorpopup"></div>
						</div>';
			
			//// OUTPUT VERIFICATION FORM
			if($user == '_nouser')
				echo '		<div id="resetCont"><a>Continue</a> to password reset.</div>
							<form id="verifyreset" class="formpopup" accept-charset="utf-8"><fieldset><table>
							<tr>
								<td><label for="reset_user">Username: </label></td>
								<td><input type="text" id="reset_user" name="reset_user" maxlength="25" class="resetinput" /></td>
							</tr>
							<tr>
								<td><label for="reset_email">E-mail address: </label></td>
								<td><input type="text" id="reset_email" name="reset_email" maxlength="255" class="resetinput" /></td>
							</tr>
							<tr id="form_btnbar"><td colspan="2"><div>
								<input type="submit" name="submit" id="form_btn" value="Submit" />
								<input type="hidden" name="reset_form" id="reset_form" value="1" />
							</div></td></tr>
						</table></fieldset></form>';
			//// OUTPUT RESET FORM
			else {
				//set var
					if(isset($_GET['id']) && $_GET['id'] != '_noid') $reset = secure_get($_GET['id'], true);
						else $reset = null;
						
					if($user == '_dummy') $user = null;
				echo '	<div id="resetBack"><a>Back</a> to e-mail confirmation.</div>
							<form id="reset" class="formpopup" accept-charset="utf-8"><fieldset><table>
							<tr>
								<td><label for="reset_user">Username: </label></td>
								<td><input type="text" id="reset_user" name="reset_user" maxlength="25" value="'.$user.'" class="resetinput" /></td>
							</tr>
							<tr>
								<td><label for="reset_code">Reset code: </label></td>
								<td><input type="text" id="reset_code" name="reset_code" maxlength="10" value="'.$reset.'" class="resetinput" /></td>
							</tr>';
				// OUTPUT CAPTCHA IF NECESSARY
				if($userValidation->getFailed() >= 3)
					include	$mod['auth']['captcha_ui'];
					
				echo '		<tr id="form_btnbar"><td colspan="2"><div>
								<input type="submit" name="submit" id="form_btn" value="Reset" />
								<input type="hidden" name="reset_form" id="reset_form" value="1" />
							</div></td></tr>
						</table></fieldset></form>';
			}
		}
	}
	// LOGGED IN -> SHOW MSG
	else echo 		$commonUi['alreadyLoggedIn'];
	
	echo '		</div>
				<script type="text/javascript" src="'.$jquery['auth']['reset_ui'].'"></script>';