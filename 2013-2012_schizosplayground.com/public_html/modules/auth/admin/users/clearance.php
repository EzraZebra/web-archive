<?php
//////////////////////////////////
///// AUTH ADMIN USERS MODULE ////
//////////////////////////////////
//// INCLUDES
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include_once $incl['db_connect'];
include_once $incl['func']['misc'];
include_once $incl['func']['login'];

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj

//// LOGGED IN & ADMIN -> CONTINUE
if($userValidation->isAdmin()) {
	//// OUTPUT UI
	echo "	<div class='adminSetContent'>
				<div class='admintools'>
					<span class='uibtnAdd' id='adminClearanceAdd'><span></span>New</span>
					<span class='uibtnSave'><span></span>Save</span> / 
					<span class='uibtnCancel'><span></span>Cancel</span> / 
					<span class='uibtnEdit'><span></span>Edit</span> / 
					<span class='uibtnDelete'><span></span>Delete</span> selected			
				</div>
				<form id='adminEdit' accept-charset='utf-8'><fieldset><table id='clearancelist' class='adminlist'>
					<thead><tr>
						<th>Title</th>
						<th>Special permissions</th>
						<th>Post news</th>
						<th id='toolscol'><label for='selectallClearance'>Select all </label><input type='checkbox' name='selectallClearance' id='selectallClearance' class='selectall' /></th>
					</tr></thead>
					<tbody>
						<tr id='newItem'>
							<td>
								<input type='text' id='admin_addClearance' name='admin_addClearance' maxlength='25' />
							</td>
							<td>
								<select class ='admin_addPermissions'>
									<option></option>
									<option class='inactiveTitle'>inactive</option>
									<option class='bannedTitle'>banned</option>
									<option class='adminTitle'>admin</option>
								</select>
							</td>
							<td class='imgcell post_news'>
								<span class='uibtnCancel checklist'><span></span></span>
								<input type='checkbox' name='admin_addPostNews' class='admin_addPostNews registerinput' />
							</td>
							<td>
								<span class='uibtnSave'><span></span>Save</span> 
								<span class='uibtnCancel'><span></span>Cancel</span>
							</td>
						</tr>
						<tr id='itemPrototype'>
							<td>
								<span class='admin_listInfo admin_listInfoTitle'></span>
								<input type='text' class='admin_editClearance admin_editInput' name='admin_editClearance' maxlength='25' />
								<input type='hidden' class='admin_editClearanceId' name='admin_editClearanceId' />
							</td>
							<td>
								<span class='admin_listInfo admin_listInfoPermissions'></span>
								<select class='admin_editPermissions registerinput'>
									<option> </option>
									<option class='inactiveTitle'>inactive</option>
									<option class='bannedTitle'>banned</option>
									<option class='adminTitle'>admin</option>
								</select>
							</td>
							<td class='imgcell post_news'><span class='uibtnCancel checklist'><span></span></span></td>
							<td>
								<span class='uibtnEdit admin_inlineEdit'><span></span>Edit</span> 
								<span class='uibtnDelete'><span></span>Delete</span>
								<span class='uibtnSave'><span></span>Save</span> 
								<span class='uibtnCancel'><span></span>Cancel</span>
								<input type='checkbox' name='selectItem' class='selectItem' />
								<input type='submit' name='submit' class='adminEditSubmit' />
							</td>
						</tr>";

	//GET CLEARANCE INFO
	$result = mysqli_query($sqldb, "SELECT clearance_id, title, special_perm, post_news FROM clearance")
					or die('Databse error');

	$num_rows = mysqli_num_rows($result);

	for($i=0; $row = mysqli_fetch_assoc($result); $i++) {
		//VARS
			//ui btns
			if($row['post_news']) $post_news = "<span class='uibtnSave checklist' title='Click to change post news permission'><span></span></span>";
				else $post_news = $post_news = "<span class='uibtnCancel checklist' title='Click to change post news permission'><span></span></span>";

			$special_perm = $row['special_perm'];
			//special_perm selected index
			$spPerm = array( 'none' => null, 'inactive' => null, 'banned' => null, 'admin' => null );
		
			//set special_perm selected index
			if($special_perm != null) {
				$titleClass =  $special_perm."Title ";
				if($special_perm == 'inactive') $spPerm['inactive'] = "selected='selected'";
					elseif($special_perm == 'banned') $spPerm['banned'] = "selected='selected'";
					elseif($special_perm == 'admin') $spPerm['admin'] = "selected='selected'";
			} else {
				$special_perm = null;
				$titleClass = null;
				$spPerm['none'] = "selected='selected'";
			}
			
			$title = secure_get($row['title'], false);
			$clearance_id = $row['clearance_id'];

		//// OUTPUT UI
		echo "			<tr>
							<td>
								<span class='admin_listInfo admin_listInfoTitle'>".$title."</span>
								<input type='text' class='admin_editClearance admin_editInput' autocomplete='off' name='admin_editClearance' maxlength='25' value='".$title."' />
								<input type='hidden' class='admin_editClearanceId' name='admin_editClearanceId' value='".$clearance_id."' />
							</td>
							<td>";
		if(!$userValidation->hasRootSpecPerm($clearance_id))
			echo "				<span class='".$titleClass."admin_listInfo admin_listInfoPermissions'>".$special_perm."</span>
								<select class='admin_editPermissions registerinput'>
									<option ".$spPerm['none']."></option>
									<option ".$spPerm['inactive']." class='inactiveTitle'>inactive</option>
									<option ".$spPerm['banned']." class='bannedTitle'>banned</option>
									<option ".$spPerm['admin']." class='adminTitle'>admin</option>
								</select>";
		else echo "				<span class='".$titleClass." admin_listInfoPermissions'>".$special_perm."</span>";
		echo "				</td>
							<td class='imgcell post_news'>".$post_news."</td>
							<td>
								<span class='uibtnEdit admin_inlineEdit'><span></span>Edit</span> 
								<span class='uibtnDelete'><span></span>Delete</span>
								<span class='uibtnSave'><span></span>Save</span> 
								<span class='uibtnCancel'><span></span>Cancel</span>
								<input type='checkbox' name='selectItem' class='selectItem' />
							</td>
						</tr>";
	}
		
	//NO RESULTS -> ERROR
	if($num_rows == 0)
		echo "			<tr>";
	else echo "			<tr class='noError'>";
	echo "					<td colspan='4' class='error'>";
	echo 						"No clearance found.
							</td>
						</tr>";

	echo "			</tbody>
				</table></fieldset></form>
			</div>
			<script type='text/javascript' src='".$jquery['auth']['admin']['clearance_ui']."'></script>";
}
//// NOT LOGGED IN -> ERROR
else echo getErrorMsg('pagenotfound', 'adminpanel');
?>