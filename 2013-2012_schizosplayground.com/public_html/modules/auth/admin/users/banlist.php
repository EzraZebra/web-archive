<?php
//////////////////////////////////
///// AUTH ADMIN BANLIST MODULE ////
//////////////////////////////////
//// INCLUDES
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include_once $incl['db_connect'];
include_once $incl['func']['misc'];
include_once $incl['func']['login'];

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj

//// ADMIN -> CONTINUE
if($userValidation->isAdmin()) {
	if(!isset($_GET["update"]) || secure_get($_GET['update'],true) != 1) {
		//OUTPUT UI
		echo "	<div class='adminSetContent'>
					<div class='admintools'>
						<span id='ipFilterTools'>
							<label for='admin_ipFilterInput'>IP: </label>
							<input type='text' id='admin_ipFilterInput' class='inputSmall' size='15' name='admin_ipFilterInput' title='Use * to specify a range' />
							<span id='admin_banIpFilter' class='uibtnBan' title='Ban IP address or range'><span></span></span>
							<span id='admin_unbanIpFilter' class='uibtnUnban uibtnShown' title='Unban IP address or range'><span></span></span>
						</span>
						<span class='admin_unbanIpSelected uibtnUnban'><span></span>Unban</span> selected IPs
					</div>					
					<table id='banlist' class='adminlist'>
						<thead><tr>
							<th>IP address</th>
							<th># Log-ins</th>
							<th># Users / User</th>
							<th id='toolscol'><label for='selectallBans'>Select all </label><input type='checkbox' name='selectallBans' id='selectallBans' class='selectall' /></th>
						</tr></thead>
						<tbody>";
	}

	//UPDATING && IPs SET -> SET IP QUERYCLAUSE
	$queryClause = null;
	if(isset($_GET["update"]) && secure_get($_GET['update'],true) == 1 && isset($_GET["ip"]) && isset($_GET["ip_range"])) {
		$ipFilter = secure_sql($_GET["ip"], true);
		$ip_rangeFilter = secure_sql($_GET["ip_range"], true);
		$queryClause = "WHERE ip >= '$ipFilter' AND ip_range <= '$ip_rangeFilter'";
	}
						
	//GET BANLIST
	$result = mysqli_query($sqldb, "SELECT	ip, ip_range FROM ipbans $queryClause")
					or die(mysqli_error($sqldb));
	$num_rows = mysqli_num_rows($result);

	while($row = mysqli_fetch_assoc($result)) {
		//SET IP DATA
		$ip = long2ip($row['ip']);
		$ip_range = long2ip($row['ip_range']);
		$ip_query = secure_sql($row['ip'], true);
		$ip_range_query = secure_sql($row['ip_range'], true);
		if($ip != $ip_range) $ip = $ip." - ".$ip_range;
		
		//GET LOGIN INFO
			$loginResult = mysqli_query($sqldb,
									"	SELECT COUNT(l.ip) noLogins, u.user_name FROM login l, users u
										WHERE	l.ip >= '$ip_query' AND
												l.ip <= '$ip_range_query' AND
												u.user_id = l.user_id
										GROUP BY l.user_id")
									or die('Database error.');
									
			$noLogins = 0;
			$noUsers = mysqli_num_rows($loginResult);
			while($loginRow = mysqli_fetch_assoc($loginResult)) {
				$noLogins += $loginRow['noLogins'];
				if($noUsers == 1) $noUsers = $loginRow['user_name'];
			}

		//OUTPUT UI
		echo "				<tr class='listitem'>
								<td class='intcell'>".
									$ip."
									<input type='hidden' class='admin_loginsIP' name='admin_loginsIP' value='".$ip_query."' />
									<input type='hidden' class='admin_loginsIPRange' name='admin_loginsIPRange' value='".$ip_range_query."' />
								</td>
								<td class='intcell'>".$noLogins."</td>
								<td class='intcell'>".$noUsers."</td>
								<td>
									<span class='admin_unbanIp uibtnUnban'><span></span>Unban IP</span>
									<input type='checkbox' name='selectItem' class='selectItem' />
									<input type='hidden' name='admin_banlistGroup' class='admin_banlistGroup' value='".$ip_query."_".$ip_range_query."' />
								</td>
							</tr>";
		mysqli_free_result($loginResult);
							
		//IP RANGE -> OUTPUT AFFECTED IPs
		if($ip != $ip_range) {
			//GET AFFECTED IPs
			$loginResult = mysqli_query($sqldb,
									"	SELECT ip, COUNT(ip) noLogins FROM login l
										WHERE	ip >= '$ip_query' AND
												ip <= '$ip_range_query'
										GROUP BY ip")
									or die('Database error.');

			//OUTPUT AFFECTED IPs
			while($row = mysqli_fetch_assoc($loginResult)) {
				$ip = $row['ip'];
				$noLogins = $row['noLogins'];

				//GET USERS
					$userResult = mysqli_query($sqldb,
											"	SELECT u.user_name FROM login l, users u
												WHERE	l.ip = '$ip' AND
														u.user_id = l.user_id
												GROUP BY l.user_id")
											or die('Database error.');
											
					
					$noUsers = mysqli_num_rows($userResult);
					if($noUsers == 1) {
						$row = mysqli_fetch_assoc($userResult);
						$noUsers = $row['user_name'];
					}
					mysqli_free_result($userResult);

				//OUTPUT UI
				echo "		<tr class='listitem'>
								<td class='intcell'><span class='admin_banlistGroupItemIP'>".
									long2ip($ip)."
									<input type='hidden' class='admin_loginsIP' name='admin_loginsIP' value='".$ip."' />
									<input type='hidden' class='admin_loginsIPRange' name='admin_loginsIPRange' value='".$ip."' />
								</span></td>
								<td class='intcell'>".$noLogins."</td>
								<td class='intcell'>".$noUsers."</td>
								<td>
									<span class='admin_unbanIp uibtnUnban'><span></span>Unban IP</span>
									<input type='checkbox' name='selectItem' class='selectItem' />
									<input type='hidden' name='admin_banlistGroup' class='admin_banlistGroup admin_banlistGroupItem' value='".$ip_query."_".$ip_range_query."' />
								</td>
							</tr>";
			}
			mysqli_free_result($loginResult);
		}
	}
	mysqli_free_result($result);

	//NOT UPDATING -> OUTPUT UI
	if(!isset($_GET["update"]) || secure_get($_GET['update'],true) != 1) {
		//NO RESULTS -> ERROR
		if($num_rows == 0)
			echo "				<tr>";
		else echo "				<tr class='noError'>";
		echo "						<td colspan='4' class='error'>
										No banned IP addresses found.
									</td>
								</tr>";
		echo "				</tbody>
						</table>";

		//USERLIST
			$on_banlistPage = true;
			$result = mysqli_query($sqldb,
								"	SELECT	user_id FROM users, clearance
									WHERE	clearance = clearance_id AND
											(special_perm = 'banned' OR special_perm = 'root_banned')")
						or die('Database error.');
						
			if(mysqli_num_rows($result) > 0)
						include $mod['adminpanelcsets']['users']['main'];

		echo "		</div>";
	}
	echo "			<script type='text/javascript' src='".$jquery['auth']['admin']['banlist_ui']."'></script>";
}
//// NOT LOGGED IN -> ERROR
else echo getErrorMsg('pagenotfound', 'adminpanel');
?>