<?php
//////////////////////////////////
///// AUTH ADMIN LOGINS MODULE ////
//////////////////////////////////
//// INCLUDES
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include_once $incl['db_connect'];
include_once $incl['func']['misc'];
include_once $incl['func']['login'];

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj

//// LOGGED IN & ADMIN -> CONTINUE
if($userValidation->isAdmin()) {
	if(!isset($on_userPage)) $on_userPage = false;

	//NOT UPDATING -> OUTPUT UI
	if(!isset($_GET["update"]) || secure_get($_GET['update'],true) != 1) {
		//OUTPUT TITLE
		echo "	<div class='adminSetContent'>";
		
		//NOT ON USER PAGE -> OUTPUT SETTINGS
		if(!$on_userPage) {
			//GET LOGIN SETTINGS
			$result = mysqli_query($sqldb, "SELECT login_remember, login_time FROM global_settings")
							or die('Database error.');
							
			$row = mysqli_fetch_assoc($result);
			mysqli_free_result($result);
			//OUTPUT SETTINGS
			echo "	<div id='adminsettings'>
						<form id='admin_loginSettings' class='admin_editSettings' accept-charset='utf-8'><fieldset><table>
							<tr>
								<td><label for='admin_editLoginRemember'>Remember: </label></td>
								<td>
									<input type='text' class='admin_editLength' id='admin_editLoginRemember' name='admin_editLoginRemember' maxlength='2' value='".$row['login_remember']."' /> logins/user<br />
								</td>
							</tr>
							<tr>
								<td><label for='admin_editLoginTime'>Remember logins for: </label></td>
								<td>
									<input type='text' class='admin_editLength' id='admin_editLoginTime' name='admin_editLoginTime' maxlength='2' value='".$row['login_time']."' /> days<br />
								</td>
							</tr>
							<tr><td colspan='2'>
								<input type='submit' name='submit' id='form_btn' class='form_btn_small' value='Save' />
								<input type='hidden' name='admin_loginSettingsForm' id='admin_loginSettingsForm' value='1' />
								<span id='admin_editSettingsSuccess' class='uibtnSave'><span></span></span>
							</td></tr>
						</table></fieldset></form>
					</div>";
		}
		//ON USER PAGE -> OUTPUT TITLE
		else
			echo "	<div class='settitle'>Log-ins</div>";

		//OUTPUT ADMIN TOOLS & ADMIN LIST
		echo "		<div class='admintools'>
						<span class='admin_banIpSelected uibtnBan'><span></span>Ban</span> / 
						<span class='admin_unbanIpSelected uibtnUnban'><span></span>Unban</span>
						 selected IPs
					</div>
					<table id='loginlist' class='adminlist'>
						<thead><tr>
							<th>User</th>
							<th>Time</th>
							<th>Expires</th>
							<th id='ipCol'>
								<label for='admin_ipFilterInput'>IP: </label>
								<input type='text' id='admin_ipFilterInput' class='inputSmall' size='15' name='admin_ipFilterInput' title='Use * to specify a range' />";
		if(!$on_userPage)
			echo "				<span class='uibtnSave uibtnShown' id='admin_applyIPFilter' title='Apply IP filter'><span></span></span>
								<span class='uibtnDelete' id='admin_removeIPFilter' title='Remove IP filter'><span></span></span>";
		echo "					<span id='admin_banIpFilter' class='uibtnBan' title='Ban IP address or range'><span></span></span>
								<span id='admin_unbanIpFilter' class='uibtnUnban uibtnShown' title='Unban IP address or range'><span></span></span>
							</th>
							<th id='toolscol'><label for='selectallLogins'>Select all </label><input type='checkbox' name='selectallLogins' id='selectallLogins' class='selectall' /></th>
						</tr></thead>
						<tbody>";
	}

	//UPDATING && IPs SET -> SET IP QUERYCLAUSE
	$queryClause = null;
	if(isset($_GET["update"]) && secure_get($_GET['update'],true) == 1 && isset($_GET["ip"]) && $_GET["ip"] != "_noip" && isset($_GET["ip_range"])) {
		$ipFilter = secure_sql($_GET["ip"], true);
		$ip_rangeFilter = secure_sql($_GET["ip_range"], true);
		$queryClause = "l.ip >= '$ipFilter' AND l.ip <= '$ip_rangeFilter' AND";
	}
	//ON USERPAGE -> SET USERNAME QUERYCLAUSE
	elseif($on_userPage) $queryClause = "u.user_name = '$user_name' AND";

	//GET LOGIN INFO
	$result = mysqli_query($sqldb,
							"	SELECT user_name, time, expires, ip, title, special_perm FROM login l, users u, clearance
								WHERE	$queryClause
										l.user_id = u.user_id AND
										clearance_id = u.clearance
								ORDER BY time DESC")
					or die('Database error.');

	$num_rows = mysqli_num_rows($result);

	//LOGINS FOUND -> CONTINUE
	for($i=0; $row = mysqli_fetch_assoc($result); $i++) {
		//SET EXPIRY
		if($row['expires'] < time()) $expires = "expired";
			else {
				$expires = $row['expires']-time();
				$days = floor($expires/(24*60*60));
				$hours = floor(($expires-$days*24*60*60)/(60*60));
				$minutes = round(($expires-$days*24*60*60-$hours*60*60)/60);
				
				$expires = $days." days, ".$hours." hours, ".$minutes." minutes";
			}

		//SET TITLE CLASS
		if($row['special_perm'] != null) $titleClass =  "class='".($row['special_perm'])."Title'";
			else $titleClass = null;
			
		//SET IP, CHECK FOR BAN
		$ip = $row['ip'];
		$ipRow = long2ip($row['ip']);
		if(!$on_userPage) $ipRow = "<a class='ipFilter'>".$ipRow."</a>";
		
		$banClass = null; $unbanClass = null;
		if($userValidation->isIPBanned($ip)) {
			$ipRow = "<span class='bannedTitle'>".$ipRow."</span>";
			$banClass = ' uibtnHidden';
		} else {
			$ipRow = "<span>".$ipRow."</span>";
			$unbanClass = ' uibtnHidden';
		}

		echo "			<tr class='listitem'>
							<td><a ".$titleClass." href='".$main_dir['adminpanel']."users/".$row['user_name']."'>".$row['user_name']."</a></td>
							<td class='intcell'>".date('d/m/Y', $row['time'])."</td>
							<td class='intcell admin_loginExpires'>".$expires."</td>
							<td class='intcell'>
								".$ipRow."
								<input type='hidden' class='admin_loginsIP' name='admin_loginsIP' value='".$ip."' /></td>
							<td>
								<span class='admin_banIp uibtnBan".$banClass."'><span></span>Ban IP</span>
								<span class='admin_unbanIp uibtnUnban".$unbanClass."'><span></span>Unban IP</span>
								<input type='checkbox' name='selectItem' class='selectItem' />
							</td>
						</tr>";
	}

	if(!isset($_GET["update"]) || secure_get($_GET['update'],true) != 1) {
		//NO RESULTS -> ERROR
		if($num_rows == 0)
			echo "		<tr>";
		else echo "		<tr class='noError'>";
		echo "				<td colspan='5' class='error'>
							No log-ins found.
						</td>
					</tr>
				</tbody>
				</table>
			</div>
			<script type='text/javascript' src='".$jquery['auth']['admin']['logins_ui']."'></script>";
	}
}
//// NOT LOGGED IN -> ERROR
else echo getErrorMsg('pagenotfound', 'adminpanel');
?>