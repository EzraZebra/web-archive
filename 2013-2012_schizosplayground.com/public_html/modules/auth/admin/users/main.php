<?php
//////////////////////////////////
///// AUTH ADMIN USERS MODULE ////
//////////////////////////////////
//// INCLUDES
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include_once $incl['db_connect'];
include_once $incl['func']['misc'];
include_once $incl['func']['login'];

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj

//// ADMIN -> CONTINUE
if($userValidation->isAdmin()) {
	//GET CLEARANCE INFO
		$result = mysqli_query($sqldb, "SELECT clearance_id, title, special_perm FROM clearance
								ORDER BY clearance_id")
					or die('Database error.');

		for($i=0; $row = mysqli_fetch_assoc($result); $i++) {
			$clearance_id[$i] = $row['clearance_id'];
			$clearance_title[$i] = $row['title'];
			$special_perm[$i] = $row['special_perm'];
		}
			
		mysqli_free_result($result);

	//// USERPAGE
	if(isset($_GET["userpage"]) && secure_get($_GET['userpage'],true) == 1) {
		if(isset($_GET["user"])) $user_name = secure_sql($_GET["user"], false);
			else $user_name = null;				

		//GET USER INFO
		$result = mysqli_query($sqldb,
							"	SELECT user_id, clearance, email FROM users
								WHERE user_name = '$user_name'")
					or die('Database error.');

		//USER FOUND -> CONTINUE
		if(mysqli_num_rows($result) != 0) {
			$row = mysqli_fetch_assoc($result);

			//OUTPUT USERPAGE
			echo '		<form id="adminUserEdit" class="adminUserForm" accept-charset="utf-8"><fieldset><div><table>
							<thead><tr><td colspan="2">
								<div id="errorpopup" class="msgpopup"></div>
								<div id="successpopup" class="msgpopup"></div>
							</td></tr></thead>
							<tbody>
								<tr>
									<td><label for="admin_userEditUser">Username: </label></td>
									<td>
										<input type="text" id="admin_userEditUser" name="admin_userEditUser" maxlength="25" class="registerinput" value="'.$user_name.'" />
										<input type="hidden" id="admin_userEditId" name="admin_userEditId" value="'.$row['user_id'].'" />
										<input type="hidden" id="admin_userEditCurUser" name="admin_userEditCurUser" value="'.$user_name.'" />
									</td>
								</tr>
								<tr class"forminfo">
									<td><span>Five or more characters<br />Allowed: _, a-Z,  0-9</span></td>
									<td><span class="usertaken">Username already in use.</span></td>
								</tr>
								<tr>
									<td><label for="admin_userEditEmail">E-mail address: </label></td>
									<td>
										<input type="text" id="admin_userEditEmail" name="admin_userEditEmail" maxlength="255" class="registerinput" value="'.$row['email'].'" />
										<input type="hidden" id="admin_userEditCurEmail" name="admin_userEditCurEmail" value="'.$row['email'].'" />
									</td>
								</tr>
								<tr class="forminfo">
									<td></td>
									<td><span class="emailtaken">E-mail already in use.</span></td>
								</tr>
								<tr>
									<td><label for="admin_userEditPwd">Password: </label></td>
									<td><input type="password" id="admin_userEditPwd" name="admin_userEditPwd" maxlength="74" class="registerinput" /></td>
								</tr>
								<tr class="forminfo">
									<td><span>Five or more characters<br />Allowed: !._-, a-Z,  0-9</span></td>
									<td></td>
								</tr>
								<tr>
									<td><label for="admin_userEditPwdc">Confirm password: </label></td>
									<td><input type="password" id="admin_userEditPwdc" name="admin_userEditPwdc" maxlength="74" class="registerinput" /></td>
								</tr>
								<tr class="forminfo">
									<td><span>Confirm new password to change</span></td>
									<td></td>
								</tr>
								<tr>
									<td><label for="admin_userEditClearance">Clearance: </label></td>
									<td><select id="admin_userEditClearance">';

			//OUTPUT CLEARANCE INFO LIST
			for($i=0; $i<count($clearance_id); $i++) {
				if($clearance_id[$i] == $row['clearance']) $selected = 'selected="selected"';
					else $selected = null;
				if($special_perm[$i] != null) $titleClass =  'class="'.$special_perm[$i].'Title"';
					else $titleClass = null;
				echo '					<option value="'.$clearance_id[$i].'" '.$titleClass.' '.$selected.'>'.$clearance_title[$i].'</option>';
			}

			echo '					</select></td>
								</tr>
								<tr><td colspan="2"><div><span id="bla">
									<input type="submit" name="submit" id="form_btn" class="form_btn_small" value="Save" />
									<div id="noform_btn" class="form_btn_small"><span id="admin_userEditDelete">Delete</span></div>
									<input type="hidden" name="admin_userEditForm" id="admin_userEditForm" value="1" />
								</span></div></td></tr>
							</tbody>
						</table></div></fieldset></form>';
			
			//INCLUDES
				//SET VARS
				$on_userPage = true;
				$user_id = $row['user_id'];

				//logins
				include $mod['adminpanelcsets']['users']['logins'];
				//news
				$result = mysqli_query($sqldb,
									"	SELECT item_id FROM items
										WHERE user_id = '$user_id'")
							or die('Database error.');						
				if(mysqli_num_rows($result) > 0) include $mod['adminpanelcsets']['news']['main'];
				
				echo '	<script type="text/javascript" src="'.$jquery['auth']['admin']['users_edit_ui'].'"></script>';
		}
		//USER NOT FOUND
		else echo '		<div class="error">User not found.</div>';
	}
	//// USER LIST
	else {
		if(!isset($on_banlistPage)) $on_banlistPage = false;
		//// NOT UPDATING -> OUTPUT UI
		if(!isset($_GET['update']) || secure_get($_GET['update'], true) != 1) {
			echo '	<div class="adminSetContent">';

			if($on_banlistPage)
				echo '	<div class="settitle">Users</div>';
			else {
				//GET ACTIVITY THRESHOLD
				$result = mysqli_query($sqldb, "SELECT activity_threshold FROM global_settings")
								or die('Database error.');
								
				$row = mysqli_fetch_assoc($result);
				mysqli_free_result($result);

				//OUTPUT SETTINGS
				echo '	<div id="adminsettings">
							<form id="admin_userSettings" class="admin_editSettings" accept-charset="utf-8"><fieldset>
								<label for="admin_editActivityThreshold">Activity threshold: </label>
								<input type="text" class="admin_editLength" id="admin_editActivityThreshold" name="admin_editActivityThreshold" maxlength="2" value="'.$row['activity_threshold'].'" /> minutes
								<br />
								<input type="submit" name="submit" id="form_btn" class="form_btn_small" value="Save" />
								<input type="hidden" name="admin_userSettingsForm" id="admin_userSettingsForm" value="1" />
								<span id="admin_editSettingsSuccess" class="uibtnSave"><span></span></span>
							</fieldset></form>
						</div>';
			}
			echo '		<div class="admintools">';
			if(!$on_banlistPage) {
				echo '		<form id="adminNew" class="adminUserForm" accept-charset="utf-8"><fieldset><div><table>
								<thead><tr><td colspan="2"><div id="errorpopup"></div></td></tr></thead>
								<tbody>
									<tr>
										<td><label for="admin_newUser">Username: </label></td>
										<td><input type="text" id="admin_newUser" name="admin_newUser" maxlength="25" class="registerinput" /></td>
									</tr>
									<tr class="forminfo">
										<td><span>Five or more characters<br />Allowed: _, a-Z,  0-9</span></td>
										<td><span class="usertaken">Username already in use.</span></td>
									</tr>
									<tr>
										<td><label for="admin_newEmail">E-mail address: </label></td>
										<td><input type="text" id="admin_newEmail" name="admin_newEmail" maxlength="255" class="registerinput" /></td>
									</tr>
									<tr class="forminfo">
										<td></td>
										<td><span class="emailtaken">E-mail already in use.</span></td>
									</tr>
									<tr>
										<td><label for="admin_newEmailc">Confirm e-mail address: </label></td>
										<td><input type="text" id="admin_newEmailc" name="admin_newEmailc" maxlength="255" autocomplete="off" class="registerinput" /></td>
									</tr>
									<tr>
										<td><label for="admin_newPwd">Password: </label></td>
										<td><input type="password" id="admin_newPwd" name="admin_newPwd" maxlength="74" class="registerinput" /></td>
									</tr>
									<tr class="forminfo">
										<td><span>Five or more characters<br />Allowed: !._-, a-Z,  0-9</span></td>
										<td></td>
									</tr>
									<tr>
										<td><label for="admin_newPwdc">Confirm password: </label></td>
										<td><input type="password" id="admin_newPwdc" name="admin_newPwdc" maxlength="74" class="registerinput" /></td>
									</tr>
									<tr>
										<td><label for="admin_newClearance">Clearance: </label></td>
										<td><select id="admin_newClearance">';

				//OUTPUT CLEARANCE INFO LIST
				for($i=0; $i<count($clearance_id); $i++) {
					if($special_perm[$i] != null) $titleClass =  "class='".$special_perm[$i]."Title'";
						else $titleClass = null;
					echo '					<option value="'.$clearance_id[$i].'" '.$titleClass.'>'.$clearance_title[$i].'</option>';
				}

				echo '					</select></td>
									</tr>
									<tr id="form_btnbar"><td colspan="2"><div>
										<input type="submit" name="submit" id="form_btn" value="Add user" /><br />
										<input type="hidden" name="admin_newUserForm" id="admin_newUserForm" value="1" />
									</div></td></tr>
									<tr><td colspan="2">
										<span class="uibtnCancel"><span></span>Cancel</span>
									</td></tr>
								</tbody>
							</table></div></fieldset></form>
							<span class="uibtnAdd" id="adminUsersAdd"><span></span>New</span>';
			}
			echo '			<span class="uibtnSave"><span></span>Save</span> / 
							<span class="uibtnCancel"><span></span>Cancel</span> / 
							<span class="uibtnEdit"><span></span>Edit</span> / 
							<span class="uibtnDelete"><span></span>Delete</span> selected			
						</div>
						<form id="adminEdit" accept-charset="utf-8"><fieldset><table id="userlist" class="adminlist">
							<thead><tr>
								<th>Username</th>
								<th>Clearance</th>
								<th>Joined</th>
								<th>E-mail</th>
								<th id="toolscol"><label for="selectallUsers">Select all </label><input type="checkbox" name="selectallUsers" id="selectallUsers" class="selectall" /></th>
							</tr></thead>
							<tbody>';
		}

		//PRESET VARS
			$queryClause = null;

			if($on_banlistPage) $queryClause = "AND (special_perm = 'banned' OR special_perm = 'root_banned')";
			//// UPDATING -> SET QUERY CLAUSE
			elseif(isset($_GET["update"]) && secure_get($_GET["update"], true) == 1 && isset($_GET["user"])) {
				$user_name = null;
				$user_name = secure_sql($_GET["user"], false);	
				$queryClause = "AND user_name = '$user_name'";
			}

		//// GET USER INFO
		$result = mysqli_query($sqldb,
								"	SELECT user_id, user_name, join_date, email, clearance, title, special_perm FROM users, clearance
									WHERE clearance_id = clearance $queryClause
									ORDER BY user_name ASC")
						or die('Database error.');
		
		$num_rows = mysqli_num_rows($result);
					
		//// OUTPUT USER LIST
		for($i=0; $row = mysqli_fetch_assoc($result); $i++) {
			$titleClass = 'admin_listInfo admin_listInfoTitle';
			if($row['special_perm'] != null) $titleClass =  $row['special_perm'].'Title '.$titleClass;
			$titleClass = 'class="'.$titleClass.'"';

			echo '				<tr>
									<td>
										<span class="admin_listInfo admin_listInfoUser"><a href="'.$main_dir['adminpanel'].'users/'.$row['user_name'].'/">'.$row['user_name'].'</a></span>
										<input type="text" class="admin_editUser  admin_editInput registerinput" autocomplete="off" name="admin_editUser" maxlength="25" value="'.$row['user_name'].'" />
										<input type="hidden" class="admin_editUserId" name="admin_editUserId" value="'.$row['user_id'].'" />
										<div class="forminfo"><div><span class="usertaken">Username already in use.</span></div></div>
									</td>
									<td>
										<span '.$titleClass.'>'.$row['title'].'</span>
										<select class="admin_editClearance admin_editInput">';

			//OUTPUT CLEARANCE INFO
			for($i=0; $i<count($clearance_id); $i++) {
				if($clearance_id[$i] == $row['clearance']) $selected = 'selected="selected"';
					else $selected = null;
				if($special_perm[$i] != null) $titleClass =  'class="'.$special_perm[$i].'Title"';
					else $titleClass = null;
				echo '						<option '.$selected.' value="'.$clearance_id[$i].'" '.$titleClass.'>'.$clearance_title[$i].'</option>';
			}

			echo '						</select>
									</td>
									<td class="intcell">'.date('d/m/Y', $row['join_date']).'</td>
									<td>
										<span class="admin_listInfo admin_listInfoEmail">'.$row['email'].'</span>
										<input type="text" class="admin_editEmail admin_editInput registerinput" autocomplete="off" name="admin_editEmail" maxlength="255" value="'.$row['email'].'"/>
										<div class="forminfo"><div><span class="emailtaken">E-mail already in use.</span></div></div>
									</td>
									<td>
										<span class="uibtnEdit admin_inlineEdit"><span></span>Edit</span> 
										<span class="uibtnDelete"><span></span>Delete</span>
										<span class="uibtnSave"><span></span>Save</span> 
										<span class="uibtnCancel"><span></span>Cancel</span>
										<input type="checkbox" name="selectItem" class="selectItem" />
										<input type="submit" name="submit" class="adminEditSubmit" />
									</td>
								</tr>';
		}
		mysqli_free_result($result);

		//// NOT UPDATING -> OUTPUT UI
		if(!isset($_GET['update']) || secure_get($_GET['update'], true) != 1) {		
			//NO RESULTS -> ERROR
			if($num_rows == 0)
				echo '			<tr>';
			else echo '			<tr class="noError">';
			echo '					<td colspan="5" class="error">
										No users found.
									</td>
								</tr>
							</tbody>
						</table></fieldset></form>';
			if(!$on_banlistPage) echo '
					</div>';
			echo '	<script type="text/javascript" src="'.$jquery['auth']['admin']['users_ui'].'"></script>';
		}
	}
}
//// NOT LOGGED IN -> ERROR
else echo getErrorMsg('pagenotfound', 'adminpanel');
?>