<?php
echo "	<div class='admintools'>
			<span class='uibtnAdd'><span></span>New</span>
			<span class='uibtnEdit'><span></span>Edit</span> / 
			<span class='uibtnDelete'><span></span>Delete</span> selected			
		</div>
		<table id='leftlist' class='adminlist'>
			<thead><tr>
				<td>Position</td>
				<td>Image</td>
				<td>Mouse-over</td>
				<td>Colour</td>
				<td>Target</td>
				<td id='toolscol'><label for='selectallLeft'>Select all </label><input type='checkbox' name='selectallLeft' id='selectallLeft' class='selectall' />
				</td>
			</tr></thead>
			<tbody>";

$result = mysqli_query($sqldb,
						"	SELECT url, url_mo, target, position, colour FROM leftcolumn
							ORDER BY position, url ASC")
				or die('Database error.');
				
for($i=0; $row = mysqli_fetch_assoc($result); $i++) {
	echo "		<tr>
					<td class='intcell'>".$row['position']."</td>
					<td class='imgcell'><img src='".$img['leftdir'].$row['url']."' alt='".$row['url']."' /></td>
					<td class='imgcell'><img src='".$img['leftdir'].$row['url_mo']."' alt='".$row['url_mo']."' /></td>
					<td>".$row['colour']."</td>
					<td>".$row['target']."</td>
					<td>
						<span class='uibtnEdit'><span></span>Edit</span>
						<span class='uibtnDelete'><span></span>Delete</span>
						<input type='checkbox' name='selectImg' class='selectImg' />
					</td>
				</tr>";
}

echo "		</tbody>
		</table>";
?>