<?php
//////////////////////////////////
///// AUTH ADMIN NEWS MODULE /////
//////////////////////////////////
//INCLUDES
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include_once $incl['db_connect'];
include_once $incl['func']['misc'];
include_once $incl['func']['login'];
include_once $plugins['php']['jbbcode'];

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj

//// LOGGED IN & ADMIN -> CONTINUE
if($userValidation->isAdmin()) {
	if(!isset($on_userPage)) $on_userPage = false;

	//// NOT UPDATING -> OUTPUT UI
	if(!isset($_GET["update"]) || secure_get($_GET["update"], true) != 1) {
		echo "	<div class='adminSetContent'>";

		//NOT ON USERPAGE -> CONTINUE
		if(!$on_userPage) {
			//GET NEWSPANEL LENGTH
			$result = mysqli_query($sqldb, "SELECT newspanel_length FROM global_settings")
							or die('Database error.');
							
			$row = mysqli_fetch_assoc($result);
			mysqli_free_result($result);

			//OUTPUT SETTINGS
			echo "	<div id='adminsettings'>
						<form id='admin_newsSettings' class='admin_editSettings' accept-charset='utf-8'><fieldset>
							<label for='admin_editNewspanelLength'>Newspanel length: </label>
							<input type='text' class='admin_editLength' id='admin_editNewspanelLength' name='admin_editNewspanelLength' maxlength='2' value='".$row['newspanel_length']."' /> items<br />
							<input type='submit' name='submit' id='form_btn' class='form_btn_small' value='Save' />
							<input type='hidden' name='admin_newsSettingsForm' id='admin_newsSettingsForm' value='1' />
							<span id='admin_editSettingsSuccess' class='uibtnSave'><span></span></span>
						</fieldset></form>
					</div>";
		}
		//ON USER PAGE -> OUTPUT TITLE
		else
			echo "	<div class='settitle'>News</div>";

		echo "		<div class='admintools'>";
		if(!$on_userPage)
			echo "		<span class='uibtnAdd' id='adminNewsAdd'><span></span>New</span>";
		echo "			<span class='uibtnDelete'><span></span>Delete</span> selected			
					</div>";

		//OUTPUT ADMIN LIST
		echo "		<table id='newslist' class='adminlist'>
						<thead><tr>
							<th>Title</th>
							<th>Text</th>
							<th>User</th>
							<th>Posted</th>
							<th id='toolscol'>
								<label for='selectallNews'>Select all </label><input type='checkbox' name='selectallNews' id='selectallNews' class='selectall' />
							</th>
						</tr></thead>
						<tbody>";
	}

	//PRESET VARS
		$item_id = null;
		$queryClause = null;

		//// UPDATING -> SET ITEM ID QUERYCLAUSE
		if(isset($_GET["update"]) && secure_get($_GET["update"], true) == 1 && isset($_GET["item_id"])) {
			$item_id = secure_sql($_GET["item_id"], true);
			$queryClause = "AND CRC32(i.item_id) = '$item_id'";
		}
		//// ON USERPAGE -> SE T USERNAME QUERYCLAUSE
		elseif($on_userPage) $queryClause = "AND u.user_name = '$user_name'";

	//// GET ITEM INFO
	$result = mysqli_query($sqldb,
							"	SELECT i.item_id, i.title, i.text, i.post_date, u.user_name FROM items i, categories_items ci, users u
								WHERE	ci.cat_id = 1 AND
										i.item_id = ci.item_id AND
										u.user_id = i.user_id $queryClause
								ORDER BY i.post_date DESC")
					or die('Database error.');		

	$num_rows = mysqli_num_rows($result);

	//// OUTPUT NEWS LIST
	for($i=0; $row = mysqli_fetch_assoc($result); $i++) {
		$content = parseBBCodeShort($row['text'], 35, 75);	
		$title = nl2br(wordwrap(substr($row['title'], 0, 35), 25, "\n", true));
		$user = $row['user_name'];


		echo "			<tr>
							<td class='admin_newsTitle'><a>".$title."</a></td>
							<td>".$content."</td>
							<td><a href='".$main_dir['adminpanel']."users/".$user."'>".$user."</a></td>
							<td class='intcell'>".date('d/m/Y', $row['post_date'])."</td>
							<td>
								<span class='uibtnEdit'><span></span>Edit</span>
								<span class='uibtnDelete'><span></span>Delete</span>
								<input type='checkbox' name='selectItem' class='selectItem' />
								<input type='hidden' class='admin_editNewsId' name='admin_editNewsId' value='".enc_item_id($row['item_id'])."' />
							</td>
						</tr>";
	}
	mysqli_free_result($result);

	//// NOT UPDATING -> OUTPUT UI
	if(!isset($_GET["update"]) || secure_get($_GET["update"], true) != 1) {		
		//NO RESULTS -> ERROR
		if($num_rows == 0)
			echo "		<tr>";
		else echo "		<tr class='noError'>";
		echo "				<td colspan='5' class='error'>
								No items found.
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<script type='text/javascript' src='".$jquery['auth']['admin']['news_ui']."'></script>";
	}
}
//// NOT LOGGED IN -> ERROR
else echo getErrorMsg('pagenotfound', 'adminpanel');
?>