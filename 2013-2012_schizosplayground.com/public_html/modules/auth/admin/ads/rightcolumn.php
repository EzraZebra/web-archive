<?php
/////////////////////////////////
///// AUTH ADMIN ADS MODULE /////
/////////////////////////////////
//// INCLUDES
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include_once $incl['db_connect'];
include_once $incl['func']['misc'];
include_once $incl['func']['login'];

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj

//// ADMIN -> CONTINUE
if($userValidation->isAdmin()) {
	echo '	<div class="adminSetContent">';
	//GET ADS LENGTH
	$result = mysqli_query($sqldb, "SELECT rightcolumn_length FROM ad_settings")
					or die('Database error.');
					
	$row = mysqli_fetch_assoc($result);
	mysqli_free_result($result);

	//OUTPUT UI
	echo '		<div id="adminsettings">
					<form id="admin_adsSettings" class="admin_editSettings admin_editSettingsRightcolumn" accept-charset="utf-8"><fieldset>
						<label for="admin_editAdsLength">Display: </label>
						<input type="text" class="admin_editLength" id="admin_editAdsLength" name="admin_editAdsLength" maxlength="1" value="'.$row['rightcolumn_length'].'" /> ads<br />
						<input type="submit" name="submit" id="form_btn" class="form_btn_small" value="Save" />
						<input type="hidden" name="admin_adsSettingsForm" id="admin_adsSettingsForm" value="1" />
						<span id="admin_editSettingsSuccess" class="uibtnSave"><span></span></span>
					</fieldset></form>
				</div>
				<div class="admintools">
					<span class="newItemContainer">
						<span class="uibtnAdd" id="adminAdAdd"><span></span>New</span> / 
						<span class="uibtnAdd uibtnAddGoogle" id="adminGoogleAdAdd" title="Google AdSense"><span></span>New</span>
					</span>
					<span class="uibtnSave"><span></span>Save</span> / 
					<span class="uibtnCancel"><span></span>Cancel</span> / 
					<span class="uibtnEdit"><span></span>Edit</span> / 
					<span class="uibtnDelete"><span></span>Delete</span> selected		
				</div>
				<form id="adminEdit" accept-charset="utf-8"><fieldset><table id="adlist" class="adminlist">
					<thead><tr>
						<th>Position</th>
						<th>Name</th>
						<th>URL / <img src="'.$img['ui']['google'].'" alt="Google AdSense" title="Google AdSense"> Slot</th>
						<th>Image / <img src="'.$img['ui']['google'].'" alt="Google AdSense" title="Google AdSense"> Size</th>
						<th>Views</th>
						<th>Clicks</th>
						<th>Earnings</th>
						<th id="toolscol">
							<label for="selectallAds">Select all </label><input type="checkbox" name="selectallAds" id="selectallAds" class="selectall" />
						</th>
					</tr></thead>
					<tbody>
						<tr id="newItem">
							<td class="intcell">
								<img src="'.$img['ui']['googleb'].'" alt="Google AdSense" title="Google AdSense" class="admin_googleAdForm">
								<input type="text" id="admin_addPriority" name="admin_addPriority" class="admin_editInputSmall" maxlength="1" />
							</td>
							<td><input type="text" id="admin_addName" name="admin_addName" size="15" maxlength="100" /></td>
							<td>
								<input class="admin_adForm" type="text" id="admin_addUrl" name="admin_addUrl" size="18"  maxlength="255" value="http://" />
								<input class="admin_googleAdForm" type="text" id="admin_addSlot" name="admin_addSlot" size="18"  maxlength="10" />
							</td>
							<td class="fileUploadCell">
								<iframe class="admin_adForm fileUpload_frame" src="'.$mod['auth']['fileUpload'].'admin_ads/rightcolumn/"></iframe>
								<span class="admin_googleAdForm admin_googleAdFormSize">
									<input type="text" id="admin_addWidth" name="admin_addWidth" size="3"  maxlength="3" /> x 
									<input type="text" id="admin_addHeight" name="admin_addHeight" size="3"  maxlength="3" />
								</span>
							</td>
							<td class="intcell">&mdash;</td>
							<td class="intcell">&mdash;</td>
							<td class="intcell">&mdash;</td>
							<td>
								<span class="uibtnSave"><span></span>Save</span> 
								<span class="uibtnCancel"><span></span>Cancel</span>
							</td>
						</tr>
						<tr id="itemPrototype">
							<td class="intcell">
								<img src="'.$img['ui']['googleb'].'" alt="Google AdSense" title="Google AdSense" class="admin_googleAdForm">
								<span class="admin_listInfo admin_listInfoPriority"></span>
								<input type="text" class="admin_editPriority admin_editInput admin_editInputSmall" autocomplete="off" name="admin_editPriority" maxlength="1" />
								<input type="hidden" class="admin_editAdId" name="admin_editAdId" />
							</td>
							<td>
								<span class="admin_listInfo admin_listInfoAdName"></span>
								<input type="text" class="admin_editAdName admin_editInput" name="admin_editAdName" size="15" maxlength="100" />
							</td>
							<td>
								<span class="admin_adForm">
									<span class="admin_listInfo admin_listInfoAdUrl"></span>
									<input type="text" class="admin_editAdUrl admin_editInput" name="admin_editAdUrl" size="18" maxlength="255" />
								</span>
								<span class="admin_googleAdForm">
									<span class="admin_listInfo admin_listInfoAdSlot"></span>
									<input type="text" class="admin_editAdSlot admin_editInput" id="admin_editAdSlotL" name="admin_editAdSlotL" size="18" maxlength="10" />
								</span>
							</td>
							<td class="fileUploadCell admin_adForm">
								<iframe class="fileUpload_frame" src="'.$mod['auth']['fileUpload'].'admin_ads/rightcolumn/"></iframe>
							</td>
							<td class="intcell admin_googleAdForm">
								<span class="admin_listInfo">
									<span class="admin_listInfoAdWidth"></span>x<span class="admin_listInfoAdHeight"></span>
								</span>
								<span class="admin_editInput">
									<input type="text" class="admin_editAdWidth admin_editInput" name="admin_editAdWidth" size="3" maxlength="3" /> x 
									<input type="text" class="admin_editAdHeight admin_editInput" name="admin_editAdHeight" size="3" maxlength="3" />
								</span>
							</td>
							<td class="intcell admin_adForm">0</td>
							<td class="intcell admin_adForm">0</td>
							<td class="intcell admin_adForm">&euro;0</td>
							<td class="intcell admin_googleAdForm">&mdash;</td>
							<td class="intcell admin_googleAdForm">&mdash;</td>
							<td class="intcell admin_googleAdForm">&mdash;</td>
							<td>
								<span class="uibtnEdit admin_inlineEdit"><span></span>Edit</span> 
								<span class="uibtnDelete"><span></span>Delete</span>
								<span class="uibtnSave"><span></span>Save</span> 
								<span class="uibtnCancel"><span></span>Cancel</span>
								<input type="checkbox" name="selectItem" class="selectItem" />
								<input type="submit" name="submit" class="adminEditSubmit" />
							</td>
						</tr>';

	//GET AD INFO
	$result = mysqli_query($sqldb,
						"	SELECT ad_id, name, url, priority, googleAd, slot, width, height, views, clicks FROM ads
							WHERE widget='rightcolumn'
							ORDER BY priority, name ASC")
					or die('Database error');
					
	$num_rows = mysqli_num_rows($result);

	while($row = mysqli_fetch_assoc($result)) {
		//SET VARS
		$priority = $row['priority'];
		$ad_id = $row['ad_id'];
		$name = secure_get($row['name'], false);
		$googleAd = $row['googleAd'];
		if(!$googleAd) {
			$url = secure_get($row['url'], false);			
			$views = secure_get($row['views'], false);
			$clicks = secure_get($row['clicks'], false);
			$earnings = '&euro;'.floor((($views/$priority*3+$clicks/$priority*4)/100)*pow(10,2))/pow(10,2);
			$rowClass = 'adminEditAd';
		}
		else {
			$slot = secure_get($row['slot'], false);
			$width = secure_get($row['width'], false);
			$height = secure_get($row['height'], false);
			$views = '&mdash;';
			$clicks = '&mdash;';
			$earnings = '&mdash;';
			$rowClass = 'adminEditGoogleAd';
		}

		// OUTPUT ADLIST
		echo '			<tr class="listitem '.$rowClass.'">
							<td class="intcell">';
		if($googleAd) echo '	<img src="'.$img['ui']['googleb'].'" alt="Google AdSense" title="Google AdSense" class="admin_googleAdForm">';
		echo '					<span class="admin_listInfo admin_listInfoPriority">'.$priority.'</span>
								<input type="text" class="admin_editPriority admin_editInput admin_editInputSmall" autocomplete="off" name="admin_editPriority" maxlength="1" value="'.$priority.'" />
								<input type="hidden" class="admin_editAdId" name="admin_editAdId" value="'.$ad_id.'" />
							</td>
							<td>
								<span class="admin_listInfo admin_listInfoAdName" title="'.$name.'">'.wordwrap($name, 20, '<br />', true).'</span>
								<input type="text" class="admin_editAdName admin_editInput" name="admin_editAdName" size="15" maxlength="100" value="'.$name.'" />
							</td>
							<td>';
		if(!$googleAd) echo '	<span class="admin_listInfo admin_listInfoAdUrl" '.$url.'">'.wordwrap($url, 20, "<br />", true).'</span>
								<input type="text" class="admin_editAdUrl admin_editInput" name="admin_editAdUrl" size="18" maxlength="255" value="'.$url.'" />';
		else echo '				<span class="admin_listInfo admin_listInfoAdSlot">'.$slot.'</span>
								<input type="text" class="admin_editAdSlot admin_editInput" id="admin_editAdSlotL" name="admin_editAdSlotL" size="18" maxlength="10" value="'.$slot.'" />';
		echo '				</td>';
		if(!$googleAd) echo '	
							<td class="fileUploadCell">
								<iframe class="fileUpload_frame" src="'.$mod["auth"]["fileUpload"].'admin_ads/rightcolumn/'.$ad_id.'/"></iframe>';
		else echo '			<td class="intcell">
								<span class="admin_listInfo">
									<span class="admin_listInfoAdWidth">'.$width.'</span>x<span class="admin_listInfoAdHeight">'.$height.'</span>
								</span>
								<span class="admin_editInput">
									<input type="text" class="admin_editAdWidth admin_editInput" name="admin_editAdWidth" size="3" maxlength="3" value="'.$width.'" /> x 
									<input type="text" class="admin_editAdHeight admin_editInput" name="admin_editAdHeight" size="3" maxlength="3" value="'.$height.'" />
								</span>';
		echo '				</td>
							<td class="intcell">'.$views.'</td>
							<td class="intcell">'.$clicks.'</td>
							<td class="intcell">'.$earnings.'</td>
							<td>
								<span class="uibtnEdit admin_inlineEdit"><span></span>Edit</span> 
								<span class="uibtnDelete"><span></span>Delete</span>
								<span class="uibtnSave"><span></span>Save</span> 
								<span class="uibtnCancel"><span></span>Cancel</span>
								<input type="checkbox" name="selectItem" class="selectItem" />
							</td>
						</tr>';
	}
	//NO RESULTS -> ERROR
	if($num_rows == 0)
		echo '			<tr class="someError">';
	else echo '			<tr class="noError">';
	echo '					<td colspan="8" class="error">';
	echo '						No ads found.
							</td>
						</tr>';
	echo '			</tbody>
				</table></fieldset></form>
			</div>
			<script type="text/javascript" src="'.$jquery["auth"]["admin"]["ads_ui"]['rightcolumn'].'"></script>
			<script type="text/javascript" src="'.$jquery["auth"]["fileUpload_ui"].'"></script>';
}
//// NOT LOGGED IN -> ERROR
else echo getErrorMsg('pagenotfound', 'adminpanel');
?>