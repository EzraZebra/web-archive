<?php
/////////////////////////////////
///// AUTH ADMIN ADS MODULE /////
/////////////////////////////////
//// INCLUDES
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include_once $incl['db_connect'];
include_once $incl['func']['misc'];
include_once $incl['func']['login'];

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj

//// ADMIN -> CONTINUE
if($userValidation->isAdmin()) {
	echo '	<div class="adminSetContent">';
	//GET ADS LENGTH
	$result = mysqli_query($sqldb, "SELECT googlePub_id FROM ad_settings")
					or die('Database error.');
					
	$row = mysqli_fetch_assoc($result);
	mysqli_free_result($result);

	//OUTPUT UI
	echo '		<div id="adminsettings">
					<form id="admin_adsSettings" class="admin_editSettings admin_editSettingsMain" accept-charset="utf-8"><fieldset>
						<label for="admin_editGooglePub_id">Google Publisher ID: </label>
						pub-ca-<input type="text" class="" id="admin_editGooglePub_id" name="admin_editGooglePub_id" maxlength="16" size="17" value="'.$row['googlePub_id'].'" />
						<br />
						<input type="submit" name="submit" id="form_btn" class="form_btn_small" value="Save" />
						<input type="hidden" name="admin_adsSettingsForm" id="admin_adsSettingsForm" value="1" />
						<span id="admin_editSettingsSuccess" class="uibtnSave"><span></span></span>
					</fieldset></form>
				</div>
			</div>
			<script type="text/javascript" src="'.$jquery["auth"]["admin"]["ads_ui"]['main'].'"></script>
			<script type="text/javascript" src="'.$jquery["auth"]["fileUpload_ui"].'"></script>';
}
//// NOT LOGGED IN -> ERROR
else echo getErrorMsg('pagenotfound', 'adminpanel');
?>