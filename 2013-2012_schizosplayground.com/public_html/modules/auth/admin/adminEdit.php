<?php
/////////////////////////////////
///// AUTH USER EDIT MODULE /////
/////////////////////////////////
//// INCLUDES
include $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include $incl['func']['misc'];
include $incl['func']['login'];

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj

//// LOGGED IN & ADMIN -> CONTINUE
if($userValidation->isAdmin()) {
	//// EDIT SETTIGNS -> CONTINUE
	if(isset($_POST["editSettings"]) && secure_get($_POST["editSettings"], true) == 1) {
		if(isset($_POST["cset"])) $cset = secure_get($_POST["cset"], true);
			else $cset = null;

		//// EDIT NEWS SETTINGS -> CONTINUE
		if($cset == 'news' && isset($_POST['newspanel_length'])) {
			$newspanel_length = secure_sql($_POST['newspanel_length'], true);

			if($newspanel_length > 0) {
				//// EDIT NEWSPANEL LENGTH
				mysqli_query($sqldb, "UPDATE global_settings SET newspanel_length = '$newspanel_length'")
						or die('Database error.');
						
				echo 'success';
			} else echo 'Invalid data.'; //invalid data
		}
		//// EDIT USER SETTINGS -> CONTINUE
		elseif($cset == 'users' && isset($_POST['activity_threshold'])) {
			$activity_threshold = secure_sql($_POST['activity_threshold'], true);

			if($activity_threshold > 0) {
				//// EDIT NEWSPANEL LENGTH
				mysqli_query($sqldb, "UPDATE global_settings SET activity_threshold = '$activity_threshold'")
						or die('Database error.');
						
				echo 'success';
			} else echo 'Invalid data.'; //invalid data
		}
		//// EDIT LOGIN SETTINGS -> CONTINUE
		elseif($cset == 'logins' && isset($_POST['login_remember']) && isset($_POST['login_time'])) {
			$login_remember = secure_sql($_POST['login_remember'], true);
			$login_time = secure_sql($_POST['login_time'], true);

			if($login_remember > 0 && $login_time > 0) {
				//// EDIT NEWSPANEL LENGTH
				mysqli_query($sqldb, "UPDATE global_settings SET login_remember = '$login_remember', login_time = '$login_time'")
						or die('Database error.');
						
				echo 'success';
			} else echo 'Invalid data.'; //invalid data
		}
		//// EDIT ADS SETTINGS -> CONTINUE
		elseif($cset == 'ads') {
			//PUB ID
			if(isset($_POST['googlePub_id'])) {
				$googlePub_id = secure_sql($_POST['googlePub_id'], true);
				if($googlePub_id != null && strlen($googlePub_id) == 16) {
					//// EDIT NEWSPANEL LENGTH
					mysqli_query($sqldb, "UPDATE ad_settings SET googlePub_id = '$googlePub_id'")
							or die('Database error.');
							
					echo 'success';
				} else echo 'Invalid data.';
			}
			//RIGHT COLUMN LENGTH
			elseif(isset($_POST['ads_length'])) {
				$ads_length = secure_sql($_POST['ads_length'], true);
				if($ads_length != null) {
					//// EDIT NEWSPANEL LENGTH
					mysqli_query($sqldb, "UPDATE ad_settings SET rightcolumn_length = '$ads_length'")
							or die('Database error.');
							
					echo 'success';
				} else echo 'Invalid data.';
			}
			else echo 'Invalid data.'; //missing data
		} else echo 'No data provided.'; //data missing
	}
	//// CLEARANCE EDIT -> CONTINUE
	elseif(isset($_POST["clearanceEdit"]) && secure_get($_POST["clearanceEdit"], true) == 1) {
		//SET VARS
		if(isset($_POST["title"])) $title = secure_sql($_POST["title"], false);
		if(isset($_POST["special_perm"])) $special_perm = secure_sql($_POST["special_perm"], false);
		if(isset($_POST["post_news"])) $post_news = secure_sql($_POST["post_news"], true);
		if(isset($_POST["clearance_id"])) $clearance_id = secure_sql($_POST["clearance_id"], true);

		//// SAVE
		if((isset($_POST["saveEdit"]) || isset($_POST["saveNew"])) && isset($title)) {
			if(!isset($clearance_id)) $clearance_id = 0;
			$result = mysqli_query($sqldb,
								"	SELECT title FROM clearance
									WHERE	title = '$title' AND
											clearance_id != '$clearance_id'")
						or die('Databse Error.');
			// NO DUPLICATE TITLE -> CONTINUE
			if(mysqli_num_rows($result) == 0) {
				//// SAVE CLEARANCE EDIT
				if(isset($_POST["saveEdit"]) && secure_get($_POST["saveEdit"], true) == 1) {
					// VALID FORM -> CONTINUE
					if(isset($clearance_id) && isset($title) && isset($special_perm)) {				
						// VALID DATA -> CONTINUE
						if(		$clearance_id > 0
						&& (	$special_perm == null || $special_perm == "inactive" || $special_perm == "banned" || $special_perm == "admin")) {
							// VALID TITLE -> CONTINUE
							if($title != null) {
								// EDITING ROOT SPEC PERM
								if($userValidation->hasRootSpecPerm($clearance_id)) {
									// NO SPEC PERM SET -> CONTINUE
									if($special_perm == null) {
										// SAVE EDIT
										mysqli_query($sqldb,
													"	UPDATE	clearance SET title = '$title'
														WHERE	clearance_id = '$clearance_id'")
												or die('Database error.');
												
										echo 'success';
									}
									// SPEC PERM SET -> ERROR
									else echo 'Cannot edit special permissions for root clearance.';
								}
								// NOT EDITING ROOT SPEC PERM
								else {
									// SAVE EDIT
									mysqli_query($sqldb,
												"	UPDATE	clearance SET title = '$title', special_perm = '$special_perm'
													WHERE	clearance_id = '$clearance_id'")
											or die('Database error.');

									// SET PERMISSIONS FOR SPECIAL PERMISSIONS
									if($special_perm != null) {
										if($special_perm == "inactive" || $special_perm == "banned") $post_news = 0;
											elseif($special_perm == "admin") $post_news = 1;

										mysqli_query($sqldb,
													"	UPDATE	clearance SET post_news = '$post_news'
														WHERE	clearance_id = '$clearance_id'")
												or die('Database error.');						
									}
									echo 'success'; //success
								}
							} else echo 'Please enter a clearance title.'; //invalid title
						} else echo 'Invalid data.'; //invalid data
					} else echo 'Missing data.'; //invalid data
				}
				//// SAVE CLEARANCE NEW
				elseif(isset($_POST["saveNew"]) && secure_get($_POST["saveNew"], true) == 1) {
					// VALID FORM -> CONTINUE
					if(isset($title) && isset($special_perm) && isset($post_news)) {				
						// VALID DATA -> CONTINUE
						if(	($special_perm == null || $special_perm == "inactive" || $special_perm == "banned" || $special_perm == "admin")
						&&	($post_news == 0 || $post_news == 1)) {
							// VALID TITLE -> CONTINUE
							if($title != null) {
								// OVERRIDE PERMISSIONS FOR SPECIAL PERMISSIONS
								if($special_perm != null) {
									if($special_perm == "inactive" || $special_perm == "banned") $post_news = 0;
										elseif($special_perm == "admin") $post_news = 1;
								}
								// SAVE NEW
								mysqli_query($sqldb,
											"	INSERT INTO clearance (title, special_perm, post_news)
												VALUES ('$title', '$special_perm', '$post_news')")
									or die('Database error.');
								echo 'success'.mysqli_insert_id($sqldb); //success
							} else echo 'Please enter a clearance title.'; //invalid title
						} else echo 'Invalid data.'; //invalid data
					} else echo 'Missing data.'; //missing data
				}
			} else echo 'Clearance title already taken.';
			mysqli_free_result($result);
		}
		//// DELETE CLEARANCE
		elseif(isset($_POST["deleteClearance"]) && secure_get($_POST["deleteClearance"], true) == 1) {
			// VALID DATA -> CONTINUE
			if(isset($clearance_id)) {				
				// VALID DATA -> CONTINUE
				if($clearance_id > 0) {
					// IS NOT ROOT CLEARANCE -> CONTINUE
					if(!$userValidation->hasRootSpecPerm($clearance_id)) {
						// AFFECTED USERS
							//GET AFFECTED USER
							$result = mysqli_query($sqldb,
												"	SELECT user_id FROM users
													WHERE	clearance = '$clearance_id'")
											or die('Database error.');

							//USERS FOUND -> CONTINUE
							if(mysqli_num_rows($result) > 0) {
								//GET SPECIAL PERM FOR CURRENT CLEARANCE
									$spec_permResult = mysqli_query($sqldb,
																	"	SELECT special_perm FROM clearance
																		WHERE	clearance_id = '$clearance_id'")
															or die('Database error.');

									$row = mysqli_fetch_assoc($spec_permResult);
									mysqli_free_result($spec_permResult);

									$spec_perm = $row['special_perm'];

								//GET ROOT CLEARANCE
								$new_clearance_id = $userValidation->getRootClearance($spec_perm);

								//UPDATE AFFECTED USERS
								while($row = mysqli_fetch_assoc($result)) {
									$user_id = $row['user_id'];
									mysqli_query($sqldb,
												"	UPDATE users SET clearance = '$new_clearance_id'
													WHERE user_id = '$user_id'")
											or die('Database error.');
								}
							}
						
						// DELETE CLEARANCE
						mysqli_query($sqldb,
									"	DELETE FROM clearance
										WHERE	clearance_id = '$clearance_id'")
								or die('Database error.');
						echo 'success'; //success
					}
					// ROOT CLEARANCE -> ERROR
					else echo 'Cannot delete root clearance.';
				} else echo 'Invalid data.'; //invalid data
			} else echo 'Missing data.'; //invalid data
		}
		//// EDIT CLEARANCE PERMISSIONS
		elseif(isset($_POST["editPermissions"]) && secure_get($_POST["editPermissions"], true) == 1) {
			// EDIT POST_NEWS
			if(isset($post_news) && isset($clearance_id)) {				
				// VALID DATA -> CONTINUE
				if(($post_news == 0 || $post_news == 1) && $clearance_id > 0) {
					//get special_perm
					$result = mysqli_query($sqldb,
										"	SELECT special_perm FROM clearance
											WHERE	clearance_id = '$clearance_id'")
							or die('Databse Error.');
							
					$row = mysqli_fetch_assoc($result);
					mysqli_free_result($result);
					// NO SPECIAL_PERM -> CONTINUE
					if($row['special_perm'] == null) {				
						// EDIT POST_NEWS
						mysqli_query($sqldb,
									"	UPDATE	clearance SET post_news = '$post_news'
										WHERE	clearance_id = '$clearance_id'")
								or die('Database error.');
						echo 'success'; //success
					} else echo 'Cannot edit permissions for '.$row['special_perm'].' users.'; //special_perm
				} else echo 'Invalid data'; //invalid data
			} else echo 'Missing data.'; //missing data
		} else header('Location: '.$main_dir['home']); //invalid form
	}
	//// AD EDIT -> CONTINUE
	elseif(isset($_POST["adEdit"]) && secure_get($_POST["adEdit"], true) == 1) {
		// SAVE AD
		if(isset($_POST["saveEdit"]) || isset($_POST["saveNew"])) {
			//SET VARS
			if(isset($_POST["priority"])) $priority = secure_sql($_POST["priority"], true);
				else $priority = null;
			if(isset($_POST["adName"])) $adName = secure_sql($_POST["adName"], false);
				else $adName = null;

			if(isset($_POST["googleAd"]) && secure_get($_POST["googleAd"], true == 1)) {
				$googleAd = 1;
				if(isset($_POST["adSlot"])) $adSlot = secure_sql($_POST["adSlot"], true);
					else $adSlot = null;
				if(isset($_POST["adWidth"])) $adWidth = secure_sql($_POST["adWidth"], true);
					else $adWidth = null;
				if(isset($_POST["adHeight"])) $adHeight = secure_sql($_POST["adHeight"], true);
					else $adHeight = null;
				$adUrl = null;
			}
			else {
				$googleAd = 0;
				if(isset($_POST["adUrl"])) $adUrl = secure_sql($_POST["adUrl"], false);
					else $adUrl = null;
				$adSlot = null;
				$adWidth = null;
				$adHeight = null;
			}

			// VALID PRIORITY -> CONTINUE
			if($priority != null) {
				// VALID NAME -> CONTINUE
				if($adName != null) {
					// VALID URL -> CONTINUE
					if($googleAd || $adUrl != null) {
						if(!$googleAd || $adSlot != null) {
							if(!$googleAd || $adWidth != null && $adHeight != null) {
								//SET AD ID
								if(isset($_POST["ad_id"])) $ad_id = secure_sql($_POST["ad_id"], true);
									else $ad_id = null;

								//SAVE EDIT
								if(isset($_POST["saveEdit"]) && secure_get($_POST["saveEdit"], true) == 1) {
									mysqli_query($sqldb,
												"	UPDATE	ads SET name = '$adName', url = '$adUrl', priority = '$priority', slot = '$adSlot', width = '$adWidth', height = '$adHeight'
													WHERE	ad_id = '$ad_id'")
											or die('Database error.');
											
									echo 'success';
								}
								//// SAVE NEW
								elseif(isset($_POST["saveNew"]) && secure_get($_POST["saveNew"], true) == 1) {
									if(isset($_POST["widget"])) $widget = secure_sql($_POST["widget"], false);
										else $widget = null;

									if($widget != null) {
										mysqli_query($sqldb,
													"	INSERT INTO ads (widget, name, url, priority, googleAd, slot, width, height)
														VALUES ('$widget', '$adName', '$adUrl', '$priority', '$googleAd', '$adSlot', '$adWidth', '$adHeight')")
											or die('Database error.');

										echo 'success'.mysqli_insert_id($sqldb); //success
									}
									else echo 'Invalid data.'; //form error
								}
							} else echo 'Please enter a valid size.';
						} else echo 'Please enter a valid Google Ad Slot';
					} else echo 'Please enter an URL.'; //invalid url
				} else echo 'Please enter a name.'; //invalid name
			} else echo 'Please enter a valid priority.'; //invalid priority
		}
		//// DELETE AD
		elseif(isset($_POST["deleteAd"]) && secure_get($_POST["deleteAd"], true) == 1) {
			// VALID DATA -> CONTINUE
			if(isset($_POST["ad_id"]) && secure_sql($_POST["ad_id"], true) != null) {
				$ad_id = secure_sql($_POST["ad_id"], true);
				//GET IMG
				$result = mysqli_query($sqldb,
									"	SELECT img FROM ads
										WHERE ad_id = '$ad_id'")
								or die('Database error.');
				$row = mysqli_fetch_assoc($result);
				mysqli_free_result($result);
				$img = $row['img'];
				
				// DELETE AD
				mysqli_query($sqldb,
							"	DELETE FROM ads
								WHERE	ad_id = '$ad_id'")
						or die('Database error.');
						

				// DELETE IMAGE
				if($img != null && file_exists('../../../images/reklam/'.$img)) unlink('../../../images/reklam/'.$img);
				
				echo 'success'; //success
			} else echo 'Missing data.'; //invalid data
		} else header('Location: '.$main_dir['home']); //invalid form
	}
	//// IPBAN & IPs VALID -> CONTINUE
	elseif(isset($_POST["ipBan"]) && secure_get($_POST["ipBan"], true) == 1
		&& isset($_POST["ip"]) && is_numeric($_POST["ip"]) && isset($_POST["ip_range"]) && is_numeric($_POST["ip_range"])) {
		//SET IPs
		$ip = secure_sql($_POST['ip'], true);
		$ip_range = secure_sql($_POST['ip_range'], true);

		//CHECK BANS
		$result = mysqli_query($sqldb,
							"	SELECT	* FROM ipbans
								WHERE	ip <= '$ip' AND
										ip_range >= '$ip_range'")
				or die('Database error.');
				
		//NOT BANNED -> CONTINUE
		if(mysqli_num_rows($result) == 0) {
			mysqli_free_result($result);
				
			//GET OVERLAPPING RANGES
			$result = mysqli_query($sqldb,
								"	SELECT	MIN(ip) ip_min, MAX(ip_range) ip_max FROM ipbans
									WHERE	(ip >= '$ip' AND ip_range <= '$ip_range') OR
											(ip < '$ip' AND ip_range >= ('$ip'-1)) OR
											(ip <= ('$ip_range'+1) AND ip_range > '$ip_range')")
					or die('Database error.');
					
			//OVERLAPPING RANGES FOUND -> CONTINUE
			if(mysqli_num_rows($result) > 0) {
				$row = mysqli_fetch_assoc($result);
				mysqli_free_result($result);

				//SET RANGES
				$ip_split = $row['ip_min'];
				$ip_range_split = $row['ip_max'];

				//DELETE OVERLAPPING RANGES
				mysqli_query($sqldb,
							"	DELETE FROM ipbans 
								WHERE	((ip >= '$ip' AND ip_range <= '$ip_range') OR
										(ip < '$ip' AND ip_range >= ('$ip'-1)) OR
										(ip <= ('$ip_range'+1) AND ip_range > '$ip_range')) AND
										ip >= '$ip_split' AND ip_range <= '$ip_range_split'")
					or die('Database error.');				
			}
			
			//NO OVERLAPPING RANGES FOUND -> SET RANGES TO CURRENT
			if(!isset($ip_split) || $ip_split == 0 || $ip_split > $ip) $ip_split = $ip;
			if(!isset($ip_range_split) || $ip_range_split == 0 || $ip_range_split < $ip_range) $ip_range_split = $ip_range;
				
			//BAN IP RANGE
			mysqli_query($sqldb,
						"	INSERT INTO ipbans (ip, ip_range)
							VALUES ('$ip_split', '$ip_range_split')")
					or die('Database error.');
					
			mysqli_query($sqldb,
						"	UPDATE login SET expires=0
							WHERE	ip >= '$ip_split' AND
									ip <= '$ip_range_split'")
					or die('Database error.');
			
			echo 'success'; //success
		}
		//ALREADY BANNED
		else {
			//GET IP STRINGS
			$ip = long2ip($ip);
			$ip_range = long2ip($ip_range);
			
			echo $ip;
			if($ip != $ip_range) echo '-'.$ip_range; //range
			echo ' has already been banned.';
		}
	}
	//// IPUNBAN & IPs VALID -> CONTINUE
	elseif(isset($_POST["ipUnban"]) && secure_get($_POST["ipUnban"], true) == 1
		&& isset($_POST["ip"]) && is_numeric($_POST["ip"]) && isset($_POST["ip_range"]) && is_numeric($_POST["ip_range"])) {
		//SET IPs
		$ip = secure_sql($_POST['ip'], true);
		$ip_range = secure_sql($_POST['ip_range'], true);

		//CHECK BANS
		$result = mysqli_query($sqldb,
							"	SELECT	* FROM ipbans
								WHERE	(ip >= '$ip' AND ip_range <= '$ip_range') OR
										(ip <= '$ip' AND ip_range >= '$ip') OR
										(ip <= '$ip_range' AND ip_range >= '$ip_range')")
				or die('Database error.');
				
		//BANNED -> CONTINUE
		if(mysqli_num_rows($result) > 0) {
			mysqli_free_result($result);
			
			//DELETE BANS WITHIN RANGE
			mysqli_query($sqldb,
						"	DELETE FROM ipbans 
							WHERE 	ip >= '$ip' AND
									ip_range <= '$ip_range'")
				or die('Database error.');

			//GET OVERLAPPING RANGES
			$result = mysqli_query($sqldb,
								"	SELECT	MIN(ip) ip_min, MAX(ip_range) ip_max FROM ipbans
									WHERE	(ip < '$ip' AND ip_range >= '$ip') OR
											(ip <= '$ip_range' AND ip_range > '$ip_range')")
					or die('Database error.');

			//OVERLAPPING RANGES FOUND -> CONTINUE
			if(mysqli_num_rows($result) > 0) {
				$row = mysqli_fetch_assoc($result);
				mysqli_free_result($result);

				//SET RANGES
				$ip_split = $row['ip_min'];
				$ip_range_split = $row['ip_max'];

				//DELETE OVERLAPPING RANGES
				mysqli_query($sqldb,
							"	DELETE FROM ipbans 
								WHERE	((ip < '$ip' AND ip_range >= '$ip') OR
										(ip <= '$ip_range' AND ip_range > '$ip_range')) AND
										ip >= '$ip_split' AND ip_range <= '$ip_range_split'")
					or die('Database error.');
				
				//OVERLAPPING RANGE UNDER
				if($ip_split < $ip && $ip_split != 0) {
					$ip--;
					mysqli_query($sqldb,
								"	INSERT INTO ipbans (ip, ip_range)
									VALUES ('$ip_split', '$ip')")
						or die('Database error.');
				}
				//OVERLAPPING RANGE OVER
				if($ip_range_split > $ip_range && $ip_range_split != 0) {
					$ip_range++;
					mysqli_query($sqldb,
								"	INSERT INTO ipbans (ip, ip_range)
									VALUES ('$ip_range', '$ip_range_split')")
						or die('Database error.');
				}
				
			}
			
			echo 'success'; //success
		}
		//NOT BANNED
		else {
			//GET IP STRINGS
			$ip = long2ip($ip);
			$ip_range = long2ip($ip_range);
			
			echo $ip;
			if($ip != $ip_range) echo '-'.$ip_range; //range
			echo ' has not been banned.';
		}
	}
	else header('Location: '.$main_dir['home']); //invalid form
} else echo 'You do not have permission to perform this action.'; //not logged in/admin
?>