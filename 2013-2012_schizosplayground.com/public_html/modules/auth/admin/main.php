<?php
//////////////////////////////////
///// AUTH ADMIN MODULE //////////
//////////////////////////////////

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj

//// ADMIN -> CONTINUE
if($userValidation->isAdmin()) {
	//SET VARS
		//get control set
		if(isset($_GET['cset'])) $cset = secure_get($_GET['cset'], true);
			else $cset = 'news';
		if(isset($_GET['subset'])) $subset = secure_get($_GET['subset'], false);
			else $subset = 'main';
		
		if($cset == 'ads') $title = 'Advertising';
		elseif($cset == 'leftcolumn') $title = 'Left Column';
		else $title = ucfirst($cset);

		if($cset == 'users' && isset($_GET["userpage"]) && secure_get($_GET['userpage'],true) == 1 && isset($_GET["user"])) 
			$subset = secure_sql($_GET["user"], false);

		if($subset != 'main') {
			if($subset == 'rightcolumn') $titleSubset = 'Right Column';
			elseif($subset == 'bottombanner') $titleSubset = 'Banner Bottom';
			elseif($subset == 'logins') $titleSubset = 'Log-ins';
			else $titleSubset = ucfirst($subset);
			
			$title = '<a href="..">'.$title.'</a> > '.$titleSubset;
		}
				



	// OUTPUT ADMIN PANEL
	echo '	<div id="title"><span>Admin Panel</span><div class="settitle">'.$title.'</div></div>
			<div id="admincontentwrapper">
				<div id="adminmenu"><ul>
					<li><a href="'.$main_dir['adminpanel'].'news/">News</a></li>
					<li><a href="'.$main_dir['adminpanel'].'users/">Users</a></li>
					<li class="adminsubmenu"><a href="'.$main_dir['adminpanel'].'users/logins/">Log-ins</a></li>
					<li class="adminsubmenu"><a href="'.$main_dir['adminpanel'].'users/banlist/">Banlist</a></li>
					<li class="adminsubmenu"><a href="'.$main_dir['adminpanel'].'users/clearance/">Clearance</a></li>
					<li><a href="'.$main_dir['adminpanel'].'ads/">Advertising</a></li>
					<li class="adminsubmenu"><a href="'.$main_dir['adminpanel'].'ads/rightcolumn/">Right Column</a></li>
					<li class="adminsubmenu"><a href="'.$main_dir['adminpanel'].'ads/bottombanner/">Bottom Banner</a></li>
					<li><a href="'.$main_dir['adminpanel'].'leftcolumn/">Left Column</a></li>
				</ul></div>
				<div id="admincontent">';	
		
	// OUTPUT CONTROL SET
	if(isset($mod['adminpanelcsets'][$cset][$subset]) && file_exists($mod['adminpanelcsets'][$cset][$subset])) include $mod['adminpanelcsets'][$cset][$subset];
		else echo getErrorMsg('pagenotfound', $cset);
			
	// OUTPUT ADMIN PANEL
	echo '		</div>
			</div>
			<script type="text/javascript" src="'.$jquery['auth']['admin']['main_ui'].'"></script>';


}
//// NOT LOGGED IN -> ERROR
else echo getErrorMsg('pagenotfound', 'adminpanel');

?>