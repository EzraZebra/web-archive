<div id="left"><ul><?php
//////////////////////////////
///// LEFT COLUMN MODULE /////
//////////////////////////////
	//// GET IMAGE INFO
		$result = mysqli_query($sqldb,
							"	SELECT left_id, url_mo, target, position, colour FROM leftcolumn
								ORDER BY position ASC")
			or die('Database error.');
		for($i=0; $row = mysqli_fetch_assoc($result); $i++) {
			$left_id[$i] = $row['left_id'];
			$url_mo[$i] = $row['url_mo'];
			$target[$i] = $row['target'];
			$position[$i] = $row['position'];
			$colour[$i] = $row['colour'];
			$used[$target[$i]] = false;
		}

		mysqli_free_result($result);

	//// PRESET VARS
		$totalcount = count($left_id);
		$last_drawn = null;
		$idstring = "";
		$posIndex = null;

	//// GENERATE LEFT IMAGE
		for($i=0; $i<10; $i++) { //10 images
			$drawn = $i;
			if($posIndex != null || $position[$drawn] == 999) { //no more positioned images -> generate random images
				if($posIndex == null) $posIndex = $drawn; //set index of first non-positioned image
				$drawn = mt_rand($posIndex, $totalcount-1); //random image index
				//generate random index until a valid one is found or no valid ones left
				for($j=0; ($j<($totalcount-$posIndex) && ($used[$target[$drawn]] || $colour[$drawn] == $colour[$last_drawn])); $j++)
					$drawn = mt_rand($posIndex, $totalcount-1);
			}

			//non-used image found
			if($used[$target[$drawn]] != true) {
				$used[$target[$drawn]] = true;

				// OUTPUT MOUSEOVER IMAGE
				if($i<7) {
					$path = $img['leftdir_php'].$url_mo[$drawn];
					$size = getimagesize($path);
					echo '	<li style="background: url(\''.$img['leftdir'].$url_mo[$drawn].'\'); height: '.$size[1].'px;">
								<a style="height: '.$size[1].'px;" href="'.$rootdoc.'toys/'.$target[$drawn].'/"></a>
							</li>';
				}

				$last_drawn = $drawn;
				if($i != 0) $idstring .= '_';
				$idstring .= $left_id[$drawn];
			} else break;
		}
?></ul></div>
<script type='text/javascript'>
	$(document).ready(function() {
		//set left bg image
		$("#left").css({"background": "url('<?php echo $mod['leftcolumn']['bg'].'?idstring='.$idstring; ?>') repeat-y"}).fadeTo(1000, 1);
	});
</script>