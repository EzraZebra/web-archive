<?php
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include_once $incl['db_connect'];
include_once $incl['func']['misc'];
include_once $incl['func']['login'];
include_once $plugins['php']['jbbcode'];

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj

//BBCODE
$parser = new JBBCode\Parser();
$parser->loadCodes(true);

// OUTPUT NEWS
	echo '	<div id="infopanel" class="widget">
				<div id="infopanelTitle" class="uibtnDownArrowBlue"><span></span>News</div>';
	if($userValidation->canPostNews())
		echo '	<div id="infopanelAdd" class="uibtnAdd"><span></span>New</div>';
	echo '		<div id="infopanelcontent" class="hidden">
					<div id="news">
						<div id="newsitemswrapper">';

	//GET NEWSPANEL LENGTH
		$result = mysqli_query($sqldb, "SELECT	newspanel_length FROM global_settings") 
						or die('Database error.');

		$row = mysqli_fetch_assoc($result);
		mysqli_free_result($result);

		$newspanel_length = $row['newspanel_length'];

	//GET ITEM INFO
		$result = mysqli_query($sqldb,
							"	SELECT	i.item_id, i.post_date, i.title, i.user_id
								FROM categories_items ci, categories c, items i
								WHERE	c.cat_url = 'home' AND
										ci.cat_id = c.cat_id AND
										i.item_id = ci.item_id
								ORDER 	BY post_date DESC") 
						or die('Database error.');			

	//OUTPUT ITEMS
		for($i=0; ($i<$newspanel_length && $row = mysqli_fetch_assoc($result)); $i++) {
			//SET ITEM INFO
			$date = $row['post_date'];
			$item_id = enc_item_id($row['item_id']);
			$title_raw = secure_get($row['title'], false);
			if(strlen($title_raw) > 30) $title_view = substr($title_raw, 0, 27).'...';
				else $title_view = $title_raw;
			$url = $main_dir['home'].$item_id.'/'.urlencodeslashes($title_raw).'/';

			//NOT ALLOWED TO VIEW
			if(date('Y', $date) < date('Y', time()) || $date > time() && !$userValidation->canEdit($row['user_id']) && !$userValidation->canPostNews()) $i--;
			//ALLOWED TO VIEW -> OUTPUT
			else echo '		<div class="newsitem"><a href="'.$url.'" title="'.$title_raw.'">
								<span class="date">'.date('d/m', $date).' </span>'.$title_view.'<br />
							</a></div>';
		}
		mysqli_free_result($result);
		
		if($i == 0) echo '	<div class="newsitem"><a><span class="rightcolumntxt">There are no recent updates.</span></a></div>';
	
		echo '			</div>
					</div>';

		$activity = $userValidation->getActivity();
		
		echo '		<div id="infopanel_stats"><ul>
						<li><b>Users online</b>: '.$activity['users'].'</li>
						<li><b>Guests online</b>: '.$activity['guests'].'</li>
					</ul></div>
				</div>
			</div>
			<script type="text/javascript" src="'.$jquery['widgets']['infopanel_ui'].'"></script>';
?>