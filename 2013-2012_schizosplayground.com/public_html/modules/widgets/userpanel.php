<?php
////////////////////////////
///// USERPANEL MODULE /////
////////////////////////////
//// INCLUDES
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include_once $mod['auth']['common_ui'];

//// GET TOGGLE INFO
	if(isset($_SESSION['userpanel'])) $toggle = secure_get($_SESSION['userpanel'], true);
		
	if(isset($toggle) && $toggle == 'shown') $toggleBtn = 'Up';
		else {
			$toggle = 'hidden';
			$toggleBtn = 'Down';
		}

if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj		
		
//// LOGGED IN -> SHOW USERPANEL
if($userValidation->loggedIn) {
	//SET PANEL HEADER
	$panel['header'] = '	<ul>
								<li class="uibtn'.$toggleBtn.'ArrowBlue" id="userpanelToggleBtn" title="Toggle userpanel"><span></span></li>
								<li>
									<a href="'.$main_dir['members'].$userValidation->username.'/">'.$userValidation->username.'</a>
								</li>
								<li><a id="logout">Log out</a></li>
							</ul>';

							
	//BANNED
		if($userValidation->isBanned())
			$panel['body'] = '<span class="uibtnDelete userpanelitemInfo"><span></span>Banned</span>';
	//NOT ACTIVE -> SHOW ACTIVATION MSG
		elseif(!$userValidation->isActive())
			$panel['body'] = '<span class="uibtnRightArrow userpanelitem"><span></span><a href="'.$rootdoc.'activation/" class="DiagLnk activation">Activation</a></span>';
	//ADMIN
		elseif($userValidation->isAdmin())
			$panel['body'] = '<span class="uibtnRightArrow userpanelitem"><span></span><a href="'.$main_dir['adminpanel'].'">Admin Panel</a></span>';
	//NONE
		else $panel['body'] = null;
}
//// NOT LOGGED IN -> SHOW LOGIN/REGISTER
else {
	//SET PANEL HEADER
	$panel['header'] = '	<ul>
								<li><a href="'.$rootdoc.'login/" class="DiagLnk login">Log in</a></li>
								<li><a href="'.$rootdoc.'register/" class="DiagLnk register">Register</a></li>
							</ul>';
							
	//NO USERPANEL
	$panel['body'] = null;
}

//// OUTPUT USERPANEL
echo '	<div id="userpanel" class="widget">
			<div id="userpanelheader">'.$panel['header'].'</div>
			<div id="userpanelmain" class="'.$toggle.'">'.$panel['body'].'</div>
		</div>
		<script type="text/javascript" src="'.$jquery['widgets']['userpanel_ui'].'"></script>';
?>