<?php
//// INCLUDES
include_once $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include_once $incl['db_connect'];
include_once $incl['func']['misc'];
include_once $incl['func']['login'];
include_once $plugins['php']['jbbcode'];
	
$title_view = null;
$text_view = getErrorMsg('missingdata', 'news');
$editTitle = null;
$editUI = null;
$editContent = null;
$user = null;

//// ID PRESENT
if(isset($_GET['id'])) {
	//// PREPARE VARS
		if(!isset($userValidation)) $userValidation = new UserValidation(); //userval obj
		
		//BBCODE
		$parser = new JBBCode\Parser();
		$parser->loadCodes();
		
		$item_id = $_GET['id'];
		$newError = false;
		if($item_id == 'new') $newEdit = true;
			else {
				$item_id = secure_sql($item_id, true);
				$newEdit = false;
			}

	//// GET ITEM INFO
	if(!$newEdit) {
		$result = mysqli_query($sqldb,
							"	SELECT i.title, i.text, i.user_id, i.post_date, u.user_name FROM items i, users u
								WHERE	CRC32(i.item_id) = '$item_id' AND
										i.user_id = u.user_id") 
			or die('Database error.');   

		$row = mysqli_fetch_assoc($result);
		$notFound = false;

		//// ITEM FOUND & CAN VIEW -> SET ITEM VARS
		if(		mysqli_num_rows($result) == 1
			&&	($row['post_date'] <= time() || $userValidation->canEdit($row['user_id']) || $userValidation->canPostNews())) {
			$title_raw = $row['title'];
			$title_view = nl2br(secure_get(wordwrap($title_raw, 25, "\n", true), false));
			$title_raw = secure_get($row['title'], false);
			$text_raw = secure_get($row['text'], false);
			
			$parser->parse($text_raw); //parse bbcode
			$text_view = nl2br($parser->getAsHtml());
			
			$userid = $row['user_id'];
			$username = $row['user_name'];
			$user = '<span id="user"><a href="'.$main_dir['members'].$username.'/">&#126;'.$username.'</a></span>';
						
			$editTitle = "<input id='itemtitleinput' size='40' maxlength='100' value='".$title_raw."' />";
			$editContent = '<textarea id="contentedit">'.$text_raw.'</textarea>';
		} else {
			$title_view = 'Not Found';
			$text_view = getErrorMsg('missingdata', $item_id);
			$notFound = true;
		}
	
		mysqli_free_result($result);
	}
	
	//// CAN POST OR CAN EDIT -> SET EDIT UI
	if($userValidation->canPostNews() || (isset($userid) && $userValidation->canEdit($userid))) {
		$editUI = '';
		
		if($userValidation->canPostNews())
			$editUI .= '	<span id="itemnewspan" class="uibtnAdd"><span></span>New</span>';
		if(isset($userid) && $userValidation->canEdit($userid) && !$notFound)
			$editUI .= '	<span id="itemeditspan" class="uibtnEdit"><span></span>Edit</span>
							<span id="itemdeletespan" class="uibtnDelete"><span></span>Delete</span>';
		
		$editUI .= '		<span id="itemsavespan" class="uibtnSave"><span></span>Save</span>
							<span id="itemcancelspan" class="uibtnCancel"><span></span>Cancel</span>';				
		if($newEdit) {
			$editTitle = "<input id='itemtitleinput' size='40' maxlength='100' />";
			$editContent = '<textarea id="contentedit"></textarea>';
		}
	} elseif($newEdit) {
		$newError = true;
		$title_view = 'No Permission';
		$text_view = 'Sorry, you don\'t have permission to post news items.';
	}
}
//// ID MISSING
else {
	$title_view = 'Not Found';
	$text_view = getErrorMsg('missingdata', 'news');
}	

//// OUTPUT HOME POPUP UI
	echo '				<div id="itemtitlepopup">';
	if(!$newEdit || $newError) echo '	<span id="itemtitlespan">'.$title_view.'</span>';
	echo 					$editTitle.
							$editUI.'
						</div>
						<div id="contentpopup">';
	if(!$newEdit || $newError) echo '	<div id="contenttxt">'.$text_view.'</div>';
	echo 					$editContent.'<br />'.
							$user.'
						</div>
						<script type="text/javascript" src="'.$jquery['auth']['itemEdit_ui'].'"></script>';
?>