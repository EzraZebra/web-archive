<?php
////////////////////////////
///// USERPANEL MODULE /////
////////////////////////////
//// INCLUDES
include $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
include $incl['db_connect'];
include $incl['func']['misc'];
include $incl['func']['login'];

//// GET PAGE INFO
	if(isset($_GET['p'])) $page = strtolower(secure_sql($_GET["p"], false)); //get page
		else $page = '_nopage';
	if(isset($_GET['id'])) $id = secure_get($_GET["id"], true);
		else $id = '_noid';
		
	if(!isset($userValidation)) $userValidation = new UserValidation(); //user validation obj

	if($id == 'new' && ($page != 'home' || $userValidation->canPostNews()) && !$userValidation->isRestricted()) $postItem = true;
		else $postItem = false;

	$popuptitle = ucwords($page); //set popup title
	
	//category -> set vars
		$result = mysqli_query($sqldb,
							"	SELECT	cat_name, cat_id FROM categories
								WHERE 	cat_url = '$page'")
			or die('Database error.');

		$is_category = false;
		if(mysqli_num_rows($result) != 0) {
			$row = mysqli_fetch_assoc($result);
			$popuptitle = $row['cat_name']; //popup title
			$is_category = true; //category?
		}
		mysqli_free_result($result);		


	//// GET TOGGLE INFO
		if($is_category && isset($_SESSION['popupheader'])) $toggle = secure_get($_SESSION['popupheader'], true);

		if(isset($toggle) && $toggle == 'hidden') {
			$toggleTitle = 'Show header';
			$toggleBtn = 'Down';
		}
			else {
				$toggle = 'shown';
				$toggleTitle = 'Hide header';
				$toggleBtn = 'Up';
			}		

//// OUTPUT POPUP UI
	//INCLUDE BBCODE & EDIT JQUERY IF CATEGORY
	if($is_category)
		echo '<script type="text/javascript" src="'.$jquery['auth']['itemEdit'].'"></script>';

	// OUTPUT POPUP UI
	echo '	<div id="closebtnpopup"></div>
			<div id="bgpopup" class="'.$page.'"><div id="inhoudpopup">
				<div id="headerpopup">
					<div id="titlepopup" class="'.$toggle.'">'.$popuptitle.'</div>';
	
	// CATEGORY -> SHOW PAGES/DATE EDIT
	if($is_category) {
		// NO ITEM EDIT -> SHOW PAGES
		if(!$postItem) {
			//GET ITEM INFO
				$itemsresult = mysqli_query($sqldb,
											"	SELECT	i.item_id, i.post_date, i.title, i.user_id FROM categories_items ci, categories c, items i
												WHERE	c.cat_url = '$page' AND
														ci.cat_id = c.cat_id AND
														i.item_id = ci.item_id
												ORDER 	BY i.post_date DESC")
					or die('Database error.');   

				$cur_index = -1;
				
				for($i=0; $row = mysqli_fetch_assoc($itemsresult); $i++) {
					//CANT VIEW ITEM
					if($page == 'home' && $row['post_date'] > time() && !$userValidation->canEdit($row['user_id']) && !$userValidation->canPostNews())
						$i--;
					//CAN VIEW ITEM
					else {
						$item_id[$i] = enc_item_id($row['item_id']);
						$timestamp[$i] = $row['post_date'];
						$date[$i] = date('d/m/Y', $timestamp[$i]);
						$title[$i] = $row['title'];
						$url[$i] = $main_dir[$page].$item_id[$i].'/'.urlencodeslashes($title[$i]).'/';
						$title[$i] = secure_get($row['title'], false);
						$userid[$i] = $row['user_id'];
						
						if($item_id[$i] == $id) $cur_index = $i;
						$year = date('Y', $timestamp[$i]);
						if(!isset($minyear) || $year < $minyear) $minyear = $year;
						if(!isset($maxyear) || $year > $maxyear) $maxyear = $year;
					}
				}
					
				mysqli_free_result($itemsresult);
			//SHOW DROPDOWN
				if(isset($item_id)) $noItems = count($item_id); //number of items
					else $noItems = 0;

				if(($noItems == 1 && $cur_index != -1) || $noItems == 0) $disablePages = 'disabled="disabled"'; //disable dropdown
					else $disablePages = null;

				//set page buttons
					$prev = null; $next = null;
					if($cur_index < 1) $next = " disabled";
					if($cur_index == $noItems-1) $prev = " disabled";

				//output pages
				echo '	<div id="pagespopup">
							<span id="prevpopup" class="pagesBtnPopup uibtnLeftArrow'.$prev.'"><span></span></span>
							<select id="selectpopup" '.$disablePages.'>';

				//output NOT FOUND
				if($cur_index == -1)
					echo '		<option id="notfound" selected="selected">Not Found</option>';
					
				//output every item
				for($i=0; $i<$noItems; $i++) {
					$selected = null;
					if($cur_index == $i) $selected = 'selected="selected"'; //set selected
					echo '		<option value="'.$url[$i].'" '.$selected.' title="'.$title[$i].'">'.
									$date[$i].' '.$title[$i].'
								</option>';
				}
				
				//output
				echo '		</select>
							<span id="nextpopup" class="pagesBtnPopup uibtnRightArrow'.$next.'"><span></span></span>
						</div>';
		}

		// CLEARANCE >=2 -> SHOW ITEM EDIT
		if($postItem || ($cur_index > -1 && $userValidation->canEdit($userid[$cur_index])) || ($page == 'home' && $userValidation->canPostNews())) {
			//set date values
				if(isset($cur_index) && isset($timestamp)) {
					if($cur_index < 0) $cur_index = 0;
					$day = date('j', $timestamp[$cur_index]);
					$month = date('n', $timestamp[$cur_index]);
					$year = date('Y', $timestamp[$cur_index]);
				} else {
					$day = date("j");
					$month = date("n");
					$year = date("Y");
				}
			
			//set number of days
				if(($month<8 && $month%2!=0) || $month>7 && $month%2==0) $days = 31; //31 days
					elseif($month == 2) { //february
						if($year%4 == 0 && $year%100 == 0 && $year%400 == 0) $days = 29; //leap year
							else $days = 28; //no leap year
					} else $days = 30; //30 days
			
			//output days
				echo '		<select id="itemDateDay" class="itemDate">';
				
				//visible
				for($i=1; $i<=$days; $i++) {
					if($i == $day) $selected = 'selected="selected"';
						else $selected = null;
					
					echo '		<option '.$selected.'>'.$i.'</option>';
				}
				
				//hidden
				for($i=$days+1; $i<=31; $i++)
					echo '		<option class="hiddenoption">'.$i.'</option>';
				
				echo '		</select>
						<select id="itemDateMonth" class="itemDate">';	
					
			//output months
				for($i=1; $i<13; $i++) {
					if($i == $month) $selected = 'selected="selected"';
						else $selected = null;
					
					 echo '	<option '.$selected.'>'.$i.'</option>';
				}
				
				echo '	</select>
						<select id="itemDateYear" class="itemDate">';
			
			//output years		
				//set year range
				$cur_year = date("Y");
				if(!isset($minyear)) $minyear = $cur_year;
				if(!isset($maxyear) || $maxyear < $cur_year) $maxyear = $cur_year;
				
				for($i=$minyear-1; $i<=$maxyear+1; $i++) {
					if($i == $year) $selected = 'selected="selected"';
						else $selected = null;
					
					 echo '	<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
				}
				
				echo '	</select>';
		}

		// SHOW HEADER TOGGLE
			echo '		<div id="toggleBtnPopup" class="uibtn'.$toggleBtn.'Arrow" title="'.$toggleTitle.'">
							<span class="toggleBtnIcon"></span>
							<span class="toggleBtnIcon"></span>
						</div>';
	}

	echo '			</div>
				<div id="mainpopup">';
	// OUTPUT PAGE POPUP UI
	switch($page) {
		case "home":		include $mod['popups']['home']; //home
			break;
		case "login":		include $mod['auth']['login_ui']; //login
			break;
		case "register":	include $mod['auth']['register_ui']; //register
			break;
		case "activation":	include $mod['auth']['activation_ui']; //activation
			break;
		case "reset":		include $mod['auth']['reset_ui']; //reset
			break;
		default:			echo getErrorMsg('itemnotfound', $page); //error
	}

	echo '		</div>
			</div></div>
			<script type="text/javascript" src="'.$jquery['popups']['main_ui'].'"></script>';