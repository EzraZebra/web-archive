<?php
header('Content-Type: text/html; charset=utf-8');

include './includes/var_dir.php';
include $incl['db_connect'];
include $incl['func']['misc'];
include $incl['func']['login'];

//// START USER VALIDATION
if(!isset($userValidation)) $userValidation = new UserValidation();

//// SET VARIABLES ////
	//item id
	if(isset($_GET['id'])) $id = secure_get($_GET['id'], true);
		else $id = 'noid';		
	//item title
	if(isset($_GET['t'])) {
		$itemtitle = urlencode($_GET['t']);
		$itemtitle = str_replace("+", "%2B",$itemtitle);
		$itemtitle = urldecode($itemtitle);
	} else $itemtitle = '_notitle';
	//page
	if(isset($_GET['p'])) $page = strtolower(secure_get($_GET["p"], true));
		else $page = "home";
			
	// page title
		if($page == "home") $titlepage = "News";
			else $titlepage = ucfirst($page);
			
		if($itemtitle != "_notitle") $title = secure_get($itemtitle, false).$title_info['insert'].$titlepage.$title_info['def_title_suf'];
			else {
				$title = $title_info['def_title'];
				if($page != "home") $title = $titlepage.$title_info['insert'].$title;
			}

	// auth variables
		$popuppage = $page;
		if($page == "login" || $page == "register" || $page == "activation"  || $page == "reset") {
			if($id == 'noid') $id = '_noid';
			$page = "home";
		}
		
		if($popuppage == "reset" && isset($_GET['user'])) $resetUser = secure_get($_GET['user'], true);
			else $resetUser = '_nouser';
//// END SET VARIABLES ////
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $title; ?></title>
	<script type="text/javascript" src="<?php echo $plugins['jquery']['main']; ?>"></script>
	<script type="text/javascript" src="<?php echo $plugins['jquery']['history']; ?>"></script>
	<script type="text/javascript" src="<?php echo $plugins['jquery']['markitup'].'jquery.markitup.js'; ?>"></script>
	<script type="text/javascript" src="<?php echo $plugins['jquery']['markitup'].'sets/bbcode/set.js'; ?>"></script>
	<link href="<?php echo $css['main']; ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo $css['popup']; ?>" rel="stylesheet" type="text/css" />
	<?php	if($page == "home")
				echo "<link href='".$css['home']."' rel='stylesheet' type='text/css' />";
			elseif($page == "adminpanel")
				echo "<link href='".$css['adminpanel']."' rel='stylesheet' type='text/css' />";
	?>
	<link href="<?php echo $plugins['jquery']['markitup'].'skins/simple/style.css'; ?>" rel="stylesheet" type="text/css" />
	<link href="<?php echo $plugins['jquery']['markitup'].'sets/bbcode/style.css'; ?>" rel="stylesheet" type="text/css" />
	<link rel="icon" href="<?php echo $main_dir['favicon']; ?>?v=6" type="image/x-icon" />
	<link rel="shortcut icon" href="<?php echo $main_dir['favicon']; ?>?v=6" type="image/x-icon" />
</head>
<body>
	<div id="bg">
		<div id="userbar"><?php
			//// OUTPUT USERPANEL
			include $mod['widget']['userpanel_cont'];
		?></div>
		<?php include $mod['leftcolumn']['load']; ?>
		<div id="infocontainer">
			<div id="navb">
				<div id="forum"><a href="<?php echo $main_dir['forum']; ?>"><span>FORUM</span></a></div>
				<div id="pinboard"><a href="<?php echo $main_dir['pinboard']; ?>"><span>PINBOARD</span></a></div>
				<div id="channels"><div id="channelsborder"><div id="channelsbordertop"></div></div><a href="<?php echo $main_dir['channels']; ?>"><span>CHANNELS</span></a></div>
			</div>
			<?php include $mod['rightcolumn']; ?>
		</div>
		<div id="header"><a href="<?php echo $main_dir['home']; ?>"></a></div>
		<div id="inhoud"><?php 
			//// OUTPUT PAGE
			if(isset($mod[$page]) && file_exists($mod[$page])) include $mod[$page];
				else echo getErrorMsg('pagenotfound', $page);

			include_once $mod['ads'];
			outputAds($rootdoc, $img['main']['adfolder'], $page, 'bottombanner');
		?></div>
	</div>
	<div id="overlaybg"></div><div id="overlaywrapper"><div id="overlay"></div></div>
	<script type="text/javascript" src="<?php echo $jquery['main']; ?>"></script>
	<script type="text/javascript" src="<?php echo $jquery['popups']['main']; ?>"></script>
	<script type="text/javascript" src="<?php echo $jquery['auth']['main']; ?>"></script>
	<?php include $jquery['loadState']; ?>
</body>
</html>