<script type='text/javascript'>
	$(document).ready(function() {
		//GET STATE
		var state = {
				page: '<?php if($popuppage == 'adminpanel' && isset($_GET['cset'])) echo $popuppage.'/'.secure_get($_GET['cset'], true);
								else echo $popuppage; ?>',
				id: '<?php echo $id; ?>',
				ref: 'ext',
				title: '<?php echo addslashes($itemtitle); ?>'
			}, user = '<?php echo $resetUser; ?>';

		if(user != '_nouser') state.user = user;

		//NO ID -> FROM HASH
		if(state.id == 'noid') {
			var hash = window.location.hash;
			
			if(typeof hash !== 'undefined' && hash != '') {
				hash = popup.getStateFrom(hash, true);		
				state.id = hash.id;
				state.title = hash.title;
			}
		}

		//ID PRESENT -> LAUNCH POPUP
		if(state.id != 'noid') {
			var history = History.getState().data; //get history state
			if(typeof state.user === 'undefined' && typeof history.user !== 'undefined') state.user = history.user;

			if	(	state.page != popup.state.page || state.id != popup.state.id
				||	(typeof state.user !== 'undefined' && (typeof popup.state.user === 'undefined' || state.user != popup.state.user))
				) popup.newState(state); //different from current -> new state
				else popup.triggerState(); //else trigger current state
		}
	});
</script>