////////////////////////
///// MAIN JQUERY //////
////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../includes/var_dir.php';
?>

//////////////////////
//// BEGIN MAIN CLASS
//////////////////////
	var main = new function() {
		this.urlencodeslashes = function(url) { //internal only (ajax post send)
			return encodeURIComponent(url).replace(/\%2F/g, "+");
		};
		
		this.urldecodeslashes = function(url) { //external (href)
			return decodeURIComponent(url);
		};
		
		this.urlreplacemodifiers = function(url) {
			return url.replace(/\//g, "+").replace(/\\/g, "+").replace(/\&/g, "+").replace(/\#/g, "+").replace(/\%/g, "+").replace(/\?/g, "");
		};

		//LAUNCH DIALOGS
		this.launchDiag = function(event) {
			event.preventDefault();

			var module = $(this).attr('class').substr(8);
			popup.newState({ page: module, id: '_noid', ref: 'loc' });
		};
		
		//UPDATE ITEM LIST
		this.updateList = function(data, action, callback) {
			//DATA PRESENT -> CONTINUE
			if(typeof data.module !== 'undefined' && typeof data.formInfo !== 'undefined') {
				if(typeof data.data !== 'undefined') data.data = data.data+"/";
					else data.data = "";

				$.ajax({ //load content
					url: "<?php echo $main_dir['adminpanel'].'"+data.module+"/upd/'; ?>"+data.data,
					cache: false
				}).always(function(response) {
					//VALID RESPONSE -> CONTINUE
					if(typeof response === 'string') {
						response = $(response); //create jquery object

						//ADD
						if(action == 'add') {
							data.formInfo.children('tbody').prepend(response).children().eq(0).hide().fadeIn(400, function() {
								if($(this).siblings().length === 1)	$(this).siblings().fadeOut(400, function() {
																		$(this).addClass('noError');
																	});
							});
						}
						//EDIT
						else if(action == 'edit') 
							//hide row
							data.formInfo.before(response.addClass('editItem')).fadeOut(itemEditAuth.fadeSpeed*2, function() {
								//add updated row
								$(this).siblings('.editItem').hide().fadeIn(400).removeClass('editItem');
								//remove row
								$(this).remove();
							});

						if(typeof callback === 'function') callback();
					}
					//INVALID RESPONSE
					else alert('Error updating list.');
				});
			}
			//DATA MISSING
			else alert('Error updating list.');
		};
		
		//IS INT //https://raw.github.com/kvz/phpjs/master/functions/network/ip2long.js
		this.isInt = function(n) {
			if(parseFloat(n) == parseInt(n) && !isNaN(n)) return true;
				else return false;
		};
		
		this.ip2long = function(IP) {
			// http://kevin.vanzonneveld.net
			// +   original by: Waldo Malqui Silva
			// +   improved by: Victor
			// +    revised by: fearphage (http://http/my.opera.com/fearphage/)
			// +    revised by: Theriault
			// *     example 1: ip2long('192.0.34.166');
  			// *     returns 1: 3221234342
			// *     example 2: ip2long('0.0xABCDEF');
			// *     returns 2: 11259375
			// *     example 3: ip2long('255.255.255.256');
			// *     returns 3: false

			var i = 0;

			// PHP allows decimal, octal, and hexadecimal IP components.
			// PHP allows between 1 (e.g. 127) to 4 (e.g 127.0.0.1) components.
			IP = IP.match(/^([1-9]\d*|0[0-7]*|0x[\da-f]+)(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?(?:\.([1-9]\d*|0[0-7]*|0x[\da-f]+))?$/i); // Verify IP format.

			// Invalid format.
			if(!IP) return false;

			// Reuse IP variable for component counter.
			IP[0] = 0;
			for (i=1; i<5; i+=1) {
				IP[0] += !! ((IP[i] || '').length);
				IP[i] = parseInt(IP[i]) || 0;
			}

			// Continue to use IP for overflow values.
			// PHP does not allow any component to overflow.
			IP.push(256, 256, 256, 256);

			// Recalculate overflow of last component supplied to make up for missing components.
			IP[4 + IP[0]] *= Math.pow(256, 4 - IP[0]);
			if (IP[1] >= IP[5] || IP[2] >= IP[6] || IP[3] >= IP[7] || IP[4] >= IP[8]) return false;

			return IP[1] * (IP[0] === 1 || 16777216) + IP[2] * (IP[0] <= 2 || 65536) + IP[3] * (IP[0] <= 3 || 256) + IP[4] * 1;
		};

	}

$(document).ready(function() {
//// PROTOTYPE FUNCTIONS
	//CAPITALIZE FIRST CHAR OF STRING
	String.prototype.capitalize = function() {
		return this.charAt(0).toUpperCase()+this.slice(1);
	}

	//LOAD CONTENT INTO JQUERY OBJECT
	jQuery.prototype.loadContent = function(container, content, callback) {
		var wrapper = this,
			fadeSpeed = 200;
		
		container.append('<div class="loading"><div>Loading. . .</div></div>'); //show loading div in container
		wrapper.fadeTo(fadeSpeed, 0.1, function() { //hide wrapper
			$.ajax({ //load content
				url: content,
				cache: false
			}).always(function(response) {
				wrapper.html(response).ready(function() { //insert content into wrapper
					$(".loading").fadeTo(fadeSpeed*2, 0, function() { $(this).remove(); }); //hide loading div
					wrapper.fadeTo(fadeSpeed, 1); //show wrapper
					
					//CALLBACK
					if(typeof callback === "function") callback(); //fire callback
				});
			});
		});
	}
	
	//CHECK DATE DAY INPUT FOR LEAP YEAR
	jQuery.prototype.dateInputCheck = function(day, month, year) {
		var days = 0;
			
		//february
		if(month == 2) {
			if(year%4 == 0 || year%100 == 0 || year%400 == 0) days = 29; //leap year
				else days = 28; //no leap year
		//31 day month
		} else if((month<8 && month%2!=0) || (month>7 && month%2 == 0)) days = 31;
		//30 day month
		else if((month<8 && month%2==0) || (month>7 && month%2 != 0)) days = 30;

		if(days != 0) {
			for(var i=28; i<days; i++) this.find("option").eq(i).removeClass("hiddenoption"); //show new range
			for(var i=days; i<31; i++) this.find("option").eq(i).addClass("hiddenoption"); //hide out of range
			if(day > days) this.prop("selectedIndex", days-1); //select last day if out of range
		}
	}

//// BINDS
	//LEFT HOVER IMAGES
	$("#left li").hover(
		function() { //mouse over
			$(this).stop(); //stop all animations
			$(this).fadeTo(500, 1); //show image
		},
		function() { //mouse off
			$(this).stop(); //stop all animations
			$(this).fadeTo(500, 0.1); //hide image
		}
	);
});