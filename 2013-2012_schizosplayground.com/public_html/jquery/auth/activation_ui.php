///////////////////////////////////////////////
///// ACTIVATION AUTHORISATION UI JQUERY //////
///////////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../includes/var_dir.php';
?>

var beginActivationUI = function() {
	$(document).ready(function() {
		//// SET FORM OBJECTS
		var container = $("#activationpopup"), //container
			form = container.find('#activateform'); //activate form

		activateAuth.formInfo =	{	activationError: form.find('.activationerror'),
									activationCode: form.find('#activation_code'),
									formBtn: form.find('#form_btn'),
									msgPopup: container.find(".msgpopup"),
									errorPopup: container.find("#errorpopup"),
									successPopup: container.find("#successpopup")
								};
						
		//// FOCUS INPUT
			if(form.find("#login_user").length === 0) $("#activation_code").select();
				else form.find("#login_user").select();
				
		//// ACTIVATE ON LOAD
			if(form.find('#login_form').length === 0 && auth.checkInfo('code', $("#activation_code").val())) { //if login form not present & code valid
				var forminfo = { //set form info
					activation: $(this).find("#activation_code").val(),
					form: $(this).find("#activation_form").val()
				};
				
				activateAuth.activate(forminfo); //activate
			}

		//////////////////////
		//// BEGIN EVENT HANDLERS
		//////////////////////
			//ACTIVATION CODE VALIDATION
			activateAuth.formInfo.activationCode.change(function() {
				activateAuth.validate();
			});
			
			//VALIDATE INPUT ON ANY CHANGE
			activateAuth.formInfo.activationCode.bind('input', function(){ 
				$(this).change();
			});

			//ACTIVATE SUBMIT
			form.submit(function() {
				var forminfo = { //set forminfo
					activation: activateAuth.formInfo.activationCode.val(),
					form: $(this).find("#activation_form").val()
				};
				
				//login form present -> include in forminfo
				if($(this).find("#login_form").length != 0) {
					forminfo.username = $(this).find("#login_user").val();
					forminfo.pwd = $(this).find("#login_pwd").val();
					forminfo.login_form = $(this).find("#login_form").val();
				}
					
				//activate
				activateAuth.activate(forminfo);
				
				return false;
			});
			
			//CLOSE BTN CLICKED
			container.find("[title='Close activation window']").click(function() {
				popup.closeState(); //close popup
			});
			
			//SEND MAIL CLICKED
			container.find("#resendActivation").click(function() {
				activateAuth.sendMail(); //send activation mail
			});
		//////////////////////
		//// END EVENT HANDLERS
		//////////////////////	
	});
};

if(!window.activateAuth) $.getScript("<?php echo $jquery['auth']['activation'] ?>", beginActivationUI);
	else beginActivationUI();