///////////////////////////////////////////
///// ITEM EDIT AUTHORISATION JQUERY //////
///////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../includes/var_dir.php';
?>

///////////////////////////
//// BEGIN ITEM EDIT CLASS
///////////////////////////
var itemEditAuth = new function() {
	this.fadeSpeed = 200;
	this.editing = false;

	// EDIT CLICKED
		this.editClicked = function() {
			//hide edit, delete, new
				itemEditAuth.formInfo.editBtn.fadeOut(itemEditAuth.fadeSpeed);
				itemEditAuth.formInfo.deleteBtn.fadeOut(itemEditAuth.fadeSpeed);
				itemEditAuth.formInfo.newBtn.fadeOut(itemEditAuth.fadeSpeed);
			//show cancel, save
				itemEditAuth.formInfo.cancelBtn.fadeIn(itemEditAuth.fadeSpeed);
				itemEditAuth.formInfo.saveBtn.fadeIn(itemEditAuth.fadeSpeed);
			
			//hide pages
			itemEditAuth.formInfo.itemDate.fadeOut(itemEditAuth.fadeSpeed, function() {
				itemEditAuth.formInfo.itemDateEdit.fadeIn(itemEditAuth.fadeSpeed); //show date select
			});
			
			//hide title
			itemEditAuth.formInfo.itemTitle.fadeOut(itemEditAuth.fadeSpeed, function() {
				itemEditAuth.formInfo.itemTitleEdit.fadeIn(itemEditAuth.fadeSpeed); //show title input
			});
			
			//hide content
			itemEditAuth.formInfo.itemContent.fadeOut(itemEditAuth.fadeSpeed, function() {
				itemEditAuth.formInfo.itemContentEdit.show(); //show editor
				itemEditAuth.formInfo.itemContentEditWrapper.fadeIn(itemEditAuth.fadeSpeed); //show editor wrapper
			});
			
			itemEditAuth.editing = true;
		};				
	
	// CANCEL CLICKED
		this.cancelClicked = function(cancelNew) {
			//CONFIRM
			if(confirm("Are you sure you want to discard your changes?")) {
				this.editing = false;
				popup.state.editItem = false;
				
				//EDIT CANCEL
					if(!cancelNew) popup.triggerState({ ref: 'loc' });
				//NEW ITEM CANCEL
					else if(typeof popup.state.srcId !== 'undefined' && popup.state.srcId != '_noid') { //back to source item
						var state = {
								id: popup.state.srcId,
								ref: 'loc'
							};

						if(typeof popup.state.srcTitle !== 'undefined') state.title = popup.state.srcTitle;

						popup.newState(state);
					} else popup.closeState(); //no source item -> close
			} else return false;
		};
	
	// NEW CLICK
		this.newClicked = function() {			
			var	state = {
					id: 'new',
					title: '_notitle',
					ref: 'loc',
					srcId: popup.state.id
				};
			
			if(typeof popup.state.title !== 'undefined') state.srcTitle = popup.state.title;

			popup.newState(state);
		};
	
	// DELETE CLICKED
		this.deleteClicked = function() {
			//CONFIRM
			if(confirm("Are you sure you want to delete this item?")) {
				//SET DATA
				var itemSelect = itemEditAuth.formInfo.itemDate.find("#selectpopup"),
					id = popup.getStateFrom(itemSelect.find("option:selected").attr("value")).id,
					data = "delete=1&id="+id;
				
				//DELETE
				$.ajax({
					type: "POST",
					url: "<?php echo $mod['auth']['itemEdit']; ?>",
					data:  data
				}).always(function(error) {
					//SUCCESS
					if(error == 'success') {
						//set info
						var selected = itemSelect.prop("selectedIndex"),
							count = itemSelect.find("option").size();

						//all items deleted -> close
						if(count == 1) popup.closeState();
							else {
								//DELETE
								itemSelect.find("option").eq(selected).remove();
								
								//BROWSE
								if(selected == 0) itemSelect.prop("selectedIndex", 0).trigger('change');
									else itemSelect.prop("selectedIndex", selected-1).trigger('change');
							}
							
						//ADMIN PANEL -> UPDATE LIST
						if(popup.state.page == 'adminpanel/news') {
							$("#newslist").find(".admin_editNewsId[value="+id+"]").parents('tr').fadeOut(400, function() {
								if($(this).siblings().length === 1)
									$(this).siblings().fadeIn(400, function() { $(this).removeClass('noError'); });
								$(this).remove();
							});
						}
						//NOT ADMIN PANEL -> RELOAD INFOPANEL
						else auth.reloadWidgets($("#infopanel"));
					//ERROR
					} else alert(error);
				});
			} else return false;
		};
	
	// SAVE CLICKED
		this.saveClicked = function(saveNew) {
			//CONFIRM
			if(confirm("Are you sure you want to save your changes?")) {
				//SET DATA
					var title = itemEditAuth.formInfo.itemTitleEdit.val(),
						data = {
							day: itemEditAuth.formInfo.itemDateEdit.filter("#itemDateDay").val(),
							month: itemEditAuth.formInfo.itemDateEdit.filter("#itemDateMonth").val(),
							year: itemEditAuth.formInfo.itemDateEdit.filter("#itemDateYear").val(),
							title: main.urlencodeslashes(title),
							content: main.urlencodeslashes(itemEditAuth.formInfo.itemContentEdit.val())
						};

					//NEW ITEM SAVE
						if(saveNew) {
							data.saveNew = 1;
							if(popup.state.page == 'adminpanel/news') data.module = 'home';
								else data.module = popup.state.page;
						}
					//EDIT SAVE
						else {
							data.saveEdit = 1;
							data.id = popup.getStateFrom(itemEditAuth.formInfo.itemDate.find("option:selected").attr("value")).id;
							var dataId = data.id;
						}

				//SAVE
				$.ajax({
					type: "POST",
					url: "<?php echo $mod['auth']['itemEdit']; ?>",
					data:  data
				}).always(function(error) { 	
					//SUCCESS
						if(error.substr(0, 7) == 'success') {
							itemEditAuth.editing = false;
							
							//new item save
							if(saveNew) {
								var id = error.substr(7);
								popup.newState({ id: id, title: title, ref: 'loc' });
							}
							//edit save
							else {
								var id = dataId;
								popup.newState({ title: title, ref: 'ext' });
							}

							//ADMIN PANEL -> UPDATE LIST
							if(window.adminNewsAuth) {
								//set data
								var data = {	module: 'news',
												data: id
											};

								//new item save
								if(saveNew) {
									var action = 'add';
									data.formInfo = $("#newslist");
								}
								//edit save
								else {
									var action = 'edit';
									data.formInfo = $("#newslist").find(".admin_editNewsId[value="+id+"]").parents().eq(1);
								}
								
								//UPDATE LIST
								main.updateList(data, action, function() {
									//reload UI scripts
									$.getScript("<?php echo $jquery['auth']['admin']['main_ui'] ?>");
									$.getScript("<?php echo $jquery['auth']['admin']['news_ui'] ?>");
								});
							} else auth.reloadWidgets($("#infopanel")); //reload infopanel
						}
					//CLEARANCE ERROR
						else if(error == 'clearanceError') {
							itemEditAuth.editing = false;
							
							if(saveNew) {
								alert('Sorry, you don\'t have permission to post items here.');
								itemEditAuth.formInfo.cancelBtn.trigger('click');
							} else {
								alert('Sorry, you don\'t have permission to edit this item.');
								popup.triggerState();
							}

							auth.reloadWidgets();
						}
					//OTHER ERROR
						else alert(error);
				});
				
				return false;
			} else return false;
		};
}