////////////////////////////////////////////
///// ACTIVATION AUTHORISATION JQUERY //////
////////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../includes/var_dir.php';
?>

////////////////////////////
//// BEGIN ACTIVATION CLASS
////////////////////////////
	var activateAuth = new function() {			
		//// VALIDATE ACTIVATION
			this.validate = function() {
				this.formInfo.activationError.fadeOut(auth.fadeSpeed); //hide error msg
				
				if(auth.checkInfo('code', this.formInfo.activationCode.val())) this.formInfo.activationCode.validateInput('nostate'); //no error
					else this.formInfo.activationCode.validateInput('error'); //error
			};
		
		//// FILTER ACTIVATION CODE
			this.check = function(activation) {
				activation = activation.replace(/[^a-z0-9]/gi,'');
				if(typeof activation === 'undefined' || activation === '' || activation === 0) activation = '_noid';
				
				return activation;
			};
		
		//// SHOW ERROR MSG
			this.error = function() {
				this.formInfo.activationCode.focus();
				this.formInfo.activationCode.validateInput('error');
				this.formInfo.activationError.fadeIn(auth.fadeSpeed);
			};

		//// ACTIVATE
			this.activate = function(forminfo) {
				this.formInfo.formBtn.attr('disabled', 'disabled'); //disable form
				
				// LOGIN FORM PRESENT -> LOG IN
					if(typeof forminfo.login_form !== 'undefined' && forminfo.login_form == 1) {
						var logininfo = { //get login info
							username: forminfo.username,
							pwd: forminfo.pwd,
							form: forminfo.login_form,
							module: "#activate"
						};
						
						auth.logIn(logininfo, function(error) { // LOG IN
							var activation = activateAuth.check(forminfo.activation); //set activation code

							//SUCCESS OR CLEARANCE ERROR -> RELOAD DIALOG
								if(error == 'success' || error == 'clearanceError') popup.triggerState({ id: activation });
							//OTHER ERROR -> RE-ENABLE FORM
								else {
									activateAuth.formInfo.formBtn.removeAttr('disabled');
									activateAuth.validate();
								}
						});
					}
				// LOGIN FORM NOT PRESENT -> ACTIVATE
					else if(typeof forminfo.form !== 'undefined' && forminfo.form == 1) { // VALID FORM -> CONTINUE
						this.formInfo.msgPopup.fadeOut(auth.fadeSpeed).empty(); //hide error
						
						if(typeof captchaAuth !== 'undefined' && captchaAuth.exists()) var captchaExists = true;
							else var captchaExists = false;
						
						if(!captchaExists || captchaAuth.check()) { // CAPTCHA VALID OR NOT PRESENT -> CONTINUE
							if(typeof forminfo.activation !== 'undefined' && auth.checkInfo('code', forminfo.activation)) { // ACTIVATION CODE IS VALID -> CONTINUE
								var activation = forminfo.activation, data = "activate=1&activation="+activation; //set vars
								
								$.ajax({ //ACTIVATE
									type: "POST",
									url: "<?php echo $mod['auth']['activation']; ?>",
									data:  data
								}).always(function(error) {									
									// BEGIN ERROR HANDLING
										//SUCCESS
											if(error == 'success') {
												if(captchaExists) captchaAuth.disable(); //disable captcha
												auth.reloadWidgets(); //reload widgets

												popup.triggerState({page: 'activation'}); //success dialog
											}
										//ERRORS
											else {
												if(captchaExists) captchaAuth.change(); //change captcha							
												activateAuth.formInfo.msgPopup.fadeOut(auth.fadeSpeed).empty(); //hide error
												
											//INCORRECT ACTIVATION CODE
												if(error == 'activationError') activateAuth.error(); //show error
											//CLEARANCE ERROR
												else if(error == 'clearanceError') {
													auth.reloadWidgets(); //reload widgets
													popup.triggerState({ id: activation }); //reload dialog
												}
											//ALREADY ACTIVE
												else if(error == 'alreadyActive') popup.triggerState();
											//UNKNOWN ERROR
												else { //unknown error
													activateAuth.validate(); //validate activation form
													if(error != 'formError')
														activateAuth.formInfo.errorPopup.append('An unknown error occurred.<br />').fadeIn(auth.fadeSpeed); //show error msg
												}
													
												activateAuth.formInfo.formBtn.removeAttr('disabled'); //re-enable form
											}
									// END ERROR HANDLING
								});
							} else { //INVALID ACTIVATION CODE
								this.error(); //show error
								this.formInfo.formBtn.removeAttr('disabled'); //re-enable form
							}
						} else { //INVALID CAPTCHA CODE
							captchaAuth.error(); //show error
							this.formInfo.formBtn.removeAttr('disabled'); //re-enable form
						}
					} else window.location.pathname = "<?php echo $rootdoc.'activation/'; ?>"; //INVALID FORM -> REDIRECT
			};
			
			// SEND ACTIVATION EMAIL
			this.sendMail = function() {				
				// SEND MAIL
				$.ajax({
					type: "POST",
					url: "<?php echo $mod['auth']['activation']; ?>",
					data:  "sendAct=1"
				}).always(function(error) {	
					activateAuth.formInfo.msgPopup.fadeOut(auth.fadeSpeed).empty(); //hide error				
					
					//SUCCESS -> SHOW MSG
					if(error == 'success') activateAuth.formInfo.successPopup.append('Activation e-mail sent successfully.<br />').fadeIn(auth.fadeSpeed);
					//ALREADY ACTIVE OR CLEARANCE -> RELOAD
					else if(error == 'alreadyActive' || error == 'clearanceError') popup.triggerState();
					//OTHER ERRORS
					else {
						var errorMsg = ''; //preset error msg
						
						//FAILED TO SEND MAIL
						if(error == 'mailFailed') errorMsg += 'Failed to send activation e-mail.<br />';
						//USER NOT FOUND
						else if(error == 'userError') errorMsg += 'User not found.<br />';
						//UNKNOWN ERROR
						else errorMsg += 'An unknown error occurred. <br />';

						activateAuth.formInfo.errorPopup.append(errorMsg).fadeIn(auth.fadeSpeed); //show error msg
					}
				});
			};
	}
//////////////////////////
//// END ACTIVATION CLASS
//////////////////////////