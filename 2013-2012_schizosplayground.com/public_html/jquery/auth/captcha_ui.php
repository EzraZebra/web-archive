/////////////////////////////////////////
//// CAPTCHA AUTHORISATION UI JQUERY ////
/////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../includes/var_dir.php';
?>

var beginCaptchaUI = function() {
	$(document).ready(function() {
		//// SET FORM OBJECTS
		captchaAuth.formInfo =	{	code: $('#captcha_code'),
									captchaimg: $('#captcha'),
									error: $(".forminfo .captchaerror")
								};
		//// BINDS
			//change captcha image
				$("#changecaptcha").click(function() {
					captchaAuth.change();
				});

				captchaAuth.formInfo.captchaimg.click(function() {
					captchaAuth.change();
				});

			//validate input
			captchaAuth.formInfo.code.change(function() {
				captchaAuth.validate();
			});

			//validate input on any change
			captchaAuth.formInfo.code.bind('input', function(){
				$(this).change();
			});
	});
};

if(!window.captchaAuth) $.getScript("<?php echo $jquery['auth']['captcha'] ?>", beginCaptchaUI);
	else beginCaptchaUI();