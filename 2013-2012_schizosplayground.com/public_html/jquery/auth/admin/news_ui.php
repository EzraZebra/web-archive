////////////////////////////////////////////
///// ADMIN AUTHORISATION JQUERY ///////////
////////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../../includes/var_dir.php';
?>

var beginAdminNewsUI = function() {
	$(document).ready(function() {
	////////////////////////////
	//// BEGIN EVENT HANDLERS
	////////////////////////////
		//SETTINGS
			//newspanel length
			$("#admin_newsSettings").unbind('submit').submit(function() {
				var forminfo = $(this);
				adminNewsAuth.editSettings(forminfo);
				
				return false;
			});
		//TOOLS
			//view
			$("#newslist").find(".admin_newsTitle a").unbind('click').click(function(e) {
				e.stopPropagation();
				var id = $(this).parents().eq(1).find('.admin_editNewsId').val(),
					title = $(this).text();
					
				adminNewsAuth.viewItem(id, title);
			});

			//add
			$(".admintools").children("#adminNewsAdd").unbind('click').click(adminNewsAuth.addNew);

			//edit
			$("#newslist").find(".uibtnEdit").unbind('click').click(function() {
				var id = $(this).siblings('.admin_editNewsId').val(),
					title = $(this).parent().siblings('.admin_newsTitle').children('a').text();
				
				adminNewsAuth.editItem(id, title);
			});

			//delete
			$("#newslist").find(".uibtnDelete").unbind('click').click(function() {
				adminNewsAuth.deleteItem($(this));
			});
	////////////////////////////
	//// END EVENT HANDLERS
	////////////////////////////
	});
};

if(!window.adminNewsAuth) $.getScript("<?php echo $jquery['auth']['admin']['news'] ?>", beginAdminNewsUI);
	else beginAdminNewsUI();