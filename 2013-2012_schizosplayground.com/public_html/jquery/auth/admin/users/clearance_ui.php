/////////////////////////////////////////////
///// ADMIN CLEARANCE AUTHORISATION JQUERY //////
/////////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../../../includes/var_dir.php';
?>
	
var beginAdminClearanceUI = function() {
	$(document).ready(function() {
		//////////////////////
		//// BEGIN EVENT HANDLERS
		//////////////////////
			//TOOLS
				//save edit
				$("#clearancelist").find("tr:not(#newItem)").find(".uibtnSave:not(.checklist)").unbind('click').click(adminClearanceAuth.saveEdit);
				//save new
				$("#clearancelist").find("#newItem").find(".uibtnSave:not(.checklist)").unbind('click').click(adminClearanceAuth.saveNew);
				//delete
				$("#clearancelist").find(".uibtnDelete").unbind('click').click(adminClearanceAuth.deleteClearance);
				//edit permissions
				$("#clearancelist").find("tr:not(#newItem)").find(".checklist span").unbind('click').click(adminClearanceAuth.editPermissions);
				//edit new permissions
				$("#clearancelist").find("#newItem").find(".checklist span").unbind('click').click(adminClearanceAuth.addPermissions);
				//update permissions
				$("#clearancelist").find(".admin_addPermissions").unbind('change').change(function() {
					var special_perm = $(this).children('option:selected').text(),
						button = $(this).parent().siblings('.post_news').children('.checklist');
						
					adminClearanceAuth.updatePermissions(special_perm, button);
				});
			//////////////////////
			//// END EVENT HANDLERS
			//////////////////////
	});
};

if(!window.adminClearanceAuth) $.getScript("<?php echo $jquery['auth']['admin']['clearance'] ?>", beginAdminClearanceUI);
	else beginAdminClearanceUI();