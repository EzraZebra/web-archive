/////////////////////////////////////////////////
///// ADMIN BANLIST UI AUTHORISATION JQUERY /////
/////////////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../../../includes/var_dir.php';
?>
	
var beginAdminBanlistUI = function() {
	$(document).ready(function() {
	////////////////////////////
	//// BEGIN EVENT HANDLERS
	////////////////////////////
		//UNBAN
		$(".admin_unbanIp").unbind('click').click(function() {
			//SET DATA
			var data = {	ip: $(this).parents('tr').find('.admin_loginsIP').val(),
							ip_range: $(this).parents('tr').find('.admin_loginsIPRange').val(),
							ipUnban: 1
						},
				banlistGroupInput = $(this).siblings('.admin_banlistGroup'),
				banlistGroup = $(this).siblings('.admin_banlistGroup').val(),
				selectedRows = $('#banlist').find('.admin_banlistGroup[value='+banlistGroup+']').parents('tr');

			//UNBAN
			adminIPBanAuth.IPBan(data, function() {
				//HIDE SELECTED ROWS
				selectedRows.fadeOut(400, function() {
					if($(this).siblings().length === 1) $(this).siblings().fadeIn(400);
					$(this).remove();
				});

				//GROUP ITEM -> UPDATE LIST
				if(banlistGroupInput.hasClass('admin_banlistGroupItem')) {
					var listData =	{	module: 'banlist',
										formInfo: $('#banlist'),
										data: banlistGroup.replace('_', '/')
									},
						action = 'add';
					main.updateList(listData, action);
				}
			});
		});

		//BAN FILTER
		$("#admin_banIpFilter").unbind('click').click(function() {
			//SET DATA
				var ip = $(this).siblings('#admin_ipFilterInput').val(),
					ip_range = ip;

				//GET IP RANGE
				ip = adminLoginsAuth.splitIPRange(ip);
				ip_range = ip.ip_range;
				ip = ip.ip;

				var data = {	ip: main.ip2long(ip),
								ip_range: main.ip2long(ip_range),
								ipBan: 1
							};

			//BAN
			adminIPBanAuth.IPBan(data, function() {
				var listData  = {	module: 'banlist',
									formInfo: $("#banlist")
								},
					action = 'add';
				listData.formInfo.find('tbody tr').fadeOut(400, function() { $(this).remove(); });
				main.updateList(listData, action);
			});
		});

		//UNBAN FILTER
		$("#admin_unbanIpFilter").unbind('click').click(function() {
			//SET DATA
				var ip = $(this).siblings('#admin_ipFilterInput').val(),
					ip_range = ip;

				//GET IP RANGE
				ip = adminLoginsAuth.splitIPRange(ip);
				ip_range = ip.ip_range;
				ip = ip.ip;

				var data = {	ip: main.ip2long(ip),
								ip_range: main.ip2long(ip_range),
								ipUnban: 1
							};
						
			//UNBAN
			adminIPBanAuth.IPBan(data, function() {
				var listData  = {	module: 'banlist',
									formInfo: $("#banlist")
								},
					action = 'add';
				listData.formInfo.find('tbody tr').fadeOut(400, function() { $(this).remove(); });
				main.updateList(listData, action);
			});
		});
	});
};

if(!window.adminIPBanAuth) {
	$.getScript("<?php echo $jquery['auth']['admin']['ipban'] ?>", function() {
		if(!window.adminLoginsAuth) $.getScript("<?php echo $jquery['auth']['admin']['logins'] ?>", beginAdminBanlistUI);
			else beginAdminBanlistUI();
	});
} else beginAdminBanlistUI();