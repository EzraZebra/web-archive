/////////////////////////////////////////////
///// ADMIN IP BAN AUTHORISATION JQUERY /////
/////////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../../../includes/var_dir.php';
?>

//////////////////////////////
//// BEGIN ADMIN IP BAN CLASS
//////////////////////////////
	var adminIPBanAuth = new function() {
		//// IP BAN
		this.IPBan = function(data, callback) {
			//IPs ARE INTs -> CONTINUE
			if(main.isInt(data.ip) && main.isInt(data.ip_range)) {
				//IP BAN
				$.ajax({
					type: "POST",
					url: "<?php echo $mod['adminpanelcsets']['adminEdit']['main']; ?>",
					data:  data
				}).always(function(error) {
					//SUCCESS
					if(error == 'success') {
						if(typeof callback === 'function') callback();
							else alert('IP banned or unbanned successfully');
					}
					//ERROR
					else alert(error);
				});
			}
			//INVALID IPs
			else alert('Invalid IP address.');
		};
	}
////////////////////////////
//// END ADMIN IP BAN CLASS
////////////////////////////