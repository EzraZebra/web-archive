/////////////////////////////////////////////
///// ADMIN USERS AUTHORISATION JQUERY //////
/////////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../../../includes/var_dir.php';
?>

////////////////////////////
//// BEGIN ADMIN USERS CLASS
////////////////////////////
	var adminUsersAuth = new function() {
		this.fadeSpeed = 1000;

		//// EDIT SETTINGS
		this.editSettings = function(forminfo) {
			if(forminfo.find("#admin_userSettingsForm").val() == 1) {
				//SET DATA
				var data =	{	editSettings: 1,
								cset: 'users',
								activity_threshold: forminfo.find('#admin_editActivityThreshold').val()
							};

				adminAuth.editSettings(data, forminfo);
			} else alert('Form error.');
		};

		//// VALIDATE USERNAME
		this.validateUser = function(formInfo) {
			formInfo.usertakenMsg.fadeOut(auth.fadeSpeed); //hide error msg
			
			//CURRENT USERNAME
			if(formInfo.curUsername == formInfo.usernameForm.val()) formInfo.usernameForm.validateInput('nostate');
			//INVALID USERNAME
			else if(!auth.checkInfo('user', formInfo.usernameForm.val())) formInfo.usernameForm.validateInput('error');
			//VALID USERNAME
			else {
				//CHECK IF TAKEN
				$.ajax({
					type: "POST",
					url: "<?php echo $mod['auth']['register']; ?>",
					data: "register_check=1&register_user="+formInfo.usernameForm.val()
				}).always(function(error) {
					//NOT TAKEN
					if(error == 'notTaken') formInfo.usernameForm.validateInput('correct'); //not taken -> correct
					//TAKEN
					else {
						formInfo.usernameForm.validateInput('error'); //error
						formInfo.usertakenMsg.fadeIn(auth.fadeSpeed); //taken -> show error msg
					}
				});
			}
		};

		//// VALIDATE EMAIL
		this.validateEmail = function(formInfo) {
			formInfo.emailtakenMsg.fadeOut(auth.fadeSpeed); //hide error msg
			
			//CURRENT EMAIL
			if(formInfo.curEmail == formInfo.emailForm.val()) formInfo.emailForm.validateInput('nostate');
			//INVALID EMAIL
			else if(!auth.checkInfo('email', formInfo.emailForm.val())) formInfo.emailForm.validateInput('error');
			//VALID EMAIL
			else {			
				//CHECK IF TAKEN
				$.ajax({
					type: "POST",
					url: "<?php echo $mod['auth']['register']; ?>",
					data: "register_check=1&register_email="+formInfo.emailForm.val()
				}).always(function(error) {
					//NOT TAKEN
					if(error == 'notTaken') formInfo.emailForm.validateInput('correct'); //not taken -> correct
					//TAKEN
					else {
						formInfo.emailForm.validateInput('error'); //error
						formInfo.emailtakenMsg.fadeIn(auth.fadeSpeed); //taken -> show error msg
					}
				});
			}
		}
	
		//// VALIDATE PASSWORD
		this.validatePwd = function(formInfo) {				
			if(auth.checkInfo('pwd', formInfo.pwdForm.val())) { //pwd is valid
					formInfo.pwdForm.validateInput('correct'); //correct
					if(formInfo.pwdForm.val() == formInfo.pwdcForm.val()) formInfo.pwdcForm.validateInput('correct'); //passwords match -> correct
						else if(formInfo.pwdcForm.val() != '' || formInfo.pwdcForm.is(':focus')) formInfo.pwdcForm.validateInput('error'); //passwords dont match -> error
			} else {
				formInfo.pwdForm.validateInput('error'); //error
				formInfo.pwdcForm.validateInput('nostate');
			}
		};

		//// ADD NEW ITEM
		this.addNew = function() {
			$("#adminNew").slideDown(adminUsersAuth.fadeSpeed);
			$(this).fadeOut(adminUsersAuth.fadeSpeed);
			$("#admin_newUser").select();
		};

		//// CANCEL NEW ITEM
		this.cancelNew = function() {
			$("#adminNew").slideUp(adminUsersAuth.fadeSpeed);
			$(".admintools").children(".uibtnAdd").fadeIn(adminUsersAuth.fadeSpeed);
		};
		
		//// SAVE EDIT
		this.saveEdit = function(formInfo, data, callback) {
			//CHECK USERNAME
			if(auth.checkInfo('user', data.user_name)) {
				//CHECK EMAIL
				if(auth.checkInfo('email', data.email)) {
					//CHECK PWD
						var pwdError = "Invalid password.";
						
						if(typeof data.pwd === 'undefined' && typeof data.pwdc === 'undefined') pwdError = "";
							else if(typeof data.pwd !== 'undefined' && typeof data.pwdc !== 'undefined' && auth.checkInfo('pwd', data.pwd)) {
								if(data.pwd == data.pwdc) pwdError = "";
									else pwdError = "Passwords do not match";
							}

					if(pwdError == "") {
						//SAVE EDIT
						$.ajax({
							type: "POST",
							url: "<?php echo $mod['auth']['userEdit']; ?>",
							data:  data
						}).always(function(error) {
							//CALLBACK
							callback(error);
						});
					//INVALID PWD
					} else {
						callback(pwdError);
						if(pwdError == "Invalid password.") formInfo.pwd.select();
							else if(pwdError == "Passwords do not match.") formInfo.pwdc.select();
					}
				//INVALID EMAIL
				} else {
					callback("Invalid e-mail address.");
					formInfo.email.select();
				}
			//INVALID USERNAME
			} else {
				if(auth.checkInfo('email', data.email)) callback("Invalid username.");
					else callback("Invalid username and e-mail address.");
				
				formInfo.user.select();
			}
		};

		//// DELETE USER
		this.deleteUser = function(user_name, user_id, btn, callback) {
			//CONFIRM
			if(btn.hasClass('noConfirm') || confirm("Are you sure you want to delete "+user_name+"? All data associated with this user will be permanently destroyed.")) {
				//SET DATA
				var data = {	user_id: user_id,
								deleteUser: 1
							};

				//DELETE USER
				$.ajax({
					type: "POST",
					url: "<?php echo $mod['auth']['userEdit']; ?>",
					data:  data
				}).always(function(error) {
					callback(error);
				});
			}
		};
	}
//////////////////////////
//// END ADMIN CLASS
//////////////////////////