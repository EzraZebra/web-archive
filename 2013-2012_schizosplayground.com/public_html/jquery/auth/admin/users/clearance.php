/////////////////////////////////////////////////
///// ADMIN CLEARANCE AUTHORISATION JQUERY //////
/////////////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../../../includes/var_dir.php';
?>

/////////////////////////////////
//// BEGIN ADMIN CLEARANCE CLASS
/////////////////////////////////
	var adminClearanceAuth = new function() {
		this.fadeSpeed = 1000;

		//// UPDATE PERMISSIONS
		this.updatePermissions = function(special_perm, button) {
			//SPECIAL PERMISSION -> CONTINUE
			if(special_perm != "") {
				//INACTIVE OR BANNED
				if(special_perm == "inactive" || special_perm == "banned")
					var removeClass = 'uibtnSave', addClass = 'uibtnCancel';
				//ADMIN
				else if(special_perm == "admin")
					var removeClass = 'uibtnCancel', addClass = 'uibtnSave';
				
				//UPDATE PERMISSION BTN
				if(!button.hasClass(addClass)) {
					button.fadeOut(adminAuth.fadeSpeed, function() {
						//NEW ITEM -> UPDATE CHECKBOX
						if($(this).siblings('.admin_addPostNews').length != 0) {
							var checkbox = $(this).siblings('.admin_addPostNews');
							
							if(addClass == 'uibtnSave') checkbox.attr('checked', true);
								else checkbox.attr('checked', false);
						}
						$(this).removeClass(removeClass).addClass(addClass).fadeIn(adminAuth.fadeSpeed);
						
					});
				}
			}
		};
		
		
		//// SAVE EDIT
		this.saveEdit = function() {
			//SET VARS
			var selectedRows = $(this).parents('tr'),
				data = {	clearance_id: selectedRows.find(".admin_editClearanceId").val(),
							title: selectedRows.find(".admin_editClearance").val(),
							special_perm: selectedRows.find(".admin_editPermissions").children("option:selected").text(),
							clearanceEdit: 1,
							saveEdit: 1
						};

			//SAVE EDIT
			$.ajax({
				type: "POST",
				url: "<?php echo $mod['adminpanelcsets']['adminEdit']['main']; ?>",
				data:  data
			}).always(function(error) {
				//SUCCESS
					if(error == 'success') {
						//update row
						selectedRows.find('.admin_listInfoTitle').text(data.title);
						var permissionsSpan = selectedRows.find('.admin_listInfoPermissions'),
							permissions = permissionsSpan.text();
						if(permissions != 'root_user' && permissions != 'root_banned' && permissions != 'root_inactive' && permissions != 'root_admin') {
							permissionsSpan	.text(data.special_perm)
																			.removeClass('inactiveTitle bannedTitle adminTitle root_userTitle root_inactiveTitle root_bannedTitle root_adminTitle')
																			.addClass(data.special_perm+'Title');
							adminClearanceAuth.updatePermissions(data.special_perm, selectedRows.find('.checklist'));
						}
						
						//end editing
						selectedRows.find('.uibtnCancel:not(.checklist)').click();
					}
				//ERROR
					else alert(error);
			});
		};
		
		//// SAVE NEW
		this.saveNew = function() {
			//SET VARS
				var selectedRows = $(this).parents().eq(1),
					data = {	title: selectedRows.find("#admin_addClearance").val(),
								special_perm: selectedRows.find(".admin_addPermissions").children("option:selected").text(),
								clearanceEdit: 1,
								saveNew: 1
							};
							
				if(selectedRows.find('.admin_addPostNews').is(':checked')) data.post_news = 1;
					else data.post_news = 0;

			//SAVE NEW
			$.ajax({
				type: "POST",
				url: "<?php echo $mod['adminpanelcsets']['adminEdit']['main']; ?>",
				data:  data
			}).always(function(error) {
				//SUCCESS
				if(error.substr(0, 7) == 'success') {
					//populate prototype
						var newRow = selectedRows.siblings("#itemPrototype").clone().removeAttr('id');
						newRow.find('.admin_listInfoTitle').text(data.title);
						newRow.find('.admin_editClearance').attr('value', data.title);
						newRow.find('.admin_listInfoPermissions').addClass(data.special_perm+'Title').text(data.special_perm);
						newRow.find('.admin_editClearanceId').val(error.substr(7));
						newRow.find('.admin_editPermissions')
								.prop(	"selectedIndex",
										selectedRows.find(".admin_addPermissions").prop("selectedIndex"));
										
						if(data.post_news == 1) newRow.find('.post_news .checklist').removeClass('uibtnCancel').addClass('uibtnSave');
						
					//hide add item row
					selectedRows.fadeOut(adminAuth.fadeSpeed, function() {
						adminAuth.cancelNew(selectedRows); //reset add item form
						selectedRows.siblings("#itemPrototype").after(newRow.fadeIn(adminAuth.fadeSpeed)); //insert new item

						//reload UI scripts
						$.getScript("<?php echo $jquery['auth']['admin']['main_ui'] ?>");
						$.getScript("<?php echo $jquery['auth']['admin']['clearance_ui'] ?>");
					});
				}
				//ERROR
				else alert(error);
			});
			
		};

		//// DELETE CLEARANCE
		this.deleteClearance = function() {
			//SET VARS
			var selectedRows = $(this).parents().eq(1),
				title = selectedRows.find(".admin_listInfoTitle").text();

			//CONFIRM
			if($(this).hasClass('noConfirm') || confirm("Are you sure you want to delete "+title+"?")) {
				//SET DATA
				var data = {	clearance_id: selectedRows.find(".admin_editClearanceId").val(),
								clearanceEdit: 1,
								deleteClearance: 1
							};

				//DELETE CLEARANCE
				$.ajax({
					type: "POST",
					url: "<?php echo $mod['adminpanelcsets']['adminEdit']['main']; ?>",
					data:  data
				}).always(function(error) {
					//SUCCESS
						if(error == 'success') selectedRows.fadeOut(adminAuth.fadeSpeed, function() { $(this).remove() } );
					//ERROR
						else alert(error);
				});
			}
		};
		
		//// EDIT PERMISSIONS
		this.editPermissions = function() {
			//SET VARS
			var selectedCell = $(this).parents().eq(1),
				special_perm = selectedCell.parent().find('.admin_listInfoPermissions').text();

			//NO SPECIAL PERMISSIONS -> CONTINUE
			if(	special_perm != 'inactive' && special_perm != 'banned' && special_perm != 'admin'
				&& special_perm != 'root_user' && special_perm != 'root_inactive' && special_perm != 'root_banned' && special_perm != 'root_admin') {
				var valid = false,
					data = {	clearanceEdit: 1,
								editPermissions: 1,
								clearance_id: selectedCell.siblings().find('.admin_editClearanceId').val()
							};

				//POST NEWS
				if(selectedCell.hasClass('post_news')) {
					data.post_news = 0;
					if(selectedCell.children('.checklist').hasClass('uibtnCancel')) data.post_news = 1;
					
					valid = true;
				}
			
				//VALID -> EDIT PERMISSION
				if(valid) {
					$.ajax({
						type: "POST",
						url: "<?php echo $mod['adminpanelcsets']['adminEdit']['main']; ?>",
						data:  data
					}).always(function(error) {
						//SUCCESS
						if(error == 'success') {
							//update btn
							selectedCell.children('.checklist').fadeOut(adminAuth.fadeSpeed, function() {
								$(this).removeClass('uibtnSave').removeClass('uibtnCancel');
								if(data.post_news == 0) $(this).addClass('uibtnCancel');
									else $(this).addClass('uibtnSave');
									
								$(this).fadeIn(adminAuth.fadeSpeed);
							});								
								
						}
						//ERROR
						else alert(error);
					});
				//INVALID -> ERROR
				} else alert('Permission type not found.');
			} else alert('Cannot edit permissions for '+special_perm+' users.'); //special permissions
		};
		
		//// ADD PERMISSIONS
		this.addPermissions = function() {
			//SET VARS
			var	selectedCell = $(this).parents().eq(1),
				special_perm = selectedCell.parent().find('.admin_addPermissions option:selected').text();
				
			//NO SPECIAL PERMISSIONS -> CONTINUE
			if(special_perm != 'inactive' && special_perm != 'banned' && special_perm != 'admin') {				
				var	button = selectedCell.children('.checklist'),
					checkbox = selectedCell.children('input[type=checkbox]');
				
				//TURN OFF
				if(button.hasClass('uibtnSave')) {
					checkbox.attr('checked', false); //update form
					button.fadeOut(adminAuth.fadeSpeed, function() { //change btn
						$(this).removeClass('uibtnSave').addClass('uibtnCancel').fadeIn(adminAuth.fadeSpeed);
					});
				}
				//TURN ON
				else {
					checkbox.attr('checked', true); //update form
					button.fadeOut(adminAuth.fadeSpeed, function() { //change btn
						$(this).removeClass('uibtnCancel').addClass('uibtnSave').fadeIn(adminAuth.fadeSpeed);
					});
				}
			} else alert('Cannot edit permissions for '+special_perm+' users.'); //special permissions
		};
	}
//////////////////////////
//// END ADMIN CLASS
//////////////////////////