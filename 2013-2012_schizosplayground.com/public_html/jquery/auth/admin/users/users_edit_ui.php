/////////////////////////////////////////////
///// ADMIN USERS AUTHORISATION JQUERY //////
/////////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../../../includes/var_dir.php';
?>
	
var beginAdminUsersEditUI = function() {
	$(document).ready(function() {
		//// SAVE USER
		$("#adminUserEdit").unbind('submit').submit(function() {
			//SET VARS
			var form = $(this),
				data = {	user_id: form.find("#admin_userEditId").val(),
							user_name: form.find("#admin_userEditUser").val(),
							clearance: form.find("#admin_userEditClearance option:selected").val(),
							email: form.find("#admin_userEditEmail").val(),
							editUser: form.find("#admin_userEditForm").val()
						},
				formInfo = {	email: form.find("#admin_userEditEmail"),
								user: form.find("#admin_userEditUser")
							};
							
			if(form.find("#admin_userEditPwdc").val() != "") {
				data.pwd = form.find("#admin_userEditPwd").val();
				data.pwdc = form.find("#admin_userEditPwdc").val();
				formInfo.pwd = form.find("#admin_userEditPwd");
			}

			//// SAVE USER
			adminUsersAuth.saveEdit(formInfo, data, function(error) {
				$(".msgpopup").fadeOut(auth.fadeSpeed); //hide errors

				//SUCCESS
				if(error == 'success') {
					//update form
					form.find('#admin_editSettingsSuccess').fadeIn(adminAuth.fadeSpeed*2);
					form.find('#admin_userEditCurUser').val(data.user_name);
					$(".settitle span").text(data.user_name);
					form.find("#admin_userEditUser").change();

					//update url
					History.replaceState('', $(document).attr('title'), '../'+data.user_name+'/');
					
					//show msg
					$("#successpopup").text('Changes saved successfully.').fadeIn(auth.fadeSpeed);
				}
				//ERROR
				else $("#errorpopup").text(error).fadeIn(auth.fadeSpeed);
			});
				
			return false;
		});
		
		//// DELETE USER
		$("#adminUserEdit #admin_userEditDelete").unbind('click').click(function() {
			//SET VARS
			var user_name = $("#adminUserEdit #admin_userEditUser").val(),
				user_id = $("#adminUserEdit #admin_userEditId").val();

			//// DELETE USERS
			adminUsersAuth.deleteUser(user_name, user_id, $(this), function(error) {
				$(".msgpopup").fadeOut(auth.fadeSpeed);
				//SUCCESS
				if(error == 'success') window.location.replace('../');
				//ERROR
				else $("#errorpopup").text(error).fadeIn(auth.fadeSpeed);
			});
						
		});

		//// VALIDATION
			//VALIDATE USER
			$("#adminUserEdit #admin_userEditUser").unbind('change').change(function() {
				var formInfo = {	usernameForm: $(this),
									usertakenMsg: $("#adminUserEdit .usertaken"),
									curUsername: $("#adminUserEdit #admin_userEditCurUser").val()
								}
				adminUsersAuth.validateUser(formInfo);
			});
			//VALIDATE EMAIL
			$("#adminUserEdit #admin_userEditEmail").unbind('change').change(function() {
				var formInfo = {	emailForm: $(this),
									emailtakenMsg: $("#adminUserEdit .emailtaken"),
									curEmail: $("#adminUserEdit #admin_userEditCurEmail").val()
								};

				adminUsersAuth.validateEmail(formInfo);
			});
			//VALIDATE PWD
			$("#adminUserEdit #admin_userEditPwd").unbind('change').change(function() {
				var formInfo = {	pwdForm: $(this),
									pwdcForm: $("#adminUserEdit #admin_userEditPwdc")
								};
				
				adminUsersAuth.validatePwd(formInfo);
			});
			//VALIDATE PWDC
			$("#adminUserEdit #admin_userEditPwdc").unbind('change').change(function() {
				var formInfo = {	pwdForm: $("#adminUserEdit #admin_userEditPwd"),
									pwdcForm: $(this)
								};
				
				adminUsersAuth.validatePwd(formInfo);
			});

			//VALIDATE INPUT ON ANY CHANGE
			$("#adminUserEdit").find("input").unbind('input').bind('input', function(){
				$(this).change();
			});
	});
};

if(!window.adminUsersAuth) {
	$.getScript("<?php echo $jquery['auth']['admin']['users'] ?>", beginAdminUsersEditUI);
} else beginAdminUsersEditUI();