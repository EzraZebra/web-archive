////////////////////////////////////////////////
///// ADMIN LOGINS UI AUTHORISATION JQUERY /////
////////////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../../../includes/var_dir.php';
?>
	
var beginAdminLoginsUI = function() {
	$(document).ready(function() {
	////////////////////////////
	//// BEGIN EVENT HANDLERS
	////////////////////////////
		//SETTINGS
		$("#admin_loginSettings").unbind('submit').submit(function() {
			var forminfo = $(this);
			adminLoginsAuth.editSettings(forminfo);
			
			return false;
		});

		//IP FILTER
			//APPLY FILTER CLICK
			$("#loginlist .ipFilter").unbind('click').click(function(e) {
				e.stopPropagation();
				//SET DATA
				var	formInfo = $("#loginlist"),
					ip = $(this).parents('td').find('.admin_loginsIP').val(),
					ipRow = $(this).text();

				adminLoginsAuth.IPFilter(ip, ip, formInfo);

				//SHOW FILTERED IP
				$("#loginlist #admin_ipFilterInput").val(ipRow);
			});

			//APPLY FILTER
			$("#loginlist #admin_applyIPFilter").unbind('click').click(function() {
				//SET DATA
					var	formInfo = $("#loginlist"),
						ip = $(this).siblings('#admin_ipFilterInput').val(),
						ip_range = ip;

					//GET IP RANGE
					ip = adminLoginsAuth.splitIPRange(ip);
					ip_range = ip.ip_range;
					ip = ip.ip;

					ip = main.ip2long(ip);
					ip_range = main.ip2long(ip_range);

				adminLoginsAuth.IPFilter(ip, ip_range, formInfo);
			});
			
			//REMOVE FILTER
			$("#loginlist #admin_removeIPFilter").unbind('click').click(function() {
				//SET DATA
				var formInfo = $("#loginlist"),
					ip = '_noip',
					ip_range = ip;

				adminLoginsAuth.IPFilter(ip, ip_range, formInfo);
			});

			//BAN FILTER
			$("#admin_banIpFilter").unbind('click').click(function() {
				//SET DATA
					var ip = $(this).siblings('#admin_ipFilterInput').val(),
						ip_range = ip;

					//GET IP RANGE
					ip = adminLoginsAuth.splitIPRange(ip);
					ip_range = ip.ip_range;
					ip = ip.ip;

					var data = {	ip: main.ip2long(ip),
									ip_range: main.ip2long(ip_range),
									ipBan: 1
								},
						selectedInputs = adminLoginsAuth.selectedInputsInRange($("#loginlist").find('.admin_loginsIP'), data.ip, data.ip_range);

				adminIPBanAuth.IPBan(data, function() {
					adminLoginsAuth.updateList(selectedInputs);
				});
			});

			//UNBAN FILTER
			$("#admin_unbanIpFilter").unbind('click').click(function() {
				//SET DATA
					var ip = $(this).siblings('#admin_ipFilterInput').val(),
						ip_range = ip;

					//GET IP RANGE
					ip = adminLoginsAuth.splitIPRange(ip);
					ip_range = ip.ip_range;
					ip = ip.ip;

					var data = {	ip: main.ip2long(ip),
									ip_range: main.ip2long(ip_range),
									ipUnban: 1
								},
						selectedInputs = adminLoginsAuth.selectedInputsInRange($("#loginlist").find('.admin_loginsIP'), data.ip, data.ip_range);
							
				adminIPBanAuth.IPBan(data, function() {
					adminLoginsAuth.updateList(selectedInputs, true);
				});
			});

		//BAN
		$(".admin_banIp").unbind('click').click(function() {
			//SET DATA
			var ip = $(this).parents('tr').find('.admin_loginsIP').val(),
				data = {	ip: ip,
							ip_range: ip,
							ipBan: 1
						},
				selectedInputs = $("#loginlist").find('.admin_loginsIP[value='+data.ip+']');
						
			adminIPBanAuth.IPBan(data, function() {
				adminLoginsAuth.updateList(selectedInputs);
			});
		});

		//UNBAN
		$(".admin_unbanIp").unbind('click').click(function() {
			//SET DATA
			var ip = $(this).parents('tr').find('.admin_loginsIP').val(),
				data = {	ip: ip,
							ip_range: ip,
							ipUnban: 1
						},
				selectedInputs = $("#loginlist").find('.admin_loginsIP[value='+data.ip+']');
						
			adminIPBanAuth.IPBan(data, function() {
				adminLoginsAuth.updateList(selectedInputs, true);
			});
		});
	////////////////////////////
	//// END EVENT HANDLERS
	////////////////////////////
	});
};

if(!window.adminLoginsAuth) {
	$.getScript("<?php echo $jquery['auth']['admin']['logins'] ?>", function() {
		if(!window.adminIPBanAuth) $.getScript("<?php echo $jquery['auth']['admin']['ipban'] ?>", beginAdminLoginsUI);
			else beginAdminLoginsUI();
	});
} else beginAdminLoginsUI();