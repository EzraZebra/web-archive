/////////////////////////////////////////////
///// ADMIN USERS AUTHORISATION JQUERY //////
/////////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../../../includes/var_dir.php';
?>
	
var beginAdminUsersUI = function() {
	$(document).ready(function() {
		//SETTINGS
			//activity threshold
			$("#admin_userSettings").unbind('submit').submit(function() {
				var forminfo = $(this);
				adminUsersAuth.editSettings(forminfo);
				
				return false;
			});
		//// SET REGISTER FORM INFO
		registerAuth.formInfo = {
			adminlist: $('#userlist'),
			register_form: $('#adminNew'),
			register_user: $('#admin_newUser'),
			register_email: $('#admin_newEmail'),
			register_emailc: $('#admin_newEmailc'),
			register_pwd: $('#admin_newPwd'),
			register_pwdc: $('#admin_newPwdc'),
			register_btn: $('#adminNew #form_btnbar'),
			errorpopup: $('.admintools #errorpopup')
		};

		//// FOCUS INPUT
		$("#adminNew #admin_newUser").focus();
		//////////////////////
		//// BEGIN EVENT HANDLERS
		//////////////////////
			//TOOLS
				//add
				$(".admintools").children("#adminUsersAdd").unbind('click').click(adminUsersAuth.addNew);
				//cancel add
				$("#adminNew").find(".uibtnCancel").unbind('click').click(adminUsersAuth.cancelNew);
				//save
				$("#userlist").find(".uibtnSave").unbind('click').click(function() {
					//SET VARS;
					var selectedRows = $(this).parents('tr'),
						clearanceInfo = selectedRows.find(".admin_editClearance").children("option:selected"),
						clearance = clearanceInfo.text(),
						clearanceClass = clearanceInfo.attr('class'),
						data = {	user_id: selectedRows.find(".admin_editUserId").val(),
									user_name: selectedRows.find(".admin_editUser").val(),
									clearance: clearanceInfo.val(),
									email: selectedRows.find(".admin_editEmail").val(),
									editUser: 1
								},
						formInfo = {	email: selectedRows.find(".admin_editEmail"),
										user: selectedRows.find(".admin_editUser")
									};
						
					//SAVE
					adminUsersAuth.saveEdit(formInfo, data, function(error) {
						//SUCCESS
						if(error == 'success') {
							//update row
							selectedRows.find('.admin_listInfoUser').html("<a href='"+data.user_name+"/'>"+data.user_name+"</a>");
							selectedRows.find('.admin_listInfoTitle')	.text(clearance)
																		.removeClass('inactiveTitle bannedTitle adminTitle root_userTitle root_bannedTitle root_inactiveTitle root_adminTitle')
																		.addClass(clearanceClass);
							selectedRows.find('.admin_listInfoEmail').text(data.email);
							
							//end editing
							selectedRows.find('.uibtnCancel').click();
						}
						//ERROR
						else alert(error);
					});
				});
				//delete
				$("#userlist").find(".uibtnDelete").unbind('click').click(function() {
					//SET VARS
					var selectedRows = $(this).parents().eq(1),
						user_name = selectedRows.find(".admin_listInfoUser").text();
						user_id = selectedRows.find(".admin_editUserId").val();
					
					
					adminUsersAuth.deleteUser(user_name, user_id, $(this), function(error) {
						//SUCCESS
							if(error == 'success') selectedRows.fadeOut(adminAuth.fadeSpeed, function() { $(this).remove() } );
						//ERROR
							else alert(error);
					});
				});

			//SUBMIT REGISTER FORM
				$("#adminNew").unbind('submit').submit(function() {
					$(this).find('fieldset > div').append('<div class="loading"><div>Loading . . .</div></div>');
					var	forminfo = { //get login info
						username: $(this).find('#admin_newUser').val(),
						pwd: $(this).find('#admin_newPwd').val(),
						pwdc: $(this).find('#admin_newPwdc').val(),
						email: $(this).find('#admin_newEmail').val(),
						emailc: $(this).find('#admin_newEmailc').val(),
						clearance: $(this).find('#admin_newClearance option:selected').val(),
						form: $(this).find('#admin_newUserForm').val()
					};
				
					popup.state.page = "adminpanel/users";
					registerAuth.register(forminfo, false); // register

					return false;
				});
		
			//REGISTER FORM VALIDATION
				//NEW USER
					$("#admin_newUser").unbind('change').change(function() {
						registerAuth.validateUser();
					});
					$('#admin_newEmail').unbind('change').change(function() {
						registerAuth.validateEmail();
					});
					$('#admin_newEmailc').unbind('change').change(function() {
						registerAuth.validateEmail();
					});
					$('#admin_newPwd').unbind('change').change(function() {
						registerAuth.validatePwd();
					});
					$('#admin_newPwdc').unbind('change').change(function() {
						registerAuth.validatePwd();
					});

					$("#adminNew").find("input").unbind('input').bind('input', function(){ //VALIDATE INPUT ON ANY CHANGE
						$(this).change();
					});
					
				//VALIDATE USER
					$(".admin_editUser").unbind('change').change(function() {
						var formInfo = {	usernameForm: $(this),
											usertakenMsg: $(this).siblings('.forminfo').find('.usertaken'),
											curUsername: $(this).siblings('.admin_listInfoUser').text()
										};

						adminUsersAuth.validateUser(formInfo);
					});
				//VALIDATE EMAIL
					$(".admin_editEmail").unbind('change').change(function() {
						var formInfo = {	emailForm: $(this),
											emailtakenMsg: $(this).siblings('.forminfo').find('.emailtaken'),
											curEmail: $(this).siblings('.admin_listInfoEmail').text()
										};

						adminUsersAuth.validateEmail(formInfo);
					});

					$("#userlist").find("input").unbind('input').bind('input', function(){ //VALIDATE INPUT ON ANY CHANGE
						$(this).change();
					});
			//////////////////////
			//// END EVENT HANDLERS
			//////////////////////
	});
};

if(!window.adminUsersAuth) {
	$.getScript("<?php echo $jquery['auth']['admin']['users'] ?>", function() {
		if(!window.registerAuth) $.getScript("<?php echo $jquery['auth']['register'] ?>", beginAdminUsersUI);
			else beginAdminUsersUI();
	});
} else beginAdminUsersUI();