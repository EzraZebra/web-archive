/////////////////////////////////////////////
///// ADMIN LOGINS AUTHORISATION JQUERY /////
/////////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../../../includes/var_dir.php';
?>

//////////////////////////////
//// BEGIN ADMIN LOGINS CLASS
//////////////////////////////
	var adminLoginsAuth = new function() {
		//// EDIT SETTINGS
		this.editSettings = function(forminfo) {
			if(forminfo.find("#admin_loginSettingsForm").val() == 1) {
				//SET DATA
				var data =	{	editSettings: 1,
								cset: 'logins',
								login_remember: forminfo.find('#admin_editLoginRemember').val(),
								login_time: forminfo.find('#admin_editLoginTime').val()
							};

				adminAuth.editSettings(data, forminfo);
			} else alert('Form error.');
		};
		
		//// IP FILTER
		this.IPFilter = function(ip, ip_range, formInfo) {
			//IPs VALID -> CONTINUE
			if(ip == '_noip' || (main.isInt(ip) && main.isInt(ip_range))) {
				//SET DATA
				var data = {	module: 'logins',
								formInfo: formInfo,
								data: ip+'/'+ip_range
							};

				//NO ROWS TO REMOVE -> UPDATE LIST
				if(data.formInfo.find('.listitem').length === 0) {
					data.formInfo.find('tbody tr').fadeOut(400, function() {
						main.updateList(data, 'add', function() {
							//reload UI scripts
							$.getScript("<?php echo $jquery['auth']['admin']['main_ui'] ?>", function() {
								$.getScript("<?php echo $jquery['auth']['admin']['logins_ui'] ?>");
							});
						});
					});
				}		
				//REMOVE ROWS
				else data.formInfo.find('.listitem').fadeOut(400, function() {
					$(this).remove();

					//ALL ROWS REMOVED -> UPDATE LIST
					if(data.formInfo.find('.listitem').length === 0) {
						main.updateList(data, 'add', function() {
							//reload UI scripts
							$.getScript("<?php echo $jquery['auth']['admin']['main_ui'] ?>", function() {
								$.getScript("<?php echo $jquery['auth']['admin']['logins_ui'] ?>");
							});
						});
					}
				});
			}
			//INVALID IPs
			else alert('Invalid IP address.');
		};
		
		//// UPDATE LIST
		this.updateList = function(selectedInputs, unban) {
			//CHECK UNBAN
			if(typeof unban === 'undefined') var unban = false;
				else if(unban != true) unban = false;

			//LOGINS WITHIN RANGE
			if(selectedInputs.length > 0) {
				var selectedRows = selectedInputs.parents('tr');

				//SET IP CLASS
				selectedInputs.siblings('span').fadeOut(400, function() {
					if(unban) $(this).removeClass('bannedTitle').fadeIn(400);
						else $(this).addClass('bannedTitle').fadeIn(400);
				});
				
				//UNBAN -> SHOW BAN BTN
				if(unban) {
					selectedRows.find(".admin_banIp").removeClass("uibtnHidden");
					selectedRows.find(".admin_unbanIp").addClass("uibtnHidden");
				}
				//EXPIRE LOGIN/SHOW UNBAN BTN
				else {
					selectedRows.find('.admin_loginExpires').fadeOut(400, function() {
						$(this).text('expired').fadeIn(400);
					});
					selectedRows.find(".admin_banIp").addClass("uibtnHidden");
					selectedRows.find(".admin_unbanIp").removeClass("uibtnHidden");
				}
			}
			//NO LOGINS WITHIN RANGE
			else {
				if(unban) alert('IP address has been unbanned.'); //unban msg
					else alert('IP address has been banned.'); //ban msg
			}
		};

		//// GET SELECTED INPUTS IN RANGE
		this.selectedInputsInRange = function(inputs, ip, ip_range) {
			var selectedInputs = "";

			inputs.each(function() {
				var curIP = $(this).val();
				if(curIP >= ip && curIP <= ip_range) {
					if(selectedInputs == "") selectedInputs = $(this);
						else selectedInputs = selectedInputs.add($(this));
					}
			});
			
			return selectedInputs;
		}
		
		//// SPLIT IP RANGE
		this.splitIPRange = function(ip) {
			var ip_range = ip;

			if(ip.indexOf(".*.") != -1 || ip.indexOf("*.") == 0 || ip.indexOf(".*") == ip.length-2) {
				ip_range = ip.replace(/\*/g, '255');
				ip = ip.replace(/\*/g, '0');
			}
			
			return { ip: ip, ip_range: ip_range };
		};
	}
////////////////////////////
//// END ADMIN LOGINS CLASS
////////////////////////////