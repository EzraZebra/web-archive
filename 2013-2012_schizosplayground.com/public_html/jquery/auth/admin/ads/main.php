/////////////////////////////////////////////
///// ADMIN ADLIST AUTHORISATION JQUERY /////
/////////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../../../includes/var_dir.php';
?>

/////////////////////////////////
//// BEGIN ADMIN ADLIST CLASS
/////////////////////////////////
	var adminAdsAuth = new function() {
		//// EDIT SETTINGS
		this.editSettings = function(forminfo) {
			if(forminfo.find("#admin_adsSettingsForm").val() == 1) {
				//SET DATA
				var data =	{	editSettings: 1,
								cset: 'ads'
							};

				if(forminfo.hasClass('admin_editSettingsMain')) data.googlePub_id = forminfo.find('#admin_editGooglePub_id').val();
				else if(forminfo.hasClass('admin_editSettingsRightcolumn')) data.ads_length = forminfo.find('#admin_editAdsLength').val();

				adminAuth.editSettings(data, forminfo);
			} else alert('Form error.');
		};

		//// SAVE EDIT
		this.saveEdit = function(selectedRows, data, widget) {
			//SAVE EDIT
			$.ajax({
				type: "POST",
				url: "<?php echo $mod['adminpanelcsets']['adminEdit']['main']; ?>",
				data:  data
			}).always(function(error) {
				//SUCCESS
					if(error == 'success') {
						//SUBMIT IMAGE
						var iframeContents = selectedRows.find('.fileUpload_frame').contents();
						if(iframeContents.find('#fileUpload_file').val())
							iframeContents.find("#fileUploadForm").submit();

						//update row
						if(widget == 'rightcolumn') selectedRows.find('.admin_listInfoPriority').text(data.priority);
						selectedRows.find('.admin_listInfoAdName').text(data.adName);
								
						if(selectedRows.hasClass('adminEditAd') )
							selectedRows.find('.admin_listInfoAdUrl').text(data.adUrl);
						else if(selectedRows.hasClass('adminEditGoogleAd')) {
							selectedRows.find('.admin_listInfoAdSlot').text(data.adSlot);
							selectedRows.find('.admin_listInfoAdWidth').text(data.adWidth);
							selectedRows.find('.admin_listInfoAdHeight').text(data.adHeight);
						}
						
						//end editing
						selectedRows.find('.uibtnCancel').click();
					}
				//ERROR
					else alert(error);
			});
		};

		//// SAVE NEW
		this.saveNew = function(selectedRows, data, widget) {
			//SAVE NEW
			$.ajax({
				type: "POST",
				url: "<?php echo $mod['adminpanelcsets']['adminEdit']['main']; ?>",
				data:  data
			}).always(function(error) {
				//SUCCESS
				if(error.substr(0, 7) == 'success') {
					//SET VARS
					var newRow = selectedRows.siblings("#itemPrototype").clone().removeAttr('id'),
						//IMAGE UPLOAD IFRAME
						iframeContents = selectedRows.find('.fileUpload_frame').contents(),
						newIframeObj = newRow.find('.fileUpload_frame');

					//SUBMIT IMAGE 
						if(iframeContents.find('#fileUpload_file').val()) {
							var iframeForm = iframeContents.find("#fileUploadForm");
							iframeForm.attr('action', iframeForm.attr('action')+'new/'+error.substr(7)+'/');
							iframeForm.submit();
						}

					//POPULATE PROTOTYPE
						if(widget == 'rightcolumn') {
							newRow.find('.admin_listInfoPriority').text(data.priority);
							newRow.find('.admin_editPriority').val(data.priority);
						}
						newRow.find('.admin_listInfoAdName').text(data.adName);
						newRow.find('.admin_editAdName').val(data.adName);
						newRow.find('.admin_editAdId').val(error.substr(7));
						//normal ad
						if(selectedRows.hasClass('adminAdAdd')) {
							newRow.addClass('adminEditAd');
							newRow.find('.admin_listInfoAdUrl').text(data.adUrl);
							newRow.find('.admin_editAdUrl').val(data.adUrl);
							newIframeObj.attr('src', newIframeObj.attr('src')+error.substr(7)+'/');
							
							//delete google ad fields							
							newRow.find('.admin_googleAdForm').remove();
						}
						//google ad
						else if(selectedRows.hasClass('adminGoogleAdAdd')) {
							newRow.addClass('adminEditGoogleAd');
							newRow.find('.admin_listInfoAdSlot').text(data.adSlot);
							newRow.find('.admin_editAdSlot').val(data.adSlot);
							newRow.find('.admin_listInfoAdWidth').text(data.adWidth);
							newRow.find('.admin_listInfoAdHeight').text(data.adHeight);
							newRow.find('.admin_editAdWidth').val(data.adWidth);
							newRow.find('.admin_editAdHeight').val(data.adHeight);
							
							//delete normal ad fields							
							newRow.find('.admin_adForm').remove();
						}
						
					//HIDE ADD ITEM ROW
					selectedRows.fadeOut(adminAuth.fadeSpeed, function() {
						adminAuth.cancelNew($(this)); //reset add item form
						$(this).siblings("#itemPrototype").after(newRow.fadeIn(adminAuth.fadeSpeed)); //insert new item
						if(newRow.siblings().length == 3)	newRow.siblings('.someError').fadeOut(adminAuth.fadeSpeed, function() {
																$(this).removeClass('someError').addClass('noError');
															});
						
						//RELOAD UI SCRIPTS
						$.getScript("<?php echo $jquery['auth']['admin']['main_ui'] ?>");
						if(widget == 'rightcolumn') $.getScript("<?php echo $jquery['auth']['admin']['ads_ui']['rightcolumn'] ?>");
						else if(widget == 'bottombanner') $.getScript("<?php echo $jquery['auth']['admin']['ads_ui']['bottombanner'] ?>");
						$.getScript("<?php echo $jquery['auth']['fileUpload_ui'] ?>");
					});
				}
				//ERROR
				else alert(error);
			});
		};

		//// DELETE AD
		this.deleteAd = function() {
			//SET VARS
			var selectedRows = $(this).parents('tr'),
				name = selectedRows.find(".admin_listInfoAdName").text();

			//CONFIRM
			if($(this).hasClass('noConfirm') || confirm("Are you sure you want to delete "+name+"?")) {
				//SET DATA
				var data = {	ad_id: selectedRows.find(".admin_editAdId").val(),
								adEdit: 1,
								deleteAd: 1
							};

				//DELETE AD
				$.ajax({
					type: "POST",
					url: "<?php echo $mod['adminpanelcsets']['adminEdit']['main']; ?>",
					data:  data
				}).always(function(error) {
					//SUCCESS
						if(error == 'success') {
							selectedRows.fadeOut(adminAuth.fadeSpeed, function() {
								if($(this).siblings().length == 3)	$(this).siblings('.noError').fadeIn(adminAuth.fadeSpeed, function() {
																		$(this).removeClass('noError').addClass('someError');
																	});
								$(this).remove();
							});
								
						}
					//ERROR
						else alert(error);
				});
			}
		};
		
		//// TYPE SWITCH (normal <-> google)
		this.typeSwitch = function() {
			var btnId = $(this).attr('id');

			if(btnId == 'adminAdAdd') {
				var formHide = $("#newItem .admin_googleAdForm"),
					formShow = $("#newItem .admin_adForm");

				$("#newItem").removeClass('adminGoogleAdAdd').addClass(btnId);
				formShow.parents('td').removeClass('intcell').addClass('fileUploadCell');
			}
			else if(btnId == 'adminGoogleAdAdd') {
				var formHide = $("#newItem .admin_adForm"),
					formShow = $("#newItem .admin_googleAdForm");

				$("#newItem").removeClass('adminAdAdd').addClass(btnId);
				formShow.parents('td').removeClass('fileUploadCell').addClass('intcell');
			}
			else return false;

			formHide.hide();
			formShow.show();
		};
	}
//////////////////////////
//// END ADMIN CLASS
//////////////////////////