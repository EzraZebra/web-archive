//////////////////////////////////////////
///// ADMIN ADS AUTHORISATION JQUERY /////
//////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../../../includes/var_dir.php';
?>
	
var beginAdminAdsBottomBannerUI = function() {
	$(document).ready(function() {
		//////////////////////
		//// BEGIN EVENT HANDLERS
		//////////////////////
			//TOOLS
				//SAVE EDIT
				$("#adlist").find("tr:not(#newItem)").find(".uibtnSave").unbind('click').click(function() {				
					//SET VARS
						var selectedRows = $(this).parents('tr'),
							data = {	ad_id: selectedRows.find(".admin_editAdId").val(),
										priority: 1,
										adName: selectedRows.find(".admin_editAdName").val(),
										adEdit: 1,
										saveEdit: 1
									};
										
						if(selectedRows.hasClass('adminEditAd'))
							data.adUrl = selectedRows.find(".admin_editAdUrl").val();
						else if(selectedRows.hasClass('adminEditGoogleAd')) {
							data.adSlot = selectedRows.find(".admin_editAdSlot").val();
							data.adWidth = selectedRows.find(".admin_editAdWidth").val();
							data.adHeight = selectedRows.find(".admin_editAdHeight").val();
							data.googleAd = 1;
						}

					//SAVE EDIT
					adminAdsAuth.saveEdit(selectedRows, data, 'bottombanner');
				});
				//SAVE NEW
				$("#adlist").find("#newItem .uibtnSave").unbind('click').click(function() {
					//SET VARS
						var selectedRows = $(this).parents('tr'),
							data = {	widget: 'bottombanner',
										priority: 1,
										adName: selectedRows.find("#admin_addName").val(),
										adEdit: 1,
										saveNew: 1
									};

						//normal ad
						if(selectedRows.hasClass('adminAdAdd'))
							data.adUrl = selectedRows.find("#admin_addUrl").val();
						//google ad
						else if(selectedRows.hasClass('adminGoogleAdAdd')) {
							data.adSlot = selectedRows.find("#admin_addSlot").val();
							data.adWidth = selectedRows.find("#admin_addWidth").val();
							data.adHeight = selectedRows.find("#admin_addHeight").val();
							data.googleAd = 1;
						}

					//SAVE NEW
					adminAdsAuth.saveNew(selectedRows, data, 'bottombanner');
				});
				//delete
				$("#adlist").find(".uibtnDelete").unbind('click').click(adminAdsAuth.deleteAd);
				//add new <-> google add new
				$(".uibtnAdd").click(adminAdsAuth.typeSwitch);
			//////////////////////
			//// END EVENT HANDLERS
			//////////////////////
	});
};

if(!window.adminAdsAuth) $.getScript("<?php echo $jquery['auth']['admin']['ads'] ?>", beginAdminAdsBottomBannerUI);
	else beginAdminAdsBottomBannerUI();