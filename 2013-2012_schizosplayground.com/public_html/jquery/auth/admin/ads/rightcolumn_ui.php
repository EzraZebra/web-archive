//////////////////////////////////////////
///// ADMIN ADS AUTHORISATION JQUERY /////
//////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../../../includes/var_dir.php';
?>
	
var beginAdminAdsRightColumnUI = function() {
	$(document).ready(function() {
		//////////////////////
		//// BEGIN EVENT HANDLERS
		//////////////////////
			//SETTINGS
				//ads length
				$("#admin_adsSettings").unbind('submit').submit(function() {
					var forminfo = $(this);
					adminAdsAuth.editSettings(forminfo);
					
					return false;
				});
			//TOOLS
				//SAVE EDIT
				$("#adlist").find("tr:not(#newItem)").find(".uibtnSave").unbind('click').click(function() {				
					//SET VARS
						var selectedRows = $(this).parents('tr'),
							data = {	ad_id: selectedRows.find(".admin_editAdId").val(),
										priority: selectedRows.find(".admin_editPriority").val(),
										adName: selectedRows.find(".admin_editAdName").val(),
										adEdit: 1,
										saveEdit: 1
									};
										
						if(selectedRows.hasClass('adminEditAd'))
							data.adUrl = selectedRows.find(".admin_editAdUrl").val();
						else if(selectedRows.hasClass('adminEditGoogleAd')) {
							data.adSlot = selectedRows.find(".admin_editAdSlot").val();
							data.adWidth = selectedRows.find(".admin_editAdWidth").val();
							data.adHeight = selectedRows.find(".admin_editAdHeight").val();
							data.googleAd = 1;
						}

					//SAVE EDIT
					adminAdsAuth.saveEdit(selectedRows, data, 'rightcolumn');
				});

				//SAVE NEW
				$("#adlist").find("#newItem .uibtnSave").unbind('click').click(function() {
					//SET VARS
						var selectedRows = $(this).parents('tr'),
							data = {	widget: 'rightcolumn',
										priority: selectedRows.find("#admin_addPriority").val(),
										adName: selectedRows.find("#admin_addName").val(),
										adEdit: 1,
										saveNew: 1
									};

						//normal ad
						if(selectedRows.hasClass('adminAdAdd'))
							data.adUrl = selectedRows.find("#admin_addUrl").val();
						//google ad
						else if(selectedRows.hasClass('adminGoogleAdAdd')) {
							data.adSlot = selectedRows.find("#admin_addSlot").val();
							data.adWidth = selectedRows.find("#admin_addWidth").val();
							data.adHeight = selectedRows.find("#admin_addHeight").val();
							data.googleAd = 1;
						}

					//SAVE NEW
					adminAdsAuth.saveNew(selectedRows, data, 'rightcolumn');
				});

				//delete
				$("#adlist").find(".uibtnDelete").unbind('click').click(adminAdsAuth.deleteAd);
				//add new <-> google add new
				$(".admintools").find(".uibtnAdd").click(adminAdsAuth.typeSwitch);
			//////////////////////
			//// END EVENT HANDLERS
			//////////////////////
	});
};

if(!window.adminAdsAuth)$.getScript("<?php echo $jquery['auth']['admin']['ads'] ?>", beginAdminAdsRightColumnUI);
	else beginAdminAdsRightColumnUI();