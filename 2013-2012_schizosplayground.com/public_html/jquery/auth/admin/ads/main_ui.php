//////////////////////////////////////////
///// ADMIN ADS AUTHORISATION JQUERY /////
//////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../../../includes/var_dir.php';
?>
	
var beginAdminAdsUI = function() {
	$(document).ready(function() {
		//////////////////////
		//// BEGIN EVENT HANDLERS
		//////////////////////
			//SETTINGS
				//google pub id
				$("#admin_adsSettings").unbind('submit').submit(function() {
					var forminfo = $(this);
					adminAdsAuth.editSettings(forminfo);
					
					return false;
				});
			//////////////////////
			//// END EVENT HANDLERS
			//////////////////////
	});
};

if(!window.adminAdsAuth) $.getScript("<?php echo $jquery['auth']['admin']['ads'] ?>", beginAdminAdsUI);
	else beginAdminAdsUI();