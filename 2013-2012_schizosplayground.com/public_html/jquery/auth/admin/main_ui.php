////////////////////////////////////////////
///// ADMIN AUTHORISATION JQUERY ///////////
////////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../../includes/var_dir.php';
?>
	
var beginAdminUI = function() {
	$(document).ready(function() {
	////////////////////////////
	//// BEGIN EVENT HANDLERS
	////////////////////////////
		//SETTINGS			
			//SETTINGS VALIDATION
			$("#adminsettings form input").unbind('change').change(function() {
				$("#admin_editSettingsSuccess").fadeOut(adminAuth.fadeSpeed);
				if($(this).attr('id') == 'admin_editGooglePub_id') var field = 'settings_gpub_id';
					else field = 'settings_length';
				if(!auth.checkInfo(field, $(this).val())) $(this).validateInput('error');
					else  $(this).validateInput('correct');
			});

			//VALIDATE INPUT ON ANY CHANGE
			$("#adminsettings form input").unbind('input').bind('input', function(){
				$(this).change();
			});
		//TOOLS
			//add new
			$(".admintools").find(".uibtnAdd:not(#adminNewsAdd):not(#adminUsersAdd)").click(adminAuth.addNew);
			//save selected
			$(".admintools").children(".uibtnSave").unbind('click').click(function() {
				$(this).parents('.adminSetContent').find(".editing .selectItem:checked").siblings(".uibtnSave").click();
			});
			//cancel selected
			$(".admintools").children(".uibtnCancel").unbind('click').click(function() { adminAuth.cancelEdit(true); });
			//edit selected
			$(".admintools").children(".uibtnEdit").unbind('click').click(function() { adminAuth.editInfo(true); });

			//delete selected
			$(".admintools").children(".uibtnDelete").unbind('click').click(function() {
				var selectedRows = $(this).parents('.adminSetContent').find(".selectItem:checked");

				if(selectedRows.length > 0 && confirm("Are you sure you want to delete all selected items?"))
					selectedRows.siblings(".uibtnDelete").addClass('noConfirm').click().removeClass('noConfirm');
				else return false;
			});
			//ban selected
			$(".admin_banIpSelected").unbind('click').click(function() {
				$(this).parents('.adminSetContent').find('.selectItem:checked').siblings('.admin_banIp:not(.uibtnHidden)').click();
			});
			//unban selected
			$(".admin_unbanIpSelected").unbind('click').click(function() {
				$(this).parents('.adminSetContent').find('.selectItem:checked').siblings('.admin_unbanIp:not(.uibtnHidden)').click();
			});
			//edit
			$(".adminlist").find(".admin_inlineEdit").unbind('click').click(adminAuth.editInfo);
			//cancel
			$(".adminlist").find("tr:not(#newItem)").find(".uibtnCancel:not(.checklist)").unbind('click').click(adminAuth.cancelEdit);
			//cancel new
			$(".adminlist").find("#newItem").find(".uibtnCancel:not(.checklist)").unbind('click').click(adminAuth.cancelNew);
			//save on enter
			$("#adminEdit").unbind('submit').submit(function() {
				$(this).find('input[type=text]:focus').parents('tr').find('.uibtnSave:not(.checklist)').click();
				
				return false;
			});
		
		//SELECTION
			//change row colour on check
			$(".adminlist").find(".selectItem").unbind('change').change(function() {
				var row = $(this).parents('tr');
				
				if($(this).is(':checked')) row.addClass('itemSelected');
					else row.removeClass('itemSelected');
			});

			//select all rows
			$(".adminlist").find(".selectall").unbind('change').change(function() {
				var selectedRows = $(this).parents('.adminlist').find("tr:not(#itemPrototype)").find(".selectItem");
				if($(this).is(':checked')) selectedRows.attr('checked', true);
					else selectedRows.attr('checked', false);
					
				selectedRows.change();
			});
			
			//select row on row click
			$(".adminlist").find("tbody tr").unbind('click').click(function() {
				if(!$(this).hasClass('editing')) {
					var checkbox = $(this).find(".selectItem");
					
					if(!checkbox.is(':checked')) checkbox.attr('checked', true);
						else checkbox.attr('checked', false);
						
					checkbox.change();
				}
			});
			
			//prevent checkbox change on tools column
			$(".adminlist").find("td:last-child").click(function(e) {
				e.stopPropagation();
			});
			
			//prevent checkbox change on checklist
			$(".adminlist").find(".checklist").click(function(e) {
				e.stopPropagation();
			});
			
			//prevent checkbox change on links
			$(".adminlist a").click(function(e) {
				e.stopPropagation();
			});
	////////////////////////////
	//// END EVENT HANDLERS
	////////////////////////////
	});
};

if(!window.adminAuth) $.getScript("<?php echo $jquery['auth']['admin']['main'] ?>", beginAdminUI);
	else beginAdminUI();