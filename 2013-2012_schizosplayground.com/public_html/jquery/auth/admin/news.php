////////////////////////////////////////////
///// ADMIN NEWS AUTHORISATION JQUERY ///////////
////////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../../includes/var_dir.php';
?>

////////////////////////////
//// BEGIN ADMIN NEWS CLASS
////////////////////////////
	var adminNewsAuth = new function() {
		//// EDIT SETTINGS
		this.editSettings = function(forminfo) {
			if(forminfo.find("#admin_newsSettingsForm").val() == 1) {
				//SET DATA
				var data =	{	editSettings: 1,
								cset: 'news',
								newspanel_length: forminfo.find('#admin_editNewspanelLength').val()
							};

				adminAuth.editSettings(data, forminfo);
			} else alert('Form error.');
		};

		//// ADD NEW ITEM
		this.viewItem = function(id, title) {			
			var	state = {
					page: 'adminpanel/news',
					id: id,
					title: title,
					ref: 'loc'
				};

			popup.newState(state);
		};
		
		//// ADD NEW ITEM
		this.addNew = function() {
			var	state = {
					page: 'adminpanel/news',
					id: 'new',
					title: '_notitle',
					ref: 'loc'
				};

			popup.newState(state);
		};

		//// EDIT ITEM
		this.editItem = function(id, title) {
			var	state = {
					page: 'adminpanel/news',
					id: id,
					title: title,
					ref: 'loc',
					editItem: true
				};

			popup.newState(state);
		};

		//// DELETE ITEM
		this.deleteItem = function(btn) {
			var selectedRow = btn.parents().eq(1),						
				title = selectedRow.children('.admin_newsTitle').find('a').text();

			//CONFIRM
			if(btn.hasClass('noConfirm') || confirm("Are you sure you want to delete "+title+"?")) {
				//SET DATA
				var id = btn.siblings(".admin_editNewsId").val(),
					data = "delete=1&id="+id;

				//DELETE
				$.ajax({
					type: "POST",
					url: "<?php echo $mod['auth']['itemEdit']; ?>",
					data:  data
				}).always(function(error) {
					//SUCCESS
					if(error == 'success') {
						selectedRow.fadeOut(400, function() {
							if($(this).siblings().length === 1)
								$(this).siblings().fadeIn(400);
							$(this).remove();
						});
					}
					//ERROR
					else alert(error);
				});
			} else return false;
		};
	}
//////////////////////////
//// END ADMIN NEWS CLASS
//////////////////////////