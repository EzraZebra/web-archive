////////////////////////////////////////////
///// ADMIN AUTHORISATION JQUERY ///////////
////////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../../includes/var_dir.php';
?>

////////////////////////////
//// BEGIN ADMIN CLASS
////////////////////////////
	var adminAuth = new function() {
		this.fadeSpeed = 400;

		//// EDIT SETTINGS
		this.editSettings = function(data, forminfo) {
			if(data.editSettings == 1) {
				$.ajax({
					type: "POST",
					url: "<?php echo $mod['adminpanelcsets']['adminEdit']['main']; ?>",
					data:  data
				}).always(function(error) {
					if(error == 'success') forminfo.find('#admin_editSettingsSuccess').fadeIn(adminAuth.fadeSpeed*2);
						else alert(error);
				});
			} else alert('Form error.');
		};

		//// ADD NEW ITEM
		this.addNew = function() {
			var addRow = $(".adminlist #newItem");
			addRow.find(".uibtnSave:not(.checklist)").show(); //save btn
			addRow.find(".uibtnCancel:not(.checklist)").show(); //cancel btn
			addRow.fadeIn(adminAuth.fadeSpeed*2, function() {
				$(this).find("input[type=text]").eq(0).select();
			});
		};

		//// CANCEL NEW ITEM
		this.cancelNew = function() {
			var addRow = $(".adminlist #newItem");

			addRow.fadeOut(adminAuth.fadeSpeed, function() {
				addRow.find('input[type=checkbox]').attr('checked', false);
				addRow.find('input[type=text]').val('');
				addRow.find('select').prop("selectedIndex", 0);
				addRow.find('.checklist').removeClass('uibtnSave').addClass('uibtnDelete');
			});
		};	
		
		//// EDIT INFO
		this.editInfo = function(selected) {
			//EDIT SELECTED
			if(typeof selected !== 'undefined' && selected == true) var selectedRows = $(".adminlist .itemSelected:not(.editing)");
			//EDIT SINGLE
			else selectedRows = $(this).parents().eq(1);

			selectedRows.addClass('editing');
			//HIDE BTNS/LIST INFO
			selectedRows.find(".uibtnEdit").fadeOut(this.fadeSpeed); //edit btn
			selectedRows.find(".uibtnDelete").fadeOut(this.fadeSpeed); //deletebtn
			selectedRows.find(".admin_listInfo").fadeOut(this.fadeSpeed, function() { //list info
				//SHOW BTNS/INPUT
				selectedRows.find(".admin_editInput").fadeIn(this.fadeSpeed, function() {
					selectedRows.find("input[type=text]").eq(0).select();
				});
				//inputs
				selectedRows.find("select").fadeIn(this.fadeSpeed); //dropdown list
				selectedRows.find(".uibtnSave:not(.checklist)").fadeIn(this.fadeSpeed); //save btn
				selectedRows.find(".uibtnCancel:not(.checklist)").fadeIn(this.fadeSpeed); //cancel btn
			});

			//FILE UPLOAD
			if(selectedRows.find(".fileUpload_frame").length > 0) {
				//GET IFRAME DATA
				var iframeObj = selectedRows.find(".fileUpload_frame"),
					iframeContents = iframeObj.contents();
					
				//SHOW EDITING UI
				iframeContents.find('#fileUpload_wrapper').append('<div class="loading"><div>Loading. . .</div></div>');
				iframeContents.find('#fileUploadForm').fadeIn(this.fadeSpeed, function() {
					iframeObj.parent().height(140);
					iframeContents.find('#img_Delete_container').addClass('editing');
					iframeContents.find(".loading").fadeTo(this.fadeSpeed, 0, function() { $(this).remove(); });
				});
			}
		};
		
		//// CANCEL EDIT
		this.cancelEdit = function(selected) {
			//CANCEL SELECTED
			if(typeof selected !== 'undefined' && selected == true) var selectedRows = $(".adminlist .editing.itemSelected");
			//CANCEL SINGLE
			else var selectedRows = $(this).parents('tr');

			selectedRows.removeClass('editing');
			//HIDE BTNS/INPUT
			selectedRows.find(".uibtnSave:not(.checklist)").fadeOut(this.fadeSpeed); //save btn
			selectedRows.find(".uibtnCancel:not(.checklist)").fadeOut(this.fadeSpeed); //cancel btn
			selectedRows.find(".admin_editInput").fadeOut(this.fadeSpeed, function() { //inputs
				$(this).filter('input[type=text]:not(.admin_editAdWidth):not(.admin_editAdHeight)').val($(this).siblings('span').text()).change(); //reset inputs
				$(this).filter('.admin_editAdWidth').val($(this).parents('td').find('.admin_listInfoAdWidth').text()).change();
				$(this).filter('.admin_editAdHeight').val($(this).parents('td').find('.admin_listInfoAdHeight').text()).change();
				//SHOW BTNS/LIST INFO
				selectedRows.find(".admin_listInfo").fadeIn(this.fadeSpeed); //list info
				selectedRows.find(".uibtnEdit").fadeIn(this.fadeSpeed); //edit btn
				selectedRows.find(".uibtnDelete:not(.img_Delete)").fadeIn(this.fadeSpeed); //delete btn
			});
			selectedRows.find("select").fadeOut(this.fadeSpeed, function() { //dropdown lists
				$(this).find("option:contains('"+$(this).siblings('span').text()+"')").attr('selected', 'selected'); //reset lists
			});

			//FILE UPLOAD
			if(selectedRows.find(".fileUpload_frame").length > 0) {
				//GET IFRAME DATA
				var iframeObj = selectedRows.find(".fileUpload_frame"),
					iframeContents = iframeObj.contents();

				//HIDE EDITING UI
				iframeContents.find('#fileUpload_wrapper').append('<div class="loading"><div>Loading. . .</div></div>');
				iframeContents.find('#fileUploadForm').fadeOut(this.fadeSpeed, function() {
					iframeObj.parent().height(90);
					iframeContents.find('#img_Delete_container').removeClass('editing');
					iframeContents.find(".loading").fadeTo(this.fadeSpeed, 0, function() { $(this).remove(); });
				});
			}
		};	
	}
//////////////////////////
//// END ADMIN CLASS
//////////////////////////