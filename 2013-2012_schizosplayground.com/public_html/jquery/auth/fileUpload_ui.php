//////////////////////////////////////////////
//// FILE UPLOAD AUTHORISATION UI JQUERY /////
//////////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../includes/var_dir.php';
?>

$(document).ready(function() {
	//IFRAME HAS LOADED -> BINDS
	$('.fileUpload_frame').load(function() {
		//GET IFRAME CONTENTS
		var iframeContents = $(this).contents();
		
		//HIDE LOADING DIV
		iframeContents.find(".loading").fadeOut(400);
		
		//CHOOSE IMAGE
		iframeContents.find("#img_Choose").unbind('click').click(function() {
			iframeContents.find("#fileUpload_file").click(); //bind to file input
		});
		iframeContents.find("#fileUpload_pathClicker").unbind('click').click(function() {
			iframeContents.find("#fileUpload_file").click(); //bind to file input
		});
		
		//UPDATE IMAGE FILENAME INPUT
		iframeContents.find("#fileUpload_file").unbind('change').change(function() {
			iframeContents.find("#fileUpload_path").val($(this).val());
		});

		//DELETE IMAGE
		iframeContents.find("#img_Delete_container").unbind('click').click(function() {
			if($(this).hasClass('editing') && confirm("Are you sure you want to delete this image?"))
				$(this).siblings('#img_Delete_form').submit();
			else return false;
		});

		//ERROR
		if(iframeContents.find("#error").text()) alert(iframeContents.find("#error").text());
	});
});