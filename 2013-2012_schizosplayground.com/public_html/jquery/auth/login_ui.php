//////////////////////////////////////
///// LOGIN AUTHORISATION JQUERY /////
//////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../includes/var_dir.php';
?>

$(document).ready(function() {	
	//// FOCUS INPUT
	$("#loginpopup #login_user").select();
	
	//// SUBMIT LOGIN FORM
	$("#loginpopup #loginform").submit(function(event) {
		
		event.preventDefault();
		
		var btn = $(this).find('#login_btn');
		btn.attr('disabled', 'disabled'); //disable login button
		
		var	forminfo = { //GET LOGIN INFO
			username: $(this).find('#login_user').val(),
			pwd: $(this).find('#login_pwd').val(),
			remember: $(this).find('#login_remember').is(':checked'),
			form: $(this).find('#login_form').val(),
			module: '#loginpopup'
		};

		auth.logIn(forminfo, function(error) { //LOG IN
			if(error == 'success') popup.closeState(); //close on success
				else if(error == 'clearanceError') popup.triggerState(); //reload if already logged in

			btn.removeAttr('disabled'); //re enable login button	
		});
		
		return false;
	});
});