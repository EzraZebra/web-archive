//////////////////////////////////////
///// MAIN AUTHORISATION JQUERY //////
//////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../includes/var_dir.php';
?>

//////////////////////
//// BEGIN AUTH CLASS
//////////////////////
	var auth = new function() {
		this.fadeSpeed = 200;

		//// RELOAD WIDGETS
			this.reloadWidgets = function(widgets) {
				if(typeof widgets === 'undefined') widgets = $('.widget'); //no widgets given -> all widgets
				
				//FOR EACH WIDGET
				widgets.each(function() {
					var widget = $(this).attr('id'); //current widget;
					
					var wrapper = $(this).parent(), //set data
						container = wrapper.parent(),
						content = "<?php echo $mod['widgets']; ?>"+widget+'/';
					 
					//LOAD CONTENT
					 wrapper.loadContent(container, content);
				});
			};

		//// CHECK FORM INFO
			this.checkInfo = function(field, value) {
				var regex = {	user: /^[0-9a-zA-Z_]{5,}$/,
								pwd: /^[A-Za-z0-9!._-]{5,}$/,
								email: /^[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/,
								code: /^[A-Za-z0-9]{10}$/,
								settings_length: /^[0-9]{1,2}$/,
								settings_gpub_id: /^[0-9]{16}$/
							};
							
				if(typeof regex[field] !== 'undefined' && regex[field].test(value)) return true;
				
				return false;
			};
		
		//// SHOW ERROR MSG
			this.error = function(module, captchaExists) {
				if(captchaExists) captchaAuth.change(); //change captcha if it exists

				$(module).find(".resetPwdMsg").fadeOut(this.fadeSpeed, function() { //show failed message
					$(this).find(".hideLoginFailedMsg").removeClass("hideLoginFailedMsg");
					$(this).fadeIn(auth.fadeSpeed);
				});
				
				$(module).find("#login_pwd").val(''); //reset login form
				$(module).find("#login_user").select();
			};
		
		//// LOAD CAPTCHA
			this.loadCaptchaBefore = function(module) {
				if(module.attr('id') != 'userpanel') {
					module.append('<div class="loading"><div>Loading. . .</div></div>'); //show loading div in container
					
					$.get("<?php echo $mod['auth']['captcha']; ?>", function(data) {
						module.find("#form_btnbar").before(data, function() {
							$(".loading").fadeTo(fadeSpeed*2, 0, function() { $(this).remove(); }); //hide loading div
						});
					});
				}
				
				auth.reloadWidgets($('#userpanel')); //reload userpanel
			};

		//// LOG IN
			this.logIn = function(forminfo, callback) {
				if(typeof forminfo.form !== 'undefined' && forminfo.form == 1) {	//VALID FORM -> CONTINUE
					if(typeof captchaAuth !== 'undefined' && captchaAuth.exists()) var captchaExists = true; //CHECK FOR CAPTCHA
						else var captchaExists = false;					

			
					if(!captchaExists || captchaAuth.check()) { //NO CAPTCHA OR VALID CAPTCHA -> CONTINUE		
						if	(	typeof forminfo.username !== 'undefined' && this.checkInfo('user', forminfo.username) //USERNAME & PWD & MODULE VALID -> CONTINUE
							&&	typeof forminfo.pwd !== 'undefined' && this.checkInfo('pwd', forminfo.pwd)
							&&	typeof forminfo.module !== 'undefined'
							)
						{
							//SET VARS
							var data = { //data
									login: 1,
									login_user: forminfo.username,
									login_pwd: forminfo.pwd
								};
								
							if(typeof forminfo.remember !== 'undefined') data.login_remember = forminfo.remember; //include remember if present
							if(captchaExists) data.captcha_code = captchaAuth.code(); //include captcha if present
							
							//LOG IN
							$.ajax({
								type: "POST",
								url: "<?php echo $mod['auth']['login']; ?>",
								data:  data
							}).always(function(error) {
								// BEGIN ERROR HANDLING
									//SET CAPTCHA MISSING
										if(error == 'captchaError' && !captchaExists) error = 'captchaMissing';
									
									//SUCCESS OR CLEARANCE ERROR
										if(error == 'success' || error == 'clearanceError') {
											if(captchaExists) captchaAuth.disable(); //disable captcha
											auth.reloadWidgets(); //reload widgets
										}
									//BANNED
										else if(error == 'bannedError') alert('That account has been banned.');
									//CAPTCHA ERROR
										else if(error == 'captchaError') captchaAuth.error(); //show captcha error msg
									//OTHER ERROR
										else {
										//CAPTCHA MISSING
											if(error == 'captchaMissing') auth.loadCaptchaBefore($(forminfo.module));
										//ERROR
											auth.error(forminfo.module, captchaExists);
										}
									
									//CALLBACK
									if(typeof callback === "function") callback(error);
								// END ERROR HANDLING
							});
						} else { //FORM INFO INVALID
							if(typeof forminfo.module !== 'undefined') this.error(forminfo.module, captchaExists); //show error
							
							//CALLBACK
							if(typeof callback === "function") callback('formError');
						}
					} else { //INVALID CAPTCHA
						captchaAuth.error(); //show captcha error
						
						//CALLBACK
						if(typeof callback === "function") callback('captchaError');
					}
				} else window.location.pathname = "<?php echo $rootdoc.'login/'; ?>"; //INVALID FORM -> REDIRECT
			};
		
		//// LOG OUT
			this.logOut = function(callback) {
				$.ajax({
					type: "POST",
					url: "<?php echo $mod['auth']['login']; ?>",
					data: "logout=1"
				}).always(function(error) {
					if(error != 'success') alert(error);
					auth.reloadWidgets();
					if(typeof callback === 'function') callback();
				});
			};
	}
//////////////////////
//// END AUTH CLASS
//////////////////////
$(document).ready(function() {
	//// SET INPUT CSS
	jQuery.prototype.validateInput = function(state) {
		this.removeClass('inputcorrectpopup').removeClass('inputerrorpopup'); //remove any state
		this.addClass('input'+state+'popup'); //add state
	}
});