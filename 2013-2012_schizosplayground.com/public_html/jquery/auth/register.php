//////////////////////////////////////////
///// REGISTER AUTHORISATION JQUERY //////
//////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../includes/var_dir.php';
?>

///////////////////////////
//// BEGIN REGISTER CLASS
///////////////////////////
	var registerAuth = new function() {		
		//// VALIDATE USERNAME
		this.validateUser = function(userString) {
			if(typeof userString === 'undefined') { //check for passed var
				var usernameForm = this.formInfo.register_user,
					username = usernameForm.val();
				userString = false;
			}	else {
					var username = userString;
					userString = true;
				}
				
			this.formInfo.register_form.find('.forminfo .usertaken').fadeOut(auth.fadeSpeed); //hide error msg
			if(auth.checkInfo('user', username)) { //username is valid
				if(!userString) {
					$.ajax({
						type: "POST",
						url: "<?php echo $mod['auth']['register']; ?>",
						data: "register_check=1&register_user="+username
					}).always(function(error) {					
						if(error == 'notTaken') usernameForm.validateInput('correct'); //not taken -> correct
							else {
								usernameForm.validateInput('error'); //error
								registerAuth.formInfo.register_form.find('.forminfo .usertaken').fadeIn(auth.fadeSpeed); //taken -> show error msg
							}
					});
				} else return true; //return true if var passed
			} else if(!userString) usernameForm.validateInput('error'); //error
				else return false; //return false if var passed
		};
	
		//// VALIDATE PASSWORD
		this.validatePwd = function(pwdString, pwdcString) {				
			if(typeof pwdString === 'undefined' && typeof pwdcString === 'undefined') { //check for passed vars
				var pwdForm = this.formInfo.register_pwd,
					pwd = pwdForm.val(),
					pwdcForm = this.formInfo.register_pwdc,
					pwdc = pwdcForm.val(),
					pwdStrings = false;
			}	else if(typeof pwdString !== 'undefined' && typeof pwdcString !== 'undefined'){
					var pwd = pwdString,
						pwdc = pwdcString,
						pwdStrings = true;
				} else return false;
				
			if(auth.checkInfo('pwd', pwd)) { //pwd is valid
				if(!pwdStrings) {
					pwdForm.validateInput('correct'); //correct
					if(pwd == pwdc) pwdcForm.validateInput('correct'); //passwords match -> correct
						else if(pwdc != '' || pwdcForm.is(':focus')) pwdcForm.validateInput('error'); //passwords dont match -> error
				} else if(pwd == pwdc) return true; //return true if vars passed
			} else if(!pwdStrings) {
				pwdForm.validateInput('error'); //error
				pwdcForm.validateInput('nostate');
			}
			
			if(pwdStrings) return false; //return false if vars passed
		};

		//// VALIDATE EMAIL
		this.validateEmail = function(emailString, emailcString) {
			if(typeof emailString === 'undefined' && typeof emailcString === 'undefined') { //check for passed vars
				var emailForm = this.formInfo.register_email,
					email = emailForm.val(),
					emailcForm = this.formInfo.register_emailc,
					emailc = emailcForm.val(),
					emailStrings = false;
			} else if(typeof emailString !== 'undefined' && typeof emailcString !== 'undefined'){
				var email = emailString,
					emailc = emailcString,
					emailStrings = true;
			} else return false;
			
			registerAuth.formInfo.register_form.find('.forminfo .emailtaken').fadeOut(auth.fadeSpeed); //hide error msg
			if(auth.checkInfo('email', email)) { //email is valid
				if(!emailStrings) {
					$.ajax({
						type: "POST",
						url: "<?php echo $mod['auth']['register']; ?>",
						data: "register_check=1&register_email="+email
					}).always(function(error) {
						if(error == 'notTaken') { //not taken
							emailForm.validateInput('correct'); //-> correct
							
							if(email.toLowerCase() == emailc.toLowerCase()) emailcForm.validateInput('correct'); //emails match -> correct
								else if(emailc != '' || emailcForm.is(':focus')) emailcForm.validateInput('error'); //dont match -> error
						}	else { //taken
								emailForm.validateInput('error'); //error
								emailcForm.validateInput('nostate');
								registerAuth.formInfo.register_form.find('.forminfo .emailtaken').fadeIn(auth.fadeSpeed, function() { //show error msg
									$(this).stop(); //prevent stacking from emailc
								});
							}
					});
				} else if(email.toLowerCase() == emailc.toLowerCase()) return true; //return true if vars passed
			} else if(!emailStrings) { //error
				emailForm.validateInput('error');
				emailcForm.validateInput('nostate');
			}
			
			if(emailStrings) return false; //return false if vars passed
		};
		
		//// ERROR -> VALIDATE ALL
		this.error = function() {
			this.validateUser();
			this.formInfo.register_user.select();
			this.validatePwd();
			this.validateEmail();
		};
		
		
		//// REGISTER
		this.register = function(forminfo, useCaptcha) {
			this.formInfo.register_btn.attr('disabled', 'disabled'); //disable register button
			
			//CHECK CAPTCHA
			var captchaCheck = false,
				captchaExists = false;
			if(	typeof captchaAuth !== 'undefined' && captchaAuth.exists()) { //CAPTCHA EXISTS
				captchaCheck = true;
				captchaExists = true;
			}	else if(typeof useCaptcha !== 'undefined' && useCaptcha == false) captchaCheck = true; //NO CAPTCHA USED

			if(typeof forminfo.form !== 'undefined' && forminfo.form == 1) { //VALID FORM -> CONTINUE
				if(captchaCheck) {
					registerAuth.formInfo.errorpopup.fadeOut(auth.fadeSpeed).empty(); //hide error msg
					if(!captchaExists || captchaAuth.check()) {
						// INFO PRESENT & VALID -> CONTINUE
						if	(	typeof forminfo.username !== 'undefined' && this.validateUser(forminfo.username)
							&&	typeof forminfo.pwd !== 'undefined'
							&&	typeof forminfo.pwdc !== 'undefined' && this.validatePwd(forminfo.pwd, forminfo.pwdc)
							&&	typeof forminfo.email !== 'undefined'
							&&	typeof forminfo.emailc !== 'undefined' && this.validateEmail(forminfo.email, forminfo.emailc)	)
						{
							//SET DATA
							var data =	{
									register: 1,
									register_user: forminfo.username,
									register_pwd: forminfo.pwd,
									register_pwdc: forminfo.pwdc,
									register_email: forminfo.email,
									register_emailc: forminfo.emailc,
								};
								
							if(captchaExists) data.captcha_code = captchaAuth.code();
							if(typeof forminfo.clearance !== 'undefined' && !isNaN(forminfo.clearance))
								data.register_clearance = forminfo.clearance;
									
							$.ajax({ //REGISTER
								type: "POST",
								url: "<?php echo $mod['auth']['register']; ?>",
								data:  data
							}).always(function(error) {
								// BEGIN ERROR HANDLING									
									//SUCCESS
										if(error == 'success') {
											//CAPTCHA EXISTS -> disable/login
											if(captchaExists) {
												var logininfo = { //set login info
													username: forminfo.username,
													pwd: forminfo.pwd,
													form: forminfo.form,
													module: '#register'
												};

												captchaAuth.disable(); //disable captcha
										
												auth.logIn(logininfo, function(error) { //log in
													popup.newState({ page: 'activation', id: '_noid', ref: 'browse' }); //show activation popup
												});
											}
											//ADMIN PANEL -> UPDATE LIST
											else if(window.adminUsersAuth) {
												//SET DATA
												var listData = {	module: 'users',
																formInfo: registerAuth.formInfo.adminlist,
																data: data.register_user
															};

												main.updateList(listData, 'add', function() {
													//reload UI scripts
													$.getScript("<?php echo $jquery['auth']['admin']['main_ui'] ?>");
													$.getScript("<?php echo $jquery['auth']['admin']['users_ui'] ?>");

													//reset form
													registerAuth.formInfo.register_form.find(".registerinput").val('').validateInput('nostate');
													registerAuth.formInfo.register_form.find("option").removeAttr("selected");
													registerAuth.formInfo.register_user.focus();
													registerAuth.formInfo.register_form.find('.loading').fadeTo(auth.fadeSpeed*2, 0, function() {
														$(this).remove();
													}); //hide loading div
												});
											}
											//NO CAPTCHA ERROR
											else alert("Captcha error.");
										}	
									//CAPTCHA ERROR
										else {
											if(error == 'captchaError') {
												if(captchaExists) captchaAuth.error();	
													else registerAuth.formInfo.errorpopup.append('Captcha error.').fadeIn(auth.fadeSpeed);
											}
										//CLEARANCE ERROR
											else if(error == 'clearanceError') {
												if(captchaExists) popup.triggerState();
													else registerAuth.formInfo.errorpopup.append(error).fadeIn(auth.fadeSpeed);
											}
										//OTHER ERRORS
											else {
												if(captchaExists) captchaAuth.change(); //change captcha
											//INVALID DATA
												if(error == 'formError') registerAuth.error();
											//DUPLICATE EMAIL AND/OR USERNAME
												else if(error.substr(0,9)  == 'duplicate') {
													if(error == 'duplicateError' || error == 'duplicateUser') {
														registerAuth.validateUser();
														registerAuth.formInfo.register_user.select();
													}
													if(error == 'duplicateError' || error == 'duplicateEmail') {
														registerAuth.validateEmail();
														if(error == 'duplicateEmail') registerAuth.formInfo.register_email.select();
													}
												}
											//FAILED TO SEND MAIL
												else if(error == 'mailFailed') {
													registerAuth.formInfo.errorpopup.append('Failed to send e-mail. Please review your registration.<br />').fadeIn(auth.fadeSpeed);
													registerAuth.validateEmail();
													registerAuth.formInfo.register_email.select();
												}
											//UNKNOWN ERROR
												else registerAuth.formInfo.errorpopup.append('An error occurred. Please review your registration.<br />'+error).fadeIn(auth.fadeSpeed);
											}

											registerAuth.formInfo.register_form.find('.loading').fadeTo(auth.fadeSpeed*2, 0, function() {
												$(this).remove();
											}); //hide loading div
										}
								// END ERROR HANDLING
								registerAuth.formInfo.register_btn.removeAttr('disabled'); //re enable register button
							});
						} else { //INFO NOT PRESENT OR VALID -> VALIDATE FORM
							if(captchaExists) captchaAuth.change();
							else	registerAuth.formInfo.register_form.find('.loading').fadeTo(auth.fadeSpeed*2, 0, function() {
										$(this).remove();
									}); //hide loading div
							this.error();
							this.formInfo.register_btn.removeAttr('disabled'); //re enable register button
						} 
					} else { //INVALID CAPTCHA -> ERROR
						if(captchaExists) captchaAuth.error();
						registerAuth.formInfo.errorpopup.append('Captcha error').fadeIn(auth.fadeSpeed);
						this.formInfo.register_btn.removeAttr('disabled'); //re enable register button
					}
				} else popup.triggerState(); //NO CAPTCHA -> RELOAD POPUP
			} else window.location.pathname = "<?php echo $rootdoc.'register/'; ?>"; //INVALID FORM -> REDIRECT
		};		
	}
///////////////////////////
//// END REGISTER CLASS
///////////////////////////