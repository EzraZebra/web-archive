/////////////////////////////////////////////
///// REGISTER AUTHORISATION UI JQUERY //////
/////////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../includes/var_dir.php';
?>

var beginRegisterUI = function() {
	$(document).ready(function() {
		registerAuth.formInfo = {
			register_form: $('#register'),
			register_user: $('#register_user'),
			register_email: $('#register_email'),
			register_emailc: $('#register_emailc'),
			register_pwd: $('#register_pwd'),
			register_pwdc: $('#register_pwdc'),
			register_btn: $('#register #form_btn'),
			errorpopup: $('#errorpopup')
		};

		//// FOCUS INPUT
		$("#register #register_user").focus();
			
		//////////////////////
		//// BEGIN EVENT HANDLERS
		//////////////////////
			//SUBMIT REGISTER FORM
				$("#registerpopup #register").submit(function() {
					var	forminfo = { //get login info
						username: $(this).find('#register_user').val(),
						pwd: $(this).find('#register_pwd').val(),
						pwdc: $(this).find('#register_pwdc').val(),
						email: $(this).find('#register_email').val(),
						emailc: $(this).find('#register_emailc').val(),
						form: $(this).find('#register_form').val(),
					};
					
					registerAuth.register(forminfo); // register

					return false;
				});

			//REGISTER FORM VALIDATION
				registerAuth.formInfo.register_user.change(function() {
					registerAuth.validateUser();
				});
				$('#register_email').change(function() {
					registerAuth.validateEmail();
				});
				$('#register_emailc').change(function() {
					registerAuth.validateEmail();
				});
				$('#register_pwd').change(function() {
					registerAuth.validatePwd();
				});
				$('#register_pwdc').change(function() {
					registerAuth.validatePwd();
				});
				
				$("#register input").bind('input', function(){ //VALIDATE INPUT ON ANY CHANGE
					$(this).change();
				});
		//////////////////////
		//// END EVENT HANDLERS
		//////////////////////
	});
};

if(!window.registerAuth) $.getScript("<?php echo $jquery['auth']['register'] ?>", beginRegisterUI);
	else beginRegisterUI();