///////////////////////////////////////////////////
///// PASSWORD RESET AUTHORISATION UI JQUERY //////
///////////////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../includes/var_dir.php';
?>

var beginResetUI = function() {
	$(document).ready(function() {
		resetAuth.formInfo = {
			verifyreset_btn: $('#verifyreset #form_btn'),
			errorpopup: $('#errorpopup'),
			resetpopup: $('#resetpopup'),
			reset_btn: $('#reset #form_btn')
		};

		//// FOCUS INPUT
		$("#resetpopup #reset_user").select();

		//// SUBMIT RESET FORM FUNCTION
		var submitReset = function() {	
			var	forminfo = { //GET LOGIN INFO
				username: $(this).find('#reset_user').val(),
				code: $(this).find('#reset_code').val(),
				form: $(this).find('#reset_form').val()
			};

			resetAuth.reset(forminfo);

			return false;
		};
		
		//// SUBMIT RESET FORM
		$("#resetpopup #reset").submit(submitReset);

		//// RESET ON LOAD
		if(auth.checkInfo('code', $("#reset_code").val())) { //if code valid
			$("#resetpopup #reset").submit(); //trigger submit event
		}
		
		//// SUBMIT VERIFICATION FORM
		$("#resetpopup #verifyreset").submit(function() {
			var	forminfo = { //GET LOGIN INFO
				username: $(this).find('#reset_user').val(),
				email: $(this).find('#reset_email').val(),
				form: $(this).find('#reset_form').val()
			};

			resetAuth.sendVerification(forminfo);

			return false;
		});
		
		//GO TO LOGIN
		$("[title='Continue to log-in']").click(function() {
			popup.newState({ page: 'login', id: '_noid', ref: 'loc' });
		});

		//CONTINUE TO RESET
		$("#resetCont a").click(function() {
			popup.newState({ page:'reset', id:'_noid', ref: 'browse', user: '_dummy' });
		});

		//BACK TO VERIFICATION
		$("#resetBack a").click(function() {
			popup.newState({ page:'reset', id:'_noid', ref: 'browse' });
		});
	});
};

if(!window.resetAuth) $.getScript("<?php echo $jquery['auth']['reset'] ?>", beginResetUI);
	else beginResetUI();