//////////////////////////////////////
//// CAPTCHA AUTHORISATION JQUERY ////
//////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../includes/var_dir.php';
?>

//////////////////////////
//// BEGIN CAPTCHA CLASS
//////////////////////////
var captchaAuth = new function() {
	//// DISABLE CAPTCHA
	this.disable = function() {
		this.formInfo.code = '';
	};
	
	//// CHECK IF CAPTCHA EXISTS
	this.exists = function() {
		if(this.formInfo.code.length) return true;
		
		return false;
	};
	
	//// GET CAPTCHA CODE
	this.code = function() {
		if(this.exists()) return this.formInfo.code.val();
		
		return 'null';
	};
	
	//// VALIDATE CODE
	this.check = function() {
		var regex = /^[A-Za-z0-9]{6}$/;
		if(regex.test(this.code())) return true;
		
		return false;
	};
	
	//// CHANGE CAPTCHA
	this.change = function () {
		this.formInfo.code.val('').focus(); //reset form
		this.formInfo.captchaimg.attr("src", '<?php echo $img["form"]["captcha"]."?"; ?>'+Math.random()); //change captcha
	};
	
	//// VALIDATE INPUT
	this.validate = function() {
		this.formInfo.error.fadeOut(auth.fadeSpeed); //hide error message
			
		if(this.check()) this.formInfo.code.validateInput('nostate'); //no error
			else this.formInfo.code.validateInput('error'); //error
	};
	
	//// SHOW ERROR
	this.error = function() {
		this.change();
		this.validate();
		this.formInfo.error.fadeIn(auth.fadeSpeed);
	};
}
//////////////////////////
//// END CAPTCHA CLASS
//////////////////////////