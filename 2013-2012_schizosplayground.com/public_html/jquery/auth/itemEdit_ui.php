///////////////////////////////////////////
//// ITEM EDIT AUTHORISATION UI JQUERY ////
///////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../includes/var_dir.php';
?>

$(document).ready(function() {
	//PREPARE EDITOR
		$("#contentedit").markItUp(myBbcodeSettings);
	
	//SET FORM IFNO
		itemEditAuth.formInfo = {
			//edit btn
				editBtn: $("#itemeditspan"),
				deleteBtn: $("#itemdeletespan"),
				newBtn: $("#itemnewspan"),
				cancelBtn: $("#itemcancelspan"),
				saveBtn: $("#itemsavespan"),
			//date info
				itemDate: $("#pagespopup"),
				itemDateEdit: $(".itemDate"),
			//title info
				itemTitle: $("#itemtitlespan"),
				itemTitleEdit: $("#itemtitleinput"),
			//content info
				itemContent: $("#contenttxt"),
				itemContentEdit: $("#contentedit"),
				itemContentEditWrapper: $(".bbcode")
		};

	//EDIT BTNS
		//EDIT
		$("#itemeditspan").click(itemEditAuth.editClicked);
		if(popup.state.editItem == true) $("#itemeditspan").click();
		
		//CANCEL
		$("#itemcancelspan").click(function() {
			if(popup.state.id == 'new') itemEditAuth.cancelClicked(true); //cancel new item
				else itemEditAuth.cancelClicked(); //cancel edit
		});

		//DELETE
		$("#itemdeletespan").click(itemEditAuth.deleteClicked);

		//SAVE
		$("#itemsavespan").click(function() {
			itemEditAuth.saveClicked(popup.state.id == 'new'); //save item
		});

		//NEW
		$("#itemnewspan").click(itemEditAuth.newClicked);

	//SET DATE INPUT
		$(".itemDate").change(function() {
			//SET DATE INFO
				var changed = $(this).attr('id'),
					dayForm = $(this).siblings("#itemDateDay");
					day = dayForm.find("option:selected").val(),
					month = 0, year = 0, check = true;

				//month changed
				if(changed == 'itemDateMonth') {
					month = $(this).find('option:selected').val();
					year = $(this).siblings("#itemDateYear").find("option:selected").val();
				//year changed
				} else if(changed == 'itemDateYear') {
					month = $(this).siblings("#itemDateMonth").find("option:selected").val();
					year = $(this).find('option:selected').val();
				//day changed -> no check
				} else check = false;

			//CHECK
			if(check) dayForm.dateInputCheck(day, month, year);
		});

	//NEW ITEM -> SHOW EDIT UI
		if(popup.state.id == 'new') {
			//hide edit, delete, new
				itemEditAuth.formInfo.editBtn.fadeOut(itemEditAuth.fadeSpeed);
				itemEditAuth.formInfo.deleteBtn.fadeOut(itemEditAuth.fadeSpeed);
				itemEditAuth.formInfo.newBtn.fadeOut(itemEditAuth.fadeSpeed);
			//show cancel, save
				itemEditAuth.formInfo.cancelBtn.fadeIn(itemEditAuth.fadeSpeed);
				itemEditAuth.formInfo.saveBtn.fadeIn(itemEditAuth.fadeSpeed);
			
			//show date input
			itemEditAuth.formInfo.itemDateEdit.fadeIn(itemEditAuth.fadeSpeed);
			
			//show title input
			itemEditAuth.formInfo.itemTitleEdit.fadeIn(itemEditAuth.fadeSpeed);
			
			//show editor
				itemEditAuth.formInfo.itemContentEdit.show(); //editor
				itemEditAuth.formInfo.itemContentEditWrapper.fadeIn(itemEditAuth.fadeSpeed); //Wrapper
				
			itemEditAuth.editing = true;
		}
});