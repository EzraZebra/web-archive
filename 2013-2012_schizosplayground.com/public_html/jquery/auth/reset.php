////////////////////////////////////////////////
///// PASSWORD RESET AUTHORISATION JQUERY //////
////////////////////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../includes/var_dir.php';
?>

////////////////////////////
//// BEGIN RESET AUTH CLASS
////////////////////////////
	var resetAuth = new function() {
		//// SEND VERIFICATION E-MAIL
		this.sendVerification = function(forminfo) {
			this.formInfo.verifyreset_btn.attr('disabled', 'disabled'); //disable reset button

			if(typeof forminfo.form !== 'undefined' && forminfo.form == 1) { //VALID FORM -> CONTINUE
				this.formInfo.errorpopup.fadeOut(auth.fadeSpeed); //hide error msg
				
				//USERNAME & EMAIL VALID -> CONTINUE
				if	(	(typeof forminfo.username !== 'undefined' && auth.checkInfo('user', forminfo.username))
					||	(typeof forminfo.email !== 'undefined' && auth.checkInfo('email', forminfo.email))
					)
				{
					//SET DATA
					var data = { //data
							sendVer: 1,
							reset_user: forminfo.username,
							reset_email: forminfo.email
						};
					
					// SEND VERIFICATION
					$.ajax({
						type: "POST",
						url: "<?php echo $mod['auth']['reset']; ?>",
						data:  data
					}).always(function(error) {
						// SUCCESS -> GO TO RESET
						if(error == 'success') popup.newState({ page: 'reset', id: '_noid', ref: 'browse', user: forminfo.username });
						// ERRORS
							//CLEARANCE ERROR -> RELOAD
							else if(error == 'clearanceError') popup.triggerState();
							//NO MATCH
							else if(error == 'formError' || error == 'noMatchError') var errorMsg = 'No matching account found. Please review your submission.<br />';
							//ALREADY SENT
							else if(error == 'alreadySent')
								var errorMsg = 'A password reset has already been requested for <b>'+forminfo.username+'</b>.<br />You will be able to do so again within 24 hours.';
							//FAILED TO SEND MAIL
							else if(error == 'mailFailed') var errorMsg = 'Failed to send mail. Please try again.<br />';
							//UNKNOWN ERROR
							else var errorMsg = 'An unknown error occurred. Please review your submission.<br />';

							if(typeof errorMsg !== 'undefined') resetAuth.formInfo.errorpopup.html(errorMsg).fadeIn(auth.fadeSpeed); //display error msg
							resetAuth.formInfo.resetpopup.find("#reset_user").select(); //select user input
						
						resetAuth.formInfo.verifyreset_btn.removeAttr('disabled'); //re-enable reset button
					});
				} else { //FORM ERROR
					this.formInfo.errorpopup.html('No matching account found. Please review your submission.<br />').fadeIn(auth.fadeSpeed);
					this.formInfo.resetpopup.find("#reset_user").select();
						
					this.formInfo.verifyreset_btn.removeAttr('disabled'); //re-enable reset button
				}
			} else window.location.pathname = "<?php echo $rootdoc.'reset/'; ?>"; //INVALID FORM -> REDIRECT
		};
		
		//// RESET
		this.reset = function(forminfo) {
			if(typeof forminfo.form !== 'undefined' && forminfo.form == 1) { //VALID FORM -> CONTINUE
				this.formInfo.reset_btn.attr('disabled', 'disabled'); //disable reset button				
				this.formInfo.errorpopup.fadeOut(auth.fadeSpeed); //hide error msg
				
				if(typeof captchaAuth !== 'undefined' && captchaAuth.exists()) var captchaExists = true; //CHECK FOR CAPTCHA
					else var captchaExists = false;					

				if(!captchaExists || captchaAuth.check()) { //NO CAPTCHA OR VALID CAPTCHA -> CONTINUE					
					//USERNAME & EMAIL VALID -> CONTINUE
					if	(	typeof forminfo.username !== 'undefined' && auth.checkInfo('user', forminfo.username)
						&&	typeof forminfo.code !== 'undefined' && auth.checkInfo('code', forminfo.code)
						)
					{
						//SET DATA
							var data = { //data
									reset: 1,
									reset_user: forminfo.username,
									reset_code: forminfo.code
								};
							
							if(captchaExists) data.captcha_code = captchaAuth.code(); //include captcha if present
						
						// RESET
						$.ajax({
							type: "POST",
							url: "<?php echo $mod['auth']['reset']; ?>",
							data:  data
						}).always(function(error) {
							//SET CAPTCHA MISSING
								if(error == 'captchaError' && !captchaExists) error = 'captchaMissing';
							//SUCCESS -> GO TO SUCCESS
								if(error == 'success') popup.newState({ page:'reset', id:'_noid', ref: 'browse', status: '_success' });
							//ERRORS
								//CAPTCHA ERROR
								else if(error == 'captchaError') captchaAuth.error(); //show captcha error msg
								//CAPTCHA MISSING
								else if(error == 'captchaMissing') auth.loadCaptchaBefore($("#reset #form_btnbar"));
								//FAILED TO SEND MAIL
								else if(error == 'mailFailed') var errorMsg = 'Failed to send mail. Please try again.<br />';
								//CLEARANCE ERROR -> RELOAD
								else if(error == 'clearanceError') popup.triggerState();
								//OTHER ERRORS
								else if(error == 'error') {
									var errorMsg = 'Failed to reset. Please review your submission.<br />';
									resetAuth.formInfo.resetpopup.find("#reset_user").select();
								}
								//UNKNOWN ERROR
									else var errorMsg = 'An unknown error occurred. Please review your submission.<br />';

							if(typeof errorMsg !== 'undefined') resetAuth.formInfo.errorpopup.html(errorMsg).fadeIn(auth.fadeSpeed); //display error msg
							resetAuth.formInfo.reset_btn.removeAttr('disabled'); //re-enable reset button
						});
					} else { // FORM ERROR
						this.formInfo.errorpopup.html('Failed to reset. Please review your submission.<br />').fadeIn(auth.fadeSpeed);
						this.formInfo.resetpopup.find("#reset_user").select();

						this.formInfo.reset_btn.removeAttr('disabled'); //re-enable reset button
					}
				} else captchaAuth.error(); //show captcha error
			} else window.location.pathname = "<?php echo $rootdoc.'reset/'; ?>"; //INVALID FORM -> REDIRECT
		};
	}
///////////////////////////
//// END RESET AUTH CLASS
///////////////////////////