//////////////////////////
///// OVERLAY JQUERY /////
//////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../includes/var_dir.php';
?>

$(document).ready(function() {
//// BINDS
	//// HEADER
		// TOGGLE
		$("#toggleBtnPopup").click(function() {
			popup.toggleHeader($(this).parent());
		});

		// BROWSE
		$("#selectpopup").change(function () {
			popup.browse($(this).parent(), $(".itemDate"));
		});

		// CLICK BROWSE BTN
		$(".pagesBtnPopup").click(function() {
			popup.browseBtn($(this).attr('id'), $(this).siblings('#selectpopup'));
		});

	// CLOSE POPUP ON BTN CLICK
	$("#closebtnpopup").click(function() {
		popup.closeState();
	});

	// CLOSE POPUP ON ESCAPE PRESS
	$(document).keyup(function(e) {
	  if (e.keyCode == 27) popup.closeState();
	});

	// LOG OUT BTN CLICKED
	$("[title='Log out']").click(function() {
		auth.logOut(function() {
			popup.triggerState();
		});
	});

	// DIALOG LINK CLICKED (forgot pwd)
	$(".DiagLnk").click(main.launchDiag);

	// IE8 SELECT DROPDOWN -> SHOW ENTIRE TEXT
	if ($.browser.msie && $.browser.version < 9)
		$('#selectpopup')	.bind('focus mouseover', function() { $(this).addClass('expand').removeClass('clicked'); })
							.bind('click', function() { $(this).toggleClass('clicked'); })
							.bind('mouseout', function() { if (!$(this).hasClass('clicked')) { $(this).removeClass('expand'); }})
							.bind('blur', function() { $(this).removeClass('expand clicked'); });
});