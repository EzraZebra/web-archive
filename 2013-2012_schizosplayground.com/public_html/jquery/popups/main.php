//////////////////////////
///// POPUPS JQUERY //////
//////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../includes/var_dir.php';
?>

//////////////////////
//// BEGIN POPUP CLASS
//////////////////////
	var popup = new function() {
		//// SET OBJECT VARS
			this.state = { //preset state
				page: '_nopage',
				id: '_noid',
				ref: 'none',
				srcPage: '_nopage',
				srcId: '_noid',
				srcTitle: '_notitle'
			};
			this.formInfo = {
				overlay: $('#overlay'),
				overlaywrapper: $('#overlaywrapper'),
				overlaybg: $('#overlaybg')
			};
			this.fadeSpeed = 150;

		//// GET STATE FROM URL
			this.getStateFrom = function(url, isHash) {
				var state = ['_nopage', '_noid', '_notitle']; //preset state
				
				if(typeof isHash !== 'undefined' && isHash) { //is hash -> get hash
					url = url.substring(1).split("/");
					if(url[0].charAt(0) != '') { //id present -> set
						state[1] = url[0];
						if(url[1].charAt(0) != '?') state[2] = url[1]; //title present -> set
					}
					return { id: state[1], title: state[2] }; //return hash state
				} else {
					url = url.substring('<?php echo $rootdoc; ?>'.length).split("/"); //split url
					for(var i=0; (i<url.length && url[i] != ''); i++) state[i] = url[i]; //get available info

					return { page: state[0], id: state[1], title: main.urldecodeslashes(state[2]) }; //return state
				}
			};
			
		//// GET STATE FROM DOM ELEMENT
			this.getStateFromElement = function(element, attr) {
				var url = element.attr(attr),
					isHash = false;

				if(url.indexOf("#") != -1) isHash = true;
				
				state = this.getStateFrom(url, isHash);
				if(typeof element.attr("title") !== 'undefined') state.title = element.attr("title");
				
				return state;
			};				
		
		////// BEGIN UI FUNCTIONS //////
			//// UI/LAUNCH POPUP
				this.launch = function(state) {
					if(typeof state === 'undefined') { //no state passed -> get state
						state = this.state;
					}

					// PREPARE VARS				
						if(state.ref == 'browse' && this.formInfo.overlay.is(':hidden')) state.ref = 'loc'; //browse & popup hidden -> loc
						
						var content = '<?php echo $rootdoc; ?>'+state.page+'/'; //set content url
						if(typeof state.status !== 'undefined') content += state.status+'/'; //include status ("_success" in reset)
							else {
								if(typeof state.user !== 'undefined') content += state.user+'/'; //include user
								content += state.id+'/'; //set content url
							}

						if(state.ref == 'browse') { //browse -> load into main
							content += 'browse/';
							var target = $("#mainpopup");
						} else { //else -> load into overlay
							content += 'launch/';
							var target = this.formInfo.overlay;
						}

					// PREPARE POPUP
						this.formInfo.overlay.removeClass();
						//set popup width
						if(state.page == 'login' || state.page == 'reset') this.formInfo.overlay.addClass('login');
							else if(state.page == 'register' || state.page  == 'activation') this.formInfo.overlay.addClass('register');

						//show popup bg
						$("body").css({'overflow': 'hidden'});
						this.formInfo.overlaywrapper.show();
						this.formInfo.overlaybg.fadeTo(this.fadeSpeed, 0.6);

					// LOAD CONTENT
						target.loadContent(this.formInfo.overlay, content);
				};

			//// UI/CLOSE POPUP
				this.close = function() {
					this.formInfo.overlay.fadeOut(this.fadeSpeed*2, function() {
						$(this).empty();
						if(typeof captchaAuth !== 'undefined' && captchaAuth.exists()) captchaAuth.disable();
					});
					this.formInfo.overlaywrapper.fadeOut(this.fadeSpeed*2);
					this.formInfo.overlaybg.fadeOut(this.fadeSpeed*2);
					$("body").css({'overflow': 'auto'});
				};
		
			//// UI/TOGGLE HEADER UP/DOWN
				this.toggleHeader = function(header) {
					var toggleBtn = header.children("#toggleBtnPopup"); //get togglebar
						
					toggleBtn.unbind('click'); //unbind toggle
					
					//PREPARE VARS
						var	title = header.children("#titlepopup"), toggle = title.attr('class'),
							toggleSpeed = 400;
							
						if(toggle == 'hidden') {
							toggle = 'shown';
							var toggleBtns = 'Up',
								toggleTitle = 'Hide header';
						} else {
							toggle = 'hidden';
							var toggleBtns = 'Down',
								toggleTitle = 'Show header';
						}
						
					$.ajax({ //send to session
						type: "POST",
						url: "<?php echo $mod['backend']; ?>",
						data: "valid=1&var=popupheader&val="+toggle
					})
					
					//TOGGLE HEADER
					title.slideToggle(toggleSpeed, function() {
							$(this).removeClass().addClass(toggle);
					});

					//REVERSE ICONS
					toggleBtn.fadeOut(toggleSpeed, function() { //hide icons
						$(this).removeClass().addClass('uibtn'+toggleBtns+'Arrow');
						$(this).attr('title', toggleTitle);
						
						$(this).fadeIn(toggleSpeed); //show icons
						toggleBtn.click(function() { //rebind toggle
							popup.toggleHeader(header);
						});
					});
				};

		////// END UI FUNCTIONS //////

		////// BEGIN STATE FUNCTIONS //////
			//// STATE/TRIGGER THE CURRENT STATE
				this.triggerState = function(state) {
					var history = History.getState().data; //get history state

					// STATE DOESNT MATCH WITH HISTORY -> GET STATE FROM HISTORY
					if	(	(typeof history.page !== 'undefined' && typeof history.id !== 'undefined')
						&&	(this.state.page != history.page || this.state.id != history.id)
						) this.state = history;

					// SET PASSED VARS
					if(typeof state !== 'undefined') {
						if(typeof state.id !== 'undefined') this.state.id = state.id;
						if(typeof state.ref !== 'undefined') this.state.ref = state.ref;
						if(this.state.page == 'reset') {
							if(typeof state.user !== 'undefined') this.state.user = state.user;
							if(typeof state.status !== 'undefined') this.state.status = state.status;
						}
					}
					
					// LAUNCH POPUP
					if	(		this.state.ref != 'none' && this.state.page != '_nopage' 
							&&	(	this.state.id != '_noid'
								||	(typeof state !== 'undefined'
									&& (state.page == 'login' || state.page == 'register' || state.page == 'activation' || state.page == 'reset'))
								)
						) this.launch();
					
					// OR CLOSE POPUP
					else this.close();
				};

			//// STATE/SET NEW STATE
				this.newState = function(state) {
					// PREPARE VARS 
						if(typeof state.page === 'undefined') state.page = this.state.page;
						if(typeof state.id === 'undefined') state.id = this.state.id;
						if(typeof state.title === 'undefined') state.title = this.state.title;
						if(typeof state.ref === 'undefined') state.page = this.state.ref;
						
						var url = '<?php echo $rootdoc; ?>'+state.page+'/',
							pageTitle = "<?php echo $title_info['def_title']; ?>";

						state.srcPage = this.state.srcPage;
						
						if(state.page != "home") pageTitle = state.page.capitalize()+"<?php echo $title_info['insert']; ?>"+pageTitle;		

						//AUTH PAGE -> CHECK SOURCE PAGE
						if(state.page == 'login' || state.page == 'register' || state.page == 'activation' || state.page == 'reset') {
							if(this.state.srcPage == '_nopage') { //no source page set
								//get from page or url
								if(this.state.page == '_nopage') state.srcPage = this.getStateFrom(window.location.pathname).page;
									else state.srcPage = this.state.page;

								//still auth page -> set to home
								if(state.srcPage == 'login' || state.srcPage == 'register' || state.srcPage == 'activation' || state.page == 'reset')
									state.srcPage = 'home';
							}
						}
						//NO AUTH PAGE -> COMPLETE URL AND TITLE
						else if(state.id != '_noid') {
							url += state.id+'/';
							if(typeof state.title !== 'undefined' && state.title != '_notitle') {							
								var titlepage = state.page.capitalize();

								if(titlepage == "Home") var titlepage = "News";
								pageTitle = state.title+"<?php echo $title_info['insert']; ?>"+titlepage+"<?php echo $title_info['def_title_suf']; ?>";
								url += main.urlreplacemodifiers(state.title)+'/';
							}
						}

						// SET STATE
							this.state = state; //popup state

							//history
							if(state.ref == 'ext') { //external -> replace history state and trigger current state
								var history = History.getState().data; //get history state
								
								//state different -> replace
								if	(	typeof history.page === 'undefined' || history.page != this.state.page
									||	typeof history.id === 'undefined' || history.id != this.state.id
									||	typeof history.title === 'undefined' || history.title != this.state.title
									) History.replaceState(this.state, pageTitle, url);
								//state the same -> trigger
								else this.triggerState();
							} else History.pushState(this.state, pageTitle, url); //not external -> new state
				};
			
			//// STATE/CLOSE POPUP STATE
				this.closeState = function() {
					if(typeof itemEditAuth === 'undefined' || !itemEditAuth.editing || confirm("Are you sure you want to discard your changes?")) {
						var page = this.state.srcPage; //get source page

						if(page == '_nopage') page = this.state.page; //no source page -> get page

						if(page == '_nopage') this.close();
							else {
								this.newState({ page: page, id: '_noid', ref: 'none' }); //set new state
								this.state.srcPage = '_nopage'; //delete source page
								this.state.user = '';
								this.state.status = '';
							}
					} else return false;
				};
		////// END STATE FUNCTIONS //////

		////// BEGIN BROWSING FUNCTIONS //////
			//// BROWSING/BROWSE
				this.browse = function(browsingBar, itemDate) {
					//PREPARE VARS
						var selected = browsingBar.find("#selectpopup option:selected"),
							state = this.getStateFromElement(selected, 'value');
						
						state.page = this.state.page;
						state.ref = 'browse';
						
						if($("#notfound").length) $("#notfound").remove();
						var count = browsingBar.find("#selectpopup option").length;

						if(count == 1) browsingBar.find("#selectpopup").attr("disabled", "disabled");
						
					//SET BROWSING BUTTONS
						if(selected.index() == count-1) browsingBar.children("#prevpopup").addClass('disabled');
							else browsingBar.children("#prevpopup").removeClass('disabled');
						if(selected.index() == 0) browsingBar.children("#nextpopup").addClass('disabled');
							else browsingBar.children("#nextpopup").removeClass('disabled');
					
					//SET DATE INFO
						var date = selected.text().substr(0, 10),
							day = date.substr(0, 2),
							month = date.substr(3, 2),
							year = date.substr(6, 4),
							yearIndex = itemDate.filter('#itemDateYear').children('option[value="'+year+'"]').index();

					//SET CORRECT EDIT DATE
						itemDate.filter("#itemDateDay").prop("selectedIndex", day-1);
						itemDate.filter("#itemDateMonth").prop("selectedIndex", month-1);
						itemDate.filter("#itemDateYear").prop("selectedIndex", yearIndex);
						
						itemDate.trigger('change');

					//SET STATE
						popup.newState(state);
				};
			
			//// BROWSING/BROWSE BTN
				this.browseBtn = function(btn, select) {
					//PREPARE VARS
						var pageIncr = 0,
							selectedIndex = select.prop("selectedIndex");
							
						if(btn == "prevpopup" && selectedIndex < select.children('option').size()-1) pageIncr = 1;
							else if(btn == "nextpopup" && selectedIndex != 0) pageIncr = -1;

					//SET SELECTED OPTION AND TRIGGER CHANGE
					if(pageIncr != 0) select.prop("selectedIndex", selectedIndex+pageIncr).trigger('change');
				};
		////// END BROWSING FUNCTIONS //////
	}
//////////////////////
//// END POPUP CLASS
//////////////////////

$(document).ready(function() {
//////////////////////
//// EVENT HANDLERS
//////////////////////
	//// PREVENT UPWARDS PROPAGATION IN THE DOM TREE ON OVERLAY
	popup.formInfo.overlay.click(function(event) { //dont close overlay on overlay click
	   event.stopPropagation();
	});

	//// CLOSE POPUP ON BG CLICK
	popup.formInfo.overlaywrapper.click(function() {
		popup.closeState();
	});
});

(function(window,undefined){
    // Prepare
    var History = window.History; // Note: We are using a capital H instead of a lower h

    // Bind to StateChange Event
    History.Adapter.bind(window,'statechange',function(){ // Note: We are using statechange instead of popstate
        var State = History.getState(); // Note: We are using History.getState() instead of event.state
		
		if(typeof State.data.id === 'undefined') State.data.id = '_noid';
		
		popup.triggerState(State.data);
    });
})(window);