/////////////////////////////
///// USERPANEL JQUERY //////
/////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../includes/var_dir.php';
?>

/////////////////////////////
////// USERPANEL CLASS //////
/////////////////////////////
var userpanel = new function() {
	this.formInfo = $("#userpanel");
	
	//// TOGGLE USERPANEL
	this.toggle = function() {
		var header = $(this);

		header.unbind('click'); //prevent stacking
		
		var btn = header.children("span"),
			main = userpanel.formInfo.children("#userpanelmain"),
			toggle = main.attr('class'), toggleSpeed = 300; //get toggle direction
		
		if(toggle == 'shown') { //userpanel is shown -> hide
			toggle = "hidden";
			var toggleBtn = "Down";
		}	else { //userpanel is hidden -> show
				toggle = "shown";
				var toggleBtn = "Up";
			}
		
		$.ajax({ //send to session
			type: "POST",
			url: "<?php echo $mod['backend']; ?>",
			data: "valid=1&var=userpanel&val="+toggle
		});

		btn.fadeOut(toggleSpeed, function() { //change button
			header.removeClass().addClass('uibtn'+toggleBtn+'ArrowBlue');
			$(this).fadeIn(toggleSpeed);
		});

		main.slideToggle(toggleSpeed*2, function() { //toggle userpanel
			main.removeClass().addClass(toggle);
		
			header.click(userpanel.toggle);
		});
	};
}

//// ADD STYLESHEET
var style = "<?php echo $css['widgets']['userpanel']; ?>";
if (document.createStyleSheet){
	document.createStyleSheet(style);
}	else {
		$("head").append($("<link href='"+style+"' rel='stylesheet' type='text/css' />"));
	}