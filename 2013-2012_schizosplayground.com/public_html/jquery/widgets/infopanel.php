/////////////////////////////
///// INFOPANEL JQUERY //////
/////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../includes/var_dir.php';
?>

/////////////////////////////
////// INFOPANEL CLASS //////
/////////////////////////////
var infopanel = new function() {
	this.formInfo = $("#infopanel");
	
	//// TOGGLE INFOPANEL
	this.toggle = function() {
		var header = $(this);
		header.unbind('click'); //prevent stacking
		
		var btn = header.children("span"),
			main = infopanel.formInfo.find('#infopanelcontent'),
			toggle = main.attr('class'), toggleSpeed = 300; //get toggle direction
		
		if(toggle == 'shown') { //infopanel is shown -> hide
			toggle = "hidden";
			var toggleBtn = "Down";
		}	else { //infopanel is hidden -> show
				toggle = "shown";
				var toggleBtn = "Up";
			}

		btn.fadeOut(toggleSpeed, function() { //change button
			header.removeClass().addClass('uibtn'+toggleBtn+'ArrowBlue');
			$(this).fadeIn(toggleSpeed);
		});

		main.slideToggle(toggleSpeed*2, function() { //toggle infopanel
			main.removeClass().addClass(toggle);
		
			header.click(infopanel.toggle);
		});
	};
}

//// ADD STYLESHEET
var style = "<?php echo $css['widgets']['infopanel']; ?>";
if (document.createStyleSheet){
	document.createStyleSheet(style);
}	else {
		$("head").append($("<link href='"+style+"' rel='stylesheet' type='text/css' />"));
	}