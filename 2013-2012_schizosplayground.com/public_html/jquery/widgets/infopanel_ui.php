////////////////////////////////
///// INFOPANEL UI JQUERY //////
////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../includes/var_dir.php';
?>


$(document).ready(function() {
///////////////////////////
//// BEGIN EVENT HANDLERS
///////////////////////////
	infopanel.formInfo = $("#infopanel");
	
	//// OPEN NEWS POPUP
	$("#infopanel #news a").unbind('click').click(function(event) {
		var state = popup.getStateFromElement($(this), 'href'); //get info
		state.ref = 'loc';
		popup.newState(state); //launch popup
		
		return false;
	});
	
	//TOGGLE USERPANEL
	$("#infopanelTitle").unbind('click').click(infopanel.toggle);

	//NEW
	$("#infopanelAdd").click(function() {
		popup.newState({page: 'home', id: 'new', title: '_notitle', ref: 'loc' });
	});
});