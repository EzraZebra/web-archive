////////////////////////////////
///// USERPANEL UI JQUERY //////
////////////////////////////////
<?php
	header("Content-Type: application/javascript");
	include '../../includes/var_dir.php';
?>


$(document).ready(function() {
///////////////////////////
//// BEGIN EVENT HANDLERS
///////////////////////////
	userpanel.formInfo = $("#userpanel");
	
	//TOGGLE USERPANEL
	$("#userpanelToggleBtn").unbind('click').click(userpanel.toggle);
	
	//SUBMIT USERPANEL LOGIN
	$("#userpanel #userpanellogin").unbind('submit').submit(function() {
		var btn = $(this).find('#login_btn');
		btn.attr('disabled', 'disabled'); //disable login button
		
		var	forminfo = { //get login info
			username: $(this).find('#login_user').val(),
			pwd: $(this).find('#login_pwd').val(),
			remember: $(this).find('#login_remember').is(':checked'),
			form: $(this).find('#login_form').val(),
			module: '#userpanel'
		};
		
		auth.logIn(forminfo, function(error) {
			btn.removeAttr('disabled'); //re enable login button
		}); //log in
		
		return false;
	});
	
	//LOGOUT
	$("#logout").unbind('click').click(auth.logOut);
	
	$(".DiagLnk").unbind('click').click(main.launchDiag);
///////////////////////////
//// END EVENT HANDLERS
///////////////////////////
});