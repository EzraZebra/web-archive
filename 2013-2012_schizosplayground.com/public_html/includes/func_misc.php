<?php
//// MISC FUNCTIONS ////

//// ENCODE ITEM ID ////
function enc_item_id($item_id) {
	return sprintf("%u", crc32($item_id));
}

function urlencodeslashes($url) { //external (href)
	$search = array("/", "&", "#", "%", "\\");
	
	$url = str_replace("?", "", $url);
	return rawurlencode(str_replace($search, "+", $url));
}

function urldecodeslashes($url) { //internal only (post receive)
	return rawurldecode(str_replace("+", "%2F", $url));
}

//// SECURE GET VARIABLES ////
function secure_get($var, $strip) {
	if($strip) $var = strip_tags($var);
	
	return htmlentities($var, ENT_QUOTES, "UTF-8");
}

function secure_sql($var, $is_int) { //secure sql variables
	global $sqldb;
	if($is_int && !is_numeric($var)) return null;
		else return mysqli_real_escape_string($sqldb, $var);
}

//// SALT PASSWORD HASH based on http://phpsec.org/articles/2005/password-hashing.html ////
define('SALT_LENGTH', 10);
function generateHash($plainText, $salt = null, $usesalt = true)
{
	if($usesalt) { //use salt?
		if ($salt === null) $salt = substr(md5(uniqid(mt_rand(), true)), 0, SALT_LENGTH); //generate salt
			else $salt = substr($salt, 0, SALT_LENGTH); //or slice from $salt
	} else $salt = null;
	
    return $salt . hash('sha256', $salt . $plainText); //return salted hash
}

//// GENERATE RANDOM STRING ////
function genRandomString($length) {
    $charset = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $string = '';

    for ($i=0; $i<$length; $i++) {
        $string .= $charset[mt_rand(0, strlen($charset)-1)];
    }

    return $string;
}

//// CHECK IF ARRAY HAS ONLY NUMERICAL VALUES
function is_numeric_array($array) {
    foreach ($array as $key => $value) {
        if (!is_numeric($value)) return false;
    }
    return true;
}

//// ERROR OUTPUT ////
function getErrorMsg($type, $error) {
	include $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/includes/var_dir.php';
	$errorMsg = "								<div class='error'>";
	if(!isset($error)) $error = "error";
	
	if($type == "pagenotfound") $errorMsg .= "		Not a single <b>".$error."</b> was to be seen anywhere near the monkeybars...<br />";
	elseif($type == "itemnotfound") $errorMsg .= "	There was no sign of <b>".$error."</b> on the merry-go-round...<br />";
	elseif($type == "missingdata") $errorMsg .= "	As I realized <b>".$error."</b> of its screws had been maliciously removed, the overhead bars collapsed...<br />";
	else $errorMsg .= "								An <b>".$error."</b> had sabotaged the swingset for no discernible reason...<br />";
		
	$errorMsg .= "									So I decided to take a step <a href='javascript:javascript:History.back()'>back</a>, or look <a href='".$main_dir['home']."'>somewhere else</a>.
												</div>";
	
	return $errorMsg;
}

//// PARSE BBCODE SHORTHAND
function parseBBCodeShort($content, $wrap=45, $length=125) {
	//BBCODE
	$parser = new JBBCode\Parser();
	$parser->loadCodes(true);
	
	$content = $parser->parse($content); //parse bbcode
	$content = $parser->getAsText();

	$content =	nl2br( //convert new lines to breaks
					secure_get( //convert html entities
						wordwrap( //apply word wrap
							substr( //shorten string
								str_replace("\n"," ", $content), //convert newlines to spaces
								0, $length //substr string start, end
							),
							$wrap, "\n", true //wordwrap length, break, cut
						),
						false //secure_get dont strip tags
					)
				)."... ";
				
	return $content;
}
?>