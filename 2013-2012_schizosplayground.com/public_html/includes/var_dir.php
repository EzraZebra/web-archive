<?php
//// DIRECTORY VARIABLES ////

$include_path = $_SERVER["DOCUMENT_ROOT"].'/archive/2013-2012_schizosplayground.com/';
$rootdoc = '/archive/2013-2012_schizosplayground.com/'; //root directory

//// TITLE INFO ////
$title_info['title'] = "Schizo's Playground";
$title_info['slogan'] = "Attempting Art";
$title_info['insert'] = " :: ";
$title_info['def_title'] = $title_info['title'].$title_info['insert'].$title_info['slogan'];
$title_info['def_title_suf'] = $title_info['insert'].$title_info['title'];

//// REGEX ////
$regex['user'] = "/^[0-9a-zA-Z_]{5,}$/";
$regex['pwd'] = "/^[A-Za-z0-9!._-]{5,}$/";
$regex['email'] = "/^[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/";
$regex['code'] = "/^[A-Za-z0-9]{10}$/";

//// MAIN DIRECTORIES ////
$main_dir['home'] = $rootdoc.'home/';
$main_dir['adminpanel'] = $rootdoc.'adminpanel/';
$main_dir['forum'] = $rootdoc.'forum/';
$main_dir['pinboard'] = $rootdoc.'pinboard/';
$main_dir['channels'] = $rootdoc.'channels/';
$main_dir['word'] = $rootdoc.'word/';
$main_dir['image'] = $rootdoc.'image/';
$main_dir['sound'] = $rootdoc.'sound/';
$main_dir['performance'] = $rootdoc.'performance/';
$main_dir['members'] = $rootdoc.'members/';
$main_dir['favicon'] = $rootdoc.'favicon.ico?';

//// MODULES ////
$modules = 'modules/';
$modules_include =$include_path.'modules/';
//main
$mod['home'] = $modules_include.'home.php';
$mod['underconstruction'] = $modules_include.'underconstruction.html';
$mod['image'] = $mod['underconstruction'];
$mod['word'] = $mod['underconstruction'];
$mod['sound'] = $mod['underconstruction'];
$mod['performance'] = $mod['underconstruction'];
$mod['channels'] = $mod['underconstruction'];
$mod['pinboard'] = $mod['underconstruction'];
$mod['leftcolumn']['bg'] = $rootdoc.$modules.'leftcolumn_bg.php'; //bg img generation
$mod['leftcolumn']['load'] = $modules_include.'leftcolumn.php'; //main left column module
$mod['ads'] = $modules_include.'ads.php';
$mod['rightcolumn'] = $modules_include.'rightcolumn.php';
$mod['backend'] = $rootdoc.'mod_backend/';
//popups
$mod['popups']['home'] = $modules_include.'popups/home.php';
//auth
$authfolder = $modules_include.'auth/';
	//admin
	$adminfolder = $authfolder.'admin/';
	$mod['adminpanel'] = $adminfolder.'main.php';
	$mod['adminpanelcsets']['adminEdit']['main'] = $rootdoc.'auth_adminEdit/';
	$mod['adminpanelcsets']['news']['main'] = $adminfolder.'news.php';
	$adminusersfolder = $adminfolder.'users/';
	$mod['adminpanelcsets']['users']['main'] = $adminusersfolder.'main.php';
	$mod['adminpanelcsets']['users']['logins'] = $adminusersfolder.'logins.php';
	$mod['adminpanelcsets']['users']['banlist'] = $adminusersfolder.'banlist.php';
	$mod['adminpanelcsets']['users']['clearance'] = $adminusersfolder.'clearance.php';
	$adminadsfolder = $adminfolder.'ads/';
	$mod['adminpanelcsets']['ads']['main'] = $adminadsfolder.'main.php';
	$mod['adminpanelcsets']['ads']['rightcolumn'] = $adminadsfolder.'rightcolumn.php';
	$mod['adminpanelcsets']['ads']['bottombanner'] = $adminadsfolder.'bottombanner.php';
	$mod['adminpanelcsets']['leftcolumn']['main'] = $adminfolder.'leftcolumn.php';
$mod['auth']['login'] = $rootdoc.'auth_login/';
$mod['auth']['login_ui'] = $authfolder.'login_ui.php';
$mod['auth']['register'] = $rootdoc.'auth_register/';
$mod['auth']['register_ui'] = $authfolder.'register_ui.php';
$mod['auth']['activation'] = $rootdoc.'auth_activation/';
$mod['auth']['activation_ui'] = $authfolder.'activation_ui.php';
$mod['auth']['reset'] = $rootdoc.'auth_reset/';
$mod['auth']['reset_ui'] = $authfolder.'reset_ui.php';
$mod['auth']['captcha'] = $rootdoc.'auth_captcha/';
$mod['auth']['captcha_ui'] = $authfolder.'captcha_ui.php';
$mod['auth']['common_ui'] = $authfolder.'common_ui.php';
$mod['auth']['itemEdit'] = $rootdoc.'auth_itemEdit/';
$mod['auth']['userEdit'] = $rootdoc.'auth_userEdit/';
$mod['auth']['fileUpload'] = $rootdoc.'auth_fileUpload/';
$mod['auth']['fileUpload_ui'] = $rootdoc.$authfolder.'fileUpload_ui.php';
//widgets
$mod['widgets'] = $rootdoc.'widgets/';
$mod['widget']['userpanel'] = $modules_include.'widgets/userpanel.php';
$mod['widget']['userpanel_cont'] = $modules_include.'widgets/userpanel_cont.php';
$mod['widget']['infopanel'] = $modules_include.'widgets/infopanel.php';
$mod['widget']['infopanel_cont'] = $modules_include.'widgets/infopanel_cont.php';

//// INCLUDES ////
$includes = $include_path.'includes/';
$incl['db_connect'] = str_replace('/public_html', '', $_SERVER["DOCUMENT_ROOT"]).'/auth/db_connect';
$incl['func']['misc'] = $includes.'func_misc.php';
$incl['func']['login'] = $includes.'func_login.php';
$incl['func']['fileUpload'] = $includes.'func_fileUpload.php';

//// JQUERY ////
$jqueryfolder = 'jquery/';
$jqueryroot = $rootdoc.$jqueryfolder;
// main jquery
$jquery['main'] = $jqueryroot.'main.php';
$jquery['loadState'] = $jqueryfolder.'loadState.php';
//popups
$jquerypopups = $jqueryroot.'popups/';
$jquery['popups']['main'] = $jquerypopups.'main.php';
$jquery['popups']['main_ui'] = $jquerypopups.'main_ui.php';
$jquery['popups']['home'] = $jquerypopups.'home.php';
// auth
$jqueryauth = $jqueryroot.'auth/';
$jquery['auth']['main'] = $jqueryauth.'main.php';
$jquery['auth']['login_ui'] = $jqueryauth.'login_ui.php';
$jquery['auth']['register'] = $jqueryauth.'register.php';
$jquery['auth']['register_ui'] = $jqueryauth.'register_ui.php';
$jquery['auth']['activation'] = $jqueryauth.'activation.php';
$jquery['auth']['activation_ui'] = $jqueryauth.'activation_ui.php';
$jquery['auth']['reset'] = $jqueryauth.'reset.php';
$jquery['auth']['reset_ui'] = $jqueryauth.'reset_ui.php';
$jquery['auth']['captcha'] = $jqueryauth.'captcha.php';
$jquery['auth']['captcha_ui'] = $jqueryauth.'captcha_ui.php';
$jquery['auth']['itemEdit'] = $jqueryauth.'itemEdit.php';
$jquery['auth']['itemEdit_ui'] = $jqueryauth.'itemEdit_ui.php';
$jquery['auth']['fileUpload_ui'] = $jqueryauth.'fileUpload_ui.php';
	//admin
	$jqueryauthadmin = $jqueryauth.'admin/';
	$jquery['auth']['admin']['main'] = $jqueryauthadmin.'main.php';
	$jquery['auth']['admin']['main_ui'] = $jqueryauthadmin.'main_ui.php';
	$jquery['auth']['admin']['news'] = $jqueryauthadmin.'news.php';
	$jquery['auth']['admin']['news_ui'] = $jqueryauthadmin.'news_ui.php';
	$jqueryauthadminusers = $jqueryauth.'admin/users/';
		$jquery['auth']['admin']['users'] = $jqueryauthadminusers.'main.php';
		$jquery['auth']['admin']['users_ui'] = $jqueryauthadminusers.'main_ui.php';
		$jquery['auth']['admin']['users_edit_ui'] = $jqueryauthadminusers.'users_edit_ui.php';
		$jquery['auth']['admin']['logins'] = $jqueryauthadminusers.'logins.php';
		$jquery['auth']['admin']['logins_ui'] = $jqueryauthadminusers.'logins_ui.php';
		$jquery['auth']['admin']['ipban'] = $jqueryauthadminusers.'ipban.php';
		$jquery['auth']['admin']['banlist_ui'] = $jqueryauthadminusers.'banlist_ui.php';
		$jquery['auth']['admin']['clearance'] = $jqueryauthadminusers.'clearance.php';
		$jquery['auth']['admin']['clearance_ui'] = $jqueryauthadminusers.'clearance_ui.php';
	$jqueryauthadminads = $jqueryauth.'admin/ads/';
	$jquery['auth']['admin']['ads'] = $jqueryauthadminads.'main.php';
	$jquery['auth']['admin']['ads_ui']['main'] = $jqueryauthadminads.'main_ui.php';
	$jquery['auth']['admin']['ads_ui']['rightcolumn'] = $jqueryauthadminads.'rightcolumn_ui.php';
	$jquery['auth']['admin']['ads_ui']['bottombanner'] = $jqueryauthadminads.'bottombanner_ui.php';
//widgets
$jquerywidgets = $jqueryroot.'widgets/';
$jquery['widgets']['userpanel'] = $jquerywidgets.'userpanel.php';
$jquery['widgets']['userpanel_ui'] = $jquerywidgets.'userpanel_ui.php';
$jquery['widgets']['infopanel'] = $jquerywidgets.'infopanel.php';
$jquery['widgets']['infopanel_ui'] = $jquerywidgets.'infopanel_ui.php';

//// PLUGINS ////
//jquery
$jqueryplugins = $rootdoc.'plugins/jquery/';
$plugins['jquery']['main'] = $jqueryplugins.'jquery-1.8.2.min.js';
$plugins['jquery']['history'] = $jqueryplugins.'balupton-history.js/scripts/bundled/html4+html5/jquery.history.js';
$plugins['jquery']['markitup'] = $jqueryplugins.'markitup/';
//php
$phpplugins = 'plugins/php/';
$plugins['php']['jbbcode'] = $include_path.$phpplugins.'jbbcode/Parser.php';
$plugins['php']['securimage'] = $include_path.$phpplugins.'securimage/securimage.php';

//// CSS ////
$cssfolder = $rootdoc.'css/';
$css['main'] = $cssfolder.'opmaak.php';
$css['home'] = $cssfolder.'home.php';
$css['popup'] = $cssfolder.'popup.php';
$css['fileUpload'] = $cssfolder.'fileUpload.php';
$css['adminpanel'] = $cssfolder.'adminpanel.php';
$csswidgets = $cssfolder.'widgets/';
$css['widgets']['userpanel'] = $csswidgets.'userpanel.css';
$css['widgets']['infopanel'] = $csswidgets.'infopanel.css';

//// IMAGES ////
$images = $rootdoc.'images/';
//left
$img['leftdir'] = $images.'left/';
$img['leftdir_php'] = 'images/left/';
//main
$img_main = $images.'main/';
$img['main']['bg'] = $img_main.'bg.png';
$img['main']['header'] = $img_main.'header.png';
$img['main']['forum'] = $img_main.'forum.jpg';
$img['main']['channels'] = $img_main.'channels.jpg';
$img['main']['pinboard'] = $img_main.'pinboard.jpg';
$img['main']['adfolder'] = $images.'reklam/';
//home
$img_home = $images.'home/';
$img['home']['wordb'] = $img_home.'word.jpg';
$img['home']['soundb'] = $img_home.'sound.jpg';
$img['home']['imageb'] = $img_home.'image.jpg';
$img['home']['performanceb'] = $img_home.'performance.jpg';
//ui
$img_ui = $images.'ui/';
$img['ui']['arrowx'] = $img_ui.'arrowx.png';
$img['ui']['arrowy'] = $img_ui.'arrowy.png';
$img['ui']['arrowyblue'] = $img_ui.'arrowyblue.png';
$img['ui']['add'] = $img_ui.'add.png';
$img['ui']['delete'] = $img_ui.'delete.png';
$img['ui']['edit'] = $img_ui.'edit.png';
$img['ui']['save'] = $img_ui.'save.png';
$img['ui']['ban'] = $img_ui.'ban.png';
$img['ui']['unban'] = $img_ui.'unban.png';
$img['ui']['google'] = $img_ui.'google.png';
$img['ui']['googleadd'] = $img_ui.'googleadd.png';
$img['ui']['googleb'] = $img_ui.'googleb.png';
	//form
	$img['form']['enter'] = $img_ui.'enter.png';
	$img['form']['refresh'] = $img_ui.'refresh.png';
	$img['form']['captcha'] = $rootdoc.$phpplugins.'securimage/securimage_show.php';
//popups
$img_popups = $images.'popups/';
$img['popups']['closebtn'] = $img_popups.'closebtn.png';
$img['popups']['bg']['news'] = $img_popups.'news/newsbg.png';
$img['popups']['bg']['main'] = $img_popups.'mainbg.png';
?>