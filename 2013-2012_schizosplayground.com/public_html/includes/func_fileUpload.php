<?php
//// FILEUPLOAD FUNCTIONS

include_once $incl['db_connect'];

//// FILE UPLOAD CLASS ////
class FileUpload
{
	//// CONSTRUCTOR -> SET COMMON VARS
	function __construct() {
		$this->filename = null;
		$this->filetype = null;
		
		$this->imageWhitelist = array(".jpg",".jpeg",".png"); 
	}
	
	//// RESIZE IMAGE -> https://github.com/maxim/smart_resize_image
	private function smart_resize_image(	$file,
											$width              = 0, 
											$height             = 0, 
											$proportional       = false, 
											$output             = 'file', 
											$delete_original    = true, 
											$use_linux_commands = false ) {
      
		if($height <= 0 && $width <= 0) return false;

		# Setting defaults and meta
		$info                         = getimagesize($file);
		$image                        = '';
		$final_width                  = 0;
		$final_height                 = 0;
		list($width_old, $height_old) = $info;

		# Calculating proportionality
		if($proportional) {
			if      ($width  == 0)  $factor = $height/$height_old;
			elseif  ($height == 0)  $factor = $width/$width_old;
			else                    $factor = min( $width / $width_old, $height / $height_old );

			$final_width  = round( $width_old * $factor );
			$final_height = round( $height_old * $factor );
		}
		else {
			$final_width = ( $width <= 0 ) ? $width_old : $width;
			$final_height = ( $height <= 0 ) ? $height_old : $height;
		}

		# Loading image to memory according to type
		switch ( $info[2] ) {
			case IMAGETYPE_GIF:   $image = imagecreatefromgif($file);   break;
			case IMAGETYPE_JPEG:  $image = imagecreatefromjpeg($file);  break;
			case IMAGETYPE_PNG:   $image = imagecreatefrompng($file);   break;
			default: return false;
		}
		
		
		# This is the resizing/resampling/transparency-preserving magic
		$image_resized = imagecreatetruecolor( $final_width, $final_height );
		if( ($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG) ) {
			$transparency = imagecolortransparent($image);

			if($transparency >= 0) {
				$transparent_color  = imagecolorsforindex($image, $trnprt_indx);
				$transparency       = imagecolorallocate($image_resized, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
				imagefill($image_resized, 0, 0, $transparency);
				imagecolortransparent($image_resized, $transparency);
			}
			elseif($info[2] == IMAGETYPE_PNG) {
				imagealphablending($image_resized, false);
				$color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
				imagefill($image_resized, 0, 0, $color);
				imagesavealpha($image_resized, true);
			}
		}
		imagecopyresampled($image_resized, $image, 0, 0, 0, 0, $final_width, $final_height, $width_old, $height_old);
		
		# Taking care of original, if needed
		if($delete_original) {
			if($use_linux_commands) exec('rm '.$file);
			else @unlink($file);
		}

		# Preparing a method of providing result
		switch( strtolower($output) ) {
			case 'browser':
				$mime = image_type_to_mime_type($info[2]);
				header("Content-type: $mime");
				$output = NULL;
				break;
			case 'file':
				$output = $file;
				break;
			case 'return':
				return $image_resized;
				break;
			default:
				break;
		}
		
		# Writing image according to type to the output destination
		switch( $info[2] ) {
			case IMAGETYPE_GIF:   imagegif($image_resized, $output);    break;
			case IMAGETYPE_JPEG:  imagejpeg($image_resized, $output);   break;
			case IMAGETYPE_PNG:   imagepng($image_resized, $output);    break;
			default: return false;
		}

		return true;
	}
	
	//// IMAGE UPLOAD -> http://stackoverflow.com/questions/4166762/php-image-upload-security-check-list
	public function imageUpload($file, $uploaddir, $dimensions) {
		// GET FILE INFO
		$this->filename = strtolower($file['name']);
		$this->filetype = strtolower($file['type']);

		// CHECK EXTENSION
			$file_ext = strrchr($this->filename, '.'); //get the file ext
			if(!(in_array($file_ext, $this->imageWhitelist))) //check if its allowed or not
				// INVALID EXTENSION
				return 'Only jpg, jpeg and png extensions are allowed.';

		// CHECK UPLOAD TYPE
		$pos = strpos($this->filetype,'image');
		// INVALID FILETYPE
		if($pos === false) return 'Specified file is not an image.';

		// CHECK MIME TYPE
		$imageinfo = getimagesize($file['tmp_name']);
		if($imageinfo['mime'] != 'image/jpeg' && $imageinfo['mime'] != 'image/jpg' && $imageinfo['mime'] != 'image/png')
			// INVALID MIME TYPE
			return 'Only jpeg and png images are allowed.';

		// CHECK DOUBLE FILE TYPE (image with comment)
		if(substr_count($this->filetype, '/') > 1)
			// DOUBLE FILE TYPE
			return 'Only jpeg and png images are allowed.';

		// UPLOAD		
			//CREATE DIR
			if(!file_exists($uploaddir)) mkdir( $uploaddir, 0775);
			//SET FILE NAME
			$uploadfile_name = enc_item_id(basename($this->filename)).$file_ext;
			$uploadfile = $uploaddir.$uploadfile_name;
			
			for($i=0; file_exists($uploadfile); $i++) {
				$uploadfile_name = enc_item_id(basename($this->filename+$i)).$file_ext;
				$uploadfile = $uploaddir.$uploadfile_name;
			}

			// UPLOAD FILE
			if(move_uploaded_file($file['tmp_name'], "$uploadfile")) {
				// SUCCESS -> RESIZE
				$this->smart_resize_image($uploadfile, $dimensions['width'], $dimensions['height'], true);
				return array('success', $uploadfile_name);
			}
			// FAILED
			else return 'Failed to upload image.';
	}
}
?>