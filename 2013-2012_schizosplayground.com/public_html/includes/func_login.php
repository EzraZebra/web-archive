<?php
//// LOGIN FUNCTIONS
//// based on http://tinsology.net/2009/06/creating-a-secure-login-system-the-right-way/

session_start();

include_once $incl['db_connect'];

//// USER VALIDATION CLASS ////
class UserValidation
{
	//// CONSTRUCTOR -> SET COMMON VARS
	function __construct() {
		$this->userid = 0;
		$this->username = 'Guest';

		$this->loggedIn = $this->isLoggedIn();
		$this->logActivity();
	}

	//// GET LONG IP
	private function getLongIP() {
		//Test if it is a shared client
		if(!empty($_SERVER['HTTP_CLIENT_IP'])) $ip = $_SERVER['HTTP_CLIENT_IP'];
		//Is it a proxy address
		elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else $ip = $_SERVER['REMOTE_ADDR'];
		
		return ip2long($ip);
	}

	//// LOGIN FAILURE
		// GET FAIL COUNT
		public function getFailed() {
			if(isset($_SESSION['failed'])) return $_SESSION['failed'];
				else return 0;
		}

		// RESET FAIL COUNT
		public function resetFailed() {
			if(isset($_SESSION['failed'])) $_SESSION['failed'] = 0;
		}

		// INCREMENT FAIL COUNT
		public function loginFailed() {
			if(!isset($_SESSION['failed'])) $_SESSION['failed'] = 1;
				else $_SESSION['failed']++;

			return $_SESSION['failed'];
		}
	//// END LOGIN FAILURE

	//// PERMISSIONS
		////SPECIAL PERMISSIONS
			// GET SPECIAL PERM FROM STRING
			public function getSpecialPermName($special_perm) {
				switch($special_perm) {
					//BANNED
					case 'banned': case 'root_banned': return 'banned';
					//INACTIVE
					case 'inactive': case 'root_inactive': return 'inactive';
					//ADMIN
					case 'admin': case 'root_admin': return 'admin';
					//NULL
					default: return null;
				}
			}

			// GET SPECIAL PERMISSION FROM USER
			private function getSpecialPerm($user_id = null) {
				global $sqldb;

				if($user_id == null) $user_id = $this->userid;
					else $user_id = secure_sql($user_id, true);
				$result = mysqli_query($sqldb,
									"	SELECT c.special_perm FROM clearance c, users u
										WHERE	u.user_id = '$user_id' AND
												u.clearance = c.clearance_id")
							or die('Database Error.');

				$row = mysqli_fetch_assoc($result);
				mysqli_free_result($result);

				$special_perm = $row['special_perm'];
				
				
				return $this->getSpecialPermName($row['special_perm']);
			}
			
			// CLEARANCE HAS ROOT SPECIAL PERM
			public function hasRootSpecPerm($clearance_id) {
				global $sqldb;

				$clearance_id = secure_sql($clearance_id, true);

				$result = mysqli_query($sqldb,
									"	SELECT special_perm FROM clearance
										WHERE clearance_id = '$clearance_id'")
							or die('Databse Error.');

				$row = mysqli_fetch_assoc($result);
				$special_perm = $row['special_perm'];

				switch($special_perm) {
					//HAS ROOT SPECIAL PERM
					case 'root_user': case 'root_banned': case 'root_inactive': case 'root_admin':
						return true;
					//HAS NO ROOT SPECIAL PERM
					default: return false;
				}
			}
			
			//GET ROOT CLEARANCE FROM SPECIAL PERMISSION
			public function getRootClearance($special_perm=null) {
				global $sqldb;

				switch($special_perm) {
					case 'banned': case 'inactive': case 'admin':
						$special_perm = 'root_'.$special_perm; break;
					default: $special_perm = 'root_user'; break;
				}

				//GET ROOT CLEARANCE
				$result = mysqli_query($sqldb,
									"	SELECT	clearance_id FROM clearance
										WHERE	special_perm = '$special_perm'")
										or die('Database error.');

				$row = mysqli_fetch_assoc($result);
				mysqli_free_result($result);

				return $row['clearance_id'];
			}
		////END SPECIAL PERMISSIONS

		////CLEARANCE
			// IS IP BANNED
			public function isIPBanned($ip = null) {
				global $sqldb;

				if($ip == null) $ip = $this->getLongIP();
				$result = mysqli_query($sqldb,
									"	SELECT * FROM ipbans
										WHERE	ip <= '$ip' AND
												ip_range >= '$ip'")
							or die('Database Error.');

				if(mysqli_num_rows($result) > 0) return true;
					else return false;			
			}

			// IS BANNED
			public function isBanned($user_id = null) {
				return ($this->isIPBanned() || ($this->loggedIn && $this->getSpecialPerm($user_id) == "banned"));
			}

			// IS ACTIVE
			public function isActive() {
				return ($this->loggedIn && $this->getSpecialPerm() != "inactive");
			}
			
			// IS RESTRICTED (NOT LOGGED IN/BANNED/INACTIVE)
			public function isRestricted() {
				return ($this->isBanned() || !$this->isActive());
			}

			// IS ADMIN
			public function isAdmin() {
				return ($this->loggedIn && $this->getSpecialPerm() == "admin" && !$this->isIPBanned());
			}
		////END CLEARANCE

		// CAN EDIT
		public function canEdit($userid) {
			return ((!$this->isRestricted() && $userid == $this->userid) || $this->isAdmin());
		}

		// CAN POST
		public function canPostNews() {
			global $sqldb;

			//not restricted -> continue
			if(!$this->isRestricted()) {
				$result = mysqli_query($sqldb,
									"	SELECT c.post_news FROM clearance c, users u
										WHERE	u.user_id = '$this->userid' AND
												u.clearance = c.clearance_id")
							or die('Database error.');

				$row = mysqli_fetch_assoc($result);
				mysqli_free_result($result);

				return ($row['post_news'] == 1 || $this->isAdmin());
			}
			
			return false;
		}
	//// END PERMISSIONS

	//// VALIDATION
		// DESTROY SESSION AND COOKIES
		private function destroyValidation() {
			//RESET SESSION
			$_SESSION = array();
			session_regenerate_id(true);
			
			//DELETE COOKIES
			if(isset($_COOKIE['userid'])) setcookie("userid", "", 0, "/");
			if(isset($_COOKIE['sid'])) setcookie("sid", "", 0, "/");
		}
		
		// CHECK FOR IP BAN BEFORE VALIDATION
		public function validateUser($userid, $remember) {
			if(!$this->isIPBanned()) $this->validateUserChecked($userid, $remember);
		}

		// VALIDATE USER
		private function validateUserChecked($userid, $remember) {
			global $sqldb;

			//DESTROY VALIDATION
				$this->destroyValidation();
			//SET SESSION DATA
				$_SESSION['valid'] = 1; 
				$_SESSION['userid'] = $userid;
			//SET VARS
				$sid = session_id();
				$ip = $this->getLongIP();
				$this->resetFailed();
				$time = time();
				$exp = null;

			//GET LOGIN SETTINGS
				$result = mysqli_query($sqldb, "SELECT login_remember, login_time FROM global_settings")
							or die('Database error.');

				$row = mysqli_fetch_assoc($result);
				mysqli_free_result($result);

				$login_remember = $row['login_remember']-1;
				$login_time = $row['login_time'];

			//SET COOKIES
			if($remember) {
				$exp = $time+$login_time*24*60*60;
				setcookie("sid", $sid, $exp, "/");
				setcookie("userid", $userid, $exp, "/");
			}
			
			//DELETE OLDEST SESSION DATA FROM DB
				$result = mysqli_query($sqldb,
									"	SELECT	session_id FROM login
										WHERE	user_id='$userid'
										ORDER BY time ASC")
							or die('Database error.');
							
				$noResults = mysqli_num_rows($result);
				if($noResults > $login_remember) {
					for($i=0; $i<$noResults-$login_remember; $i++) {
						$row = mysqli_fetch_assoc($result);
						
						mysqli_query($sqldb,
									"	DELETE FROM login 
										WHERE 	user_id='$userid' AND
												session_id='".$row['session_id']."'")
							or die('Database error.');
					}
					mysqli_free_result($result);
				}
			
			//INSERT NEW SESSION DATA INTO DB
			$sid = generateHash($sid, null, false); //hash session id			
			mysqli_query($sqldb,
						"	UPDATE	users SET reset=NULL, resetTime=NULL
							WHERE	user_id = '$userid'")
				or die('Delete reset.');
				
			mysqli_query($sqldb,
						"	INSERT INTO login (user_id, session_id, time, ip, expires)
							VALUES ('$userid', '$sid', '$time', '$ip', '$exp')")
				or die('Database error.');
		}

		// LOG OUT
		public function logout() {
			global $sqldb;

			//EXPIRE LOGIN
				//IF SESSION IS VALID
					if(isset($_SESSION['valid']) && $_SESSION['valid'] === 1) $sid = generateHash(session_id(), null, false);
				//ELSE IF COOKIES VALID
					elseif(isset($_COOKIE['sid'])) $sid = generateHash($_COOKIE['sid'], null, false);

				//EXPIRE LOGIN
				mysqli_query($sqldb,
							"	UPDATE login SET expires=0
								WHERE 	user_id='$this->userid' AND
										session_id='$sid'")
					or die('Database error.');
		

			//DESTROY VALIDATION
				$this->destroyValidation();
				$this->deleteActivity();
				$this->userid = 0;
				$this->username = 'Guest';
				session_destroy(); //end session
				
			return 'success';
		}

		// CHECK SESSION, COOKIES FOR LOGIN
		private function isLoggedIn() {
			global $sqldb;

			$session_valid = false;
			//IF SESSION IS VALID
				if(isset($_SESSION['valid']) && $_SESSION['valid'] === 1 && isset($_SESSION['userid'])) {
					//set log in data
					$userid = secure_sql($_SESSION['userid'], true);
					$sid = generateHash(session_id(), null, false);
					$session_valid = true;
				}
			//ELSE IF COOKIES VALID
				elseif(isset($_COOKIE['sid']) && isset($_COOKIE['userid'])) {
					//set login data data
					$userid = secure_sql($_COOKIE['userid'], true);
					$sid = generateHash($_COOKIE['sid'], null, false);
				}
			//ELSE -> NOT LOGGED IN
				else return false;
				
			
			//CHECK LOGIN DATA WITH DB
			$result = mysqli_query($sqldb,
								"	SELECT u.user_name, u.clearance, l.expires FROM users u, login l
									WHERE 	u.user_id = '$userid' AND
											l.user_id = '$userid' AND
											l.session_id = '$sid'")
				or die('Database error.');
			
			if(mysqli_num_rows($result) == 1) { //login data matches
				$row = mysqli_fetch_assoc($result);
				$expires = $row['expires'];
				$username = $row['user_name'];
				mysqli_free_result($result);
				
				//if cookies used -> log in
				if(!$session_valid) {
					if($expires < time()) { //if cookies have expired -> log out (delete cookies)
						$this->logout();
						return false; //NOG LOGGED IN
					} else {
						//EXPIRE LOGIN
						mysqli_query($sqldb,
									"	UPDATE login SET expires=0
										WHERE 	user_id='$userid' AND
												session_id='$sid'")
							or die('Database error.');

						$this->validateUser($userid, true); //log in
					}
				}

				$this->userid = $userid;
				$this->username = $username;
				return true; //LOGGED IN
			}
				
			return false; //NOT LOGGED IN
		}

		// DELETE ACTIVITY
		private function deleteActivity() {
			global $sqldb;

			$result = mysqli_query($sqldb, "SELECT activity_threshold FROM global_settings")
							or die("Database error.");
			$row = mysqli_fetch_assoc($result);
			mysqli_free_result($result);

			$threshold = time()-60*$row['activity_threshold'];
			$ip = $this->getLongIP();			
			
			mysqli_query($sqldb,
						"	DELETE FROM user_activity
							WHERE	(user_id != 0 AND user_id = '$this->userid') OR
									ip = '$ip' OR
									time < '$threshold'")
					or die("Database error.");
		}

		// LOG ACTIVITY
		private function logActivity() {
			global $sqldb;

			$time = time();
			$ip = $this->getLongIP();

			$this->deleteActivity();

			mysqli_query($sqldb,
						"	INSERT INTO user_activity (user_id, time, ip)
							VALUES ('$this->userid', '$time', '$ip')")
				or die("Database error.");
		}
		
		// GET ACTIVITY
		public function getActivity() {
			global $sqldb;

			$result = mysqli_query($sqldb,
								"	SELECT COUNT(*) users FROM user_activity
									WHERE user_id != 0")
							or die("Database error.");
			$row = mysqli_fetch_assoc($result);
			mysqli_free_result($result);

			$users = $row['users'];

			$result = mysqli_query($sqldb,
								"	SELECT COUNT(*) guests FROM user_activity
									WHERE user_id = 0")
							or die("Database error.");
			$row = mysqli_fetch_assoc($result);
			mysqli_free_result($result);

			$guests = $row['guests'];
			
			return array('users' => $users, 'guests' => $guests);
		}
	//// END VALIDATION
}
?>