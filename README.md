### Archive of unfinished/abandoned web projects.

### [ezrazebra.net (2019)](https://gitlab.com/EzraZebra/web-archive/tree/master/2019_ezrazebra.net)
Personal website with CMS.  
[Live demo](https://ezrazebra.net/archive/2019_ezrazebra.net/) ([login](https://ezrazebra.net/archive/2019_ezrazebra.net/admin): `demo1` / `demodemo`)

#### [ezrazebra.net (2018)](https://gitlab.com/EzraZebra/web-archive/tree/master/2018_ezrazebra.net)
Personal website with CMS.  
[Live demo](https://ezrazebra.net/archive/2018_ezrazebra.net/) ([login](https://ezrazebra.net/archive/2018_ezrazebra.net/admin/): `demo1@ezrazebra.net` / `demo1`)

#### [List-Break (2017)](https://gitlab.com/EzraZebra/web-archive/tree/master/2017_list-break)
List organiser.  
[Live demo](https://ezrazebra.net/archive/2017_list-break/list-break.html)

#### [ezra.hertmans.be (2016)](https://gitlab.com/EzraZebra/web-archive/tree/master/2016_ezra.hertmans.be)
Personal website (static).  
[Live demo](https://ezrazebra.net/archive/2016_ezra.hertmans.be/)

#### [dirk.hertmans.be (2016)](https://gitlab.com/EzraZebra/web-archive/tree/master/2016_dirk.hertmans.be-2)
Personal website (static).  
[Live demo](https://ezrazebra.net/archive/2016_dirk.hertmans.be-2/)

#### [Homo Diaspora (2013)](https://gitlab.com/EzraZebra/web-archive/tree/master/2013_homo-diaspora)
Map of human evolution.  
[Live demo](https://ezrazebra.net/archive/2013_homo-diaspora/)

#### [Schizo's Playground (2013)](https://gitlab.com/EzraZebra/web-archive/tree/master/2013-2012_schizosplayground.com)
~~Creative sharing platform~~ with CMS.  
[Live demo](https://ezrazebra.net/archive/2013-2012_schizosplayground.com/) (login: `demo1` / `demo1`)

#### [cineFiles (2010)](https://gitlab.com/EzraZebra/web-archive/tree/master/2010_cineFiles.com)
Simple jQuery shopping cart.  
[Live demo](https://ezrazebra.net/archive/2010_cineFiles.com/)

#### [A2E Online (2007)](https://gitlab.com/EzraZebra/web-archive/tree/master/2007_a2e-online.com)
Product catalogue.  
[Live demo](https://ezrazebra.net/archive/2007_a2e-online.com/)

---

_Disclaimer: this repository contains copies of open-source frameworks, libraries and plugins from different authors which are subject to their own licenses._
